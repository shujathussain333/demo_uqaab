
import sqlalchemy as sa
import pandas as pd
import numpy as np
import unicodedata
from nltk.tokenize import RegexpTokenizer
from environment import config
import re

import time

CONNECTION_STRING = config('SA_CONNECTION_STRING')
DB2_CONNECTION_STRING = CONNECTION_STRING.replace('hblUqaab','hblUqaab2')
class CrimeRemarks:
    def __init__(self, query=None):
        self.query = query
        self.query_data = pd.DataFrame()
        self.return_data = pd.DataFrame()
        self.return_data_2 = pd.DataFrame()
        self.engine = sa.create_engine(DB2_CONNECTION_STRING, connect_args = {'fast_executemany': True})
        self.dic={
        "Homicide":"murder|slay|mutilate|kill|homicid|shoot|death|manslaughter|race|murder|assasin|assassin",
        "SexualOffence":"harassment|lust|porn|sex|rape|sodomy|molest|lewd|prostitution|abduct|intercourse|debauchery|copulation|lascivious|lecherous",
        "JuvenileRelatedCrime":"juvenile|child",
        "Theft":"theft|steal|bandit|dacoit|larceny|burglar|shoplifting|shop lifting|rob",
        "Bribery":"bribe|corrupt",
        "AbuseOfPower":"abuse of power|abuse of authority|abuse of official powers|abuse of office",
        "Violence":"strangl|assault|violen|riot|bomb|racketeering|hooliganism|mob action|explosive|wound",
        "WeaponViolation":"ammuniti|firearm|arson|weapon|armed|gun|knives",
        "Forgery":"forge|false|counterfeit|piracy of product",
        "Fraud":"fraud|abusing trust|cheat|swindl|market manipulation|illegal short selling|market misconduct|scandal|collusive",
        "FinancialCrime":"money laun|monetary|embezzlement|laund|misappropriation|peculation|terror financ|price fix|insider trad|sham contracts|falsified export documentation|launder|transfer funds|money service provider|transfer funds to taliban|money provider|money service provider|accessing security markets|hawalas|informal money remittance services|suspected of providing some funding|suspected of providing funding|launder monetary instruments|extorts money|extortion|extort|collects money from drug traffickers|in exchange for a financial benefit|racket|racketeering|terror funding|funding terrorists|funding alqaeda|funding al qaeda|funding taliban |financing terrorism |financing terrorist|hawalaa|hawala|fraudulently obtaining|misappropriat|spoof|false account|transactions with separatist groups|launer money|transactions with the separatist groups|terrorist financ|defraud|ponzi|freeezing of funds|ponzi|fraudulently solicit|defrauding|finance taliban|business without regulat|launder money|laundering money|fraudulent|ban on investment|funds to taliban|monetary penalty|financial services to taliban",
        "DrugViolation": "trafico ilicito de drogas|contraband drug|contraband drugs|dispensed without a valid prescription|\\bdma\\b|\\bdmt\\b|\\bdom\\b|\\bghb\\b|\\bhcl\\b|\\blsd\\b|\\bmda\\b|\\bmmc\\b|\\bpce\\b|\\bpcp\\b|\\bpma\\b|\\bpot\\b|\\bstp\\b|\\btcp\\b|\\btma\\b|acetaminophen|acetorphine|acetylalphamethylfentanyl|acetyldihydrocodeine|acetylmethadol|acid|aerosol|afeem|alcohol|alfentanil|allobarbital|allylprodine|allylprodune|alphacetylmethadol|alphameprodine|alphamethadol|alphamethylfentanyl|alphamethylthiofentanyl|alphaprodine|alprazolam|amfepramone|amfetamine|aminorex|amobarbital|amphetamine|amphetamines|anabolic|anabolic steroids|anadrol|analogs|angel dust|anileridine|arbital|b utobarbital|barbital|barbiturates|bath salts|benzethidine|benzfetamine|benzodiazepines|benzos|benzphetamine|benzylmorphine|betacetyl methadol|betacetylmethadol|betahydroxy methylfentanyl|betahydroxyfentanyl|betameprodine|betamethadol|betaprodine|bezitramide|bitartrate|blind squid|blotter|blue nitro|brolamfetamine|bromazepam|brotizolam|buprenorphine|butalbital|butobarbital|camazepam|candy|cannabinoids|cannabis|cannabis resin|carboxylic acid|cat valium|cathine|cathinone|central nervous system|ch lordiazepoxide|charas|chlordiazepoxide|clc lrazepam|clo lrazepam|clo razepate|clo tiazepam|clobazam|clonazepam|clonazolam|clonitazene|clorazepate|clotiazepam|cloxazolam|coca|coca leaf|cocaina|cocaine|codeine|codoxime|concentrate of poppy straw|crystal|crystal meth|cyclobarbital|cylert|dancing shoes|date rape|dealing drugs|delorazepam|depressant|depressants|desomorphine|dexamfetamine|dexamphetamine|dextroamphetamine|dextromoramide|dextropropoxyphene|diampromide|dianabol|diazepam|diethylamide|diethylpropion|diethylthiambutene|difenoxin|digydrocodeine|dihydrocodeine|dihydromorphine|dimenoxadol|dimepheptanol|dimethylthiambutene|dioxaphetyl butyrate|diphenoxylate|dipipanone|dlysergic acid diethylamide|dmhp|doet|dope|drogas|dronabinol|drone|drotebanol|drug|drug abuse|drug dealing|drug g|drug trafficking|drugs|durabolin|ecgonine|ecstacy|ecstasy|el trafico de drogas|electric kool aid|ephedrine|estazolam|ethchlorvynol|ethelilorvynol|ethinamate|ethyl loflazepate|ethylmethylthiambutene|ethylmorphine|eticyclidine|etil amfetamine|etil amphetamine|etonitazene|etorphine|etoxeridine|etrotizolam|etryptamine|fencamfamin|fenetylline|fenproporex|fentanyl|fludiazepam|flunitrazepam|flurazepam|furethidine|gaanja|gamma|gammahydroxybutyric|geeb|georgia home boy|glutethimide|goey|halazepam|hallucinogens|haloxazolam|hashish|heroin|horse|hydrocodone|hydromorphinol|hydromorphone|hydroxybutyrate|hydroxypethidine|inhalants|intoxicated|isomethadone|jet k|junk|ketamine|ketazolam|ketobemidone|khold|laughing gas|lefetamine|levamfetamine|levamphetamine|levomethamphetamine|levomethorphan|levomethorphean|levomoramide|levophenacylmorphan|levorphanol|loprazolam|lorazepam|lormetazepam|lucy in the sky with diamonds|lysergic|lysergide|magic mushrooms|malcolm|malcolm x|marijuana|mazindol|mcat|mdma|meazepam mefenorex|mecloqualone|medazepam|mefenorex|meow meow|mephedrone|meprobamate|mescaline|mesocarb|metamfetamine|metamfetamine racemate|metanfetamina|metazocine|methadone|methadone intermediate|methamphetamine|methamphetamine racemate|methamphetamines|methaqualone|methcathinone|metheathinone|methyl aminorex|methyl ephedrone|methyl methcathinone|methylaminorex|methylamphetamine|methyldesorphine|methyldihydromorphine|methylfentanyl|methylphenidate|methylphenobarbital|methylthiofentanyl|methyprylon|metopon|midazolam|mkyopophine|mmda|moramide|moramide intermediate|morpheridine|morphine|morphine methobromide|morphinenoxide|mppp|myrophine|narca|narco|narco trafficking|narcoticos|narcotics|narcotics traffickers|narcotraffickers|nethyl mda|nhydroxy mda|nicocodine|nicodicodine|nicomorphine|nicotine|nimetazepam|nitrazepam|nitrogen morphine|noracymethadol|norcodeine|nordazepam|norlevorphanol|normethadone|normorphine|norpipanone|nzphetamine|opiate|opiods|opioids|opium|oxandrin|oxazepam|oxazolam|oxycodone|oxymorphone|parafluorofentanyl|parahexyl|pcpy|pemoline|pentazocine|pentobarbital|pepap|pethidine|pethidine intermediate a|pethidine intermediate b|pethidine intermediate c|phenadoxone|phenampromide|phenazocine|phencyclidine|phendimetrazine|phenmetrazine|phenobarbital|phenomorphan|phenoperidine|phentermine|phenylpiperidine|pholcodine|piminodine|pinazepam|pipradrol|piptadrol|piritramide|poppy|prazepam|pregabalin|prescription drugs|proheptazine|properidine|propiram|pseudoephedrine|psilocine|psilocybin|psilocybine|psilocyn|psilotsin|purple haze|purple rain|pwid|pyrovalerone|racemethorphan|racemoramide|racemorphan|recemethorphan|ritalin|rocket fuel|rolicyclidine|secbutabarbital|secobarbital|shabu|skag|smack|snow|solvents|special k|spice|stanozol|stardust|steroids|substance abuse|sufentanil|sugar cubes|super acid|t m a a|temazepam|tenamfetamine|tenamphetamine|tenocyclidine|tetrahydrocannabmol|tetrazepam|thebacon|thebaine|thinner|thiofentanyl|tilidine|toot|traficar drogas|tranquillisers|triazolam|trimeperidine|vinylbital|vitamin k|weed|white dust|white horse|white magic|whiz|yellow fever|yellow sunshine|zipeprol|zombie",
        "Kidnapping":"kidnap|ransom|hostag",
        "WomenRelatedCrime":"woman|girl|female|lady|women",
        "Migrant":"migrant",
        "WarCrime":"war|massacre|mass killing|holocaust|mass destruct|halabja|chemical weapon",
        "Terrorism":"terror|most wanted|militant|islamic state|hamas|hammas|taliban|qaida|qaeda|boko haram|hizbullah|hazbullah|shabab|fatemiyoun|fatimiyoun|bin laden",
        "Extortion":"felon|extort|exaction|extorsion",
        "TaxCrime":"tax",
        "Trafficking":"traffick|traffic in|smuggling of human",
        "Genocide":"genocide|extermination",
        "MaliciousWounding":"bodily harm|bodily injur|maim",
        "AggravatedAssault":"aggravated assault",
        "OrganizedCrime":"gang|organization|organized crime|plot|organised crime|criminal group|criminal org|band|haven|hammas|hamas|taliban|qaida|qaeda|boko haram|hizbullah|hazbullah|shabab|fatemiyoun|fatimiyoun|bin laden",
        "CyberCrime": "cyber|hack|phish|virus|worm|identity theft|id theft|cyber bully|cyber stalk|malicious software|denial of service|malware|spam"
        }



    def query_data_frame(self):
        cnxn = self.engine.connect().connection

        try:
            cnxn = self.engine.connect().connection
            cursor = cnxn.cursor()
            DELETE_QUERY = "DELETE FROM tbl_crimeentities"
            cursor.execute(DELETE_QUERY)
            if self.query == None:
                print("reading sql crime entities")
                query = pd.read_sql("""WITH CTE AS (
                        SELECT 
                        ListID,
                        SubListID, 
                        [Version], 
                        Convert(Datetime,LastModified) as LastModified, 
                        ListType,
                        IsEnabled, 
                        RN = ROW_NUMBER() OVER (PARTITION BY ListID ORDER BY Convert(Datetime,LastModified)  DESC)  FROM 
                        tbl_subwatchlist 
                        ) 

                        SELECT 
                        Concat(sw.SubListID,',')

                        FROM CTE  sw 
                        Inner join tbl_watchlist w 
                        ON 
                        sw.ListID = w.ListID 
                        WHERE 
                        RN = 1 
                        and 
                        w.IsEnabled = 1 
                        and 
                        sw.IsEnabled = 1""", self.engine)

            else:
                query = pd.read_sql(self.query, self.engine)

            string = ''
            for i in range (len (query)):
                string = string + query.iloc[i][0]
            string = string.strip(",")
            string = "select * from tbl_scrappedData where sublistId in (" + string + ")"
            self.query_data = pd.read_sql(string, self.engine)
            self.return_data = self.query_data[["EID", "Remarks"]].copy()
            print(len(self.return_data))
            cnxn.commit()
            cursor.close()
            cnxn.close()

        except Exception as e:
            cnxn.close()
            print('Connection to database not established')

    def helper_preprocessing(self, row):
        tokenizer = RegexpTokenizer(r'\w+')
        stopWordList = []
        #for i in range(ord('a'), ord('z') + 1):
        #    stopWordList.append(chr(i))
        if pd.isnull(row):
            return None
        row = unicodedata.normalize('NFKD', row).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        row = re.sub(r"(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+", "", row)
        removed_extra_char = []
        for i in row:
            removed_extra_char.append (i)
        c = unicodedata.normalize('NFKD', "".join(removed_extra_char)).encode('ascii', 'ignore')
        text_1 = c.split()
        text_1 = b" ".join(text_1)
        text_1 = text_1.decode("utf-8")
        str2 = '1234567890-=~@#$%^&*()+[!{”:\’><./”}]'
        str3 = "_,;?"
        for w in text_1:
            if w in str2:
                text_1 = text_1.replace(w, '')
            if w in str3:
                text_1 = text_1.replace(w, ' ')
        wordList = [x.lower().strip() for x in tokenizer.tokenize (text_1)]
        removedList = [x for x in wordList if not x in stopWordList]
        text_1 = ' '.join(removedList)
        return text_1

    def preprocessing(self):
        self.return_data['Processed Remark'] = self.return_data['Remarks'].map(
            lambda com: self.helper_preprocessing (com))
        self.return_data = self.return_data.replace(to_replace='None', value=np.nan).dropna ( )

    def result(self):
        for key, val in self.dic.items():
            try:
                self.return_data[key] = self.return_data["Processed Remark"].str.contains(val,regex=True)
                self.return_data[key] = self.return_data[key].astype('int8')
            except TypeError:
                self.return_data[key] = 0

        false_positive_detector = "non alcoholic|nonalcohol|alcohol beverage control|federal drug control|drug control|drug store|drugstore|drug house|pvt|private|ltd|limited|anti narcotic|antinarcotic|pharma"
        
        false_positive_detector_priority = """Image: http://japan.kantei.go.jp/96_abe/meibo/daijin/__icsFiles/afieldfile/2013/03/04/shinzo_abe.jpg ;a; Date of Birth:September 21, 1954 ; Place of Birth:Tokyo ; Member of the House of Representatives (Elected 7 times) ; Constituency:Yamaguchi 4th district (Shimonoseki and Nagato cities) ; (R):Member of the House of Representatives ; Profile: http://japan.kantei.go.jp/96_abe/meibo/daijin/abe_e.html|16/2014 / Zubair Ali Almani & Ot her s|Crime: 2018/01/08 ; Crime: 
 
 On 08 January 2018 between 08:30 and 13:30 the victim was with her young daughter hitch hiking on Old Plaston Road on their way to Kabokweni Clinic, White river, Mpumalanga. A white Toyota Tazz with unknown registration number with two male occupants offered them a lift. On their way to Kabokweni, the driver suddenly changed direction and drove towards Spioenkop Plantation farm. The victim’s daughter was allegedly threatened with a knife by one male passengers while the driver stopped the vehicle and suddenly produced a fire-arm and threatened the victim. He pushed her to the grass next to the road overpowered her and allegedly raped her. With the help of the victim the police managed to construct two separate identikits of the suspects. ; Crime: 2018/01/08 ; Station: 013-7500888|Action: Application for consent order granted; Penalty agreed upon: 2 yearsstayed suspension, 2 years probation, $1,000 fine. ,Summary: Licensee did not contest the charge of failure to diagnose acondition, failure to use a dental dam, failure to inform a patient of aperforation and inappropriate documentation.|Reason: Abdul Hakim Murad was listed on , Additional Info: Abdul Hakim Murad is a member of Jemaah Islamiyah (QDe.092). He is a licensed commercial pilot who trained in flight schools in the United States. He was arrested in Manila, the Philippines, in January 1995 in a raid that resulted in the seizure of a large quantity of chemicals and other paraphernalia used in the manufacture of explosives. He was turned over to the United States and indicted for conspiring to blow up simultaneously 12 United States commercial airliners while airborne, a project codenamed “Bojinka”. The key planners of the plot were Al-Qaida (QDe.004) operations directors Khalid Shaikh Mohammad and Ramzi Yousef. Khalid Shaikh Mohammad was indicted as a co-conspirator of the “Bojinka” plot in 1996 but avoided standing trial by eluding law authorities. Murad admitted to the authorities of the United States, in step-by-step details, his involvement in preparing for the operation. On 5 September 1996, Abdul Hakim Murad, along with co-defendants Ramzi Yousef and Wail Khan Main Shah, were convicted on all counts related to the “Bojinka” bombing conspiracy.|Reason: Bilal bin Marwan was listed , Additional Info: Bilal bin Marwan has fought in Tajikstan and Chechnya, the Russian Federation, under the command of Omar Ibn al-Khattab, commander of the Islamic International Brigade (IIB) (QDe.099). In 1992, he became a trainer at Khalden camp, Afghanistan. He first met Usama bin Laden (deceased) in the mid-1990s. In 1997, Marwan joined the Taliban and later began work for Al-Qaida (QDe.004), becoming a senior lieutenant for Bin Laden.|"case_detail: 
 
 The ICAC investigated corruption allegations that Steven Prestage, an
 
 information and communication technologies (ICT) contractor working on
 
 an ICT project at the NSW Department of Finance, Services and Innovation
 
 (DFSI), dishonestly and partially exercised his public official
 
 functions in exchange for a financial benefit. 
 
 It was alleged that while working as DFSI’s project manager on the
 
 rollout of a project known as the Clarity Project, Mr Prestage
 
 dishonestly arranged for a company effectively controlled by him to be
 
 paid by DFSI for system development work performed by a number of ICT
 
 contractors. 
 
 In its report on the investigation, made public on 16 January 2019, the
 
 Commission makes findings of serious corrupt conduct against Mr
 
 Prestage. 
 
 The ICAC is of the opinion that the advice of the Director of Public
 
 Prosecutions should be obtained with respect to the prosecution of Mr
 
 Prestage for various offences. The Commission makes 15 corruption
 
 prevention recommendations to DFSI to help improve its corruption
 
 prevention systems and prevent the recurrence of the conduct exposed in
 
 this investigation. , further_detail_links: https://www.icac.nsw.gov.au/ArticleDocuments/831/Investigation-into-the-conduct-of-a-department-of-finance-services-and-innovation-ict-project-manager_operation-yarrow.pdf.aspx,https://www.icac.nsw.gov.au/media-centre/media-releases/2019-media-releases/icac-finds-dfsi-ict-project-manager-corrupt-after-hijacking-business-name-to-obtain-half-million-dollar-benefit,https://www.icac.nsw.gov.au/ArticleDocuments/831/Op%20Yarrow%20fact%20sheet.pdf.aspx"""
      
        false_positive_and = "drug|medical|appliance"

        high_priority = "el trafico de drogas|drug deal|drug lord|drug baron|kingpin|narcotrafficker|trade in drugs|morphine|drug traffick|narcotics traffick|opium|opiate|methy methcathinone|heroin|drunk|drugged|cannabi|cocaine|substance abuse|intoxicated|drug abuse|niswaar|illegal drug|controlled drug|class a drug|illicitly used drug|controlled substances"
        
        for i in self.return_data[["DrugViolation"]].index:
            if self.return_data["DrugViolation"][i] == 1:
                a = self.return_data["Processed Remark"][i]

                a_prime = self.return_data["Remarks"][i]

                b = re.findall(false_positive_detector, a)
                c = re.findall(self.dic["DrugViolation"], a)
                d = re.findall(false_positive_and, a)
                e = re.findall(high_priority, a)
                f = re.findall(false_positive_detector_priority, a_prime)
                if (len(b) > 0 or len(d) == 3) and len(e) == 0:
                    self.return_data["DrugViolation"][i] = 0
                if len(f) > 0:
                    self.return_data["DrugViolation"][i] = 0
                
        self.return_data["Others"] = 0
#        self.return_data.to_excel("result_premature.xlsx")
        #for i in self.return_data.index:
        #    if sum(list(self.return_data.loc[i, :])[4:-1]) == 0:
        #        self.return_data["Others"][i] = 1
        for i in self.return_data.index:
            if self.return_data.loc[i, list(self.dic.keys())].sum() == 0:
                #A = time.time()
                #self.return_data.loc[i]["Others"] = 1
                self.return_data.loc[i, "Others"] = 1
                #print(time.time() - A)
        self.return_data_2 = self.return_data.copy()
        self.return_data_1 = self.return_data[["EID"] + ["Remarks"] + list(self.dic.keys()) + ["Others"]]
        self.return_data = self.return_data[["EID"] + list(self.dic.keys()) + ["Others"]]
        print (self.return_data)
        print (self.return_data_2)
        #self.return_data_1 = self.return_data[["EID"] + ["Remarks"] + list(dic.keys()) + ["Others"]]
        #print(len(self.return_data)); self.return_data_1.to_excel("result_1.xlsx")
        self.return_data.to_sql('tbl_crimeentities',self.engine,if_exists='replace')
        return self.return_data, self.return_data_2

#time_1 = time.time()
#crime = CrimeRemarks()
#crime.query_data_frame()
#crime.preprocessing()
#result = crime.result()
#print(time.time() - time_1)
