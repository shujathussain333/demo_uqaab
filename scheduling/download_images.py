#!/usr/bin/env python3
import asyncio
import logging
from contextlib import closing
import aiohttp
import os
import posixpath
from urllib.parse import urlsplit, unquote
import sqlalchemy as sa
from environment import config

CONNECTION_STRING = config('SA_CONNECTION_STRING')
IMAGE_PATH = config('IMAGE_PATH')
DB_IMAGE_PATH = config('DB_IMAGE_PATH')


class DownloadFiles():
    DB2_CONNECTION_STRING = CONNECTION_STRING.replace('hblUqaab','hblUqaab2')
    print(DB2_CONNECTION_STRING)
    db_engine = sa.create_engine(DB2_CONNECTION_STRING)
    cursor = db_engine.raw_connection().cursor()
    rows = []
    logging = None

    def dowload_data_request(self, logging):
        # print("_________dowload_data_request______________")
        self.cursor.execute(
            "select Distinct EID, Image, Program from tbl_entities LEFT JOIN tbl_subwatchlist ON tbl_entities.SubListID = tbl_subwatchlist.SubListID where Image like '%http%'")
        self.rows = self.cursor.fetchall()
        print (self.rows)
        print("__________________________________")
        self.logging = logging
        self.set_download()

    def url2filename(self, url):
        # print("_________url2filename______________")
        urlpath = urlsplit(url).path
        basename = posixpath.basename(unquote(urlpath))
        if (os.path.basename(basename) != basename or
                unquote(posixpath.basename(urlpath)) != basename):
            raise ValueError  # reject '%2f' or 'dir%5Cbasename.ext' on Windows
        return basename

    @asyncio.coroutine
    def download(self, row, session, semaphore, chunk_size=1 << 15):
        # print("_________download______________")
        with (yield from semaphore):  # limit number of concurrent downloads
            try:
                extra = 'extra'
                filename = self.url2filename(row[1])
                if row[2] is None:
                    print ("if")
                    db_path = "{}{}/{}".format(DB_IMAGE_PATH, extra, filename)
                    folder = '{}{}'.format(IMAGE_PATH, extra)
                    filename = '{}/{}'.format(folder, filename)
                else:
                    print ("else")
                    db_path = "{}{}/{}".format(DB_IMAGE_PATH, row[2].replace(' ', ''), filename)
                    folder = '{}{}'.format(IMAGE_PATH, row[2].replace(' ',''))
                    filename = '{}/{}'.format(folder, filename)
                if not os.path.exists(folder):
                    os.makedirs(folder)
                self.logging.info('downloading %s', filename)
                response = yield from session.get(row[1])
                with closing(response), open(filename, 'wb') as file:
                    while True:  # save file
                        chunk = yield from response.content.read(chunk_size)
                        if not chunk:
                            break
                        file.write(chunk)
                self.logging.info('done downloading %s', filename)
                self.cursor.execute(
                    "update tbl_entities set Image=N'{image}' where EID = {eid}".format(image=db_path, eid=row[0]))
                self.cursor.commit()
                return filename, (response.status, tuple(response.headers.items()))
            except Exception as e:
                print (e)
                self.logging.info('download failed %s', row[1])
                return

    def set_download(self):
        # print("_________set_download______________")
        with closing(asyncio.get_event_loop()) as loop, \
                closing(aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False))) as session:
            semaphore = asyncio.Semaphore(4)
            download_tasks = (self.download(url, session, semaphore) for url in self.rows)
            result = loop.run_until_complete(asyncio.gather(*download_tasks))

