import os
import math
import json
import pandas as pd
import sqlalchemy as sa
from environment import config
from json.decoder import JSONDecodeError

CONNECTION_STRING = config('SA_CONNECTION_STRING')

def clean_and_update():
    QUERY = "Select EID, DOB, OrigName, Gender, Title, Designation, Aliases, Documents, Address from tbl_entities where EID > 1629000 order by EID ASC;"
    engine = sa.create_engine(CONNECTION_STRING)
    connection = engine.connect()
    all_data = connection.execute(QUERY).fetchall()

    for record in all_data:
        print(record)
        columns = []
        values = []
        
        # DOB
        dob_part = clean_dob(record[1])
        if dob_part:
            values.append(dob_part)
            columns.append('DOB=?')

        # Name
        name_part = clean_name(record[2])
        if name_part:
            values.append(name_part)
            columns.append('OrigName=?')
        
        # Gender
        gender_part = clean_gender(record[3])
        if gender_part != 0:
            values.append(gender_part)
            columns.append('Gender=?')

        # Title
        title_part = clean_record(record[4], 'title')
        if title_part:
            values.append(title_part)
            columns.append('Title=?')

        # Designation
        designation_part = clean_record(record[5], 'designation')
        if designation_part:
            values.append(designation_part)
            columns.append('Designation=?')

        # Aliases
        aka_part = clean_aka(record[6])
        if aka_part:
            values.append(aka_part)
            columns.append('Aliases=?')

        # Documents
        docs_part = clean_documents(record[7])
        if docs_part:
            values.append(docs_part)
            columns.append('Documents=?')

        # Address
        address_part = clean_address(record[8])
        if address_part:
            values.append(address_part)
            columns.append('Address=?')

        if len(values) > 0:
            update_query = 'update tbl_entities set {0} where EID=?;'
            values.append(record[0])
            column_names = ', '.join(columns)
            connection.execute(update_query.format(column_names), values)
        

def replace_nan(item):
    try:
        if item == 'nan' or math.isnan(item):
            return None
    except TypeError:
        pass
    return item

def remove_nan(dobs):
    date_list = [{'DOB': replace_nan(item['DOB']), 'POB': replace_nan(item['POB'])} for item in dobs]
    return date_list

def remove_unicode(text):
    try:
        return (text.encode('ascii', 'ignore')).decode('utf-8')
    except AttributeError:
        return text

def jsonify_list(input_list, key):
    _list = None
    if input_list:
        _list = input_list

    json_string = json.dumps({key: _list})
    return json_string
    
def clean_dob(dob_record):
    if dob_record is None or dob_record.strip() == '':
        new_list = None
    else:
        try:
            dob_obj = json.loads(dob_record)
            if isinstance(dob_obj, dict):
                if dob_obj['info'] is None or dob_obj['info'] == []:
                    new_list = None
                else:
                    return None
            else:
                new_list = remove_nan(dob_obj)
        except JSONDecodeError:
            new_list = [{'DOB': dob_record, 'POB': None}]

    return jsonify_list(new_list, 'info')

def clean_name(name_record):
    try:
        name_dict = json.loads(name_record)
        if isinstance(name_dict, dict):
            name = ' '.join([remove_unicode(item['name']) for item in name_dict['info']])
            return name
        elif isinstance(name_dict, str):
            name = remove_unicode(name_dict)
            return name
    except JSONDecodeError:
        unicode_removed_name = remove_unicode(name_record)
        if unicode_removed_name != name_record:
            return unicode_removed_name
        return None

def clean_gender(gender_record):
    if gender_record is None or gender_record == 'Male' or gender_record == 'Female':
        return 0
    elif gender_record.strip() == '':
        return None
    elif gender_record.strip().upper().startswith('F'):
        return 'Female'
    elif gender_record.strip().upper().startswith('M'):
        return 'Male'

def clean_record(record, key):
    if record is None or record.strip() == '':
        new_list = None
    else:
        try:
            _obj = json.loads(record)
            if isinstance(_obj, dict):
                if _obj['info'] is None or _obj['info'] == []:
                    new_list = None
                else:
                    unicode_removed = [{key: remove_unicode(item[key])} for item in _obj['info'] if item[key]]
                    if unicode_removed != _obj['info']:
                        new_list = unicode_removed
                    else:
                        return None
            elif isinstance(_obj, str):
                new_list = [{key: remove_unicode(_obj)}]
        except JSONDecodeError:
            new_list = [{key: remove_unicode(record)}]

    return jsonify_list(new_list, 'info')

def clean_aka(record):
    try:
        return clean_record(record, 'aka')
    except KeyError:
        _obj = json.loads(record)
        new_list = [{'aka': remove_unicode(item['alias_name'])} for item in _obj['info'] if item['alias_name']]
    except AttributeError:
        _obj = json.loads(record)
        new_list = [{'aka': remove_unicode(item['aka']['aka'])} for item in _obj['info'] if item['aka']['aka']]

    return jsonify_list(new_list, 'info')

def clean_documents(record):
    if record is None or record.strip() == '':
        new_list = None
    else:
        _obj = json.loads(record)
        if isinstance(_obj, dict):
            if _obj['document_info'] is None or _obj['document_info'] == []:
                new_list = None
            else:
                try:
                    unicode_removed = [{'type': remove_unicode(item['type']),
                                        'id': remove_unicode(item['id'])} for item in _obj['document_info']]
                except KeyError:
                    unicode_removed = [{'type': remove_unicode(item['type']),
                                        'id': remove_unicode(item['value'])} for item in _obj['document_info']]
                if unicode_removed != _obj['document_info']:
                    new_list = unicode_removed
                else:
                    return None
        elif isinstance(_obj, list):
            if _obj == []:
                new_list = None
            else:
                try:
                    new_list = [{'type': remove_unicode(item['type']),
                                'id': remove_unicode(item['id'])} for item in _obj]
                except KeyError:
                    new_list = [{'type': remove_unicode(item['type']),
                                'id': remove_unicode(item['value'])} for item in _obj]

    return jsonify_list(new_list, 'document_info')

def clean_address(address):
    def clean_country(country):
        if country is None or country == '':
            return None
        if len(country) == 2:
            country_rows = df.loc[df['countryCode'] == country.upper()]
        elif len(country) == 3:
            country_rows = df.loc[df['isoAlpha3'] == country.upper()]
        else:
            country_rows = df.loc[df['countryName'] == country.title()]

        if country_rows.empty:
            return None
        else:
            return country_rows.iloc[0]['countryCode']

    def level_2_cleaning(address_list):
        cleaned_list = []
        for item in address_list:
            new_item = item
            if item['address'] is not None and len(item['address']) == 2:
                country_rows = df.loc[df['countryCode'] == item['address']]
                if not country_rows.empty:
                    country_name = country_rows.iloc[0]['countryName']
                    new_item['address'] = country_name
                    if item['country'] is None:
                        new_item['country'] = country_rows.iloc[0]['countryCode']
            cleaned_list.append(new_item)

        return cleaned_list

    path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'uqaab', 'country.xlsx')
    df = pd.read_excel(path)
        
    if address is None or address.strip() == '':
        new_list = None
    else:
        _obj = json.loads(address)
        if isinstance(_obj, dict):
            if _obj['address info'] is None or _obj['address info'] == []:
                new_list = None
            else:
                country_cleaned = [{'address': replace_nan(remove_unicode(item['address'])),
                                    'city': replace_nan(remove_unicode(item['city'])),
                                    'country': clean_country(replace_nan(remove_unicode(item['country'])))} 
                                    for item in _obj['address info']]
                final_cleaned_list = level_2_cleaning(country_cleaned)
                if final_cleaned_list != _obj['address info']:
                    new_list = final_cleaned_list
                else:
                    return None

    return jsonify_list(new_list, 'address info')

clean_and_update()