# -*- coding: utf-8 -*-
import os
import time
import shutil
import requests
import datetime
import subprocess
import sqlalchemy as sa

from environment import config
from emailer import send_summary_email, send_email
from loggers import create_logging_file

import csv
import pandas as pd
from download_images import DownloadFiles
from crimes_entities_extractor import CrimeRemarks

import shutil
import subprocess
import sys
import requests

IMAGE_PATH = config('IMAGE_PATH')
IMAGE_ZIP_PATH = config('IMAGE_ZIP_PATH') 
IMAGE_BINDING_PATH = config('IMAGE_BINDING_PATH')


CONNECTION_STRING = config('SA_CONNECTION_STRING')
print (CONNECTION_STRING, "in scheduler py")
logging = create_logging_file()


class ScheduleSpiders():

    PROJECT_NAME = 'uqaab'
    PROJECT_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'uqaab')
    SCRAPYD_SERVER_URL = config('SCRAPYD_URL')
    HEALTHCHECKS_SCRAPPINGEND_URL = config('HEALTHCHECKS_SCRAPPINGEND_URL')
    HEALTHCHECKS_SCRAPPINGSTART_URL = config('HEALTHCHECKS_SCRAPPINGSTART_URL')
    HEALTHCHECKS_CHANGEDETECTIONEND_URL = config('HEALTHCHECKS_CHANGEDETECTIONEND_URL')
    filename = None
    log_file_path = None

    def __init__(self):
        self.engine = sa.create_engine(CONNECTION_STRING)
        self.connection = self.engine.connect()
        self.list_ids = []
        self.start_time = datetime.datetime.now()

    def __call__(self):
        self.create_log_file()
        self.delete_previous_eggs()
        self.deploy_project()
        self.notify_healthchecks(self.HEALTHCHECKS_SCRAPPINGSTART_URL)
        self.notify_healthchecks(self.HEALTHCHECKS_CHANGEDETECTIONEND_URL)
        self.clear_table('tbl_entities')
        self.perform_scheduling()
        self.wait_for_scrapyd_jobs_completion()
        self.wait_for_records_insertion_completion()
        self.notify_healthchecks(self.HEALTHCHECKS_SCRAPPINGEND_URL)
        
        self.move_data_to_app_db()
        
        self.close_connection()
        self._send_summary_email()
        send_email("Image downloading has been started" ,self.start_time)
        download_files = DownloadFiles()
        download_files.dowload_data_request(logging)
        send_email("Image downloading has been completed" ,self.start_time)
        self.crime_entities_update()
    
        send_email("Image parsing work has been started", self.start_time)
        subprocess.call("rm /Uqaab/logs/images.zip", shell=True)
        subprocess.call("zip -r /Uqaab/logs/images.zip /Uqaab/images/",shell=True)
        self.callEntityScanAPI()
        send_email("Image parsing work has been ended", self.start_time)
    
    def callEntityScanAPI(self):
        try:
            r=requests.get(IMAGE_BINDING_PATH)
            print(r)
        except Exception as e:
            print("Exception",e)
        
    def _send_summary_email(self):
        try:
            df = pd.read_csv(self.log_file_path)
            df = df[(df['ListHasChanges'] == 1)]
            if not df.empty:
                sublist_ids = df['SubListID'].tolist()
                send_summary_email(sublist_ids, self.start_time)
        except Exception as e:
            print (e)


    def delete_previous_eggs(self):
        logging.info("deleting previous eggs")
        eggs_folder = os.path.join(self.PROJECT_DIR, 'eggs')
        shutil.rmtree(eggs_folder, ignore_errors=True)

    def deploy_project(self):
        logging.info("deploying scrapyd project")
        subprocess.Popen('scrapyd-deploy', cwd=self.PROJECT_DIR)
        time.sleep(30)

    def clear_table(self, table_name):
        logging.info("deleting from the tables")
        DELETE_QUERY = "DELETE FROM {0}".format(table_name)
        self.connection.execute(DELETE_QUERY)

    def perform_scheduling(self):
        QUERY = "SELECT FileName, ListID FROM tbl_watchlist WHERE IsEnabled = ?;"
        enabled_spiders = self.connection.execute(QUERY, (1,)).fetchall()

        logging.info("setting up scrapyd, sending request for scheduling scrapper")
        for spider in enabled_spiders:
            subprocess.Popen(['scrapyd-client', 'schedule', '-p', self.PROJECT_NAME, spider[0], '--arg',
                              'list_id={0}'.format(spider[1])], cwd=self.PROJECT_DIR)
            self.list_ids.append(spider[1])
            time.sleep(2)

    def close_connection(self):
        self.connection.close()
        self.engine.dispose()

    def wait_for_scrapyd_jobs_completion(self):
        jobs_url = '{0}listjobs.json?project={1}'.format(
            self.SCRAPYD_SERVER_URL,
            self.PROJECT_NAME)
        while True:
            req = requests.get(jobs_url)
            response = req.json()
            logging.info("running " + str(response["running"][::-1]))
            logging.info("finished " + str(response["finished"][::-1][:4]))
            if len(response['pending']) == 0 and len(response['running']) == 0:
                return
            time.sleep(5)

    def wait_for_records_insertion_completion(self):
        COUNT_QUERY = "SELECT COUNT(*) from tbl_entities"
        old_count = 0
        got_same_count = 0
        while got_same_count < 5:
            new_count = self.connection.execute(COUNT_QUERY).fetchone()[0]
            if new_count == old_count:
                got_same_count += 1
            old_count = new_count
            time.sleep(5)

    def populate_name_and_initials(self):
        logging.info("populating name and initials")
        cursor = self.engine.raw_connection().cursor()
        cursor.execute("exec cleanNameInsertInitials")
        cursor.commit()

    def notify_healthchecks(self, health_check_url):
        for i in range(10):
            try:
                req = requests.get(health_check_url)
                if req.status_code == 200:
                    logging.info("notifying health check success " + str(health_check_url))
                    return
                time.sleep(5)
            except:
                pass

    def move_data_to_app_db(self):
        start_time_str = str(self.start_time)
        shortened_date_time = start_time_str[0:start_time_str.index('.')]
        db_engine = sa.create_engine(CONNECTION_STRING)
        cursor = db_engine.raw_connection().cursor()
        logging.info("Moving data to application DB")
        print("exec TransportDataToAppDB '{}'".format(shortened_date_time))
        cursor.execute("exec TransportDataToAppDB '{}'".format(shortened_date_time))
        logging.info("Successfully moved data to application DB")
        cursor.commit()
        db_engine.dispose()

    def create_log_file(self):
        self.filename = 'loglist_{}.csv'.format(str(datetime.datetime.now().date()).replace(' ', '_'))
        self.log_file_path = os.path.join(config('PROJECT_PATH'), 'timelogs', self.filename)
        fields = ['ListID', 'Program', 'SubListID', 'StartedOn', 'EndedOn', 'RunningTime', 'TotalEntities',
                  'CDStartTime', 'CDEndTime', 'CDRunTime', 'ListHasChanges']

        if not os.path.exists(self.log_file_path):
            logging.info("creating logging file")
            with open(self.log_file_path, 'wt', newline='') as file:
                writer = csv.writer(file, delimiter=',')
                writer.writerow(i for i in fields)

    def upload_file(self):
        try:
            log_file_path = os.path.join(config('PROJECT_PATH'), 'timelogs', self.filename)
            p = subprocess.Popen('gdrive upload {}'.format(log_file_path), shell=True, stdout=subprocess.PIPE)
            for line in p.stdout:
                result = str(line)
            p.wait()
        except:
            logging.info("upload file to google drive failed")

    def crime_entities_update(self):
        try:
            send_email("Crime entites has been started" ,self.start_time)
            crime = CrimeRemarks()
            crime.query_data_frame()
            crime.preprocessing()
            #crime.result(crime.dic)
            crime.result()
            send_email("Crime entites has been completed" ,self.start_time)
        except Exception as e:
            send_email("Crime entities failed" ,self.start_time)
            print(e)
            logging.info("upload file to google drive failed")

scheduler = ScheduleSpiders()
scheduler()
