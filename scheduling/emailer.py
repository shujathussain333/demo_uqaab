import io
import smtplib
import datetime
import traceback
import sqlalchemy as sa
from email.mime.text import MIMEText

from environment import config

CONNECTION_STRING = config('SA_CONNECTION_STRING')

def get_list_of_changes(sublist_ids):
    CHANGES_QUERY = "SELECT SubListID, SUM(CASE WHEN ChangeType = 'addition' THEN 1 ELSE 0 END) AS additions, SUM(CASE WHEN ChangeType = 'removal' THEN 1 ELSE 0 END) AS deletions, SUM(CASE WHEN ChangeType = 'modification' THEN 1 ELSE 0 END) AS updates from tbl_changedetails WHERE SubListID in ({sublists}) GROUP BY SubListID ORDER BY SubListID;"
    WATCHLIST_QUERY = "SELECT wl.ListID, wl.Program, swl.SubListID FROM tbl_watchlist wl JOIN tbl_subwatchlist swl ON wl.ListID = swl.ListID WHERE SubListID in ({question_marks});"

    engine = sa.create_engine(CONNECTION_STRING)
    connection = engine.connect()
    sublists_string = ','.join(map(str, sublist_ids))
    change_list = connection.execute(CHANGES_QUERY.format(sublists=sublists_string)).fetchall()

    change_dict = {}
    for change in change_list:
        change_dict[change[0]] = [change[1], change[2], change[3]]

    final_change_lists = []
    question_marks = '?,' * len(change_dict.keys())
    watchlists = connection.execute(WATCHLIST_QUERY.format(question_marks=question_marks[0:-1]), (change_dict.keys(),)).fetchall()
    for wl in watchlists:
        change_list = [wl[0], wl[1]] + change_dict[wl[2]]
        final_change_lists.append(change_list)

    return final_change_lists

def create_mail_body(change_lists):
    html = """\
    <html><head></head>
    <body>
    <p>Dear Customer, <br> Below are the updates in watchlist</p>
    <table style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%;">{changes_rows}
    </table>
    <br><br>
    <p>Regards,<br>LFD Support Team</p>
    </body>
    </html>"""

    html_tr = """<tr style="background-color: #f2f2f2;">{0}{1}{2}{3}{4}</tr>"""
    html_th = """<th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; color: white; border: 1px solid #ddd; padding: 8px;">{heading}</th>"""
    html_td = """<td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 8px;">{item}</td>"""

    table_rows = [html_tr.format(html_th.format(heading='List ID'), 
                                    html_th.format(heading='Program'), 
                                    html_th.format(heading='Additions'), 
                                    html_th.format(heading='Deletions'), 
                                    html_th.format(heading='Updates'))]

    for change in change_lists:
        row = html_tr.format(html_td.format(item=change[0]),
                                html_td.format(item=change[1]),
                                html_td.format(item=change[2]),
                                html_td.format(item=change[3]),
                                html_td.format(item=change[4]))
        table_rows.append(row)
    
    changes_rows = ''.join(table_rows)
    
    return html.format(changes_rows=changes_rows)

def send_email(email_html, start_time):
    message = MIMEText(email_html, 'html')
    message['From'] = config('MAIL_FROM')
    message['To'] = config('MAIL_CUSTOMER')
    message['Subject'] = 'Watchlist Updates - {0}'.format(start_time.strftime('%d-%m-%Y'))

    msg_full = message.as_string()

    server = smtplib.SMTP('{0}:{1}'.format(config('MAIL_HOST'), config('MAIL_PORT')))
    server.starttls()
    server.login(config('MAIL_USER'), config('MAIL_PASS'))
    try:
        server.sendmail(config('MAIL_FROM'),
                        config('MAIL_CUSTOMER', cast=lambda emails: [e.strip() for e in emails.split(',')]),
                        msg_full)
    except:
        pass
    server.quit()

def create_traceback_message():
        fp = io.StringIO()
        traceback.print_exc(file=fp)
        return fp.getvalue()

def create_and_send_error_email(list_id, traceback):
    current_time = datetime.datetime.now()
    email_subject = "Error occurred while detecting changes."
    email_body = """
    Change Detection Error Notification ({start_time}):
    List ID: {list_id}

    Error Info:
    -------------------------------------
    {traceback}

    """.format(start_time=current_time.strftime('%d-%m-%Y %H:%M'),
               list_id=list_id,
               traceback=traceback)

    server = smtplib.SMTP('{0}:{1}'.format(config('MAIL_HOST'), config('MAIL_PORT')))
    server.starttls()
    server.login(config('MAIL_USER'), config('MAIL_PASS'))

    message = '\r\n'.join(['To: %s' % config('MAIL_TO'),
                        'From: %s' % config('MAIL_FROM'),
                        'Subject: %s' % email_subject,
                        '', email_body])

    try:
        server.sendmail(config('MAIL_FROM'),
                        config('MAIL_TO', cast=lambda emails: [e.strip() for e in emails.split(',')]),
                        message)
    except:
        pass
    server.quit()

def send_summary_email(sublist_ids, start_time):
    changes = get_list_of_changes(sublist_ids)
    email_body = create_mail_body(changes)
    send_email(email_body, start_time)

def send_error_email(list_id):
    traceback = create_traceback_message()
    create_and_send_error_email(list_id, traceback)