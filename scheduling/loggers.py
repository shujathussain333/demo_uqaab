import logging
import logging.handlers as handlers


def create_logging_file():
    logger = logging.getLogger('scrapper_scheduler')
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(filename)s : %(lineno)d - %(message)s')
    if not logger.handlers:
        logHandler = handlers.TimedRotatingFileHandler('scrapper.log', when='H', interval=24,  backupCount=10)
        logHandler.setLevel(logging.INFO)
        logHandler.setFormatter(formatter)
        logger.addHandler(logHandler)
    return logger