import pandas as pd
import sqlalchemy as sa
import json
import difflib
import logging
from environment import config
from emailer import send_error_email
from loggers import create_logging_file
from datetime import datetime, timedelta
import os
import glob

CONNECTION_STRING = config('SA_CONNECTION_STRING')
logging = create_logging_file()


class ChangeDetection():
    THRESHOLD = 0.75

    def __init__(self, db_connection, list_id, new_sublist_id, old_sublist_id, program_name):
        self.conn = db_connection
        self.list_id = list_id
        self.new_sublist_id = new_sublist_id
        self.old_sublist_id = old_sublist_id
        self.ExistingTable = None
        self.ScrapedTable = None
        self.source_df = None
        self.target_df = None
        self.program_name = program_name

    def detect(self):
        self._create_dataframes()
        if self._data_has_changes():
            self._enable_new_version()
            self._copy_data('tbl_entities', 'tbl_scrappedData')
            self._do_rule_based_change_detection()
            return True
        else:
            self._delete_new_version()
            return False

    def _create_dataframes(self):
        if self.old_sublist_id is not None:
            self._create_df_from_except_queries()
        else:
            self._create_df_from_select_query()

        self.source_df = self.ScrapedTable.copy()
        self.target_df = self.ExistingTable.copy()

    def _create_df_from_except_queries(self):

        NEW_RECORDS_QUERY = "SELECT Name, DOB, Aliases, Remarks, Title, DO_Inclusion from tbl_entities WHERE SubListID = ? EXCEPT SELECT Name, DOB, Aliases, Remarks, Title, DO_Inclusion from tbl_scrappedData WHERE SubListID = ?;"
        OLD_RECORDS_QUERY = "SELECT Name, DOB, Aliases, Remarks, Title, DO_Inclusion from tbl_scrappedData WHERE SubListID = ? EXCEPT SELECT Name, DOB, Aliases, Remarks, Title, DO_Inclusion from tbl_entities WHERE SubListID = ?;"

        self.ExistingTable = self._make_df_from_query(NEW_RECORDS_QUERY, (self.new_sublist_id, self.old_sublist_id))
        self.ScrapedTable = self._make_df_from_query(OLD_RECORDS_QUERY, (self.old_sublist_id, self.new_sublist_id))

    def _create_df_from_select_query(self):
        NEW_RECORDS_QUERY = "SELECT Name, DOB, Aliases, Remarks, Title, DO_Inclusion from tbl_entities WHERE SubListID = ?;"

        self.ExistingTable = self._make_df_from_query(NEW_RECORDS_QUERY, (self.new_sublist_id))
        self.ScrapedTable = pd.DataFrame()

    def _make_df_from_query(self, query, values):
        results = self._execute_query(query, values)
        df = pd.DataFrame(results.fetchall())

        try:
            df.columns = results.keys()
        except ValueError:
            pass
        return df

    def _do_rule_based_change_detection(self):
        new_records_count = len(self.ExistingTable.index)
        old_records_count = len(self.ScrapedTable.index)

        if new_records_count > 0 and old_records_count == 0:
            self._record_additions_only()
        elif new_records_count == 0 and old_records_count > 0:
            self._record_deletions_only()
        elif new_records_count > 0 and old_records_count > 0:
            changes, value_changes = self._identify_changes()
            self._record_modifications(changes, value_changes)

    def _data_has_changes(self):
        return len(self.ExistingTable.index) > 0 or len(self.ScrapedTable.index) > 0

    def _copy_data(self, source, destination):
        COPY_QUERY = """INSERT INTO {destination} (EDateTime, SubListID, ECategory, Name, Address, DOB, Nationality, Designation, Organization, Title, DO_Inclusion, DO_Exclusion, Aliases, Matching, ListType, Remarks, Status, Gender, Last_Occupation, Documents) SELECT EDateTime, SubListID, ECategory, Name, Address, DOB, Nationality, Designation, Organization, Title, DO_Inclusion, DO_Exclusion, Aliases, Matching, ListType, Remarks, Status, Gender, Last_Occupation, Documents FROM {source} WHERE SubListID = ?;"""
        self._execute_query(COPY_QUERY.format(source=source, destination=destination), (self.new_sublist_id,))

    def _delete_new_version(self):
        if self.old_sublist_id is not None:
            self._execute_query("Update tbl_entities SET SubListID = ? WHERE SubListID = ?",
                                (self.old_sublist_id, self.new_sublist_id))
            self._execute_query("DELETE FROM tbl_subwatchlist where SubListID = ?", (self.new_sublist_id))

    def _enable_new_version(self):
        self._execute_query("Update tbl_subwatchlist SET IsEnabled = ? WHERE SubListID = ?", (1, self.new_sublist_id))

    def _execute_query(self, query, values=None):
        if values:
            return self.conn.execute(query, values)
        return self.conn.execute(query)

    def similar(self, source, destination):
        x = difflib.SequenceMatcher(None, source, destination).ratio()
        return x

    def product(self, row):
        product = row['match_name'] * row['match_dob'] * row['match_title'] * row['match_remarks']
        return product

    def matched_attributes(self, row):
        ratios = [row['match_name'], row['match_dob'], row['match_title'], row['match_remarks'],
                  row['match_Aliases']]
        matched_attributes = len([m for m in ratios if m >= self.THRESHOLD])
        return matched_attributes

    def _identify_changes(self):
        logging.info("Start Identifying the changes for {}".format(self.program_name))
        changes = {}
        value_change = {}

        copy_destination_df = self.target_df

        copy_destination_df['Name'] = copy_destination_df["Name"].apply(lambda x: self._clean_string(x, ['name', ]))
        copy_destination_df["DOB"] = copy_destination_df["DOB"].apply(
            lambda x: self._clean_string(x, ['00:00:00', '"DOB"', '"POB"', 'null']))
        copy_destination_df["Title"] = copy_destination_df["Title"].apply(
            lambda x: self._clean_string(x, ['title', ]))
        copy_destination_df["Aliases"] = copy_destination_df["Aliases"].apply(
            lambda x: self._clean_string(x, ['Aliases', ]))
        copy_destination_df.loc[copy_destination_df['Remarks'].isnull(), 'Remarks'] = ''
        copy_destination_df.loc[copy_destination_df['DO_Inclusion'].isnull(), 'DO_Inclusion'] = ''

        for sindex in self.source_df.index:
            sname = self._clean_string(self.source_df.at[sindex, "Name"], ['name', ])
            sdob = self._clean_string(self.source_df.at[sindex, "DOB"], ['00:00:00', '"DOB"', '"POB"', 'null'])
            stitle = self._clean_string(self.source_df.at[sindex, "Title"], ['title', ])
            sremarks = self.source_df.at[sindex, 'Remarks'] if self.source_df.at[sindex, 'Remarks'] else ""
            sAliases = self._clean_string(self.source_df.at[sindex, "Aliases"], ['Aliases', ])
            sdateinc = self.source_df.at[sindex, 'DO_Inclusion'] if self.source_df.at[sindex, 'DO_Inclusion'] else ""

            copy_destination_df['match_name'] = copy_destination_df["Name"].apply(lambda row: self.similar(row, sname))
            copy_destination_df['match_dob'] = copy_destination_df["DOB"].apply(lambda row: self.similar(row, sdob))
            copy_destination_df['match_title'] = copy_destination_df["Title"].apply(
                lambda row: self.similar(row, stitle))
            copy_destination_df['match_Aliases'] = copy_destination_df["Aliases"].apply(
                lambda row: self.similar(row, sAliases))
            copy_destination_df['match_remarks'] = copy_destination_df['Remarks'].apply(
                lambda row: self.similar(row, sremarks))
            copy_destination_df['product'] = copy_destination_df.apply(lambda row: self.product(row), axis=1)
            copy_destination_df['matched_attributes'] = copy_destination_df.apply(
                lambda row: self.matched_attributes(row), axis=1)

            copy_destination_df2 = copy_destination_df[
                (copy_destination_df['matched_attributes'] >= 5) & (copy_destination_df['match_name'] > 0.60)]

            for tindex in copy_destination_df2.index:
                changes[(sindex, tindex)] = copy_destination_df2.at[tindex, "product"]

                if copy_destination_df2.at[tindex, "match_name"] < 1.00:
                    value_change[(sindex, tindex, "Name")] = copy_destination_df2.at[tindex, "match_name"]

                if copy_destination_df2.at[tindex, "match_Aliases"] < 1.00:
                    value_change[(sindex, tindex, "Aliases")] = copy_destination_df2.at[tindex, "match_Aliases"]

                if copy_destination_df2.at[tindex, "match_dob"] < 1.00:
                    value_change[(sindex, tindex, "DOB")] = copy_destination_df2.at[tindex, "match_dob"]

                if copy_destination_df2.at[tindex, "match_title"] < 1.00:
                    value_change[(sindex, tindex, "Title")] = copy_destination_df2.at[tindex, "match_title"]

                if copy_destination_df2.at[tindex, "match_remarks"] < 1.00:
                    value_change[(sindex, tindex, "Remarks")] = copy_destination_df2.at[tindex, "match_remarks"]

        logging.info("End Identifying the changes for {}".format(self.program_name))
        return changes, value_change

    @staticmethod
    def _clean_string(string, words_to_remove):
        char_list = ['[', ']', '{', '}', ':', ',', '"']
        try:
            new_string = string.translate({ord(x): '' for x in char_list})
            for word in words_to_remove:
                new_string = new_string.replace(word, '')
        except (AttributeError, TypeError):
            new_string = ""

        return new_string.strip()

    def _record_additions_only(self):
        logging.info("Record Additions Only start for {}".format(self.program_name))
        self._bulk_insert_into_table('addition')
        logging.info("Record Additions Only completed for {}".format(self.program_name))

    def _record_deletions_only(self):
        logging.info("Record Deletions Only start for {}".format(self.program_name))
        self._bulk_insert_into_table('removal')
        logging.info("Record Deletions Only completed for {}".format(self.program_name))

    def _bulk_insert_into_table(self, change_type):
        date = datetime.now()
        change_id = self._insert_changelog_record()
        COPY_QUERY = """INSERT INTO tbl_changedetails (EID, ChangeID, ModifiedDate, ChangeType, SubListID) 
                        Select EID, ? as ChangeID, ? as ModifiedDate, ? as ChangeType, SubListID from tbl_scrappedData where SubListID=?;"""
        self._execute_query(COPY_QUERY, (change_id, date, change_type, self.new_sublist_id))

    def _record_modifications(self, changes, value_changes):
        logging.info("Record Modification start for {}".format(self.program_name))

        changed_rows = list(changes.keys())
        value_changes_cols = list(value_changes.keys())

        source_change_rows = list(self.source_df.index)
        target_change_rows = list(self.target_df.index)

        source_cdel = [r[0] for r in changed_rows]
        target_cadd = [r[1] for r in changed_rows]
        source_deletions = list(set(source_change_rows) - set(source_cdel))
        target_additions = list(set(target_change_rows) - set(target_cadd))

        if len(self.ScrapedTable):
            self.ScrapedTable = self.ScrapedTable.drop(self.ScrapedTable.index[source_deletions])
            try:
                self.ScrapedTable = self.ScrapedTable.append(self.ExistingTable.loc[target_additions])
            except KeyError:
                pass

        date = datetime.now()
        change_id = self._insert_changelog_record()

        for deletion in source_deletions:
            logging.debug("Deleting unwanted record for scrapper {}".format(self.program_name))
            entity_id = self._perform_eid_query(self.source_df.ix[deletion], self.old_sublist_id)
            self._execute_query("INSERT INTO tbl_changedetails VALUES(?,?,?,?,?,?,?,?)",
                                (change_id, None, None, date, None, "removal", self.new_sublist_id, entity_id))

        for addition in target_additions:
            logging.debug("Inserting new record for scrapper {}".format(self.program_name))
            entity_id = self._perform_eid_query(self.target_df.ix[addition], self.new_sublist_id)
            self._execute_query("INSERT INTO tbl_changedetails VALUES(?,?,?,?,?,?,?,?)",
                                (change_id, None, None, date, None, "addition", self.new_sublist_id, entity_id))

        for val in value_changes_cols:
            src = val[0]
            trg = val[1]
            col = val[2]

            entity_id = self._perform_eid_query(self.source_df.ix[src], self.old_sublist_id)

            self.ScrapedTable.at[src, col] = self.target_df.loc[trg, col]

            if col == 'DO_Inclusion':
                old_value = str(self.source_df.loc[src, col])
                new_value = str(self.target_df.loc[trg, col])
            else:
                old_value = json.dumps(self.source_df.loc[src, col])
                new_value = json.dumps(self.target_df.loc[trg, col])

            self._execute_query("INSERT INTO tbl_changedetails VALUES(?,?,?,?,?,?,?,?)",
                                (change_id, old_value, new_value, date, col, "modification", self.new_sublist_id,
                                 entity_id))

        logging.info("Record Modification end for {}".format(self.program_name))

    def _insert_changelog_record(self):
        INSERT_QUERY = "INSERT INTO tbl_listchangelog (ModifiedDate, SubListID) VALUES (?, ?);"
        SELECT_QUERY = "SELECT TOP 1 ChangeID FROM tbl_listchangelog WHERE SubListID = ? ORDER BY ChangeID DESC;"

        date = datetime.now()
        self._execute_query(INSERT_QUERY, (date, self.new_sublist_id))
        insertetd_id = self._execute_query(SELECT_QUERY, self.new_sublist_id).fetchone()[0]
        return insertetd_id

    def _perform_eid_query(self, df_row, sublist_id):
        where_clause, values = self._create_where_clause(df_row, sublist_id)
        query = """SELECT EID from tbl_scrappedData WHERE {where};""".format(
            where=where_clause)
        try:
            result = self._execute_query(query, values).fetchone()[0]
            return result
        except TypeError:
            return None

    def _create_where_clause(self, df_row, sublist_id):
        cols = ['Name', 'Aliases', 'Title', 'DO_Inclusion']
        where_statements = []
        values = []
        for col in cols:
            statement = self._create_where_string(col, df_row[col])
            where_statements.append(statement)
            if df_row[col] is not None:
                values.append(df_row[col])

        remaining = ' and '.join(where_statements)
        where = "SubListID=? and {remainig_conditions}".format(remainig_conditions=remaining)
        values = [sublist_id] + values
        return where, values

    @staticmethod
    def _create_where_string(col_name, col_value):
        if col_value is not None:
            return "{name}=?".format(name=col_name)
        else:
            return "{name} is NULL".format(name=col_name)


def get_sublist_ids_of_changed_lists(list_id):
    start_cd_time = datetime.now()
    SUBWATCHLIST_QUERY = "SELECT TOP 2 tbl_subwatchlist.SubListID,  tbl_watchlist.IsStatic, tbl_subwatchlist.Program  FROM tbl_subwatchlist inner join tbl_watchlist On tbl_watchlist.ListID = tbl_subwatchlist.ListID WHERE tbl_subwatchlist.ListID = ?   ORDER BY SubListID DESC;"
    change = 0

    engine = sa.create_engine(CONNECTION_STRING)
    connection = engine.connect()

    sublists = connection.execute(SUBWATCHLIST_QUERY, (list_id,)).fetchall()
    try:
        if sublists[0][1] == 0:
            logging.info("Starting change detection for {}".format(sublists[0][2]))
            try:
                changedet = ChangeDetection(connection, list_id, sublists[0][0], sublists[1][0], sublists[0][2])
            except IndexError:
                changedet = ChangeDetection(connection, list_id, sublists[0][0], None, sublists[0][2])
            list_has_changes = changedet.detect()
            if list_has_changes:
                change = 1
            logging.info("Ending change detection for {}".format(sublists[0][2]))
    except Exception as e:
        logging.info("Error found on {} {}".format(list_id, e))
        send_error_email(list_id)

    update_cd_time_log_file(start_cd_time, sublists[0][0], change)

    connection.close()
    engine.dispose()
    return change


def update_cd_time_log_file(start_cd_time, sublist_id, change):
    try:
        end_cd_time = datetime.now()
        files_path = os.path.join(config('PROJECT_PATH'), 'timelogs', '*.csv')
        files = sorted(glob.iglob(files_path), key=os.path.getctime, reverse=True)
        log_file_path = files[0]
        dif = end_cd_time - start_cd_time
        df = pd.read_csv(log_file_path)
        df.loc[df['SubListID'] == sublist_id, 'CDStartTime'] = start_cd_time.strftime('%H:%M:%S')
        df.loc[df['SubListID'] == sublist_id, 'CDEndTime'] = end_cd_time.strftime('%H:%M:%S')
        # -1 sec difference in csv result is due to microseconds in time
        df.loc[df['SubListID'] == sublist_id, 'CDRunTime'] = timedelta(seconds=dif.seconds)
        df.loc[df['SubListID'] == sublist_id, 'ListHasChanges'] = change
        df.to_csv(log_file_path, encoding='utf-8', index=False)
    except Exception as e:
        logging.info(e)
