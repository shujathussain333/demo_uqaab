﻿USE [master]
GO
/****** Object:  Database [hblUqaab2]    Script Date: 7/27/18 12:36:59 PM ******/
CREATE DATABASE [hblUqaab2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'hblUqaab2', FILENAME = N'/var/opt/mssql/data/hblUqaab2.mdf' , SIZE = 40128KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'hblUqaab2_log', FILENAME = N'/var/opt/mssql/data/hblUqaab2_log.ldf' , SIZE = 104000KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [hblUqaab2] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hblUqaab2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hblUqaab2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hblUqaab2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hblUqaab2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hblUqaab2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hblUqaab2] SET ARITHABORT OFF 
GO
ALTER DATABASE [hblUqaab2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [hblUqaab2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hblUqaab2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hblUqaab2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hblUqaab2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hblUqaab2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hblUqaab2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hblUqaab2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hblUqaab2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hblUqaab2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [hblUqaab2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hblUqaab2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hblUqaab2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hblUqaab2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hblUqaab2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hblUqaab2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hblUqaab2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hblUqaab2] SET RECOVERY FULL 
GO
ALTER DATABASE [hblUqaab2] SET  MULTI_USER 
GO
ALTER DATABASE [hblUqaab2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hblUqaab2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [hblUqaab2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [hblUqaab2] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [hblUqaab2] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'hblUqaab2', N'ON'
GO
ALTER DATABASE [hblUqaab2] SET QUERY_STORE = OFF
GO
USE [hblUqaab2]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [hblUqaab2]
GO
/****** Object:  Table [dbo].[tbl_changedetails]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_changedetails](
	[DetailID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeID] [int] NULL,
	[OldValue] [nvarchar](255) NULL,
	[NewValue] [nvarchar](255) NULL,
	[ModifiedDate] [date] NULL,
	[ColumnName] [nvarchar](255) NULL,
	[ChangeType] [nvarchar](255) NULL,
	[SubListID] [int] NULL,
	[EID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_entities]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_entities](
	[EID] [int] IDENTITY(1,1) NOT NULL,
	[EDateTime] [datetime2](7) NULL,
	[SubListID] [int] NULL,
	[ECategory] [nvarchar](255) NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[DOB] [nvarchar](max) NULL,
	[Nationality] [nvarchar](255) NULL,
	[Designation] [nvarchar](255) NULL,
	[Organization] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[DO_Inclusion] [date] NULL,
	[DO_Exclusion] [date] NULL,
	[Aliases] [nvarchar](max) NULL,
	[Matching] [nvarchar](max) NULL,
	[ListType] [nvarchar](255) NULL,
	[Remarks] [nvarchar](max) NULL,
	[Status] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[Last_Occupation] [nvarchar](50) NULL,
	[Documents] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_entities] PRIMARY KEY CLUSTERED 
(
	[EID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_listchangelog]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_listchangelog](
	[ChangeID] [int] IDENTITY(1,1) NOT NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[LastModifiedDate] [smalldatetime] NULL,
	[SubListID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_scrappedData]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_scrappedData](
	[EID] [int] IDENTITY(1,1) NOT NULL,
	[EDateTime] [datetime] NULL,
	[SubListID] [int] NULL,
	[ECategory] [nvarchar](255) NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[DOB] [nvarchar](max) NULL,
	[Nationality] [nvarchar](255) NULL,
	[Designation] [nvarchar](255) NULL,
	[Organization] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[DO_Inclusion] [date] NULL,
	[DO_Exclusion] [date] NULL,
	[Aliases] [nvarchar](max) NULL,
	[Matching] [nvarchar](max) NULL,
	[ListType] [nvarchar](255) NULL,
	[Remarks] [nvarchar](max) NULL,
	[Status] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[Last_Occupation] [nvarchar](50) NULL,
	[Documents] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_subwatchlist]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_subwatchlist](
	[SubListID] [int] IDENTITY(1,1) NOT NULL,
	[ListType] [nvarchar](255) NULL,
	[Program] [nvarchar](255) NULL,
	[PublisherName] [nvarchar](255) NULL,
	[DatasetName] [nvarchar](255) NULL,
	[DataFormat] [nvarchar](255) NULL,
	[Datalink] [nvarchar](255) NULL,
	[Priority] [int] NULL,
	[website] [nvarchar](255) NULL,
	[DataYear] [int] NULL,
	[Version] [nvarchar](255) NULL,
	[LastModified] [nvarchar](255) NULL,
	[CreatedDate] [date] NULL,
	[IsEnabled] [bit] NOT NULL,
	[ChangeID] [int] NULL,
	[ListID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_userMaster]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_userMaster](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](55) NULL,
	[Password] [nvarchar](16) NULL,
	[FirstName] [nvarchar](55) NULL,
	[LastName] [nvarchar](55) NULL,
	[Module] [nvarchar](55) NULL,
	[IsEnable] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_watchlist]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_watchlist](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[ListType] [nvarchar](255) NULL,
	[Program] [nvarchar](255) NULL,
	[PublisherName] [nvarchar](255) NULL,
	[DatasetName] [nvarchar](255) NULL,
	[DataFormat] [nvarchar](255) NULL,
	[FileName] [nvarchar](50) NULL,
	[Datalink] [nvarchar](255) NULL,
	[Year] [int] NULL,
	[Version] [nvarchar](255) NULL,
	[LastModified] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NULL,
	[IsEnabled] [bit] NULL,
 CONSTRAINT [PK_tbl_watchlist] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_watchlist] ON 

INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (1, N'SAN', N'National Procurement Authority (NPA)', NULL, N'debarred-companies', N'Webpage', N'afghan_npa_spider', N'http://www.npa.v.af/en/debarred-companies', 2018, N'v1', N'Jul  1 2018 11:20AM', CAST(N'2018-07-01T11:20:15.607' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (2, N'SAN', N'African Development Bank Group', NULL, N'debarment-and-sanctions-procedures', N'Webpage', N'african_dev_bank', N'http://www.afdb.org/en/projects-and-operations/procurement/debarment-and-sanctions-procedures/', 2018, N'v1', N'Jul  1 2018 11:28AM', CAST(N'2018-07-01T11:28:51.983' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (3, N'SAN', N'Asian Development Bank', NULL, N'ADB published sanctions list', N'Webpage', N'asian_dev_bank', N'http://lnadbg4.adb.org/oga0009p.nsf/sancALL1P', 2018, N'v1', N'Jul  1 2018 11:31AM', CAST(N'2018-07-01T11:31:31.323' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (4, N'SIP', N'Australian National Security', NULL, N'Terrorist Organization', N'Webpage', N'australia_nat_sec_spider', N'https://www.nationalsecurity.v.au/Listedterroristorganisations/Pages/default.aspx', 2018, N'v1', N'Jul  1 2018 11:32AM', CAST(N'2018-07-01T11:32:55.380' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (5, N'SAN', N'National e-vernment Procurement (e-GP)', NULL, N'Debarment', N'Webpage', N'bangladesh_deblist_spider', N'https://www.eprocure.v.bd/resources/common/DebarmentRpt.jsp', 2018, N'v1', N'Jul  1 2018 11:33AM', CAST(N'2018-07-01T11:33:45.147' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (6, N'SAN', N'BIS 8-Consolidated Screening List', NULL, N'Consolidated Screening List', N'XLXS', N'BISList_Spider', N'https://api.trade.v/consolidated_screening_list/search.csv?api_key=OHZYuksFHSFao8jDXTkfiypO', 2018, N'v1', N'Jul  1 2018 11:34AM', CAST(N'2018-07-01T11:34:59.110' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (7, N'SAN', N'BSCN Sanction Countries List', NULL, N'BSCN-Sanction-Countries', N'Webpage', N'BSCN-Sanction-Countries', N'http://www.bscn.nl/sanctions-consulting/sanctions-list-countries/', 2018, N'v1', N'Jul  1 2018 11:37AM', CAST(N'2018-07-01T11:37:07.507' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (8, N'SAN', N'Regulations Implementing the United Nations Resolution on the Democratic Peoples Republic of Korea', NULL, N'Names of individuals & Entities', N'XLXS', N'canada_financial_scrapper', N'"http://www.osfi-bsif.gc.ca/Eng/fi-if/amlc-clrpc/snc/unas-slnu/Pages/kp.aspx', 2018, N'v1', N'Jul  1 2018 11:43AM', CAST(N'2018-07-01T11:43:54.203' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (9, N'SAN', N'Anti-terrorism Financing', NULL, N'Names of individuals & Entities', N'XLXS', N'canada_financial_scrapper_2', N'http://www.osfi-bsif.gc.ca/Eng/fi-if/amlc-clrpc/atf-fat/Pages/default.aspx', 2018, N'v1', N'Jul  1 2018 11:46AM', CAST(N'2018-07-01T11:46:40.787' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (10, N'SAN', N'Regulations Implementing the United Nations Resolution on Iran', NULL, N'Names of individuals & Entities', N'XLXS', N'canada_financial_scrapper_3', N'http://www.osfi-bsif.gc.ca/Eng/fi-if/amlc-clrpc/snc/unas-slnu/Pages/ir.aspx', 2018, N'v1', N'Jul  1 2018 11:47AM', CAST(N'2018-07-01T11:47:25.967' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (11, N'SIP', N'Public Safety - Canada Gazzete', NULL, N'Canada Gazette Entities', N'XLXS', N'canada_public_safety_spider', N'https://www.publicsafety.gc.ca/cnt/ntnl-scrt/cntr-trrrsm/lstd-ntts/crrnt-lstd-ntts-en.aspx', 2018, N'v1', N'Jul  1 2018 11:49AM', CAST(N'2018-07-01T11:49:33.627' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (12, N'SAN', N'Department of Foreign Affairs and Trade', NULL, N'Consolidated List ', N'XLXS', N'consolidated_xls', N'http://dfat.v.au/international-relations/security/sanctions/pages/consolidated-list.aspx', 2018, N'v1', N'Jul  1 2018 11:51AM', CAST(N'2018-07-01T11:51:23.813' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (13, N'SIP', N'European Bank Ineligible entities', NULL, N'Ineligible entities List ', N'Webpage', N'ERDB-Entities', N'https://www.ebrd.com/ineligible-entities-list.html', 2018, N'v1', N'Jul  1 2018 11:54AM', CAST(N'2018-07-01T11:54:03.337' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (14, N'SAN', N'Inter Dev Bank Group of Sanctions', NULL, N'IDB Group list of Sanctioned Firms and Individuals', N'Webpage', N'eu_dev_bank_idb', N'https://idblegacy.iadb.org/en/topics/transparency/integrity-at-the-idb-group/sanctioned-firms-and-individuals,1293.html', 2018, N'v1', N'Jul  1 2018 11:55AM', CAST(N'2018-07-01T11:55:51.743' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (15, N'SIP', N'Wanted List from Interpol', NULL, N'Interpol Wanted  List ', N'Webpage', N'interpol_wanted_spider', N'https://www.interpol.int/notice/search/wanted', 2018, N'v1', N'Jul  1 2018 11:57AM', CAST(N'2018-07-01T11:57:53.097' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (16, N'SAN', N'List of Sanctioned Athletes Kenya', NULL, N'List of Sanctiond Athletes Kenya', N'Webpage', N'ken_athletes', N'http://www.athleticskenya.or.ke/medicalanti-doping/list-of-sanctioned-athletes/', 2018, N'v1', N'Jul  1 2018 11:59AM', CAST(N'2018-07-01T11:59:46.727' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (17, N'SAN', N'Kenya Debarred Firms', NULL, N'Kenya Debarred Firms', N'PDF', N'kenya_firms_spider', N'http://www.ppoa..ke/images/downloads/boraqs-registered-firms/LIST%20OF%20DEBARRED%20FIRMS%20_1_.pdf', 2018, N'v1', N'Jul  1 2018 12:06PM', CAST(N'2018-07-01T12:06:37.917' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (18, N'SIP', N'National sanctionlist terrorism', NULL, N'Netherlands terrorism Firms', N'XLXS', N'netherlands_list_spider', N'https://www.vernment.nl/binaries/vernment/documents/reports2016/01/15/national-terrorism-list/eng-terrorismelijst.ods', 2018, N'v1', N'Jul  1 2018 12:07PM', CAST(N'2018-07-01T12:07:59.223' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (19, N'SAN', N'New Zealand Police Organizations Resolutions 1267/1989/2253 and 1988', NULL, N'New Zealand Police Organizations Resolutions 1267/1989/2253 and 1988 List (Entities)', N'XLXS', N'nzpolice_res_organization_spider', N'https://www.police.vt.nz/sites/default/files/publications/designated-entities-18-june-2018.xls', 2018, N'v1', N'Jul  1 2018 12:13PM', CAST(N'2018-07-01T12:13:25.960' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (20, N'SAN', N'New Zealand Police Organizations Resolutions 1267/1989/2253 and 1988', NULL, N'New Zealand Police Organizations Resolutions 1267/1989/2253 and 1988 List (Individuals)', N'XLXS', N'nzpolice_res_organization_spider', N'https://www.police.vt.nz/sites/default/files/publications/designated-entities-18-june-2018.xls', 2018, N'v1', N'Jul  1 2018 12:14PM', CAST(N'2018-07-01T12:14:23.943' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (22, N'SAN', N'Ukraine financial intelligence unit publishes this list of sanctioned individuals.', N'Ukraine State Finance Monitoring Service', N' SDFM Blacklist', N'XML', N'sdfm_blacklist', N'http://www.sdfm.v.ua/content/file/Site_docs/Black_list/zBlackListFull.xm', 2018, N'v1', N'Jul  1 2018  1:06PM', CAST(N'2018-07-01T13:06:27.383' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (23, N'SAN', N'Investor Alert List (Monetary Authority of Singapore)', NULL, N'Investor Alert List', N'Webpage', N'sgp_mas', N'http://www.mas.v.sg/IAL.aspx?sc_p=all', 2018, N'v1', N'Jul  1 2018  1:08PM', CAST(N'2018-07-01T13:08:03.030' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (24, N'SAN', N'Consolidated list of Ukraine related sanctions', NULL, N'Consolidated list of Ukraine related sanctions', N'Webpage', N'ukrain_list_spider', N'https://www.riskadvisory.com/sanctioned-individuals/', 2018, N'v1', N'Jul  1 2018  1:08PM', CAST(N'2018-07-01T13:08:58.460' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (25, N'SAN', N'UN AL-Qaida', NULL, N'UN Sanctions (AL-Qaida & Taliban)', N'XML', N'UN-AL-Qaida', N'https://scsanctions.un.org/al-qaida/', 2018, N'v1', N'Jul  1 2018  1:10PM', CAST(N'2018-07-01T13:10:49.600' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (26, N'SAN', N'UN Central-Afircan-Republic', NULL, N'UN Sanctions (UN-Central-Afircan-Republic)', N'XML', N'UN-Central-Afircan-Republic', N'https://scsanctions.un.org/car/', 2018, N'v1', N'Jul  1 2018  1:11PM', CAST(N'2018-07-01T13:11:20.217' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (27, N'SAN', N'UN Consolidated-Sanctions', NULL, N'UN Sanctions (UN-Consolidated-Sanctions)', N'XML', N'UN-Consolidated-Sanctions', N'https://scsanctions.un.org/Consolidated/', 2018, N'v1', N'Jul  1 2018  1:11PM', CAST(N'2018-07-01T13:11:55.303' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (28, N'SAN', N'UN Democratic-People-Korea', NULL, N'UN Sanctions (UN-Democratic-People-Korea)', N'XML', N'UN-Democratic-People-Korea', N'https://scsanctions.un.org/dprk/', 2018, N'v1', N'Jul  1 2018  1:12PM', CAST(N'2018-07-01T13:12:47.633' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (29, N'SAN', N'UN-Democratic-Republic-Congo', NULL, N'UN Sanctions (UN Democratic-Republic-Con)', N'XML', N'UN-Democratic-Republic-Congo', N'https://scsanctions.un.org/drc/', 2018, N'v1', N'Jul  1 2018  1:13PM', CAST(N'2018-07-01T13:13:24.693' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (30, N'SAN', N'UN Iran', NULL, N'UN Sanctions (UN Iran)', N'XML', N'UN-Iran', N'https://scsanctions.un.org/Iran/', 2018, N'v1', N'Jul  1 2018  1:13PM', CAST(N'2018-07-01T13:13:47.427' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (31, N'SAN', N'UN Iraq', NULL, N'UN Sanctions (UN Iraq)', N'XML', N'UN-Iraq', N'https://scsanctions.un.org/Iraq/', 2018, N'v1', N'Jul  1 2018  1:14PM', CAST(N'2018-07-01T13:14:00.997' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (32, N'SAN', N'UN Libya', NULL, N'UN Sanctions (UN Libya)', N'XML', N'UN-Libya', N'https://scsanctions.un.org/Libya/', 2018, N'v1', N'Jul  1 2018  1:14PM', CAST(N'2018-07-01T13:14:14.500' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (33, N'SAN', N'UN somalia', NULL, N'UN Sanctions (UN somalia)', N'XML', N'UN-Somalia', N'https://scsanctions.un.org/somalia/', 2018, N'v1', N'Jul  1 2018  1:14PM', CAST(N'2018-07-01T13:14:27.890' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (34, N'SAN', N'UN South-Sudan', NULL, N'UN Sanctions (UN South-Sudan)', N'XML', N'UN-South-Sudan', N'https://scsanctions.un.org/South-Sudan/', 2018, N'v1', N'Jul  1 2018  1:14PM', CAST(N'2018-07-01T13:14:38.020' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (35, N'SAN', N'UN Sudan', NULL, N'UN Sanctions (UN Sudan)', N'XML', N'UN-Sudan', N'https://scsanctions.un.org/Sudan/', 2018, N'v1', N'Jul  1 2018  1:14PM', CAST(N'2018-07-01T13:14:53.657' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (36, N'SAN', N'UN yemen', NULL, N'UN Sanctions (UN yemen)', N'XML', N'UN-Yemen', N'https://scsanctions.un.org/yemen/', 2018, N'v1', N'Jul  1 2018  1:15PM', CAST(N'2018-07-01T13:15:09.973' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (37, N'SAN', N'UN UNDP-Ineligble-list', NULL, N'UN UNDP-Ineligble-list', N'XML', N'UNDP-Ineligble-list', N'http://www.undp.org/content/undp/en/home/operations/procurement/business/protest-and-sanctions/ineligibility-list/', 2018, N'v1', N'Jul  1 2018  1:16PM', CAST(N'2018-07-01T13:16:06.730' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (38, N'SAN', N'List of Statutorily Debarred Partiest', NULL, N'List of Statutorily Debarred Parties', N'Webpage', N'us_debarred_parties', N'https://mary.dtas-online.pmddtc.state.v/compliance/debar.html', 2018, N'v1', N'Jul  1 2018  1:17PM', CAST(N'2018-07-01T13:17:15.867' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (39, N'SIP', N'US Denied Person List', NULL, N'US Denied Person List', N'TEXT', N'us_denied_persons', N'https://www.bis.doc.v/dpl/dpl.txt', 2018, N'v1', N'Jul  1 2018  1:17PM', CAST(N'2018-07-01T13:17:50.983' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (40, N'SIP', N'World Bank Listing of Ineligible Firms & Individuals', NULL, N'Listing of Ineligible Firms & Individuals', N'Wepage', N'world_bank_top', N'http://web.worldbank.org/external/default/main?theSitePK=84266&contentMDK=64069844&menuPK=116730&pagePK=64148989&piPK=64148984', 2018, N'v1', N'Jul  1 2018  1:18PM', CAST(N'2018-07-01T13:18:37.030' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (41, N'SAN', N'Financial-sanction', NULL, N'List of Financial-sanction', N'XLXS', N'belgium_xls', N'https://finance.belgium.be/en/about_fps/structure_and_services/general_administrations/treasury/financial-sanctions/national', 2018, N'v1', N'Jul  1 2018  1:20PM', CAST(N'2018-07-01T13:20:43.107' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (54, N'SAN', N'executive_order_13599_list', NULL, N'Office of Foreign Assets Control', N'TXT', N'executive_order_13599_list', N'https://www.treasury.gov/ofac/downloads/13599/13599list.txt', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:47:21.690' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (55, N'SAN', N'financial_crimes_enforcement_network', NULL, N'Financial Crimes Enforcement Network (FinCEN)', N'xls', N'financial_crimes_enforcement_network', N'https://www.fincen.gov/financial_institutions/msb/msbstateselector.html', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:47:44.000' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (56, N'SAN', N'boe_spain22_jan_2002_list', NULL, N'BOE Spain 22-Jan-2002 List', N'xls', N'boe_spain22_jan_2002_list', N'http://www.boe.es/diario_boe/xml.php?id=BOE-A-2002-1217', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:48:05.240' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (61, N'SAN', N'freezing_assets_of_corrupt_foreign_officials', NULL, N'Freezing Assets of Corrupt Foreign Officials Act (FACFOA)', N'XML', N'freezing_assets_of_corrupt_foreign_officials', N'http://laws-lois.justice.gc.ca/eng/XML/SOR-2011-78.xml', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:53:35.210' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (62, N'SAN', N'central_intelligence_agency', NULL, N'Central Intelligence Agency', N'Webpage', N'central_intelligence_agency', N'https://www.cia.gov/library/publications/resources/world-leaders-1/index.html', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:53:49.760' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (63, N'SAN', N'bis_entity_list', NULL, N'Bureau of Industry and Security (BIS)', N'Webpage', N'bis_entity_list', N'https://www.ecfr.gov/cgi-bin/retrieveECFR?gp=1&SID=9ae4a21068f2bd41d4a5aee843b63ef1&ty=HTML&h=L&n=15y2.1.3.4.28&r=PART#ap15.2.744_122.4', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:11.867' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (64, N'SAN', N'bis_unverified_list', NULL, N'Bureau of Industry and Security (BIS)', N'Webpage', N'bis_unverified_list', N'https://www.ecfr.gov/cgi-bin/retrieveECFR?gp=1&SID=9ae4a21068f2bd41d4a5aee843b63ef1&ty=HTML&h=L&n=15y2.1.3.4.28&r=PART#ap15.2.744_122.6', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:30.480' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (65, N'SAN', N'terrorist_exclusion_list', NULL, N'Terrorist Exclusion List', N'Webpage', N'terrorist_exclusion_list', N'https://www.state.gov/j/ct/rls/other/des/123086.htm', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (66, N'SAN', N'state_sponsors_of_terrorism_list', NULL, N'State Sponsors of Terrorism List', N'Webpage', N'state_sponsors_of_terrorism_list', N'https://www.state.gov/j/ct/list/c14151.htm', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (67, N'SAN', N'nevada_gaming_control_board', NULL, N'Gaming Exclusions Lists', N'Webpage', N'nevada_gaming_control_board', N'http://gaming.nv.gov/index.aspx?page=72', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (68, N'SAN', N'cuba_restricted_list', NULL, N'Cuba Restricted List', N'Webpage', N'cuba_restricted_list', N'https://www.state.gov/e/eb/tfs/spi/cuba/cubarestrictedlist/275331.htm', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (69, N'SAN', N'caatsa_section_231_list', NULL, N'CAATSA Section 231 list', N'Webpage', N'caatsa_section_231_list', N'https://www.state.gov/t/isn/caatsa/275116.htm', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (70, N'SAN', N'drug_enforcement_agency', NULL, N'Drug Enforcement Agency', N'Webpage', N'drug_enforcement_agency_list', N'https://www.dea.gov', 2018, N'v1', N'Jul 1 2018 1:20PM', CAST(N'2018-07-16T17:54:41.363' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (74, N'SAN', N'OFAC Consolidated SDN/561LIST', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:00.483' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (75, N'SAN', N'OFAC Consolidated SDN/BALKANS', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:42.393' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (76, N'SAN', N'OFAC Consolidated SDN/BELARUS', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:58.080' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (77, N'SAN', N'OFAC Consolidated SDN/BPI-PA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:32.540' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (78, N'SAN', N'OFAC Consolidated SDN/BPI-SDNTK', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:59.300' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (79, N'SAN', N'OFAC Consolidated SDN/BURMA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:18.883' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (80, N'SAN', N'OFAC Consolidated SDN/BURUNDI', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:34.940' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (81, N'SAN', N'OFAC Consolidated SDN/CAATSA - RUSSIA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:51.703' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (82, N'SAN', N'OFAC Consolidated SDN/CAR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:06.007' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (83, N'SAN', N'OFAC Consolidated SDN/COTED', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:19.760' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (84, N'SAN', N'OFAC Consolidated SDN/CUBA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:30.330' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (85, N'SAN', N'OFAC Consolidated SDN/CYBER', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:44.910' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (86, N'SAN', N'OFAC Consolidated SDN/CYBER2', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:55.680' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (87, N'SAN', N'OFAC Consolidated SDN/DARFUR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:29:07.943' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (88, N'SAN', N'OFAC Consolidated SDN/DPRK', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:29:21.943' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (90, N'SAN', N'OFAC Consolidated SDN/DPRK2', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:21.133' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (91, N'SAN', N'OFAC Consolidated SDN/DPRK4', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:31.783' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (92, N'SAN', N'OFAC Consolidated SDN/DRCONGO', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:42.350' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (93, N'SAN', N'OFAC Consolidated SDN/EO13622', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:52.900' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (94, N'SAN', N'OFAC Consolidated SDN/EO13645', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:31:04.663' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (95, N'SAN', N'OFAC Consolidated SDN/FSE-SY', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:31:41.810' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (96, N'SAN', N'OFAC Consolidated SDN/FSE-IR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:05.993' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (97, N'SAN', N'OFAC Consolidated SDN/FSE-WMD', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:25.140' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (98, N'SAN', N'OFAC Consolidated SDN/FSE-SDGT', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:48.837' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (99, N'SAN', N'OFAC Consolidated SDN/GLOMAG', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:23.003' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (100, N'SAN', N'OFAC Consolidated SDN/HRIT-SY', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:42.280' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (101, N'SAN', N'OFAC Consolidated SDN/HRIT-IR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:56.713' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (102, N'SAN', N'OFAC Consolidated SDN/IFCA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:14.057' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (103, N'SAN', N'OFAC Consolidated SDN/IFSR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:26.677' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (104, N'SAN', N'OFAC Consolidated SDN/IRAN', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:37.920' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (105, N'SAN', N'OFAC Consolidated SDN/IRAN-HR', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:49.300' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (106, N'SAN', N'OFAC Consolidated SDN/IRAN-TRA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:06.203' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (107, N'SAN', N'OFAC Consolidated SDN/IRAQ2', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:18.413' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (108, N'SAN', N'OFAC Consolidated SDN/IRAQ3', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:32.807' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (109, N'SAN', N'OFAC Consolidated SDN/IRGC', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:36:15.953' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (110, N'SAN', N'OFAC Consolidated SDN/ISA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:08.593' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (111, N'SAN', N'OFAC Consolidated SDN/JADE', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:21.713' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (112, N'SAN', N'OFAC Consolidated SDN/LEBANON', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:33.477' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (113, N'SAN', N'OFAC Consolidated SDN/LIBERIA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:43.693' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (114, N'SAN', N'OFAC Consolidated SDN/LIBYA3', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:01.723' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (115, N'SAN', N'OFAC Consolidated SDN/MAGNIT', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:20.500' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (116, N'SAN', N'OFAC Consolidated SDN/NPWMD', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:35.007' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (117, N'SAN', N'OFAC Consolidated SDN/NS-ISA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:48.660' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (118, N'SAN', N'OFAC Consolidated SDN/NS-PLC', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:39:00.483' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (119, N'SAN', N'OFAC Consolidated SDN/SDGT', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:40:57.947' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (120, N'SAN', N'OFAC Consolidated SDN/SDNT', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:09.057' AS DateTime), 0)
GO
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (121, N'SAN', N'OFAC Consolidated SDN/SDNTK', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:18.867' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (122, N'SAN', N'OFAC Consolidated SDN/SDT', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:27.680' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (124, N'SAN', N'OFAC Consolidated SDN/SUDAN', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:46.940' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (125, N'SAN', N'OFAC Consolidated SDN/SOUTH SUDAN', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:59.153' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (126, N'SAN', N'OFAC Consolidated SDN/SYRIA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:42:10.500' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (127, N'SAN', N'OFAC Consolidated SDN/TCO', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:42:34.050' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (131, N'SAN', N'OFAC Consolidated SDN/UKRAINE-EO13685', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:43:38.833' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (132, N'SAN', N'OFAC Consolidated SDN/VENEZUELA', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:43:56.827' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (133, N'SAN', N'OFAC Consolidated SDN/YEMEN', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:44:05.730' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (134, N'SAN', N'OFAC Consolidated SDN/ZIMBABWE', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:44:14.090' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (135, N'SAN', N'OFAC Consolidated SDN/UKRAINE-EO13662', NULL, N'OFAC Consolidated SDN List', N'XML', N'OFAC_Consolidated', N'https://www.treasury.gov/ofac/downloads/consolidated/consolidated.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:59.300' AS DateTime), 0)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (138, N'SAN', N'OFAC SDN/561LIST', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:00.483' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (139, N'SAN', N'OFAC SDN/BALKANS', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:42.393' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (140, N'SAN', N'OFAC SDN/BELARUS', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:25:58.080' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (141, N'SAN', N'OFAC SDN/BPI-PA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:32.540' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (142, N'SAN', N'OFAC SDN/BPI-SDNTK', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:59.300' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (143, N'SAN', N'OFAC SDN/BURMA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:18.883' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (144, N'SAN', N'OFAC SDN/BURUNDI', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:34.940' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (145, N'SAN', N'OFAC SDN/CAATSA - RUSSIA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:27:51.703' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (146, N'SAN', N'OFAC SDN/CAR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:06.007' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (147, N'SAN', N'OFAC SDN/COTED', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:19.760' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (148, N'SAN', N'OFAC SDN/CUBA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:30.330' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (149, N'SAN', N'OFAC SDN/CYBER', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:44.910' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (150, N'SAN', N'OFAC SDN/CYBER2', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:28:55.680' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (151, N'SAN', N'OFAC SDN/DARFUR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:29:07.943' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (152, N'SAN', N'OFAC SDN/DPRK', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:29:21.943' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (153, N'SAN', N'OFAC SDN/DPRK2', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:21.133' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (154, N'SAN', N'OFAC SDN/DPRK4', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:31.783' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (155, N'SAN', N'OFAC SDN/DRCONGO', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:42.350' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (156, N'SAN', N'OFAC SDN/EO13622', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:30:52.900' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (157, N'SAN', N'OFAC SDN/EO13645', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:31:04.663' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (158, N'SAN', N'OFAC SDN/FSE-SY', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:31:41.810' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (159, N'SAN', N'OFAC SDN/FSE-IR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:05.993' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (160, N'SAN', N'OFAC SDN/FSE-WMD', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:25.140' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (161, N'SAN', N'OFAC SDN/FSE-SDGT', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:32:48.837' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (162, N'SAN', N'OFAC SDN/GLOMAG', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:23.003' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (163, N'SAN', N'OFAC SDN/HRIT-SY', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:42.280' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (164, N'SAN', N'OFAC SDN/HRIT-IR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:33:56.713' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (165, N'SAN', N'OFAC SDN/IFCA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:14.057' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (166, N'SAN', N'OFAC SDN/IFSR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:26.677' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (167, N'SAN', N'OFAC SDN/IRAN', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:37.920' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (168, N'SAN', N'OFAC SDN/IRAN-HR', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:34:49.300' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (169, N'SAN', N'OFAC SDN/IRAN-TRA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:06.203' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (170, N'SAN', N'OFAC SDN/IRAQ2', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:18.413' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (171, N'SAN', N'OFAC SDN/IRAQ3', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:35:32.807' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (172, N'SAN', N'OFAC SDN/IRGC', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:36:15.953' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (173, N'SAN', N'OFAC SDN/ISA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:08.593' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (174, N'SAN', N'OFAC SDN/JADE', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:21.713' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (175, N'SAN', N'OFAC SDN/LEBANON', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:33.477' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (176, N'SAN', N'OFAC SDN/LIBERIA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:37:43.693' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (177, N'SAN', N'OFAC SDN/LIBYA3', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:01.723' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (178, N'SAN', N'OFAC SDN/MAGNIT', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:20.500' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (179, N'SAN', N'OFAC SDN/NPWMD', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:35.007' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (180, N'SAN', N'OFAC SDN/NS-ISA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:38:48.660' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (181, N'SAN', N'OFAC SDN/NS-PLC', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:39:00.483' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (182, N'SAN', N'OFAC SDN/SDGT', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:40:57.947' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (183, N'SAN', N'OFAC SDN/SDNT', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:09.057' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (184, N'SAN', N'OFAC SDN/SDNTK', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:18.867' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (185, N'SAN', N'OFAC SDN/SDT', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:27.680' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (186, N'SAN', N'OFAC SDN/SUDAN', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:46.940' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (187, N'SAN', N'OFAC SDN/SOUTH SUDAN', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:41:59.153' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (188, N'SAN', N'OFAC SDN/SYRIA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:42:10.500' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (189, N'SAN', N'OFAC SDN/TCO', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:42:34.050' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (190, N'SAN', N'OFAC SDN/UKRAINE-EO13685', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:43:38.833' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (191, N'SAN', N'OFAC SDN/VENEZUELA', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:43:56.827' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (192, N'SAN', N'OFAC SDN/YEMEN', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:44:05.730' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (193, N'SAN', N'OFAC SDN/ZIMBABWE', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:44:14.090' AS DateTime), 1)
INSERT [dbo].[tbl_watchlist] ([ListID], [ListType], [Program], [PublisherName], [DatasetName], [DataFormat], [FileName], [Datalink], [Year], [Version], [LastModified], [CreatedDate], [IsEnabled]) VALUES (194, N'SAN', N'OFAC SDN/UKRAINE-EO13662', NULL, N'OFAC SDN List', N'XML', N'OFAC_SDN', N'https://www.treasury.gov/ofac/downloads/sdn.xml', 2018, N'v1', N'Jul 1 2018 12:38PM', CAST(N'2018-07-31T12:26:59.300' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tbl_watchlist] OFF
ALTER TABLE [dbo].[tbl_entities] ADD  CONSTRAINT [DF_tbl_entities_EDateTime]  DEFAULT (getdate()) FOR [EDateTime]
GO
ALTER TABLE [dbo].[tbl_watchlist] ADD  CONSTRAINT [Datetime]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  StoredProcedure [dbo].[truncate_table]    Script Date: 7/27/18 12:36:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[truncate_table] 
AS BEGIN 

truncate table tbl_entities
END
GO
USE [master]
GO
ALTER DATABASE [hblUqaab2] SET  READ_WRITE 
GO
