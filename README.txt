Instructions to setup the project
---------------------------------

1.    Create a new directory where all projects folders will be created.
2.    cd path/to/project/directory/
3.    Create virtual environment in the project directory so that python environment stays clean. This step is optional but recommended. (Read the article 'https://medium.com/the-python-corner/using-virtual-environments-with-python-7166d3bfa218' regarding virtual environment to have a better understanding of them.)
4.    Activate the virtual environment (source name-of-venv/bin/activate). Only required if you have created virtual environment in step 3.
5.    Create a new sub directory in the project directory.
6.    cd sub-directory/
7.    git clone https://gitlab.com/LFD_Salman/Uqaab.git
8.    install Java Runtime Environment (https://www.java.com/en/download/manual.jsp)
9.    pip install -r requirements.txt
10.   cd uqaab/
11.   Create a directory named 'logs'. This is where our log files will reside.
12.   Create a file named '.env'.
13.   Insert CONNECTION_STRING = "YOUR DB CONNECTION STRING" in the .env file.
14.   Insert PROJECT_PATH = path/to/project/directory/sub-directory in the .env file. This path should be the path of the folder where README.txt file resides.
15.   Run spiders using "scrapy crawl <spider_name> -a list_id=<ListID_in_tblwatchlist>" e.g. "scrapy crawl african_dev_bank -a list_id=2"
16.   Add Image folder and db paths in .env file for example IMAGE_PATH = /home/oem/test_images/, DB_IMAGE_PATH = /home/oem/test_images/

Project Deployment Checklist
----------------------------
1.    database is created and 'tbl_watchlist' is populated. If not then use 'uqaab.sql' file to create and populate database
2.    scrapyd server is running (localhost:6800)
3.    healthchecks server is running
4.    the absolute path to '.env' file in 'uqaab/uqaab/environment.py' and 'scheduling/environment.py' is correct
5.    values in 'uqaab/.env' file are correct, specially the values of 'CONNECTION_STRING', 'SA_CONNECTION_STRING', 'SCRAPYD_URL', 'PROJECT_PATH' and 'HEALTHCHECKS_URL' are correct


Gdrive Setup (Uploading log file google drive)
----------------------------------------------

1. cd ~ wget https://docs.google.com/uc?id=0B3X9GlR6EmbnWksyTEtCM0VfaFE&export=download
2. mv uc\?id\=0B3X9GlR6EmbnWksyTEtCM0VfaFE gdrive
3. chmod +x gdrive
4. sudo install gdrive /usr/local/bin/gdrive
5. gdrive list
6. Access the link provided from point 5 in browser
7. Copy key from page accessed in browser
8. Paste key in terminal and press enter