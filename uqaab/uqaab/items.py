import scrapy


class Entity(scrapy.Item):
    sublist_ID = scrapy.Field()
    category = scrapy.Field()
    name = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    country = scrapy.Field()
    date_of_birth = scrapy.Field()
    nationality = scrapy.Field()
    designation = scrapy.Field()
    organization = scrapy.Field()
    title = scrapy.Field()
    inclusion_date = scrapy.Field()
    exclusion_date = scrapy.Field()
    aka = scrapy.Field()
    matching = scrapy.Field()
    type = scrapy.Field()
    remarks = scrapy.Field()
    Status = scrapy.Field()
    gender = scrapy.Field()
    last_occupation = scrapy.Field()
    document = scrapy.Field()
    image = scrapy.Field()

    
