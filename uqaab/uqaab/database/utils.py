import math
import json
from json.decoder import JSONDecodeError

def create_key_value_list(key, value):
    return [{key: item} for item in value]


def create_list_of_addresses(entity):
    addresses = _convert_to_list_if_string(entity.pop('address', None))
    cities = _convert_to_list_if_string(entity.pop('city', None))
    countries = _convert_to_list_if_string(entity.pop('country', None))
    return _create_list_of_addresses(addresses, cities, countries)


def _convert_to_list_if_string(value):
    if not isinstance(value, list):
        return [value]
    return value


def _create_list_of_addresses(addresses, cities, countries):
    address_list = _address_list_from_addresses(addresses, cities, countries)
    if len(address_list) == 0:
        address_list += _address_list_from_city_and_country(cities[0], countries[0])

    if len(address_list) == 0:
        return None
    return address_list


def _address_list_from_addresses(addresses, cities, countries):
    address_list = []
    for idx, address in enumerate(addresses):
        if address:
            try:
                address_list.append({
                    'address': _decode(address),
                    'city': _decode(cities[idx]),
                    'country': _decode(countries[idx])
                })
            except IndexError:
                address_list.append({
                    'address': _decode(address),
                    'city': _decode(cities[0]),
                    'country': _decode(countries[0])
                })
    return address_list


def _address_list_from_city_and_country(city, country):
    address_list = []
    if city or country:
        address_list.append({
            'address': None,
            'city': _decode(city),
            'country': _decode(country)
        })
    return address_list


def _decode(string):
    try:
        return string.decode('utf-8')
    except AttributeError:
        return string

def _replace_nan(item):
    try:
        if item == 'nan' or math.isnan(item):
            return None
    except TypeError:
        pass
    return item

def _remove_nan(dobs):
    date_list = [{'DOB': _replace_nan(item['DOB']), 'POB': _replace_nan(item['POB'])} for item in dobs]
    return date_list

def _remove_unicode(text):
    try:
        return (text.encode('ascii', 'ignore')).decode('utf-8')
    except AttributeError:
        return text

def _jsonify_list(input_list, key):
    _list = None
    if input_list:
        _list = input_list

    json_string = json.dumps({key: _list})
    return json_string
    
def clean_dob(dob_record):
    if dob_record is None or dob_record.strip() == '':
        new_list = None
    else:
        try:
            dob_obj = json.loads(dob_record)
            if isinstance(dob_obj, dict):
                if dob_obj['info'] is None or dob_obj['info'] == []:
                    new_list = None
                else:
                    return dob_record
            else:
                new_list = _remove_nan(dob_obj)
        except JSONDecodeError:
            new_list = [{'DOB': dob_record, 'POB': None}]

    return _jsonify_list(new_list, 'info')

def clean_name(name_record):
    if name_record:
        try:
            name_dict = json.loads(name_record)
            if isinstance(name_dict, dict):
                name = ' '.join([_remove_unicode(item['name']) for item in name_dict['info']])
                return name
            elif isinstance(name_dict, str):
                name = _remove_unicode(name_dict)
                return name
        except JSONDecodeError:
            unicode_removed_name = _remove_unicode(name_record)
            if unicode_removed_name != name_record:
                return unicode_removed_name
            return name_record
    else:
        raise ValueError('Name cannot be empty')

def clean_gender(gender_record):
    if gender_record is None or gender_record == 'Male' or gender_record == 'Female':
        return gender_record
    elif gender_record.strip() == '':
        return None
    elif gender_record.strip().upper().startswith('F'):
        return 'Female'
    elif gender_record.strip().upper().startswith('M'):
        return 'Male'

def clean_record(record, key):
    if record is None or record.strip() == '':
        new_list = None
    else:
        try:
            _obj = json.loads(record)
            if isinstance(_obj, dict):
                if _obj['info'] is None or _obj['info'] == []:
                    new_list = None
                else:
                    unicode_removed = [{key: _remove_unicode(item[key])} for item in _obj['info'] if item[key]]
                    if unicode_removed != _obj['info']:
                        new_list = unicode_removed
                    else:
                        return record
            elif isinstance(_obj, str):
                new_list = [{key: _remove_unicode(_obj)}]
        except JSONDecodeError:
            new_list = [{key: _remove_unicode(record)}]

    return _jsonify_list(new_list, 'info')

def clean_aka(record):
    try:
        return clean_record(record, 'aka')
    except KeyError:
        _obj = json.loads(record)
        new_list = [{'aka': _remove_unicode(item['alias_name'])} for item in _obj['info'] if item['alias_name']]
    except AttributeError:
        _obj = json.loads(record)
        new_list = [{'aka': _remove_unicode(item['aka']['aka'])} for item in _obj['info'] if item['aka']['aka']]

    return _jsonify_list(new_list, 'info')

def clean_documents(record):
    if record is None or record.strip() == '':
        new_list = None
    else:
        _obj = json.loads(record)
        if isinstance(_obj, dict):
            if _obj['document_info'] is None or _obj['document_info'] == []:
                new_list = None
            else:
                try:
                    unicode_removed = [{'type': _remove_unicode(item['type']),
                                        'id': _remove_unicode(item['id'])} for item in _obj['document_info']]
                except KeyError:
                    unicode_removed = [{'type': _remove_unicode(item['type']),
                                        'id': _remove_unicode(item['value'])} for item in _obj['document_info']]
                if unicode_removed != _obj['document_info']:
                    new_list = unicode_removed
                else:
                    return record
        elif isinstance(_obj, list):
            if _obj == []:
                new_list = None
            else:
                try:
                    new_list = [{'type': _remove_unicode(item['type']),
                                'id': _remove_unicode(item['id'])} for item in _obj]
                except KeyError:
                    new_list = [{'type': _remove_unicode(item['type']),
                                'id': _remove_unicode(item['value'])} for item in _obj]

    return _jsonify_list(new_list, 'document_info')

def clean_address(address):        
    if address is None or address.strip() == '':
        new_list = None
    else:
        _obj = json.loads(address)
        if isinstance(_obj, dict):
            if _obj['address info'] is None or _obj['address info'] == []:
                new_list = None
            else:
                cleaned = [{'address': _replace_nan(_remove_unicode(item['address'])),
                                    'city': _replace_nan(_remove_unicode(item['city'])),
                                    'country': _replace_nan(_remove_unicode(item['country']))} 
                                    for item in _obj['address info']]
                if cleaned != _obj['address info']:
                    new_list = cleaned
                else:
                    return address

    return _jsonify_list(new_list, 'address info')