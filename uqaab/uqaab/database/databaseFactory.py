from scrapy.utils.project import get_project_settings
from uqaab.database.mssql import MSSQL
from uqaab.database.mongo import Mongo


def get_enabled_database():
    settings = get_project_settings()
    enabled_db = settings.get('DATABASE_ENABLED', 'MSSQL')

    if enabled_db == 'MSSQL':
        return MSSQL()

    if enabled_db == 'MONGO':
        return Mongo()
