import pyodbc
from datetime import datetime
from uqaab.environment import config

CONNECTION_STRING = config('CONNECTION_STRING')

class MSSQL():
    def __init__(self):
        self.connection = pyodbc.connect(CONNECTION_STRING)
        self.cursor = self.connection.cursor()

    def close_connection(self):
        self.cursor.close()
        del self.cursor
        self.connection.close()

    def insert_entity(self, entity):
        query, values = self._create_entity_insert_query(entity)
        self._insert_into(query, values)

    def get_program(self, list_id):
        SELECT_QUERY = 'SELECT Program FROM dbo.tbl_watchlist WHERE ListID = {0}'.format(list_id)
        result = self.cursor.execute(SELECT_QUERY).fetchone()
        return result[0]

    def insert_new_sublist_record(self, list_id):
        query, values = self._create_sub_watch_list_query(list_id)
        self._insert_into(query, values)
        new_sublist_id = self._select_new_sublist(list_id)
        return new_sublist_id

    def get_sub_programs_list(self, list_id):
        return self._get_sub_programs(list_id)

    def insert_new_watchlist_programs(self, list_id, programs_names):
        self._insert_into_watchlist(list_id, programs_names)

    def get_static_data_count(self, list_id):
        sublist_id = self._get_subwatchlist_id(list_id)
        if sublist_id is not None:
            return self._get_static_data_count(sublist_id)
        return 0

    def copy_data(self, list_id, source, destination):
        sublist_id = self._get_subwatchlist_id(list_id)
        self._copy_data(sublist_id, source, destination)
        
    def _create_entity_insert_query(self, entity):
        item_to_db_map = {
            'sublist_ID': 'SubListID',
            'category': 'ECategory',
            'name': 'OrigName',
            'address': 'Address',
            'date_of_birth':'DOB',
            'nationality':'Nationality',
            'designation':'Designation',
            'organization': 'Organization',
            'title' : 'Title',
            'inclusion_date': 'DO_Inclusion',
            'exclusion_date': 'DO_Exclusion',
            'aka':'Aliases',
            'matching':'Matching',
            'type' :'ListType',
            'remarks':'Remarks',
            'Status':'Status',
            'gender' :'Gender',
            'last_occupation' :'Last_Occupation',
            'document':'Documents',
            'image':'Image'
        }
        columns_list = []
        values_list = []

        for key, value in entity.items():
            columns_list.append(item_to_db_map[key])
            values_list.append(value)

        question_marks_string='?,' * len(values_list)
        query = "INSERT INTO tbl_entities({col_names}) VALUES ({question_marks})".format(
            col_names=', '.join(columns_list),
            question_marks=question_marks_string[0:-1]
        )
        return query, values_list

    def _insert_into(self, query, values):
        args = self._concatenate_to_list(query, values)
        self.cursor.execute(*args)
        self.connection.commit()

    @staticmethod
    def _concatenate_to_list(query, values):
        return [query] + values

    def _create_sub_watch_list_query(self, list_id):
        SELECT_QUERY = "SELECT TOP 1 ListType, Program, PublisherName, DatasetName, DataFormat, Datalink, Priority, website, DataYear, Version, LastModified, CreatedDate, IsEnabled, ListID FROM tbl_subwatchlist WHERE ListID = {list_id} ORDER BY SubListID DESC;"
        INSERT_QUERY = "INSERT INTO tbl_subwatchlist (ListType, Program, PublisherName, DatasetName, DataFormat, Datalink, Priority, website, DataYear, Version, LastModified, CreatedDate, IsEnabled, ListID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
        VERSION_INDEX = 9
        MODIFIED_INDEX = 10
        ENABLED_INDEX = 12

        result = self.cursor.execute(SELECT_QUERY.format(list_id=list_id)).fetchone()
        try:
            values = list(result)
            values[VERSION_INDEX] = self._generate_new_version_string(values[VERSION_INDEX])
        except TypeError:
            values = self._get_values_from_watch_list(list_id)

        values[MODIFIED_INDEX] = datetime.now().strftime('%b %d %Y %I:%M%p')
        values[ENABLED_INDEX] = False
        return INSERT_QUERY, values

    @staticmethod
    def _generate_new_version_string(old_version_string):
        old_version = old_version_string.replace('v', '')
        new_version = int(old_version) + 1
        return 'v{0}'.format(new_version)

    def _get_values_from_watch_list(self, list_id):
        SELECT_QUERY = "SELECT ListType, Program, PublisherName, DatasetName, DataFormat, Datalink, 1 AS Priority, Datalink AS website, Year, Version, LastModified, CreatedDate, IsEnabled, ListID FROM tbl_watchlist WHERE ListID = {list_id};"
        result = self.cursor.execute(SELECT_QUERY.format(list_id=list_id)).fetchone()
        return list(result)

    def _select_new_sublist(self, list_id):
        SELECT_QUERY = "SELECT TOP 1 SubListID FROM tbl_subwatchlist WHERE ListID = {list_id} ORDER BY SubListID DESC;"
        result = self.cursor.execute(SELECT_QUERY.format(list_id=list_id)).fetchone()
        return result[0]

    def _get_sub_programs(self, list_id):
        SELECT_QUERY = """SELECT SUBSTRING(Program, CHARINDEX('/', Program)+1, LEN(Program)) AS program FROM tbl_watchlist where program like 
                (SELECT SUBSTRING(Program, 1, CHARINDEX('/', Program)) AS program FROM tbl_watchlist where listid = ?) +'%'"""
        results = [r[0] for r in self.cursor.execute(SELECT_QUERY, (list_id)).fetchall()]
        return results

    def _insert_into_watchlist(self, list_id, programs):
        PROGRAM_INDEX = 1
        MODIFIED_INDEX = 9
        CREATED_INDEX = 10
        SELECT_QUERY = """SELECT ListType, Program, PublisherName, DatasetName, DataFormat, FileName, Datalink, Year, Version, LastModified, CreatedDate, IsEnabled FROM tbl_watchlist WHERE ListID = ?"""
        INSERT_QUERY = """INSERT INTO tbl_watchlist (ListType, Program, PublisherName, DatasetName, DataFormat, FileName, Datalink, Year, Version, LastModified, CreatedDate, IsEnabled) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"""
        values = list(self.cursor.execute(SELECT_QUERY, (list_id)).fetchone())
        values[MODIFIED_INDEX] = datetime.now().strftime('%b %d %Y %I:%M%p')
        values[CREATED_INDEX] = datetime.now()
        for program_name in set(programs):
            values[PROGRAM_INDEX] = program_name
            self._insert_into(INSERT_QUERY, values)

    def _get_subwatchlist_id(self, list_id):
        SELECT_QUERY = "SELECT TOP 1 SubListID FROM tbl_subwatchlist WHERE ListID = ? ORDER BY SubListID DESC;"
        result = self.cursor.execute(SELECT_QUERY, (list_id)).fetchone()
        try:
            return result[0]
        except TypeError:
            return None

    def _get_static_data_count(self, sublist_id):
        COUNT_QUERY = 'SELECT COUNT(*) from tbl_staticData WHERE sublistid=?'
        result = self.cursor.execute(COUNT_QUERY, (sublist_id)).fetchone()
        return result[0]

    def _copy_data(self, sublist_id, source, destination):
        COPY_QUERY = """INSERT INTO {0} (EDateTime, SubListID, ECategory, OrigName, Address, DOB, Nationality, Designation, Organization, Title, DO_Inclusion, DO_Exclusion, Aliases, Matching, ListType, Remarks, Status, Gender, Last_Occupation, Documents, Initials, Name, Image)
                        SELECT EDateTime, SubListID, ECategory, OrigName, Address, DOB, Nationality, Designation, Organization, Title, DO_Inclusion, DO_Exclusion, Aliases, Matching, ListType, Remarks, Status, Gender, Last_Occupation, Documents, Initials, Name, Image FROM {1}
                        WHERE SublistID = ?;"""
        self._insert_into(COPY_QUERY.format(destination, source), [sublist_id])

    def populate_name_and_initials(self):
        self.cursor.execute("exec cleanNameInsertInitials")
        self.cursor.commit()