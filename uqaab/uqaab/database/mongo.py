from datetime import datetime
import pymongo
from uqaab.environment import config
from .utils import create_list_of_addresses

MONGO_CONNECTION_STRING = config('MONGO_CONNECTION_STRING')


class Mongo():
    def __init__(self):
        self.client = pymongo.MongoClient(MONGO_CONNECTION_STRING)
        self.db = self.client[config('MONGO_DATABASE_NAME')]

    def close_connection(self):
        self.client.close()

    def insert_entity(self, entity):
        entity_dict = self._create_entity_record(entity)
        self.db['sanctions_list'].insert_one(entity_dict)

    def _create_entity_record(self, entity):
        record = {
            'category': entity.pop('category', 'Unknown'),
            'name': entity.pop('name'),
            'addresses': create_list_of_addresses(entity),
            'date': datetime.today(),
            'list_name': entity.pop('list_name', None),
            'is_latest': True,
            'last_updated': datetime.today(),
        }
        record['custom'] = self._create_custom_fields_dict(entity)
        return record
        
    def _create_custom_fields_dict(self, entity):
        return {key: value for key, value in entity.items()}
        