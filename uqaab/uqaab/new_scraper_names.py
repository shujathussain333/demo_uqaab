import re
import pyodbc
from os import listdir
from os.path import abspath, dirname, isfile, join

from environment import config

CONNECTION_STRING = config('CONNECTION_STRING')

connection = pyodbc.connect(CONNECTION_STRING)
cursor = connection.cursor()

def get_watchlist_entries():
    watchlist_query = "Select FileName from tbl_watchlist;"
    results = cursor.execute(watchlist_query).fetchall()
    return set([r[0] for r in results])

def print_new_spider_names():
    existing_spiders = get_watchlist_entries()
    SPIDER_DIR = join(dirname(abspath(__file__)), 'spiders')
    
    new_scrapers = []
    for _file in listdir(SPIDER_DIR):
        if isfile(join(SPIDER_DIR, _file)) and _file != '__init__.py':
            with open(join(SPIDER_DIR, _file)) as scrapy_file:
                print('Opened: {}'.format(_file))
                data = scrapy_file.read()
                spider_name = re.search(r"name\s*=\s*['\"]([\w-]+)['\"]\s*", data).group(1)
                if spider_name not in existing_spiders:
                    new_scrapers.append('{0} -- {1}'.format(spider_name, _file))

    print('************************************')
    for idx, spider in enumerate(new_scrapers):
        print('{} {}'.format(idx, spider))
    print('************************************')

if __name__ == '__main__':
    print_new_spider_names()