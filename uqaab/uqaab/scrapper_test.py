import subprocess
import pyodbc
from .environment import config
import sys
import json
import math
import csv
import traceback
from .loggers import create_logging_file
import os
from uqaab.spiders.utils.emails import send_list_error_mail
from datetime import datetime, timedelta
import glob

CONNECTION_STRING = config('CONNECTION_STRING')
logging = create_logging_file()

class ScrapperTest():
    PROJECT_NAME = 'uqaab'
    PROJECT_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'uqaab')

    def __init__(self, list_id=None, spider_name=None):
        self.connection = pyodbc.connect(CONNECTION_STRING)
        self.cursor = self.connection.cursor()
        self.list_id = list_id
        self.spider_name = spider_name

    def scrapper_tester(self):

        scrapy_command = 'scrapy crawl {spider_name} -a list_id={list_id}'.format(spider_name=self.spider_name,
                                                                                  list_id=self.list_id)
        p = subprocess.Popen(scrapy_command, shell=True, stdout=subprocess.PIPE, cwd=self.PROJECT_DIR)
        for line in p.stdout:
            logging.info(line)
        p.wait()

        entities = self.check_entities_validity()
        logging.info(entities[0])

        keys = entities[0].keys()
        with open(self.spider_name + '.csv', 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(entities)

    def scrapper_list_tester(self):
        self.cursor.execute(
            'SELECT distinct SubListID, Program FROM tbl_subwatchlist where LastModified >= cast(floor(cast(dateadd(day, -1, getdate()) as float)) as datetime)')
        rows = self.cursor.fetchall()
        for row in rows:
            logging.info("sending sub list id and Program {} {}".format(row[0], row[1]))
            self.check_entities_validity(row[0], row[1])

    def check_entities_validity(self, sublistid=None, program=None, started_on=None, list_id=None):
        entities = []
        rows = []
        failed_data = []
        self.list_id = list_id

        if sublistid is None:
            SELECT_QUERY = 'SELECT top 1 SubListID, Program FROM dbo.tbl_subwatchlist WHERE ListID = {0} order by SubListID desc'.format(
                self.list_id)
            result = self.cursor.execute(SELECT_QUERY).fetchone()
            sublistid = result[0]
            program = result[1]

        self.cursor.execute("""select
                            [OrigName],
                            [ECategory] as 'Category',
                            [Title] ,
                            [Nationality],
                            [Gender],
                            [Organization],
                            [Designation],
                            [DO_Inclusion],
                            [DO_Exclusion],
                            [Documents],
                            [Aliases],
                            [DOB],
                            [Address],
                            [Remarks]
                            from
                            tbl_entities
                    where SubListID = {sublistid}""".format(sublistid=sublistid))
        rows = self.cursor.fetchall()

        for row in rows:
            try:
                taliases = row.Aliases
                titles = row.Title
                designations = row.Designation
                documents = row.Documents
                aka = None
                title = None
                designation = None
                passport = ''
                national_identification = ''

                if titles is not None and titles != '':
                    titles_dict = json.loads(titles)
                    if titles_dict['info'] is not None:
                        try:
                            title = ' | '.join([title['title'] for title in titles_dict['info']])
                        except TypeError:
                            title = ' | '.join([title['title']['title'] for title in titles_dict['info']])
                        except KeyError:
                            title = ' | '.join([title['title_name'] for title in titles_dict['info']])

                if designations is not None and designations != '':
                    designations_dict = json.loads(designations)
                    if designations_dict['info'] is not None:
                        try:
                            designation = ' | '.join(
                                [designation['designation'] for designation in designations_dict['info']])
                        except TypeError:
                            designation = ' | '.join(
                                [designation['designation']['designation'] for designation in
                                 designations_dict['info']])
                        except KeyError:
                            designation = ' | '.join(
                                [designation['designation_name'] for designation in designations_dict['info']])

                if documents is not None and documents != '':
                    documents_dict = json.loads(documents)
                    if documents_dict['document_info'] is not None:
                        try:
                            for document in documents_dict['document_info']:
                                if 'passport' in document["type"].lower() and len(document["id"]) > 4:
                                    passport = passport + document["id"] + ' | '
                                if 'national' in document["type"].lower() and len(document["id"]) > 4:
                                    national_identification = national_identification + document["id"] + ' | '
                        except (TypeError, KeyError):
                            pass
                        else:
                            passport = passport.strip()[:-1]
                            national_identification = national_identification.strip()[:-1]

                            if len(passport) <= 0:
                                passport = None
                            if len(national_identification) <= 0:
                                national_identification = None

                if taliases is not None and taliases != '':
                    try:
                        aliases_dict = json.loads(taliases)
                        if aliases_dict['info'] is not None:
                            try:
                                aka = ' | '.join([alias['aka'] for alias in aliases_dict['info']])
                            except TypeError:
                                aka = ' | '.join([alias['aka']['aka'] for alias in aliases_dict['info']])
                            except KeyError:
                                aka = ' | '.join([alias['alias_name'] for alias in aliases_dict['info']])
                    except ValueError:
                        aka = taliases

                tdob = row.DOB
                dob = []
                pob = []
                if tdob is not None and tdob != '':
                    try:
                        dob_list = json.loads(tdob)
                        if isinstance(dob_list, dict):
                            if dob_list["info"] is not None:
                                for item in dob_list["info"]:
                                    try:
                                        tdob = item['DOB']
                                    except KeyError:
                                        tdob = item['date_of_birth']['Date of Birth']
                                    if tdob is not None and tdob.lower() != 'nan':
                                        dob.append(str(tdob))
                                    try:
                                        tpob = item['POB']
                                    except KeyError:
                                        tpob = item['date_of_birth']['Place of Birth']
                                    if tpob is not None and tpob != math.isnan(float('NaN')):
                                        pob.append(str(tpob))
                        else:
                            for item in dob_list:
                                tdob = item['DOB']
                                if tdob is not None and tdob.lower() != 'nan':
                                    dob.append(str(tdob))
                                tpob = item['POB']
                                if tpob is not None and tpob != math.isnan(float('NaN')) and str(tpob).lower() != 'nan':
                                    pob.append(str(tpob))
                    except ValueError:
                        dob.append(tdob)

                dob = ' | '.join(dob)
                pob = ' | '.join(pob)

                taddress = row.Address
                addressList = None
                addresses = []
                if taddress is not None and taddress != '':
                    try:
                        address_dict = json.loads(taddress)
                        if address_dict["address info"] is not None:
                            for item in address_dict["address info"]:
                                address_list = []
                                if item['address'] is not None:
                                    address_list.append(item['address'])
                                if item['city'] is not None:
                                    address_list.append(item['city'])
                                if item['country'] is not None:
                                    address_list.append(item['country'])
                                addresses.append(','.join(address_list))
                            addressList = ' | '.join(addresses)
                    except ValueError:
                        addressList = taddress

                entities.append({
                    'Name': row.OrigName,
                    'Organization': row.Organization,
                    'Category': row.Category,
                    'Title': title,
                    'Nationality': row.Nationality,
                    'Gender': row.Gender,
                    'Aliases': aka,
                    'DOB': dob,
                    'POB': pob,
                    'Address': addressList,
                    'Passport': passport,
                    'NationalIdentityNo': national_identification,
                    'Designation': designation,
                    'DOInclusion': row.DO_Inclusion,
                    'DOExlusion': row.DO_Exclusion,
                    'Remarks': row.Remarks
                })
            except Exception as e:
                error_details = traceback.format_exc()
                failed_data.append('{} found'.format(error_details))

        logging.info("total rows from query {}".format(len(rows)))
        logging.info("total rows of entities {}".format(len(entities)))

        self.create_or_update_log_file(program, sublistid, started_on, rows)

        if len(rows) == 0:
            subject = 'Scrapper {} 0 record on sublist {}'.format(program, sublistid)
            message = "No entities fetched while running download tests. Disabling this list. Check and re-enable the list manually."
            logging.info("No entities fetched, scrapper did not run successfully")
            self._disable_scrapper(sublistid)
            send_list_error_mail(subject, message, failed_data)
        elif len(entities) < len(rows):
            subject = 'Scrapper {} not all record scrapped on sublist {}'.format(program, sublistid)
            message = "There were errors while running download tests. Disabling this list. Check and re-enable the list manually."
            logging.info("scrapper does not fetch proper data")
            self._disable_scrapper(sublistid)
            send_list_error_mail(subject, message, failed_data)
        else:
            logging.info("Scrapper ran successfully and fetched proper records")

        logging.info("Started On {}".format(started_on))
        logging.info("Ended On {}".format(datetime.now()))

        return entities

    def _disable_scrapper(self, sublistid):
        self.cursor.execute(
            """update tbl_subwatchlist set IsEnabled=0 where SubListID = {sublistid}""".format(sublistid=sublistid))
        self.cursor.commit()

    def create_or_update_log_file(self, program, sublistid, started_on, rows):
        logging.info("Started On {}".format(started_on))
        logging.info("Ended On {}".format(datetime.now()))

        loglist = []
        ended_on = datetime.now()
        dif = ended_on - started_on
        loglist.append(self.list_id)
        loglist.append(program)
        loglist.append(sublistid)
        loglist.append(started_on.strftime ('%H:%M:%S'))
        loglist.append(ended_on.strftime ('%H:%M:%S'))
        # -1 sec difference in csv result is due to microseconds in time
        loglist.append(timedelta(seconds=dif.seconds))
        loglist.append(len(rows))

        files_path = os.path.join(config('PROJECT_PATH'), 'timelogs', '*.csv')
        files = sorted(glob.iglob(files_path), key=os.path.getctime, reverse=True)
        filename = files[0]
        logging.info("updating log file")

        with open(filename, 'a') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(loglist)

if __name__ == "__main__":
    try:
        scrapper_test = ScrapperTest(int(sys.argv[1]), sys.argv[2])
        scrapper_test.scrapper_tester()
    except IndexError:
        scrapper_test = ScrapperTest()
        scrapper_test.scrapper_list_tester()
