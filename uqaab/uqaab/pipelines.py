import json
from uqaab.database import utils
from uqaab.database.databaseFactory import get_enabled_database
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
from datetime import datetime

class EntityPipeline():

    def open_spider(self, spider):
        self.db = get_enabled_database()
        spider.start_time = datetime.now()

    def close_spider(self, spider):
        try:
            if len(spider.new_programs) > 0:
                self.db.insert_new_watchlist_programs(spider.list_id, spider.new_programs)
        except AttributeError:
            pass

        if isinstance(spider, StaticDataSpider):
            if not spider.static_data_already_exists():
                self.db.copy_data(spider.list_id, 'tbl_entities', 'tbl_staticData')
        else:
            item_scraped_count = spider.crawler.stats.get_stats().get('item_scraped_count', 0)
            error_count = spider.crawler.stats.get_stats().get('log_count/ERROR', 0)
            if item_scraped_count == 0 and error_count == 0:
                self._send_zero_count_email(spider)
            else:
                spider.run_tests()
                spider.populate_name_and_initials()
                spider.change_detection()

        self.db.close_connection()

    def process_item(self, entity, spider):
        entity = self._convert_multi_valued_fields_to_lists(entity)
        entity = self._perform_pre_processing(entity)
        entity['sublist_ID'] = spider.sublist_id
        self.db.insert_entity(entity)
        return entity

    def _send_zero_count_email(self, spider):
        subject = "Scraper scraped 0 records"
        message = "Scraper executed successfully but no records were scraped."
        spider.warning_email_wrapper(subject, message)

    @staticmethod
    def _convert_multi_valued_fields_to_lists(entity):
        for key, value in entity.items():
            try:
                if '|' in value:
                    entity[key] = [item.strip() for item in value.split('|')]
            except TypeError:
                pass

        return entity

    def _perform_pre_processing(self, entity):
        entity = self._jsonify_fields(entity)
        entity['name'] = utils.clean_name(entity['name'])

        try:
            entity['date_of_birth'] = utils.clean_dob(entity['date_of_birth'])
        except KeyError:
            entity['date_of_birth'] = json.dumps({'info': None})

        try:
            entity['gender'] = utils.clean_gender(entity['gender'])
        except KeyError:
            pass

        try:
            entity['title'] = utils.clean_record(entity['title'], 'title')
        except KeyError:
            entity['title'] = json.dumps({'info': None})

        try:
            entity['designation'] = utils.clean_record(entity['designation'], 'designation')
        except KeyError:
            entity['designation'] = json.dumps({'info': None})

        try:
            entity['aka'] = utils.clean_aka(entity['aka'])
        except KeyError:
            entity['aka'] = json.dumps({'info': None})

        try:
            entity['document'] = utils.clean_documents(entity['document'])
        except KeyError:
            entity['document'] = json.dumps({'document_info': None})

        try:
            entity['address'] = utils.clean_address(entity['address'])
        except KeyError:
            entity['address'] = json.dumps({'address info': None})

        if 'image' not in entity or entity['image'] is None:
            entity['image'] = '/media/placeholder.jpg'

        return entity

    def _jsonify_fields(self, entity):
        entity['address'] = self._create_address_json(entity)
        for key, value in entity.items():
            if isinstance(value, list):
                entity[key] = self._create_json(key, value)

        return entity

    @staticmethod
    def _create_address_json(entity):
        address_list = utils.create_list_of_addresses(entity)
        if address_list:
            return json.dumps({'address info': address_list})
        else:
            return json.dumps({'address info': None})

    @staticmethod
    def _create_json(key, value):
        final_list = utils.create_key_value_list(key, value)
        if final_list:
            return json.dumps({'info': final_list})
        else:
            return json.dumps({'info': None})

