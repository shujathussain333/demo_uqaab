#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import dateutil.parser

class USA_Food_And_Drug_Administration(UqaabBaseSpider):
    name = 'usa_food_and_drug_administration'
    start_urls = ['https://www.fda.gov/files/api/datatables/static/warning-letters.json?_=1563361740721']

    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        jsonresponse=json.loads(jsonresponse)
        return len(jsonresponse) > 0

    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        jsonresponse=json.loads(jsonresponse)
        i = 0
        while i < len(jsonresponse):
            name = jsonresponse[i]['field_company_name_warning_lette'].split("\">")
            name = name[1].replace("</a>","")
            inclusionDate = jsonresponse[i]['field_letter_issue_datetime']
            inclusionDate = dateutil.parser.parse(inclusionDate)    
            exclusionDate = jsonresponse[i]['field_change_date_2']
            exclusionDate = dateutil.parser.parse(exclusionDate)
            issuingOffice = jsonresponse[i]['field_building']
            subject = jsonresponse[i]['field_detailed_description_2'].replace("<br />","")
            remarks = "Issuing Office: {} ,Subject: {}".format(issuingOffice,subject)
            i+=1
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "remarks": remarks,
                "inclusion_date": inclusionDate,
                "exclusion_date": exclusionDate
            })