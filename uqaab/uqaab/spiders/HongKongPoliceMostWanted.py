##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
from bs4 import BeautifulSoup
import re


class HongKongPoliceMostWanted(UqaabBaseSpider):
    name = 'hong_kong_police_most_wanted'
    start_urls = ['https://www.police.gov.hk/info/appeals_public/wanted_persons/w_en.xml']

    def structure_valid(self, response):
        soup = BeautifulSoup(response.body, "xml")
        persons = soup.findAll('case')
        return len(persons) > 0

    def extact_data(self, response):
        soup = BeautifulSoup(response.body, "xml")
        persons = soup.findAll('case')

        for person in persons:
            other = person.find('other').text
            image = person.find('img')
            cleanr = re.compile('<.*?>')
            image = re.sub(cleanr, '', str(image))
            other_info = other.split('<br/>')
            image = 'https://www.police.gov.hk' + image
            indices = [i for i, s in enumerate(other_info) if 'Wanted Person' in s]

            remarks = ''.join(map(str, other_info))
            cleanr = re.compile('<.*?>')
            remarks = re.sub(cleanr, '', remarks)

            for indice in indices:
                document = []
                person_detail = other_info[indice]
                aka = []
                person_detail = person_detail.split(',')
                if "male" in person_detail[0].lower():
                    name = person_detail[0].lower().split('male')[-1]
                else:
                    name = person_detail[0].lower().split(':')[-1]

                identity_indice = [i for i, s in enumerate(person_detail) if 'Identity' in s]
                identity = person_detail[identity_indice[0]]
                identity = re.split("[.:]+", identity)[1]
                document.append({'type': "national identity no", 'id': identity})
                document_info = json.dumps({'document_info': document})
                alias_indice = [i for i, s in enumerate(person_detail) if 'alias' in s]
                alias = person_detail[alias_indice[0]]
                alias = alias.split(" ", 2)[-1]
                aka.append({
                    'aka': alias,
                })
                alias_info = json.dumps({'info': aka})

                yield Entity({
                    "category": "Individual",
                    "name": name.title(),
                    "document": document_info,
                    "remarks": remarks,
                    "type": "SIP",
                    "aka": alias_info,
                    "image":image
                })
