#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class ICACJudgmentsandRulings(UqaabBaseSpider):

    name = 'icac_judgments_and_rulings'
    start_urls = ['https://www.icac.mu/judgments-and-rulings/',]

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='fusion-text']//a[text()!='ICAC' and text()!=' ']")
        return len(data_rows) > 0

    def extact_data(self,response):
        cases = response.xpath("//div[@class='fusion-text']//a[text()!='ICAC' and text()!=' ']")
        for case in cases:
            name = case.xpath(".//text()").extract_first()
            if name:
                name = name.strip()
            if "ICAC v" in name:
                name = name.split("v")[1].strip()
            elif "v ICAC" in name:
                name = name.split("v")[0].strip()
            elif "v" in name:
                name = name.replace("v","").strip()    
            remarks = case.xpath(".//@href").extract_first()
            remarks = "case_details: {}".format(remarks)            
            yield Entity({
                "category": "Individual",
                "name":name,
        		"remarks":remarks,	
        		"type": "SIP"
        	}) 