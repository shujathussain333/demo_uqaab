#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Gibraltar_Financial_Services(UqaabBaseSpider):
    name = "gibraltar_financial_services"
    start_urls = ['https://www.fsc.gi/news?category=1&page=1']
    
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
    }

    def structure_valid(self, response):
        data_rows = response.xpath('//ul[@class="news-list"]//li')
        return len(data_rows) > 0

    def extact_data(self, response):
    	baseUrl = "https://www.fsc.gi"
    	nextDataRowUrl = response.xpath('//div[@class="col-xs-12"]//ul[@class="news-list"]//li')
    	for data in nextDataRowUrl:
    		url = baseUrl + data.xpath('./a/@href').extract_first()
    		if url is not None:
    			yield scrapy.Request(url, callback=self.extract_link)
    	baseUrlForNextPage = "https://www.fsc.gi/news"
    	nextPageUrl =  response.xpath('//div[@class="col-xs-12 col-sm-12 col-md-6 padded text-center paginator"]//li[@class="next"]/a/@href').extract_first()
    	if nextPageUrl is not None:
    		nextPageUrl = baseUrlForNextPage + nextPageUrl
    		yield scrapy.Request(nextPageUrl, callback=self.extact_data)

    def extract_link(self, response):
    	name = response.xpath('//div[@class="col-xs-12"]//h2//text()').extract_first()
    	name = name.strip()
    	remarks = response.xpath('//div[@class="col-xs-12 col-md-12"]//text()').extract()
    	if(len(remarks) == 0):
    		remarks = response.xpath('//div[@class="col-xs-12"]//p//text()').extract()
    	remarks = ' '.join(remarks)
    	remarks = ''.join(remarks.splitlines())
    	remarks = ' '.join(remarks.split())
    	remarks = "Summary: {}".format(remarks)
    	yield Entity({
	        "name": name,
	        "category": "Group",
	        "type": "SAN",
	        "remarks": remarks
		})