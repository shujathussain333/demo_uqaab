# Author =  Barkat Khan

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 
from bs4 import BeautifulSoup

class UnauthorisedFirms_CentralBank(UqaabBaseSpider):

    name = 'unauthorisedfirms_centralbank'
    start_urls = ['https://www.centralbank.ie/regulation/how-we-regulate/authorisation/unauthorised-firms/search-unauthorised-firms']

    def structure_valid(self, response):

        soup = BeautifulSoup(response.text,'html.parser')
        mainclass = soup.find_all('script')

        for i in range(len(mainclass)):
            if len(mainclass[i].contents) > 0:
                if "unauthorisedFirmsTableContent" in mainclass[i].contents[0]:
                    self.classnum = i
                    break
                    
        return len(mainclass[self.classnum].contents[0])  

    def extact_data(self, response):
        soup = BeautifulSoup(response.text , 'html.parser')
        mainclass = soup.find_all('script')

        subclass = (mainclass[self.classnum].contents[0]).split('var unauthorisedFirmsTableContent = ',1)[-1].rsplit('var loadAngular = true;', 1)[0].strip()
        subclass = subclass.replace("[" , "").replace("]" , "").replace(";" , "").replace("\n" , "").replace("\r" , "")
        subclasslist = subclass.split("},{")
        
        for field in subclasslist:
            field = field.replace("{" , "").replace("}" , "")
            field = "{" + field + "}"
            
            try:
                data = json.loads(field)   
            except:
                data = ""
                continue
        
            if data:
                url = data["url"]
                if url:
                    if "http" in url:
                        remarksfield =  "Url: {0}, Date_Of_Notice: {1}".format(data["url"],data["warningDate"])
                    else:
                        remarksfield =  "Url: https://www.centralbank.ie{0}, Date_Of_Notice: {1}".format(data["url"],data["warningDate"])
                else:
                    remarksfield =  "Date_Of_Notice: {0}".format(data["warningDate"])

                
                country_list = data["country"].split(",")
                country_codes = []
                
                for country in country_list:
                    code = self.get_country_code(country.strip())
                    if code == None:
                        country_codes.append(country.strip())
                    else:
                        country_codes.append(code)

                if len(country_codes) == 1:
                    country_codes = country_codes[0]

                yield Entity({
                    'name': data["firmName"].strip(),
                    'country':country_codes,
                    'remarks': remarksfield,
                    'category': 'Group'
                })
  
                
   

  
