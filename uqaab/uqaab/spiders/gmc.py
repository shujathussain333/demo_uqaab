from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
from bs4 import BeautifulSoup

class GMC_Scrapper(UqaabBaseSpider):
    name = 'gmc_scrapper'
    start_urls = ['https://www.gmp.police.uk/news/news-search?ct=Wanted']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www.gmp.police.uk/news/news-search/?q=&ct=Wanted&fdte=01%2F06%2F2017&tdte=27%2F06%2F2019'
        response = requests.get(url)
        soup = BeautifulSoup(response.text)
        records = soup.find_all('div', {'class': 'c-news-results'})
        doinclusion = [record.span.text for record in records]
        names = [record.h3.text.strip().split('-')[0] for record in records]
        remarks = [record.find('p', {'class': 'c-news-results_text'}).text.strip() for record in records]
        gmp_df = pd.DataFrame({'Name': names, 'DOInclusion': doinclusion, 'Remarks': remarks})

        for index, row in gmp_df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'inclusion_date': row.DOInclusion,
                'category': 'Group',
                'type': "SIP"
            })