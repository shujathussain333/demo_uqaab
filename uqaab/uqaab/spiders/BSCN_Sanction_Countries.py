# -*- coding: utf-8 -*-
from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class BscnSanctionCountriesSpider(UqaabBaseSpider):
    name = 'BSCN-Sanction-Countries'
    # allowed_domains = ['www.bscn.nl/sanctions-consulting/sanctions-list-countries']
    start_urls = ['http://www.bscn.nl/sanctions-consulting/sanctions-list-countries/']

    def structure_valid(self, response):
        data_rows = response.css('table')[0].css('tr')[1:]
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table')[0].css('tr')[1:]
        for row in data_rows:
            columns = row.css('td')

            yield Entity({
                'name': columns[0].css('::text')[0].extract().strip(),
                'category' : 'Country',
                'country': [self.get_country_code(columns[0].css('::text')[0].extract().strip())],
                'remarks': 'EU Sanctions :'+ str(columns[1].css('::text').extract())+ ' US Sanction :' +str(columns[2].css('::text').extract()),
            })


 