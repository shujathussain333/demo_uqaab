#Edited by: Daniyal Faquih
# -*- coding: utf-8 -*-

import tabula
import re
import json
import datetime
import numpy as np
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class FIA_Most_Wanted(UqaabBaseSpider):
    name = 'fia_terrorist_spider'
    start_urls = ["http://www.fia.gov.pk/en/terrorist.pdf",]
    split_words = ['r/o', 's/o', 'd/o', 'w/o']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):

        url = "http://www.fia.gov.pk/en/terrorist.pdf"

        fia = tabula.read_pdf(url, encoding="latin1", lattice=True, multiple_tables=True,pages="all")

        total_pages = len(fia)

        page = 21

        while page < total_pages:
            case = fia[page]
            case = case.dropna(axis=[0,1], how="all")
            case = case.T
            cols = list(case.iloc[0])
            cols = [str(col).replace(" ","").replace(":","") for col in cols]
            case.columns = cols
            case = case.iloc[1:]   
            check = list(case.columns)
            
            if page==486:
                d = pd.DataFrame()
                d = d.append([fia[pnum] for pnum in range(486,501)]).reset_index()
                cols = list(d.iloc[0])
                cols = [str(col).replace(" ","").replace(":","") for col in cols]
                d.columns = cols
                d = d.iloc[1:]
                d = d.replace({"\r":" "}, regex=True)
                d = d.reset_index(drop=True)
                
                for index,row in d.iterrows():
                    if row["S.#"] is np.nan:

                        d.at[index-1,'NAMEANDADDRESS'] = str(d.iloc[index-1]['NAMEANDADDRESS']) + " " + str(d.iloc[index]['NAMEANDADDRESS'])
                        d.at[index-1,'FIR'] = str(d.iloc[index-1]['FIR']) + " " +  str(d.iloc[index]['FIR'])

                for index, row in d.iterrows():
                    if row["S.#"] is not np.nan:
                        name = self.extract_name(row["NAMEANDADDRESS"])
                        details = re.split(r'{}'.format('|'.join(self.split_words)), name)
                        name = details[0]
                        address = [details[-1]]
                        remarks = self.extract_remarks(row["REWARD\r(RS)"] + "|" + "FIR: " +  row["FIR"])

                        yield Entity({
                        "name" : name,
                        "remarks": remarks.replace('|', ' '),
                        "category": CATEGORY_OPTIONS[0],
                        "type" : "SIP",
                        "address": address
                    })
                        
                page = page + 14

                
                
            
            else:
                if 'Anyotheruseful\rinformation' not in check and "Anyotherinformation" not in check and 'nyotherinformation' not in check:
                    test2 = fia[page+1]
                    test2 = test2.dropna(axis=[0,1], how="all")
                    if test2.shape[0] != 0:
                        test2 = test2.T
                        cols = list(test2.iloc[0])
                        cols = [str(col).replace(" ","").replace(":","") for col in cols]
                        test2.columns = cols
                        test2 = test2.iloc[1:]
                        case = case.reset_index(drop=True)
                        test2 = test2.reset_index(drop=True)
                        case = pd.concat([case, test2], axis=1)
                        case = case.reset_index(drop=True)
                        page = page + 1
                    else:
                        page = page + 1

                check = list(case.columns)
                if 'Anyotheruseful\rinformation' not in check and "Anyotherinformation" not in check and 'nyotherinformation' not in check:
                    test3 = fia[page+1]
                    test3 = test3.dropna(axis=[0,1], how="all")
                    test3 = test3.T
                    cols = list(test3.iloc[0])
                    cols = [str(col).replace(" ","").replace(":","") for col in cols]
                    test3.columns = cols
                    test3 = test3.iloc[1:]
                    case = case.reset_index(drop=True)
                    test3 = test3.reset_index(drop=True)
                    case = pd.concat([case, test3], axis=1)
                    case = case.reset_index(drop=True)
                    page = page + 1


                remarks2 = set(case.columns) - set(["Name&Parentage", "Address", "Religious&Political\rAffiliation"])
                remarks2 = list(remarks2)

                r = ''
                for rem in remarks2:
                    r = r + " " + str(rem) + ": " + "|" + str(case[rem].iloc[0]).replace("\r"," ")

                dob_info = {}    
                document = {}

                pob = r.find("PlaceofBirth")
                dob = r.find("Date of Birth")
                cnic = r.find("CNIC")

                
                if cnic != -1:
                    document["type"] = "CNIC"
                    value = r[cnic:]
                    p = re.compile(r"[a-zA-Z]+")
                    value = value.strip().replace(" ", "").replace(".","")
                    alp = p.findall(value)
                    for a in alp:
                        value = value.replace(a,"")
                    end = value.find(":")
                    value = value[:end]
                    document["id"] = value
                else:
                    document = None

                if document is not None:
                    document = json.dumps({'document_info': [document]})
                else:
                    document = json.dumps({'document_info': None})

                if pob != -1:
                    comma = r[pob:cnic].find(",") 
                    if comma != -1:
                        comma = comma + pob
                        place = r[pob+12:comma]
                        dob_info["POB"] = place
                    else:
                        fstop = r[pob:cnic].find('.')
                        fstop = fstop + pob
                        place = r[pob+12:fstop]
                        dob_info["POB"] = place
                else:
                    dob_info["POB"] = None
            
                if dob != -1:
                    comma = r[dob:cnic].find(",")
                    if comma != -1:
                        comma = comma + dob
                        date = r[dob+13:comma]
                        date = date.strip().replace(".","").replace(" ","")
                        if len(date) == 4:
                            date =  date + ' ' + '01' + ' ' + '01'
                            date = datetime.datetime.strptime(date, "%Y %m %d")
                        else:
                            date = datetime.datetime.strptime(date, "%d-%m-%Y")
                        dob_info["DOB"] = str(date)
                    else:
                        fstop = r[dob:cnic].find('.')
                        fstop = fstop + dob
                        date = r[dob+13:fstop]
                        date = date.strip().replace(".","").replace(" ","")
                        dob_info["DOB"] = str(date)
                    
                else:
                    dob_info["DOB"] = None
                
                if dob_info['DOB'] or dob_info['POB']:
                    DOB_complete = json.dumps({'info': [dob_info]})
                else:
                    DOB_complete = json.dumps({'info': None})

                if "Name&Parentage" in case.columns:
                    name = self.extract_name(case["Name&Parentage"].iloc[0].replace("\r"," "))
                else:
                    name = self.extract_name(case['ame&Parentage'].iloc[0].replace("\r"," "))

                address = self.extract_address(case["Address"].iloc[0].replace("\r"," "))

                if "Religious&Political\rAffiliation" in case.columns:
                    organization = self.extract_organization(case["Religious&Political\rAffiliation"].iloc[0].replace("\r"," "))

                elif "Religious&Political\rAffiliations" in case.columns:
                    organization = self.extract_organization(case["Religious&Political\rAffiliations"].iloc[0].replace("\r"," "))

                elif "Religious&Political" in case.columns:
                    organization = self.extract_organization(case["Religious&Political"].iloc[0].replace("\r"," "))

                elif 'Religious&Political\raffiliation' in case.columns:    
                    organization = self.extract_organization(case['Religious&Political\raffiliation'].iloc[0].replace("\r"," "))

                else:
                    organization = None

                page = page + 1
                try:
                    details = re.split(r'{}'.format('|'.join(self.split_words)), name)
                    name = details[0]
                except TypeError:
                    name = name

                if '@' in name:
                    name = name.split('@')[0]

                if 'Name&Parentage' in name:
                    name = str(name).split('Name&Parentage')[-1]
                    name = name.split('@')[0]
                    address = str(address).replace('Address', '').strip()
                    organization = str(organization)

                yield Entity({
                        "name" : name.strip(),
                        "category": CATEGORY_OPTIONS[0],
                        "organization":organization,
                        "address": [address],
                        "country": "PK",
                        "document" : document,
                        "date_of_birth": DOB_complete,
                        "type" : "SIP",
                        'image': '/media/images/FIAMostWanted/name.png'
                    })

    @staticmethod
    def extract_name(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

            
    @staticmethod
    def extract_remarks(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

    @staticmethod
    def extract_address(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

    @staticmethod
    def extract_organization(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None     





