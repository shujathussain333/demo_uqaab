# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class DeptForeignAffairsAutonomousSanctionsLibya(UqaabBaseSpider):
    name = 'dept_foreign_affairs_autonomous_sanctions_libya'
    start_urls = ['https://www.legislation.gov.au/Details/F2018L00101/Html/Text#_Toc504991628']

    def structure_valid(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        data_rows = data_rows[1:]
        datas = []
        for row in data_rows:
            columns = row.css('td')
            column_name = columns[1].css('p').css('*::text').extract_first()
            try:
                if "Individual" in column_name:
                    dictionary = {}
                    name_individual = columns[2].css('p').css('*::text').extract_first()
                    name_entity = None
                    category = "Individual"
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name_individual, "category": category, "address": address, 'dob': None,
                         'designation': None, 'remarks': '', "aka": None, "nationality": None})

                elif "Entity" in column_name:
                    dictionary = {}
                    name_entity = columns[2].css('p').css('*::text').extract_first()
                    category = "Group"
                    name_individual = None
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name_entity, "category": category, "address": address, 'dob': None,
                         'designation': None, 'remarks': '', "aka": None, "nationality": None})

                elif 'aka' in column_name:
                    value = columns[2].css('p').css('*::text').extract_first()
                    akas = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                                value) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

                    aka = [{'aka': aka} for aka in akas]
                    alias_info = json.dumps({'info': aka})
                    dictionary.update({"aka": alias_info})

                elif "Citizenship" in column_name:
                    nationality = columns[2].css('p').css('*::text').extract_first()
                    dictionary.update({"nationality": nationality})

                elif "Date" in column_name:
                    dob = columns[2].css('p').css('*::text').extract_first()
                    dobs = {'DOB': dob, 'POB': None}
                    dictionary.update({"dob": dobs})

                elif "Place" in column_name:
                    pob = columns[2].css('p').css('*::text').extract_first()
                    dictionary['dob']['POB'] = pob
                    dictionary.update({"dob": dobs})

                elif "Address" in column_name:
                    address = [columns[2].css('p').css('*::text').extract_first()]
                    dictionary.update({"address": address})
                    datas.append(dictionary)

                elif "Additional" in column_name:
                    # incase of entity in some details "Additional" and "Listing" are given, so removing dictionary from list
                    value = columns[2].css('p').css('*::text').extract_first()
                    if name_entity:
                        del datas[-1]
                        dictionary.update({"remarks": value})
                    elif name_individual:
                        designation = {"designation": value.strip()}
                        designation_info = json.dumps({'info': [designation]})
                        dictionary.update({"designation": designation_info})
                    datas.append(dictionary)

                elif "Listing" in column_name:
                    remarks = columns[2].css('p').css('*::text').extract_first()
                    del datas[-1]
                    dictionary["remarks"] = dictionary["remarks"] + '.' + remarks
                    dictionary.update({"remarks": remarks})
                    if dictionary['dob'] is not None:
                        dictionary.update({"dob": json.dumps({"info":[dictionary['dob']]})})
                    datas.append(dictionary)

            except (TypeError, AttributeError):
                continue

        for data in datas:
            yield Entity({
                "name": data['name'],
                "address": data["address"],
                "category": data["category"],
                "type": "SAN",
                "date_of_birth": data["dob"],
                "designation": str(data["designation"]),
                "nationality": data["nationality"],
                "aka": data["aka"],
            })
