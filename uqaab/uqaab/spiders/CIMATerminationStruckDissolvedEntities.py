# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import tabula
import pandas as pd
import numpy as np
import dateutil.parser


class CIMATerminationStruckDissolvedEntities(UqaabBaseSpider):
    name = 'cima_termination_struck_dissolved_entities'
    start_urls = ['https://www.cima.ky']

    def structure_valid(self, response):
        url = 'https://www.cima.ky/upimages/noticedoc/PublicNotice-TerminationofStruckorDissolvedInvestmentEntities_1521119176.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'https://www.cima.ky/upimages/noticedoc/PublicNotice-TerminationofStruckorDissolvedInvestmentEntities_1521119176.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)
        cols = ["No", "Name", "Empty1", "Empty2"]

        df = pd.DataFrame()
        for page in range(0, total_len):
            df = pd.concat([df, bin[page]])

        cols = [col.replace("\r", " ") for col in cols]

        df.columns = cols
        df = df.replace(np.nan, '', regex=True)
        df = df[df['Name'] != 'Name']
        df = df[df['Name'] != '']
        df = df[df['Name'] != 'Registration']
        df = df[df['Name'] != 'Number']

        for index, row in df.iterrows():
            yield Entity({
                'category': "Group",
                'name': row["Name"],
                'type': 'SAN',
                'remarks': "License or Registration No: {}".format(row["No"])
            })



