# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import tabula
import pandas as pd
import numpy as np
import dateutil.parser


class BermudaMonetaryAuthority(UqaabBaseSpider):
    name = 'bermuda_monetary_authority'
    start_urls = ['http://www.bma.bm']

    def structure_valid(self, response):
        url = 'https://cdn.bma.bm/documents/2019-02-13-08-18-02-Registered-Non-Licensed-Persons---Securities-Sector---as-of-30-January-2019.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all')
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'https://cdn.bma.bm/documents/2019-02-13-08-18-02-Registered-Non-Licensed-Persons---Securities-Sector---as-of-30-January-2019.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all')

        for index, row in bin.iterrows():

            name = row[1]

            if len(name) > 0:
                yield Entity({
                    'category': "Group",
                    'name': name.strip(),
                    'type': 'SAN'
                })




