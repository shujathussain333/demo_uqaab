##Author: Sharjeel Ali Shaukat
## Created on: 13-02-2019

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import scrapy

class NewZealandPoliceMostWanted(UqaabBaseSpider):
    
    name = 'newzealand_police_most_wanted'
    start_urls = ['http://www.police.govt.nz/stolenwanted/wanted-to-arrest?location=All&items_per_page=30&edit-submit-wanteds-1=Go']

    def structure_valid(self,response):
        links = response.xpath(
            "//div[@class='field field-name-field-mp-photos field-type-image field-label-hidden']//a/@href").extract()
        return len(links) > 0

    def extact_data(self,response):
        url = 'http://www.police.govt.nz'

        links = response.xpath("//div[@class='field field-name-field-mp-photos field-type-image field-label-hidden']//a/@href").extract()
        for link in links:
            new_url = url + link
            yield scrapy.Request(url=new_url, callback=self.extract_link)

    def extract_link(self,response):
        name = response.xpath("//h2[@class='field field-name-field-wanteds-name field-type-text field-label-hidden']/text()").extract_first()
        remarks = response.xpath("//div[@class='ds-2col-stacked node node-wanted view-mode-full  clearfix']//div[@class='field field-name-field-reason field-type-text field-label-inline inline']//div[@class='field-item even']/text()").extract_first()
        city = response.xpath(
            "//div[@class='ds-2col-stacked node node-wanted view-mode-full  clearfix']//div[@class='field field-name-field-wanteds-location field-type-taxonomy-term-reference field-label-inline inline']//div[@class='field-item even']/text()").extract_first()
        reference = response.xpath("//*[contains(text(), 'File reference:')]/following::div[@class='field-items']/text()").extract_first()
        document = [{'type': "File Reference", 'id': reference}]
        document_info = json.dumps({'document_info': document})
        image = response.xpath("//img[@class='field-slideshow-image field-slideshow-image-1 img-responsive']/@src").extract_first()

        yield Entity({
            "category": "Individual",
            "name": name,
            'city': city,
            "document": document_info,
            "remarks": remarks,
            "type": "SIP",
            "image":image
        })


