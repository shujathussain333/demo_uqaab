import re
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser


class KenyanAthletesSpider(UqaabBaseSpider):
    name = "ken_athletes"

    start_urls = [
        'http://www.athleticskenya.or.ke/medicalanti-doping/list-of-sanctioned-athletes/',
    ]

    def structure_valid(self, response):
        data_cols = response.css('div.entry-content tbody tr td')
        return len(data_cols) > 0

    def extact_data(self, response):
        data_rows = response.css('tbody tr')
        data_rows = data_rows[3:]
        for row in data_rows:
            columns = row.css('td')[1:]

            sanction_period = columns[2].css('*::text').extract_first()

            date_reg_exp2 = re.compile(
                r'(\d{1,2}(/|-|\.)\w{3}(/|-|\.)\d{1,4})|([a-zA-Z]{3}\s\d{1,2}(,|-|\.|,)?\s\d{4})|(\d{1,2}(/|-|\.)\d{1,2}(/|-|\.)\d+)')
            dates = [x.group() for x in
                     date_reg_exp2.finditer(sanction_period)]

            try:
                inclusion_date = dateutil.parser.parse(dates[0])
                exclusion_date = dateutil.parser.parse(dates[1])
            except Exception as e:
                inclusion_date = None
                exclusion_date = None

            yield Entity({
                'name': columns[0].css('::text').extract_first(),
                'category': 'Individual',
                'inclusion_date': inclusion_date,
                'exclusion_date': exclusion_date,
                'type':'SAN'
            })
