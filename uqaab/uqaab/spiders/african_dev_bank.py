
#Edited by Daniyal
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import scrapy
from scrapy import Request

class AfricanDevBankSpider(UqaabBaseSpider):
    name = "african_dev_bank"
    start_urls = ['https://www.afdb.org/en/projects-and-operations/procurement/debarment-and-sanctions-procedures?name=&field_project_name_value=&items_per_page=60']
    custom_settings = {
        "DOWNLOAD_DELAY": 0.15
    }

    def structure_valid(self, response):
        data_rows = response.xpath('//table[@class="views-table cols-5 table table-striped table-hover table-condensed table-0"]//tbody//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        baseUrl = "https://www.afdb.org"
        data_rows = response.xpath('//table[@class="views-table cols-5 table table-striped table-hover table-condensed table-0"]//tbody//tr')
        for data in data_rows:
            name = data.xpath('.//td[2]//span//a//text()').extract_first()
            dataUrl = data.xpath('.//td[2]//span//a//@href').extract_first()
            dataUrl = baseUrl + dataUrl
            country = data.xpath('.//td[3]//text()').extract_first().strip()
            status = data.xpath('.//td[4]//text()').extract_first().strip()
            approvalDate = dateutil.parser.parse(data.xpath('.//td[5]//span//text()').extract_first())
            remarks = "Status: {} ,Data Url: {}".format(status, dataUrl)
            yield Entity({
                'category': "Group",
                'name': name,
                'country' : country,
                'inclusion_date': approvalDate,
                'remarks': remarks
            })
        nextPageUrl = response.xpath('//div[@class="text-center"]/ul//li[@class="next"]/a/@href').extract_first()
        if(nextPageUrl):
            nextPageUrl = baseUrl + nextPageUrl
            yield scrapy.Request(nextPageUrl, callback=self.extact_data)

