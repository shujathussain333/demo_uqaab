#Author: Daniyal Faquih
import scrapy
import dateutil.parser
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class PBEOEW_Pakistan_Bureau_of_Emigration_Overseas_Warnings(UqaabBaseSpider):
    name = "pbeoew_pakistan_bureau_of_emigration"
    start_urls = ['https://beoe.gov.pk/complaints?sort=result_date&order=desc&page=']
    max_id = 69
    i = 1
    pageCount = 1

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="table-responsive"]/table[@class="table table-striped tabled-bordered"]//tr')
        return len(data_rows) > 0

    def extract_link(self, response):
        self.i+=1
        self.pageCount+=1
        item = {}
        document_info = json.dumps({'document_info': None})
        document = []

        products = response.xpath('//div[@class="table-responsive"]/table[@class="table table-striped tabled-bordered"]//tr')
        for product in products[1:]:
            document.append({'type': "LicenceNumber", 'id': product.xpath('td[2]//text()').extract_first()})
            exclusion_date = product.xpath('td[6]//text()').extract_first()       
            if(exclusion_date):
                exclusion_date = dateutil.parser.parse(exclusion_date)
            else:
                exclusion_date = None
            if document:
                    document_info = json.dumps({'document_info': document})
            
            remarks = "Fine: {}".format(product.xpath('td[5]//text()').extract_first())
            yield Entity({
                "name": product.xpath('td[3]//text()').extract_first(),
                "category": "Group",
                "type": "SIP",
                "exclusion_date": exclusion_date,
                "remarks": remarks,
                "Status": product.xpath('td[4]//text()').extract_first(),
                "document": document_info
            })

                    
    def extact_data(self, response):
        while self.i <= self.max_id:
            yield scrapy.Request('https://beoe.gov.pk/complaints?sort=result_date&order=desc&page=%d' % self.i, callback = self.extract_link)