
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class KNFPublicWarnings(UqaabBaseSpider):

    name = 'knf_public_warning'
    start_urls = ['https://www.knf.gov.pl/en/CONSUMERS/Information_for_the_financial_market_consumers/Public_warnings',]

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='table-responsive']")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.fma.govt.nz/"
        all_warnings = response.xpath("//div[@class='table-responsive']")
        for warning in all_warnings:
            warning_list = warning.xpath("table//tr[position()>1]")
            for detail in warning_list:
                name = detail.xpath("td[2]/text()").extract_first()
                identification_number = detail.xpath("td[3]/text()").extract_first()
                office_address = detail.xpath("td[4]/text()").extract_first()
                relevant_information = detail.xpath("td[6]/text()").extract_first()
                document_info = json.dumps({'document_info': None})
                if identification_number:
                    document = []  
                    document.append({'type': "identification_Number", 'id': identification_number})
                    document_info = json.dumps({'document_info': document})                                    
                yield Entity({
                    "category": "Group",
                    "name":name,
                    "address":office_address,
                    "document":document_info,
            		"remarks":relevant_information,	
            		"type": "SAN"
            	})



