# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from json import dumps

class AUORIC_Disqualified(UqaabBaseSpider):
    name = 'au_oric_disqualified'
    start_urls =  ['http://register.oric.gov.au/DisqualifiedPersonRegister.aspx']

    def structure_valid(self, response):
        self.data_rows = response.css("table").css("tr")[1:]
        return len(self.data_rows) > 0

    def extact_data(self, response):
        data_rows = self.data_rows
       
        for rows in data_rows:
            columns = rows.css("td") 
            
            last_name = columns[0].css("::text").extract_first()
            first_name = columns[1].css("::text").extract_first()
            address= columns[2].css("::text").extract_first()
            date_of_birth = columns[3].css("::text").extract_first()
            inclusiondate = columns[4].css("::text").extract_first()
            exclusiondate = columns[5].css("::text").extract_first()
            reason = columns[6].css("::text").extract_first()
            exemption = columns[7].css("::text").extract_first()

            if reason:
                remarksfield = "Reason:{0}".format(reason)
            if exemption:
                if remarksfield:
                    remarksfield = "{0} , exemption:{1}".format(remarksfield,exemption)
                else:
                    remarksfield = "Exemption:{0}".format(reason)

            dob_info = {}
            
            if date_of_birth:
                dob_info["DOB"] = str(date_of_birth)
                dob_info["POB"] = None
             
            if len(dob_info) > 0:
                DOB_complete = dumps({'info': [dob_info]})
            else:
                DOB_complete = dumps({'info': None})
            
            yield Entity({
                "name": "{0} {1}".format(first_name,last_name), 
                "inclusion_date": self.string_to_date(inclusiondate), 
                "exclusion_date": self.string_to_date(exclusiondate), 
                "remarks": remarksfield, 
                "date_of_birth": DOB_complete,
                "address": address.strip().replace("\r", " ").replace("\n", " "),
                "category": "Individual",
                "type": "SIP"
            })
            
    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%d/%m/%Y')
        except:
            return None