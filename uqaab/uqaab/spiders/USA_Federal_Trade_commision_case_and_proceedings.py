# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json

class USATradeCommisionCases(UqaabBaseSpider):
    
    name = 'usa_federal_trade_commision_cases_proceedings'
    start_urls = ['https://www.ftc.gov/enforcement/cases-proceedings']

    def structure_valid(self, response):
        data_rows = response.xpath('//table/tbody//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        base_url = "https://www.ftc.gov"
        details = response.xpath('//table/tbody//tr//a/@href').extract()
        for each_detail in details:
            yield Request(each_detail, callback=self.parse_case)             
        relative_next_page_url = response.xpath('(//a[1][text()="next ›"])[1]/@href').extract_first()
        if relative_next_page_url:
            absloute_next_page_url = base_url + relative_next_page_url
            yield Request(absloute_next_page_url, callback=self.extact_data)

    def parse_case(self,response):
        case_title = response.xpath('//div[@class="content-wrapper"]/h1/text()').extract_first()
        last_updated = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      Last Updated:\n    ")]]/time/span/text()').extract_first()
        file_number = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      FTC Matter/File Number:\n    ")]]/p/text()').extract_first()
        docket_number = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      Docket Number:\n    ")]]/p/text()').extract_first()
        civil_action_number = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      Civil Action Number:\n    ")]]/p/text()').extract_first()
        enforcement_type = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      Enforcement Type:\n    ")]]/p/text()').extract_first()
        federal_court = response.xpath('//div[@class="content-wrapper"]//div[./span[contains(text(),"\n      Federal Court:\n    ")]]/div[@class="field field-name-field-federal-court field-type-taxonomy-term-reference field-label-inline inline field-wrapper"]/text()').extract_first()
        document_info = json.dumps({'document_info': None})
        document = []  
        document.append({'type': "File Number", 'id': file_number})
        document.append({'type': "Docket Number", 'id': docket_number})
        document.append({'type': "Civil Action Number", 'id': civil_action_number})
        document_info = json.dumps({'document_info': document})               
        case_summary = response.xpath('//div[@class="field field-name-body field-type-text-with-summary field-label-above field-wrapper"]/p/text()').extract_first()
        press_release_comments = response.xpath('//div[@class="view-content"]//a[not(@title)]').extract()
        Pdf_comments = response.xpath('//div[@class="view-content"]//a[(@title)]/@href').extract()
        Pdf_comments = " ".join(Pdf_comments)
        remarks = "Case Summary: {} ,Pdf Content: {} , press_release".format(case_summary,Pdf_comments,press_release)
        yield Entity({
            "category": "group",
            "name":case_title,
            "document":document_info,
            "remarks":remarks,
            "type": "SAN"
        })