#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import urllib.request
import json

class GhanaPoliceWanted(UqaabBaseSpider):

    name = 'ghana_police_wanted'
    start_urls = ['https://police.gov.gh/en/index.php/wanted-persons/']

    def structure_valid(self, response):
        data_rows = response.xpath('//ul[@class=""]/li/div[@class="staff_info"]')
        return len(data_rows) > 0

    def extact_data(self,response):
        page_urls = response.xpath('//ul[@class=""]/li/div[@class="staff_info"]')
        for page in page_urls[1:6]:
            each_link = page.xpath('h4/a/@href').extract_first()
            absolute_url = each_link
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        aliases = address = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong="Alias"]/text()').extract_first()
        if aliases is None:
            aliases= " "
        alias = [{'aka': aliases}]
        alias_info = json.dumps({'info': alias})        
        warant_no = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong="Warrant No"]/text()').extract_first()
        document_info = json.dumps({'document_info': None})
        document = []  
        document.append({'type': "Warrant No", 'id': warant_no})
        document_info = json.dumps({'document_info': document})        
        name = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/p[1]/strong/text()').extract_first()
        native = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong="Native of"]/text()').extract_first()
        last_seen = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong ="Last seen" or  ./strong = "Last seen:  "]/text()').extract_first()
        address = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong="May be found"]/text()').extract_first()
        occupation = response.xpath('//div[@class="wpb_text_column wpb_content_element "]/*/ul/li[./strong="Occupation"]/text()').extract_first()
        designation = [{'designation': occupation}]
        designation_info = json.dumps({'info': designation})
        remarks = "Last seen: {}  native: {}".format(last_seen,native)
        image = response.xpath('//div[@class="wpb_text_column wpb_content_element "]//img/@src').extract_first()
        yield Entity({
            "category": "Individual",
            "name":name,
            "address":address,
            "remarks":remarks,
            "designation": designation_info,
            "aka":alias_info,
            "document":document_info,
            "image":image,
            "type": "SIP"
        })               