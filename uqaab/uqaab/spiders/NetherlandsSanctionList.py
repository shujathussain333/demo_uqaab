# -*- coding: utf-8 -*-
from datetime import datetime
import re
import scrapy
import requests
from pyexcel_ods import get_data
import io
import json
import pandas as pd
import numpy as np
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class NetherlandsSanctionList(UqaabBaseSpider):
    name = 'netherlands_list_spider'
    start_urls = ["https://www.government.nl/binaries/government/documents/reports2016/01/15/national-terrorism-list/eng-terrorismelijst.ods",]

    def structure_valid(self, response):
        return True

    def extact_data(self, response):

        url = "https://www.government.nl/binaries/government/documents/reports/2016/01/15/national-terrorism-list/eng-terrorismelijst.ods"
      
        netherlands = requests.get(url)

        with io.BytesIO(netherlands.content) as ods:
            ods_file = get_data(ods)
            
        
        df = pd.DataFrame(ods_file["Sheet1"], columns=["Surname", "FirstName(s)", \
                                              "Alias", "DateOfBirth(D-M-Y)",\
                                               "DateOfMinisterialDecision", "NotificationLink"])

        df = df.iloc[3:-3]

        df = df.replace(r'', np.nan)

        for index, row in df.iterrows():

            name = self.extract_name(row["Surname"], row["FirstName(s)"])
            aka = self.extract_aka(row["Alias"])
            inclusion_date = row["DateOfMinisterialDecision"]
            remarks = row["NotificationLink"]

            dob_pob = {}
             
            dob_pob["DOB"] = str(row["DateOfBirth(D-M-Y)"])
            dob_pob["POB"] = None

            if dob_pob['DOB'] or dob_pob['POB']:
                DOB_info = json.dumps({'info': [dob_pob]})
            else:
                DOB_info = json.dumps({'info': None})

            yield Entity({
                "name" : name,
                "remarks": remarks,
                "aka": aka,
                "date_of_birth" : DOB_info,
                "inclusion_date" : inclusion_date,
                "category": CATEGORY_OPTIONS[0],
                "country": "NL"
            })

    @staticmethod
    def extract_name(surname, fname):

        name = None

        try:
            if fname is np.nan:
                name = surname
            else:
                name = " ".join([surname, fname])
            
            return name

        except Exception:
            return None
    
    @staticmethod
    def extract_aka(aka):
        if aka is np.nan:
            aka = []
        else:
            aka = [aka]
        return aka






