import dateutil
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import re


# This scrapper for other sanction list
class WorldBankTopSpider(UqaabBaseSpider):
    name = "world_bank_other_sanction"

    start_urls = [
        'http://www.worldbank.org/en/projects-operations/procurement/debarred-firms',
    ]

    def structure_valid(self, response):
        tables = response.css('table')
        return len(tables) > 0

    def extact_data(self, response):
        data_rows = response.css('table tr')

        for row in data_rows[1:]:
            columns = row.css('td')
            if len(columns) > 1:
                details = columns[0].css('*::text').extract()
                name = details[0]

                try:
                    address = [details[2]]
                except IndexError:
                    address = None
                dates = columns[1].css('*::text').extract_first()
                date_reg_exp2 = re.compile(
                    r'(\d{1,2}(/|-|\.)\w{3}(/|-|\.)\d{1,4})|([a-zA-Z]{1,9}\s\d{1,2}(,|-|\.|,)?\s\d{4})|(\d{1,2}(/|-|\.)\d{1,2}(/|-|\.)\d+)')
                try:
                    dates = [x.group() for x in
                             date_reg_exp2.finditer(dates)]
                    inclusion_date = dateutil.parser.parse(dates[0].strip())
                    exclusion_date = dateutil.parser.parse(dates[1].strip())
                except IndexError:
                    inclusion_date = None
                    exclusion_date = None

                sanction_imposed = columns[2].css('*::text').extract_first()
                grounds = columns[3].css('*::text').extract_first()

                remarks = "Sanction Imposed: {}, Ground: {}".format(sanction_imposed, grounds)

                yield Entity({
                    "name": name,
                    "address": address,
                    "category": 'Individual',
                    "type": "SAN",
                    "exclusion_date": exclusion_date,
                    "inclusion_date": inclusion_date,
                    "remarks": remarks
                })
