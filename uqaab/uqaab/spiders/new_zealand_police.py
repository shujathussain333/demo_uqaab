from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class NewZealandPoliceSpider(UqaabBaseSpider):
    name = "new_zealand_police"
    
    start_urls = [
        'http://www.police.govt.nz/advice/personal-community/counterterrorism/designated-entities/lists-associated-with-resolution-1373',
        ]

    def structure_valid(self, response):
        data_rows = response.css('table')[0].css('tr')[1:]
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table')[0].css('tr')[1:]
        for row in data_rows:
            columns = row.css('td')

            aliases = self.create_aka_string(columns[0].css('p::text').extract_first())
            yield Entity({
                'name': self.extract_name(columns[0]),
                'aka': aliases,
                'inclusion_date': self.string_to_date(columns[2].css('p')[1].css('::text').extract_first())
            })

    @staticmethod
    def create_aka_string(aka_string):
        lowered_string = aka_string.lower()
        search_for = 'also known as '
        aliases = aka_string[lowered_string.index(search_for) + len(search_for) : ]
        aliases = aliases.replace('.', '')
        return aliases.split(',')

    @staticmethod
    def extract_name(column):
        name = column.css('strong::text').extract_first()
        if name is None:
            name = column.css('b::text').extract_first()
        return name

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d %B %Y')
        except TypeError:
            return None