##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
from googletrans import Translator

class HungarianPoliceMW(UqaabBaseSpider):
    
    name = 'hungarian_police_most_wanted'
    start_urls = ['http://www.police.hu/hu/koral/toplistas-korozesek']
    url = 'http://www.police.hu'

    def structure_valid(self, response):
        links = response.xpath("//div[@class='flex-grid person top']//a/@href").extract()
        return len(links) > 0

    def extact_data(self, response):
       
        links = response.xpath("//div[@class='flex-grid person top']//a/@href").extract()
        for link in links:
            new_url = self.url + link
            yield scrapy.Request(url=new_url, callback=self.extract_link)

    def extract_link(self, response):
        try:
            data = response.xpath("//div[@class='line left float-none']/text()").extract()
            remarks = response.xpath("//div[@class='content-full']//p/text()").extract()
            translator = Translator()
            name = data[0]
            place = data[2]
            date_of_birth = data[3]
            nationality = translator.translate(data[4])
            remarks = translator.translate(remarks)
            dob = [{'DOB' : date_of_birth , 'POB' : place}]
            dob_info = json.dumps({'info' : dob})
            image = response.xpath("//div[@class='image-container']//img/@src").extract_first()
            image = self.url + image
            yield Entity({
                "category": "Individual",
                "name": name,
                'date_of_birth': dob_info,
                'nationality': nationality.text,
                "remarks": remarks[0].text,
                "type": "SIP",
            })
        except IndexError:
            pass


