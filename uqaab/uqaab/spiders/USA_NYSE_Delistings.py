#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import json

class USA_NYSE_Delistings(UqaabBaseSpider):
    name = 'usa_nyse_delistings'
    start_urls = ['https://www.nyse.com/api/regulatory/delisting?market=&initiation=&filterToken=&max=10&offset=0&sortOrder=up&sortType=']

    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        data_rows=Jresponse["results"]
        return len(data_rows) > 0

    @staticmethod
    def getJsonData(data):
        name = data['issuerName']
        symbols = data['delistedSymbols']
        market = data['market']
        initiationType = data['initiationType']
        delistingDate = data['delistingDate']
        return {"name":name,"symbols":symbols,"initiationType":initiationType,"market":market, "delistingDate":delistingDate}

    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["results"]
        
        for data in actual_data:
            dataDict = self.getJsonData(data)
            delistingDate = dateutil.parser.parse(dataDict["delistingDate"])
            remarks = "Symbols: {} ,Market: {} ,Initiation Type: {}".format(dataDict["symbols"],dataDict["market"],dataDict["initiationType"])
            yield Entity({
            "name": dataDict["name"],
            "category": "Group",
            "type": "SAN",
            "exclusion_date": dataDict["delistingDate"],
            "remarks": remarks
        })