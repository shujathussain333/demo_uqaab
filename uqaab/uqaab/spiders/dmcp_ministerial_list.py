"""
Created by Muhammad Ilyas <muhammad.ilyas@lovefordata.com>
"""


import bs4 as bs
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class DmcpMinisterialList(UqaabBaseSpider):
    name = "dmcp_ministerial_list"
    allowed_domains = ['dpmc.govt.nz']
    start_urls = [
        'https://dpmc.govt.nz/our-business-units/cabinet-office/ministers-and-their-portfolios/ministerial-list'
    ]
    
    def structure_valid(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        return len(soup.find('table').find_all("td")) > 0

    def extact_data(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        data = soup.find('table').find_all("td")

        for i in range(0, len(data), 3):
            name = data[i+1].find("h4").text
            designation = data[i+1].text.replace(name, "").strip()
            other_info = data[i+2].text.strip()
            yield Entity({
                'category': 'Individual',
                'name': name,
                'nationality': 'New Zealand', 
                'designation' : designation,
                'remarks': other_info,
                'type' : "PEP",
                'country': self.get_country_code('New Zealand'),
            })      