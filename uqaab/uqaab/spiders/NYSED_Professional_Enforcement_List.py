#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import dateutil.parser 

class NYSED_Professional_Enforcement_List(UqaabBaseSpider):
    name = "nysed_professional_enforcement_list"
    start_urls = ['http://www.op.nysed.gov/opd/mar19.html']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@id="content_column"]//p')
        return len(data_rows) > 0

    def extact_data(self, response):
        dataArr = []
        actionArr = []
        summaryArr = []
        dateArr = []
        professionArr = []
        countryArr = []
        nameArr = []
        aliasArr = []
        pData = response.xpath('//div[@id="content_column"]//p//text()').extract()
        i = 21
        a = 0
        while i < (len(pData)-4):
            alias = ""
            if "Action:" in pData[i]:
                action = pData[i+1]
                action = action.lstrip()
                actionArr.append(action)
            if "Summary:" in pData[i]:
                summary = pData[i+1]
                summary = summary.lstrip()
                summaryArr.append(summary)
            if "Regents" in pData[i]:
                date = pData[i+1]
                date = date.lstrip()
                dateArr.append(date)
            if "Profession:" in pData[i]:
                profession = pData[i+1]
                profession = profession.lstrip()
                profession = profession.split("; ")
                profession = profession[0]
                professionArr.append(profession)
                country = pData[i-2]
                country = country.split("; ")
                country = country[-1]
                country = country.lstrip()
                countryArr.append(country)
                name = pData[i-3]
                
                if("a/k/a" in name):
                    name = pData[i-4]
                    alias = pData[i-3]
                    alias = alias.replace("a/k/a","")
                    alias = alias.strip()
                if ("d/b/a" in name):
                    name = pData[i-4]
                nameArr.append(name)
                aliasArr.append(alias)
            i+=1

        i = 0
        while i < len(nameArr):
            alias = []
            name = nameArr[i]
            country = countryArr[i]
            inclusion_date = dateutil.parser.parse(dateArr[i])
            remarks = "Action: {} ,Summary: {}".format(actionArr[i],summaryArr[i])
            alias_info = ""
            if aliasArr[i]:
                alias = [{'aka':aliasArr[i]}]
                alias_info = json.dumps({'info': alias})

            designation = [{'designation':professionArr[i]}]
            designation_info = json.dumps({'info':designation})
            i+=1
            yield Entity({
                "name": name,
                "aka": alias_info,
                "category": "Individual",
                "type": "SAN",
                "city": country,
                "designation": designation_info,
                "inclusion_date": inclusion_date,
                "remarks": remarks
            })