from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd

class Brunei_Wanted(UqaabBaseSpider):
    name = 'brunei_wanted'
    start_urls = ['http://www.police.gov.bn/Lists/Wanted%20Persons/AllItems.aspx']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'http://www.police.gov.bn/Lists/Wanted%20Persons/AllItems.aspx'
        df = pd.read_html(url)[1]
        df.dropna(axis='columns', inplace=True)
        df['Name'] = df['Nama']
        df['Gender'] = df['Jantina']
        df['Remarks'] = 'No. K/P : ' + df['No. K/P'] + ', Age : ' + df['Umur']

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'gender': row.Gender,
                'category': 'Individual',
                'type': "SIP"
            })


