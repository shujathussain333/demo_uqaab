# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from json import dumps

class ASIC(UqaabBaseSpider):
    name = 'asic_banned_and_disqualified_organisations'
    start_urls =  ['https://connectonline.asic.gov.au/OPH/OthersHelp/BDQOrgs/index.htm']

    def structure_valid(self, response):
        self.data_tables = response.css("table")[2:19]
        return len(self.data_tables) > 0

    def extact_data(self, response):
        data_tables = self.data_tables
        namelist = []
        for table in data_tables:
            
            remarksfield = ""
            data_rows = table.css("tr")
            
            for row in data_rows:
                key = row.css("th").css("strong::text").extract_first()
                value = row.css("td").css("::text").extract_first()
                
                if key == "Name" and value:
                    name = value
                    
                elif key == "ACN" and value:
                    acn = value
                
                elif key == "Commenced" and value:
                    inclusiondate = self.string_to_date(value)
                
                elif key == "Ceased" and value:
                    exclusiondate = self.string_to_date(value)
                
                elif key == "Comments" and value == "No comment made":
                    continue
                
                else:
                    if key and value:
                        remarksfield = "{0} {1}:{2} ".format(remarksfield, key, value)

            if remarksfield:
                document = dumps({'document_info': None})
                document_field = acn
                
                if document_field is not None:
                    document_array = [{
                        'type': 'ACN',
                        'id': document_field
                    }]
                    document = dumps({'document_info': document_array}) 

                yield Entity({
                    "name": name.strip(), 
                    "inclusion_date": inclusiondate, 
                    "exclusion_date": exclusiondate, 
                    "remarks": remarksfield,
                    "document": document, 
                    "category": "Groups",
                    "type": "SIP"
                })
            
    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%d/%m/%Y')
        except:
            return None