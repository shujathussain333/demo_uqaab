# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class DeptForeignAffairsAutonomousSanctionsZimbabwe(UqaabBaseSpider):
    name = 'dept_foreign_affairs_autonomous_sanctions_zimbabwe'
    start_urls = ['https://www.legislation.gov.au/Details/F2018L00108/Html/Text#_Toc503540504']

    def structure_valid(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        data_rows = data_rows[1:]
        datas = []
        for row in data_rows:
            columns = row.css('td')
            column_name = columns[1].css('p').css('*::text').extract_first()
            try:
                if "Individual" in column_name:
                    dictionary = {}
                    name = columns[2].css('p').css('*::text').extract_first()
                    category = "Individual"
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name, "category": category, "address": address, 'dob': None, 'designation': None})

                elif "Entity" in column_name:
                    dictionary = {}
                    name = columns[2].css('p').css('*::text').extract_first()
                    category = "Group"
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name, "category": category, "address": address, 'dob': None, 'designation': None})

                elif "Address" in column_name:
                    address = [columns[2].css('p').css('*::text').extract_first()]
                    dictionary.update({"address": address})

                elif "Birth" in column_name:
                    dob = columns[2].css('p').css('*::text').extract_first()
                    dobs = {'DOB': dob, 'POB': None}
                    dob_info = json.dumps({'info': [dobs]})
                    dictionary.update({"dob": dob_info})

                elif "Additional" in column_name:
                    designation = columns[2].css('p').css('*::text').extract_first()
                    designation = {"designation": designation.strip()}
                    designation_info = json.dumps({'info': [designation]})
                    dictionary.update({"designation": designation_info})

                elif "Listing" in column_name:
                    remarks = columns[2].css('p').css('*::text').extract_first()
                    dictionary.update({"remarks": remarks})
                    datas.append(dictionary)

            except (TypeError, AttributeError):
                continue

        for data in datas:
            yield Entity({
                "name": data['name'],
                "address": data["address"],
                "category": data["category"],
                "type": "SAN",
                "date_of_birth": data["dob"],
                "designation": str(data["designation"]),
                "country": "Zimbabwe",
            })
