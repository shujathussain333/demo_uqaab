# -*- coding: utf-8 -*-
import datetime
import re
import scrapy
import requests
import numpy as np
import io
import json
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS


class NZPoliceResOrg_Spider(UqaabBaseSpider):
    name = 'nzpolice_res_organization_spider'
    start_urls = [
        "http://www.police.govt.nz/advice/personal-community/counterterrorism/designated-entities/lists-associated-with-resolutions-1267-1989-2253-1988"]

    def structure_valid(self, response):
        link = response.xpath("//ul[@class='doclist']//li//a/@href").extract()
        return len(link) > 0

    def extact_data(self, response):

        link = response.xpath("//ul[@class='doclist']//li//a/@href").extract()
        response = requests.get("https://www.police.govt.nz{}".format(link[-1]))

        df = self.create_dataframe(response)

        addr_cols = [col for col in df if "Address" in col]

        for index, row in df.iterrows():
            if float(row["ID"]).is_integer():
                ############ Extract name ##############
                id = row["ID"]
                next_id = id + 1

                df2 = df[(df['ID'] > id) & (df['ID'] < next_id)]
                df2 = df2[['Name']]

                alias_info = json.dumps({'info': None})

                if not df2.empty:
                    df2 = df2.replace(np.nan, '', regex=True)
                    df2 = df2.add([' '] * (df2.columns.size - 1) + ['']).sum(axis=1)
                    aliases = df2.tolist()
                    alias = [{'aka': re.sub('\s+', ' ', alias)} for alias in aliases]

                if alias:
                    alias_info = json.dumps({'info': alias})

                name = self.extract_name(row["Name"])

                organization = self.extract_organization(row["Associated to"])

                addresses = self.extract_addresses(row[addr_cols])

                remarks = self.extract_remarks(row["Other"])

                inclusion_date = self.extract_incl_date(row["NZ_Designation_Date"])

                yield Entity({

                    "name": name + '781',
                    "remarks": remarks,
                    "inclusion_date": inclusion_date,
                    "category": CATEGORY_OPTIONS[1],
                    "address": addresses,
                    "organization": organization,
                    "type": "San",
                    "aka": alias_info
                })

    @staticmethod
    def extract_name(data):
        return data

    @staticmethod
    def extract_organization(row):
        if row == 1.0:
            return "Taliban"
        elif row == 2.0:
            return "Al-Qaida"
        else:
            return "None"

    @staticmethod
    def extract_addresses(row):
        n = list(row)
        addresses = []
        for add in n:
            add = str(add)
            if "nan" not in add:
                addresses.append(add)
        return addresses

    @staticmethod
    def extract_remarks(row):
        if row is np.nan:
            row = None
        return row

    @staticmethod
    def extract_incl_date(date):

        res = None

        try:
            date = str(date).replace(".", "").replace("\n", "")

            if isinstance(date, datetime.datetime) is False:
                check = date.split()[-1]
                if "/" in check:
                    sdate = check
                    sdate = datetime.datetime.strptime(sdate, "%d/%m/%Y")
                    res = sdate
                else:
                    adate = date.split()[-3:]
                    adate = " ".join(adate)
                    adate = datetime.datetime.strptime(adate, "%d %B %Y")
                    res = adate
            elif date.isspace():
                res = date
            else:
                res = date
        except Exception:
            res = date

        return res


    def create_dataframe(self,response):
        with io.BytesIO(response.content) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Organisations')
        return df