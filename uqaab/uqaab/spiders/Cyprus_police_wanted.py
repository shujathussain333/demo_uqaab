# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class CyprusPoliceWnted(UqaabBaseSpider):
    
    name = 'cyprus_police_wanted'
    start_urls = ['http://www.police.gov.cy/police/police.nsf/dmlwanted_en/dmlwanted_en?OpenDocument']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="main"]/table//tr[@valign="top"]')
        return len(data_rows) > 0

    def extact_data(self, response):
        base_url = "http://www.police.gov.cy"
        details = response.xpath('//div[@class="main"]/table//tr[@valign="top"]')
        for detail in details:
            name_date = detail.xpath('td/table/tr[1]/td/span/b/text()').extract_first()
            partial_name = name_date.split('-')
            name = partial_name[1]
            remarks = detail.xpath('td/table/tr[2]/td/span/text()').extract_first()
            image_url = detail.xpath('td/table/tr[2]/td[3]/a/@href').extract_first()
            image = base_url + image_url
            if name:
                name = name.strip()
            yield Entity({
                "category": "Individual",
                "name":name,
                "remarks":remarks,
                "image":image,
                "type": "SIP"
            })

        
