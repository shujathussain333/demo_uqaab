# -*- coding: utf-8 -*-
from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class UnConsolidatedSanctionsSpider(UqaabBaseSpider):
    name = 'UN-Consolidated-Sanctions'
    start_urls = ['https://scsanctions.un.org/resources/xml/en/consolidated.xml']

    def structure_valid(self, response):
        soup = BeautifulSoup(response.body, "lxml")
        individual = soup.findAll('individual')
        return len(individual) > 0

    def extact_data(self, response):
        soup = BeautifulSoup(response.body, "lxml")
        individual = soup.findAll('individual')
        for i in range(0, len(individual)):
            category = 'Individual'
            name = ''
            if individual[i].find('first_name') is not None:
                name = individual[i].find('first_name').text 
            if individual[i].find('second_name') is not None:
                name = name + ' ' +individual[i].find('second_name').text 
            if individual[i].find('third_name') is not None:
                name = name + ' ' +individual[i].find('third_name').text 
            if individual[i].find('fourth_name') is not None:
                name = name + ' ' + individual[i].find('fourth_name').text
            if individual[i].find('nationality') is not None: 
                nationality = individual[i].find('nationality').value.text
            if individual[i].find('last_day_updated').findAll('value')[-1].text  is not None: 
                inclusion_date = individual[i].find('last_day_updated').findAll('value')[-1].text
            if individual[i].find('comments1') is not None:
                remarks = individual[i].find('comments1').text
            if individual[i].find('listed_on') is not None:     
                listed_on = individual[i].find('listed_on').text
            if individual[i].find('un_list_type') is not None:
                list_name = 'UN ' + individual[i].find('un_list_type').text
            if individual[i].find('list_type') is not None:
                sanction_type = individual[i].find('list_type').value.text  + ' : ' + individual[i].find('un_list_type').text
            alias = individual[i].findAll('individual_alias')
            aka = []
            for j in range(0, len(alias)):
                # if alias[j].find('individual_alias') is not None:
                if alias[j].find('alias_name') is not None:
                    aka.append({
                        'quality': alias[j].find('quality').text,
                        'alias_name': alias[j].find('alias_name').text,
                    })
            aka = json.dumps(aka)
            yield Entity({
                'category': category,
                'name': name,
                'nationality': nationality, 
                'inclusion_date': self.string_to_date(inclusion_date),
                'remarks': remarks
            })

        entity = soup.findAll('entity')
        for k in range(0, len(entity)):
            if entity[k].find('first_name') is not None:
                nameE = entity[k].find('first_name').text
            if entity[k].find('last_day_updated').findAll('value')[-1].text  is not None: 
                inclusion_dateE = entity[k].find('last_day_updated').findAll('value')[-1].text
            if entity[k].find('comments1') is not None:
                remarksE = entity[k].find('comments1').text
            if entity[k].find('listed_on') is not None:     
                listed_onE = entity[k].find('listed_on').text
            if entity[k].find('un_list_type') is not None:
                list_nameE = 'UN Consolidated'
            if entity[k].find('list_type') is not None:
                sanction_typeE = entity[k].find('list_type').value.text  + ' : ' + entity[k].find('un_list_type').text
            aliasE = entity[k].findAll('entity_alias')
            akaE = []
            for l in range(0, len(aliasE)):
                # if alias[j].find('individual_alias') is not None:
                if aliasE[l].find('alias_name') is not None:
                    akaE.append({
                        'quality': aliasE[l].find('quality').text,
                        'alias_name': aliasE[l].find('alias_name').text,
                    })
            akaE = json.dumps(akaE)
            yield Entity({
                'category': 'Group',
                'name': nameE,
                # 'nationality': nationalityE, 
                'inclusion_date': self.string_to_date(inclusion_dateE),
                'remarks': remarksE
                
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%Y-%m-%d')
        except (TypeError,ValueError):
            return None

