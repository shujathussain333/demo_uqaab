# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class SFC_Disqualification(UqaabBaseSpider):
    name = 'sfc_disqualification'
    start_urls =  ['https://www.sfc.hk/web/EN/regulatory-functions/enforcement/enforcement-actions/disqualification-orders.html']
    
    def structure_valid(self, response):
        self.data_rows = response.css("table > tbody > tr")
        return len(self.data_rows) > 0

    def extact_data(self, response):
        data_rows = self.data_rows
        
        refrence_base = "https://www.sfc.hk/web/EN/"
        last_update = response.css("p[class='lastmodified']::text").extract_first()
        last_update = last_update.replace("Last update:  ","").strip()

        for i in range(len(data_rows)):
           
            columns = data_rows[i].css('td')
            name = self.extract_field(columns[0])
            reference = columns[0].css("a::attr(href)").extract_first()
            disqualification_period = columns[1].css("::text").extract_first()
            imposed_by = self.extract_field(columns[2])
            news_release = self.extract_field(columns[3])
            news_refrence = columns[3].css("a::attr(href)").extract_first()
            
            remarksfield = ""
            
            if reference:
                reference = reference.replace(" ","%20")
                remarksfield = "{0} Profile: {1}{2}".format(remarksfield,refrence_base, reference)
            
            if imposed_by:
                if not remarksfield:
                    remarksfield = "{0} Imposed By: {1}".format(remarksfield, imposed_by)
                else:
                    remarksfield = "{0} ,Imposed By: {1}".format(remarksfield, imposed_by)
            
            if news_release:
                remarksfield = "{0} ,News Realeased On: {1}".format(remarksfield, news_release)
            
            if news_refrence:
                remarksfield = "{0} ,News reference: {1}".format(remarksfield, news_refrence)
            
            if last_update:
                remarksfield = "{0} ,Website Last update: {1}".format(remarksfield, last_update)

            inclusion_exclusion = disqualification_period.split(" to ")
            inclusiondate = self.string_to_date(inclusion_exclusion[0])
            exclusiondate = self.string_to_date(inclusion_exclusion[1])
            
            
            yield Entity({
                    'category': 'Individual',
                    'type': "SIP",
                    'name': name,
                    'remarks': remarksfield, 
                    "inclusion_date": inclusiondate,
                    "exclusion_date": exclusiondate
                })
   
            
    def extract_field(self, response):
        allelements = response.css("*")
        allelementstext = allelements.css("::text").extract()

        field = "".join(sorted(list(set(allelementstext)))).strip()
             
        return field

    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%d %b %Y')
        except:
            return None
