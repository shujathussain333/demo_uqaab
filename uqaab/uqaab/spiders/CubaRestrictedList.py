#Author: Daniyal Faquih

import scrapy
import traceback
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class CubaRestrictedListSpider(UqaabBaseSpider):
    name = "cuba_restricted_list"
    start_urls = ['https://www.state.gov/cuba-sanctions/cuba-restricted-list/list-of-restricted-entities-and-subentities-associated-with-cuba-as-of-april-24-2019/']

    def structure_valid(self, response):
        pTags = response.xpath('//div[@class="entry-content"]//p')
        return len(pTags) > 0   

    def extact_data(self, response):
        pTags = response.xpath('//div[@class="entry-content"]//p')
        for p in pTags[2:-1]:
            temp = p.xpath('./u//text()').extract_first()
            if(temp == None):
                name = p.xpath('.//text()').extract_first()
                yield Entity({
                    'name': name,
                    "category": "Group",
                    "type": "SAN"   
                })