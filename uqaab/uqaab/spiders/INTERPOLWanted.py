# Author: Sharjeel Ali Shaukat

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class InterpolSpider(UqaabBaseSpider):
    name = 'interpol_wanted_spider'
    start_urls = ['https://ws-public.interpol.int/notices/v1/red']

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.10,
        "AUTOTHROTTLE_START_DELAY": 0.10,
        "AUTOTHROTTLE_MAX_DELAY": 0.15,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 10000,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    def structure_valid(self, response):
        jsonresponse = json.loads(response.body)
        total_records = jsonresponse['total']
        return total_records > 0

    def extact_data(self, response):
        jsonresponse = json.loads(response.body)
        total_records = jsonresponse['total']
        new_url = 'http://ws-public.interpol.int/notices/v1/red?resultPerPage={}'.format(total_records)
        yield scrapy.Request(url=new_url, callback=self.extract_link)

    def extract_link(self, response):
        jsonresponse = json.loads(response.body)
        links = jsonresponse['_embedded']['notices']
        for link in links:
            new_url = link['_links']['self']['href']
            yield scrapy.Request(url=new_url, callback=self.extract_details)

    def extract_details(self, response):
        jsonresponse = json.loads(response.body)

        try:
            name = "{} {}".format(jsonresponse['name'], jsonresponse['forename'])
        except KeyError:
            name = jsonresponse['name']
        # this is due to result from json name is none

        if 'none' in name.lower():
            name = name.lower().replace('none', '')
            name = name.title()

        try:
            image = jsonresponse['_links']['thumbnail']['href']
        except KeyError:
            image = None

        gender = jsonresponse['sex_id']
        date_of_birth = jsonresponse['date_of_birth']
        place_of_birth = jsonresponse['place_of_birth']
        nationalities = jsonresponse['nationalities']
        dobs = [{'DOB' : date_of_birth, 'POB' : place_of_birth}]
        arrest_warrents = jsonresponse['arrest_warrants']
        arrest_warrents = json.dumps(arrest_warrents).strip()
        remarks = "Arrest Warrents Details: {}".format(arrest_warrents.strip("[{}]"))
        remarks = remarks.replace('null', '""')
        remarks = remarks.replace('"', '')
        entity_id = jsonresponse['entity_id']
        document = []
        document.append({'type': "Entity ID",
                         'id': entity_id})
        if dobs:
            dob_info = json.dumps({'info': dobs})
        else:
            dob_info = json.dumps({'info': None})

        if document:
            document_info = json.dumps({'document_info' : document})

        try:
            nationality = ' , '.join(nationalities)
        except TypeError:
            nationality = nationalities

        yield Entity({
            "category": "Individual",
            "name": name,
            'date_of_birth': dob_info,
            'nationality': nationality,
            "document": document_info,
            "remarks": remarks,
            "type": "SIP",
            "gender":gender,
            "image":image
        })