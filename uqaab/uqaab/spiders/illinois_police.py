from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd

class Illinois_Scrapper(UqaabBaseSpider):
    name = 'illinois_state_police'
    start_urls = ['https://www.isp.state.il.us/crime/wanted.cfm']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www.isp.state.il.us/crime/wanted.cfm'
        dfs = pd.read_html(url)
        dfs[6].dropna(how='all', inplace=True)
        allcols = (list(dfs[6][0]) + list(dfs[6][1]))
        ages, names, remarkss = [], [], []
        for temp in allcols:
            temp = str(temp)
            age, name, remarks = '', '', ''
            try:
                name = temp.split('Male')[0]
            except:
                name = temp.split('Female')[0][0]
            try:
                age = temp.split('Age')[1].strip().split(' ')[0]
            except:
                age = ''
            try:
                remarks = temp.split(age)[1].strip()
            except:
                remarks = ''

            ages.append(age)
            names.append(name)
            remarkss.append(remarks)

        df = pd.DataFrame({'Name': names, 'Age': ages, 'Remarks': remarkss})

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'category': 'Individual',
                'type': "SIP"
            })