#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request
import json

class SpainGuardianCivilWanted(UqaabBaseSpider):

    name = 'spain_guardian_civil_wanted'
    start_urls = ['http://www.guardiacivil.es/en/colaboracion/buscados/index.html?pagina=1&buscar=si&category=&notshown=']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="ficha"]//a[text()="Show info"]/@href')
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "http://www.guardiacivil.es"
        page_urls = response.xpath('//div[@class="ficha"]//a[text()="Show info"]/@href').extract()
        for page in page_urls:
            each_link = page
            absolute_url = base_url + each_link
            yield Request(absolute_url, callback=self.parse_page)  
        relative_next_url = response.xpath('//div[@class="pagelist-bar"]//a[text()="2"]/@href').extract_first()
        if relative_next_url:
            absolute_next_url = base_url + relative_next_url
            yield Request(absolute_next_url, callback=self.extact_data)

    def parse_page(self,response):
        base_url = "http://www.guardiacivil.es"
        name = response.xpath('//div[@class="block-noticia-ampliada"]//h1/text()').extract_first()
        image_url = response.xpath('//div[@class="ficha"]//img/@src').extract_first()
        image = base_url + image_url
        race = response.xpath('//div[@class="observaciones"]/p[text()="Race:"]/span/text()').extract_first()
        if race is None:
            race = " "
        alaises = response.xpath('//div[@class="observaciones"]//ul[4]/li[1]/text()').extract_first()
        if alaises is None:
            alaises = " "
        if "alias" in name:
            result = re.findall(r'\(([^]]*)\)', name)
            alaises = result[0]
            name = re.sub(r'\(.*\)', '', name)
            name = name.replace("  "," ")
        alias = [{'aka': alaises}]
        alias_info = json.dumps({'info': alias})            
        remarks =   response.xpath('//div[@class="observaciones"]/p[text()="Comments:"]/span/text()').extract_first()
        if remarks is None:
            remarks = " "
        remarks = remarks + " " + race     
        date_of_birth = response.xpath('//div[@class="observaciones"]/p[text()="Date of birth:"]/span/text()').extract_first()
        if date_of_birth:
            date_of_birth = date_of_birth.strip()
        birth_place = response.xpath('//div[@class="observaciones"]/p[text()="Birthplace:"]/span/text()').extract_first()
        if birth_place:
            birth_place = birth_place.strip()
        dobs = {'DOB': date_of_birth, 'POB': birth_place}
        dob_info = json.dumps({'info': [dobs]})
        yield Entity({
            "category": "Individual",
            "name":name,
            "date_of_birth":dob_info,
            "aka":alias_info,            
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })  
