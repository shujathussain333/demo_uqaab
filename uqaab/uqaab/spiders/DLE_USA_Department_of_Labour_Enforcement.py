#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import requests 
import xml.etree.ElementTree as ET 
import dateutil.parser
import json

class DLE_USA_Department_of_Labour_Enforcement(UqaabBaseSpider):
    name = 'dle_usa_department_of_labour_enforcement'
    start_urls = ['https://www.osha.gov/topcases/xml/data-set.xml']

    def structure_valid(self, response):
        resp = requests.get(self.start_urls[0])
        with open('DLE_USA_Department_of_Labour_Enforcement.xml', 'wb') as f:
            f.write(resp.content)

        tree = ET.parse('DLE_USA_Department_of_Labour_Enforcement.xml') 
        root = tree.getroot() 
        totalEntries = root.findall('./record')
        return len(totalEntries) > 0

    def extact_data(self, response):
        tree = ET.parse('DLE_USA_Department_of_Labour_Enforcement.xml') 
        root = tree.getroot() 
        totalEntries = root.findall('./record')
        for item in root.findall('./record'):
            inclusion_date = dateutil.parser.parse(item.find("issuanceDate").text)
            document_info = json.dumps({'Inspection Number': item.find("inspectionNumber").text})    
            remarks = "Initial Penalty : " + item.find("initialPenalty").text
            yield Entity({
                "name": item.find("employer").text,
                "category": "Group",
                "type": "SIP",
                "inclusion_date": inclusion_date,
                "city": item.find("city").text,
                "remarks": remarks
            })