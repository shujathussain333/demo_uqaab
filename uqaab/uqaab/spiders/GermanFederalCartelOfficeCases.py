#Author: Sharjeel Ali Shaukat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser


class GermanFederalCartelOfficeCases(UqaabBaseSpider):
    name = "german_federal_cartel_office_cases"

    start_urls = [
        'https://www.bundeskartellamt.de/SiteGlobals/Forms/Suche/EN/Entscheidungssuche_Formular.html?nn=3589950&gts=3598628_list%253Dcl2Categories_Entscheidungsart%252Basc&resultsPerPage=97&sortOrder=score+desc%2C+dateOfIssue_dt+desc&cl2Categories_Entscheidungsart=Bussgeldentscheidung+Untersagung+EntscheidungNach32cGWB+Abstellungsverfuegung+EinstellungDesVerfahrens',
    ]

    def structure_valid(self, response):
        data_rows = response.css('table tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table tr')
        data_rows = data_rows[1:]
        url = 'https://www.bundeskartellamt.de/'
        for row in data_rows:
            columns = row.css('td')
            name = columns[1].css('a::text').extract_first()
            link = columns[1].css('a::attr(href)').extract_first()
            # organization = columns[3].css('*::text').extract_first()
            inclusion_date = columns[2].css('*::text').extract_first()
            inclusion_date = dateutil.parser.parse(inclusion_date)
            remarks = "Decision: {}".format(';'.join(columns[4].css('li::text').extract()).strip())
            reference = "Reference: {}".format(columns[0].css('*::text').extract_first())
            link = "link: {}{}".format(url, link)
            remarks = "{} ; {} ; {}".format(reference, remarks, link)

            yield Entity({
                'name': name,
                'category': 'Unknown',
                'inclusion_date': inclusion_date,
                'remarks': remarks
            })
