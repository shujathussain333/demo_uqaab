# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json
import scrapy
import dateutil.parser

class CanadianSecurityAdministratorSanction(UqaabBaseSpider):
    
    name = 'canadian_security_administrator_sanctions'
    start_urls = ['https://www.securities-administrators.ca/disciplinedpersons.aspx?id=74']

    def structure_valid(self, response):
        data_rows = response.xpath("//table[@id='ctl00_bodyContent_gv_list']//tr[td]//a/@href")
        return len(data_rows) > 0

    def extact_data(self, response):
        details = response.xpath("//table[@id='ctl00_bodyContent_gv_list']//tr[td]//a/@href").extract()
        for href_id in details:
            href_complete = href_id.split("(new WebForm_PostBackOptions(",1)
            href_partial = href_complete[1].split(", """,1)
            event_target_quotes = href_partial[0]
            event_target = event_target_quotes.replace('"',"")
            view_state = response.xpath('//*[@id="__VIEWSTATE"]/@value').extract_first()
            event_validation = response.xpath('//*[@id="__EVENTVALIDATION"]/@value').extract_first() 
            formdata = {
                    "__EVENTTARGET": event_target,
                    "__EVENTARGUMENT": "",
                    "__VIEWSTATE": view_state,
                    "__EVENTVALIDATION": event_validation,
                }
            yield scrapy.FormRequest(url=self.start_urls[0], formdata=formdata, callback=self.extract_form_data)

    def extract_form_data(self, response):
        name_complete = response.xpath('//table[@class="pagerdiv"]//tr/td[2]/span/text()').extract_first()
        name = name_complete.replace(",","")
        date_of_order =   response.xpath("//table[@class='detail-title']//tr[./th/span='Date of Order or Settlement']/td//text()").extract_first()
        if date_of_order:    
            date_of_order = dateutil.parser.parse(date_of_order)        
        banned_untill = response.xpath("//table[@class='detail-title']//tr[./th/span='Banned Until']/td//text()").extract_first()
        if banned_untill:    
            banned_untill = dateutil.parser.parse(banned_untill)        
        sanctions =  response.xpath("//table[@class='detail-title']//tr[./th/span='Sanctions']/td/span//text()").extract()
        sanctions = " ".join(sanctions)
        payement_agreed_on = response.xpath("//table[@class='detail-title']//tr[./th/span='Payment Agreed/Ordered']/td/text()").extract_first()
        violations = response.xpath("//table[@class='detail-title']//tr[./th/span='Violations']/td/span//text()").extract()
        violations = " ".join(violations)
        supporting_documents = response.xpath("//table[@class='detail-title']//tr[./th/span='Supporting Documents']/td//a/@href").extract()
        supporting_documents = " ".join(supporting_documents)
        remarks = "Sanctions: {} , Payement_agrred_on: {} , Violations: {} , Supporting_document: {}".format(sanctions,payement_agreed_on,violations,supporting_documents)
        yield Entity({
            "category": "group",
            "name":name,
            "remarks":remarks,
            "exclusion_date":banned_untill,
            "inclusion_date":date_of_order,
            "type": "SAN"
        }) 

