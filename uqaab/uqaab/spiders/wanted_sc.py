import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class SixthcodeSpider(scrapy.Spider):
    name = 'wanted_sc'
    allowed_domains = ['www.sc.com.my']
    start_urls = ['https://www.sc.com.my/persons-wanted-by-the-sc/']

    def structure_valid(self, response):
        return True

    def parse(self, response):
        tables = response.xpath('//*[@class="tab_format tab_reset"]//tbody')
        Name_list = []
        Address_list = []
        for table in tables:
          rows = table.xpath('./tr')
          if len(rows) < 4:   
            for row in rows:
                Name = row.xpath('td//text()')[2].extract()
                Address = rows.xpath('td//text()')[3].extract()
                print('First two tables')
                print(Name)
                print(Address)
          else:
              for row in rows:
                if row.xpath(".//strong[1]/text()").extract_first() == 'Name':
                    print('Last tables')
                    Name = row.xpath('td//text()')[2].extract()
                    Name_list.append(Name)
                    print(Name)
                elif row.xpath(".//strong[1]/text()").extract_first() == 'Last known residential address':
                    Address = row.xpath('td//text()').extract()
                    Address = ' '.join(Address)
                    Address = Address.replace("Last known residential address", "")
                    Address = Address.replace("\n", "")
                    print(Address)
                    Address_list.append(Address)
                    
        print(Name_list)
        print(Address_list)
        for i in range(0 , len(Name_list)):
            yield {
                    'Name': Name_list[i],
                    'Address': Address_list[i]
                    }
