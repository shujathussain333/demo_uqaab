#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Esex_Police_Most_Wanted(UqaabBaseSpider):
    name = "esex_police_most_wanted"
    start_urls = ['https://www.essex.police.uk/news/wanted/?page=2']

    def structure_valid(self, response):
        data_divs = response.xpath('//section[@class="listing wrap listingNews"][1]/div[@class="listingList"]/article[@class="listingArticle"]/div[@class="listingArticleImageWrap"]')
        return len(data_divs) > 0

    def extact_data(self, response):
        pictureDiv = response.xpath('//section[@class="listing wrap listingNews"][1]/div[@class="listingList"]/article[@class="listingArticle"]/div[@class="listingArticleImageWrap"]')
        for picture in pictureDiv:
            next_page = picture.xpath('.//a//@href').extract_first()
            if next_page is not None:
                yield scrapy.Request(next_page, callback=self.extract_link)
            
    def extract_link(self, response):
        srcInitialAdd = "www.essex.police.uk/"
        data = response.xpath('.//section[@class="contentSpotlight clearfix"]//text()').extract()
        remarks = ' '.join(data)
        remarks = remarks.strip()
        remarks = ''.join(remarks.splitlines())
        remarks = ' '.join(remarks.split())
        remarks = "Summary: {}".format(remarks)
        src = response.xpath('//main[@id="mainContent"]')
        imageSrcWithoutCleanText = src.xpath('.//section//div//img/@src').extract_first()
        imageSrcWithoutCleanText = imageSrcWithoutCleanText[7:]
        imageSource = srcInitialAdd + imageSrcWithoutCleanText
        name = src.xpath('.//div[@class="mainHeaderMeta"]//h1//text()').extract_first()
        name = name.strip()
        yield Entity({
            "name": name,
            "category": "Individual",
            "type": "SIP",
            "remarks": remarks,
            "image": imageSource
        })