from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import requests
import pandas as pd
from bs4 import BeautifulSoup

class Baltimore_Police_Department(UqaabBaseSpider):
    name = 'baltimore_police_dept'
    start_urls = ['https://www.baltimorepolice.org/news/wanted-list']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        base_url = 'https://www.baltimorepolice.org'
        url = 'https://www.baltimorepolice.org/news/wanted-list'
        soup = BeautifulSoup(requests.get(url).text)
        records = soup.find_all('article', {'class': 'content clearfix row-fluid'})
        urls = [record.find_all('div', {'class': 'field-item even'})[0].a['href'] for record in records]

        final_list = []
        for i in range(len(urls)):
            print('fetching data ...')
            soup = BeautifulSoup(requests.get(base_url + urls[i]).text)
            fields = soup.find_all('div', {'class': 'field-label-inline'})
            final_dict = {}
            for field_ in fields:
                final_dict[
                    field_.find('div', {'class': 'field-label'}).get_text().replace(':', '').strip()] = field_.find(
                    'div', {'class': 'field-item even'}).get_text()
            final_list.append(final_dict)

        df = pd.DataFrame(final_list)

        remarks = ['Age', 'Eye Color', 'Hair Color', 'Height', 'Last Known Area', 'Race', 'Weight', 'Remarks']
        df['Remarks'] = ''
        for rem in remarks:
            df['Remarks'] = df['Remarks'] + ' , ' + rem + ' : ' + df[rem]
        df = df[['Remarks', 'Name', 'Offense']]
        df.columns = ['Remarks', 'Name', 'Category']

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'category': 'Individual',
                'type': "SIP"
            })