from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests

class Springfield_Scrapper(UqaabBaseSpider):
    name = 'spring_scrapper'
    start_urls = ['https://www.springfieldmo.gov/1701/Municipal-Warrants']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www1.springfieldmo.gov/warrants/Default.aspx'
        dfs = []
        for i in range(1, 18):
            print('Fething Data ....')
            dfs.append(pd.read_html(requests.post(url, data={'__EVENTARGUMENT': "Page$" + str(i)}).text)[1])

        for df in dfs:
            df.drop([0, 1], inplace=True, axis=0)
            final_df = pd.concat(dfs)
            final_df.columns = ['Name', 'Age', 'Warrant Type', 'Category', 'Bond', 'Warrant Number',
                                'Release Condition']
            ex_columns = ['Warrant Type', 'Bond', 'Warrant Number', 'Release Condition']
            final_df['Remarks'] = ''

            for col in ex_columns:
                final_df['Remarks'] = final_df['Remarks'] + col + '  :  ' + final_df[col].fillna('') + ','

            final_df.drop(ex_columns, axis=1, inplace=True)

            for index, row in final_df.iterrows():
                yield Entity({
                    'name': row.Name,
                    'remarks': row.Remarks,
                    'category': 'Group',
                    'type': "SIP"
                })