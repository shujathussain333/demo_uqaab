#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import scrapy

class Portugal_Central_Bank(UqaabBaseSpider):
	name = 'portugal_central_bank'
	start_urls = ['https://www.bportugal.pt/en/atividade-nao-autorizada?mlid=2867']

	def structure_valid(self, response):
		dataRows = response.xpath('//div[@class="view-content clearfix"]/div')
		return len(dataRows) > 0

	def extact_data(self, response):
		dataRows = response.xpath('//div[@class="view-content clearfix"]/div')
		for row in dataRows:
			name = row.xpath('.//div[@class="views-field views-field-title"]//a//text()').extract_first()
			date = row.xpath('.//div[@class="views-field views-field-field-data-limite"]//span//text()').extract_first()
			date = dateutil.parser.parse(date)
			yield Entity({
				"name": name,
	            "category": "Group",
	            "type": "SAN",
	            "inclusion_date": date
			})
		baseUrl = "https://www.bportugal.pt"
		nextUrl = response.xpath('//li[@class="pager__item"]//a//@href').extract_first()
		if(nextUrl):
			nextUrl = baseUrl + nextUrl
			yield scrapy.Request(nextUrl, callback=self.extact_data)