import datetime
import re
import scrapy
import requests
import numpy as np
import io
import json
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
from operator import itemgetter


class Canada_Financial_Spider(UqaabBaseSpider):
    name = 'canada_financial_scrapper_2'
    start_urls = ['http://www.osfi-bsif.gc.ca/Eng/Docs/ko_indstld.xlsx',]

    def structure_valid(self, response):

        format = ['ID #3', 'Last Name 2', 'First Name2', 'Second Name2', 'Third Name2',
                  'Fourth Name2', 'POB', 'ALT POB', 'DOB4', 'Alt. DOB', 'Alt. DOB',
                  'Alt. DOB', 'Nationality', 'Alt. Nationality', 'Alt. Nationality',
                  'Title/Designation/Passport Number/National Identification Number/Address/Other Info.',
                  'Date of Designation5']

        df = self.create_dataframe(response)
        columns = df.columns
        return set(format) == set(columns)

    def extact_data(self, response):
        df = self.create_dataframe(response)
        df[['First Name2', 'Second Name2', 'Third Name2', 'Fourth Name2', 'Last Name 2',
            'Title/Designation/Passport Number/National Identification Number/Address/Other Info.']] = df[
            ['First Name2', 'Second Name2', 'Third Name2', 'Fourth Name2', 'Last Name 2',
             'Title/Designation/Passport Number/National Identification Number/Address/Other Info.']].fillna(value='')

        for index, row in df.iterrows():
            # to check first Name and Last Name are not empty if empty excel sheet contains empty columns
            if row["First Name2"] == '' and row["Last Name 2"] == '':
                break

            # to check ids does not contains any floating point
            if float(row["ID #3"]).is_integer():
                id = row["ID #3"]
                next_id = id + 1

                df2 = df[(df['ID #3'] > id) & (df['ID #3'] < next_id)]
                df2 = df2[['First Name2', 'Second Name2', 'Third Name2', 'Fourth Name2', 'Last Name 2']]
                df2 = df2.replace(np.nan, '', regex=True)
                df2 = df2.add([' '] * (df2.columns.size - 1) + ['']).sum(axis=1)

                alias_info = json.dumps({'info': None})
                aliases = df2.tolist()
                alias = [{'aka': re.sub('\s+', ' ', alias)} for alias in aliases]

                if alias:
                    alias_info = json.dumps({'info': alias})

                name = row["First Name2"] + row["Second Name2"] + row["Third Name2"] + row["Fourth Name2"] + ' ' + row[
                    "Last Name 2"]

                if type(row["DOB4"]) == float and type(row["POB"]) == float:
                    dobs = None
                elif type(row["DOB4"]) != float and type(row["POB"]) == float:
                    dobs = {'DOB': str(row["DOB4"]), 'POB': None}
                elif type(row["DOB4"]) == float and type(row["POB"]) != float:
                    dobs = {'DOB': None, 'POB': str(row["POB"])}
                else:
                    dobs = {'DOB': str(row["DOB4"]), 'POB': str(row["POB"])}

                if dobs:
                    dob_info = json.dumps({'info': [dobs]})
                else:
                    dob_info = json.dumps({'info': None})

                nationality = row["Nationality"] = '' if type(row["Nationality"]) == float else row["Nationality"]

                information = row[
                    "Title/Designation/Passport Number/National Identification Number/Address/Other Info."]

                information = information.replace("Passpost", "Passport")

                title_index, designation_index, passport_index, national_identity_index, address_index, other_info_index = map(
                    information.lower().find,
                    ['title', 'designation', 'passport', 'national identification', 'address', 'other info'])
                extra_values = [("title", title_index), ("designation", designation_index),
                                ("passport", passport_index),
                                ("national identification", national_identity_index), ("address", address_index),
                                ("other information", other_info_index)]
                extra_values = [x for x in sorted(extra_values, key=itemgetter(1))]
                extra_values = [i for i in extra_values if i[1] > -1]

                designation_info = json.dumps({'info': None})
                title_info = json.dumps({'info': None})
                document_info = json.dumps({'document_info': None})
                address = []
                document = []

                for index, item in enumerate(extra_values):

                    if "title" in item[0]:
                        try:
                            titles = information[item[1]:extra_values[index + 1][1]].split(":")[-1]
                        except:
                            titles = information[item[1]:].split(":")[-1]
                        titles = titles.strip().split(";")
                        title = [{'title': title} for title in titles]
                        title_info = json.dumps({'info': title})
                    if "designation" in item[0]:
                        try:
                            designations = information[item[1]:extra_values[index + 1][1]].split(":")[-1]
                        except:
                            designations = information[item[1]:].split(":")[-1]

                        designations = designations.strip().split(";")
                        designation = [{'designation': designation} for designation in designations]
                        designation_info = json.dumps({'info': designation})
                    if "other info" in item[0]:
                        try:
                            remarks = information[item[1]:extra_values[index + 1][1]].split(":")[-1]
                        except:
                            remarks = information[item[1]:].split(":")[-1]
                    if "address" in item[0]:
                        try:
                            address.append(information[item[1]:extra_values[index + 1][1]].split(":")[-1])
                        except:
                            address.append(information[item[1]:].split(":")[-1])
                    if "passport" in item[0]:
                        try:
                            passport_details = information[item[1]:extra_values[index + 1][1]]
                        except:
                            passport_details = information[item[1]:]

                        passport_details = re.findall(r'[0-9]{5,22}', passport_details)
                        for number in passport_details:
                            document.append({'type': "passport",
                                             'id': number})
                    if "national identification" in item[0]:
                        try:
                            id_details = information[item[1]:extra_values[index + 1][1]].split(":")[-1]
                        except:
                            id_details = information[item[1]:].split(":")[-1]
                        id_details = re.findall(r'\d+', id_details)
                        for number in id_details:
                            document.append({'type': "national identification no",
                                             'id': number})

                if document:
                    document_info = json.dumps({'document_info': document})

                yield Entity({
                    "category": "Individual",
                    "name": name,
                    'date_of_birth': dob_info,
                    'nationality': nationality,
                    "address": address,
                    "document": document_info,
                    "title": title_info,
                    "remarks": remarks,
                    "designation": designation_info,
                    "type": "SAN",
                    "aka": alias_info
                })

    @staticmethod
    def create_dataframe(response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0, header=None, index=None)

        df.drop(df.index[:5], inplace=True)

        # grab the first row for the header
        new_header = df.iloc[0]
        # take the data less the header row
        df = df[1:]
        # set the header row as the df header
        df.columns = new_header

        return df
