#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
import json
from scrapy import Request
import requests

class SouthYoukshireWantedSpider(UqaabBaseSpider):

    name = 'south_youkshire_wanted'
    start_urls = ['https://api.flickr.com/services/rest?per_page=100&page=1&extras=can_addmeta,can_comment,can_download,can_share,contact,count_comments,count_faves,count_views,date_taken,date_upload,description,icon_urls_deep,isfavorite,ispro,license,media,needs_interstitial,owner_name,owner_datecreate,path_alias,realname,rotation,safety_level,secret_k,secret_h,url_c,url_f,url_h,url_k,url_l,url_m,url_n,url_o,url_q,url_s,url_sq,url_t,url_z,visibility,visibility_source,o_dims,publiceditability&get_user_info=1&jump_to=&user_id=142510817@N02&view_as=use_pref&sort=use_pref&viewerNSID=&method=flickr.people.getPhotos&csrf=&api_key=d77054dec2801ef99802ade4dba98523&format=json&hermes=1&hermesClient=1&reqId=72f0767e&nojsoncallback=1']


    def structure_valid(self, response):
        return True  

    def extact_data(self,response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["photos"]
        data_to_scrap=actual_data['photo']
        for each_person_detail in data_to_scrap:
            ind_string=each_person_detail['title']
            k="-"
            l=":"
            if k in ind_string:
                part_string=re.split('-',ind_string)

                if (part_string[0]=="Wanted" or part_string[0]=="WANTED"):
                    name = part_string[0]
                    description = each_person_detail['description']['_content']
                    image=each_person_detail['url_m']
                else:
                    name = part_string[1]
                    description = each_person_detail['description']['_content']
                    image=each_person_detail['url_m']

                yield Entity({
                "category": "Individual",
                "name":name,
                "remarks":description,
                "image":image,
                "type": "SIP"
                })            
                
            elif l in ind_string:
                part_string=re.split(':',ind_string)
                if (part_string[0]=="Wanted" or part_string[0]=="WANTED"):
                    name = part_string[0]
                    description = each_person_detail['description']['_content']
                    image=each_person_detail['url_m']
                
                else:
                    name = part_string[1]
                    description = each_person_detail['description']['_content'] 
                    image=each_person_detail['url_m']
                yield Entity({
                "category": "Individual",
                "name":name,
                "remarks":description,
                "image":image,
                "type": "SIP"
                })                                     




        