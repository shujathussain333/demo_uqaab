#Edited By: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import dateutil.parser

class SGPMASSpider(UqaabBaseSpider):
    name = "sgp_mas"
    start_urls = ['https://www.mas.gov.sg/api/v1/ialsearch?json.nl=map&wt=json&rows=10000&q=*:*&sort=date_dt+desc&start=0']

    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        jsonresponse=json.loads(jsonresponse)
        actual_data=jsonresponse["response"]['docs']
        return len(actual_data) > 0

    @staticmethod
    def splitLines(rawstring):
        returenStr = ''.join(rawstring.splitlines())
        return returenStr

    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        jsonresponse=json.loads(jsonresponse)
        actual_data=jsonresponse["response"]['docs']
        for data in actual_data:
            name = data['unregulatedpersons_t']
            name = ''.join(name)
            addressArr = []
            address = self.splitLines(data['address_s'])
            inclusionDate = ""
            try:
                inclusionDate = dateutil.parser.parse(data['date_dt'])
            except (ValueError, KeyError, TypeError):
                inclusionDate = ""

            website = self.splitLines(data['website_s'])
            email = self.splitLines(data['email_s'])
            phoneNumber = self.splitLines(data['phonenumber_s'])
            remarks = "Website: {} ,Email: {} ,Phone Number: {}".format(website, email, phoneNumber)
            
            formerName = data['formername_s']
            alternativeName = data['alternativename_s']         
            alias_info = json.dumps({'info': None})
            aliases = []
            if(formerName):
                aliases.append(formerName)
            if(alternativeName):
                aliases.append(alternativeName)
            
            if(aliases is not None):
                alias = [{'aka': alias} for alias in aliases]
                alias_info = json.dumps({'info': alias})
            yield({
                "name" : name,
                "address": address,
                "type" : "SAN",
                "category" : "Group",
                "inclusion_date" : inclusionDate,
                "aka" : alias_info,
                "remarks" : remarks
                })