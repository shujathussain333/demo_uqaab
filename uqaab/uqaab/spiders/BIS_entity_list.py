from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser


class BisEntityListSpider(UqaabBaseSpider):
    name = "bis_entity_list"
    allowed_domains = ["https://www.ecfr.gov"]

    start_urls = [
    'https://www.ecfr.gov/cgi-bin/retrieveECFR?gp=1&SID=9ae4a21068f2bd41d4a5aee843b63ef1&ty=HTML&h=L&n=15y2.1.3.4.28&r=PART#ap15.2.744_122.4',
    ]

    table_path = "//*[@id='wrapper']/table/tbody/tr/td[3]/div[39]/div"

    def structure_valid(self, response):
        return len(response.xpath(self.table_path)) > 0    

    def extact_data(self, response):
        table = response.xpath(self.table_path).css('table')
        table_rows = table.xpath('tr')
        table_rows = table_rows[1:]

        country = ''
        for row in table_rows:
            name = city = address = remarks = inclusion_date = temp = ''
            aliases = []
            temp = row.xpath('td[1]/text()').extract()
            if len(temp) > 0:
                if len(temp[0].strip()) > 0:
                    country = self.get_country_code(temp[0].strip())

            temp = row.xpath('td[2]').css('::text').extract()
            if len(temp) > 0:

                for item in temp:
                    aliases = [item.strip() for item in temp if item.startswith("—")]

                temp = ''.join(temp)
                temp = temp.split(",")
                name = "".join(temp[:1]).strip()


                if len(aliases) > 0:
                    address = ','.join(temp).split('\xa0\xa0\xa0')[-1]
                else:
                    address = ','.join(temp[1:]).strip()

                temp_add = address.split(' and ')
                cities = []
                addresses = []
                countries = []
                for item in temp_add:
                    items = item.split(",")
                    if len(items) > 1: 
                        city = items[-2].strip()
                        temp_address = ', '.join(items[:-2])
                        if temp_address == '':
                            temp_address = None
                        cities.append(city)
                        addresses.append(temp_address)
                        countries.append(country)
                    else:
                        addresses.append(item)
                        cities.append(None)
                        countries.append(country)
                
            temp = row.xpath('td[4]/text()').extract()
            if len(temp) > 0:
                remarks = temp[0].strip()

            temp = row.xpath('td[5]/text()').extract()
            if len(temp) > 0:
                inclusion_date = "".join(temp).split(",")[-1].strip()
                inclusion_date = dateparser.parse(inclusion_date)
            else:
                inclusion_date = None

            category = 'Individual'
            if 'company' in name.lower() or 'llc' in name.lower():
                category = 'Group'

            yield Entity({
                'country': countries,
                'city': cities,
                'address': addresses,
                'name': name,
                'remarks': remarks,
                'aka': aliases,
                'inclusion_date': inclusion_date,
                'category':category,
                'type':'SIP'

            })