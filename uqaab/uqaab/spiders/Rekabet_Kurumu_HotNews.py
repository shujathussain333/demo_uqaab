#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request

class RekabetKrumuHotNews(UqaabBaseSpider):


    custom_settings = {
        'DOWNLOAD_DELAY' : 0.25
    }

    name = 'rekabet_krumu_hot_news'
    start_urls = ['https://www.rekabet.gov.tr/en/Gunceller',]

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@id='icerik01']//a[@style]")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.rekabet.gov.tr"
        case_urls = response.xpath("//div[@id='icerik01']//a[@style]/@href").extract()
        for case in case_urls:
            absolute_url = base_url + case
            yield Request(absolute_url, callback=self.parse_data)     
        relative_next_url = response.xpath("//a[@rel = 'next']/@href").extract_first()
        if relative_next_url:
            absolute_next_url = base_url + relative_next_url
            yield Request(absolute_next_url, callback=self.extact_data)

    def parse_data(self,response):
        name = response.xpath("//div[@id = 'icerik01']//h4/text()").extract_first()
        if name is None:
            name = "anaymous"
        remarks = response.xpath("//div[@id = 'icerik01']//p[@style]/text()").extract()
        remarks = ''.join(remarks)
        yield Entity({
            "category": "Group",
            "name":name,
            "remarks":remarks,
            "type": "SIP"
        })    