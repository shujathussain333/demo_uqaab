from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd

class Saudi_Most_Wanted_Terrorists(UqaabBaseSpider):
    name = 'saudi_most_wanted'
    start_urls = ['https://en.wikipedia.org/wiki/Saudi_list_of_most-wanted_suspected_terrorists']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        saudi_dfs = pd.read_html('https://en.wikipedia.org/wiki/Saudi_list_of_most-wanted_suspected_terrorists')
        saudi_dfs[0].drop([0, 2], axis=1, inplace=True)
        saudi_dfs[0].columns = ['Name', 'Remarks']
        saudi_dfs[1].drop('rank', axis=1, inplace=True)
        saudi_dfs[1].columns = ['Name', 'Nationality']
        saudi_dfs[2].columns = ['Name', 'DOInclusion', 'Status', 'Remarks']
        saudi_dfs[3].drop(['ISN', 'Rank'], axis=1, inplace=True)
        saudi_dfs[3].columns = ['Age', 'Name', 'Remarks']
        saudi_dfs[4].drop(['Arabic', 'Notes', 'Unnamed: 5'], axis=1, inplace=True)
        saudi_dfs[4].columns = ['Name', 'Nationality', 'Remarks']
        final_saudi_df = pd.concat(saudi_dfs[0:5], sort=False)
        final_saudi_df.reset_index(inplace=True, drop=True)

        for index, row in final_saudi_df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'nationality': row.Nationality,
                'category': 'Group',
                'type': "SIP"
            })