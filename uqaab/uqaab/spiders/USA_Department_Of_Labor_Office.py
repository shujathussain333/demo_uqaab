#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class USA_Department_Of_Labor_Office(UqaabBaseSpider):
    name = "usa_department_of_labor_office"
    start_urls = ['https://www.dol.gov/olms/regs/compliance/enforce_2019.htm']
    
    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="large-12 columns"]//p')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="large-12 columns"]//p')
        for data in data_rows:
        	date = data.xpath('.//strong//text()').extract_first()
        	if(date):
	        	if("On" in date):	
		        	name = data.xpath('.//text()').extract()
		        	if("On" not in name[0]):
		        		del(name[0])
		        	date = date.replace("On ","")
		        	date = ' '.join(date.split())
		        	if(date[-1] == ','):
		        		date = ''.join(date.split(','))
		        	date = dateutil.parser.parse(date)
		        	name = name[1]
		        	name = name.split(',')
		        	name = name[1:3]
		        	name = ''.join(name)
		        	name = name.strip()
		        	name = name.split(' ')
		        	name = name[:3]
		        	name = ' '.join(name)
		        	if(name != ""):
			        	if(("in the" not in name)):
			        		yield Entity({
			        			"name":name,
			        			"type":"SAN",
			        			"category":"Individual",
			        			"inclusion_date":date
			        			})