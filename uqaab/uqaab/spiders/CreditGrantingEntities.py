# Author = Barkat Khan

from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 
import tabula

class  CreditGrantingEntities(UqaabBaseSpider):
    name = 'banking_cge'
    start_urls = [ 
        "http://www.bsp.gov.ph/banking/cge.pdf"
    ]
    
    def structure_valid(self, response):
        url = response.request.url
        data_rows = tabula.read_pdf(url)
        return len(data_rows) > 0
   
    def extact_data(self, response):
        url = response.request.url
        df = tabula.read_pdf(url, encoding="utf-8", lattice=True, pandas_options={"skiprows":1})
        
        df.loc[-1] = df.columns
        df=df.replace("\r", " ", regex=True)
        cols = ["S.NO","Name_of_Entity", "Registration_Number", "Date_Registered", "Address"]
        df.columns = cols
        
        E = Entity()
        
        for row, col in df.iterrows():
            document = []
            E['name'] = df.loc[row, "Name_of_Entity"]
            E['inclusion_date']= self.extractDate(df.loc[row, "Date_Registered"])
            E['address'] = df.loc[row, "Address"]

            document.append({'type': 'Registration Number',
                            'id': df.loc[row, "Registration_Number"]})
            E['document'] =  json.dumps({'document_info': document}) 
            E['category'] = 'Group' 
            E['type'] = 'SIP'
            yield E
    
    @staticmethod
    def extractDate(entity_date):
        try:
            return datetime.strptime(entity_date, "%d-%b-%y")
        except ValueError:
            return None