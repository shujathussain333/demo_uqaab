#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json

class ATFmostWanted(UqaabBaseSpider):

    name = 'atf_most_wanted'
    start_urls = ['https://www.atf.gov/most-wanted']

    def structure_valid(self, response):
        data_rows = response.xpath('//ul[@class="slides"]/li/div[@class="views-field views-field-field-image-most-wanted"]//a')
        return len(data_rows) > 0
 
    def extact_data(self,response):
        base_url = "https://www.atf.gov"
        page_urls = response.xpath('//ul[@class="slides"]/li/div[@class="views-field views-field-field-image-most-wanted"]//a/@href').extract()
        for page in page_urls:
            absolute_url = base_url + page
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        name = response.xpath('//div[@class="group-main-content field-group-div"]/h3//text()').extract()
        name = " ".join(name)
        alaises = response.xpath('//div[@class="field field--name-field-aliases field--type-text field--label-inline clearfix"]//div[@class="field__item even"]//text()').extract_first()
        alaises = alaises.split(',')
        alias = [{'aka': alias} for alias in alaises]
        alias_info = json.dumps({'info': alias})
        offence = response.xpath('//div[@class = "field field--name-field-offense field--type-text field--label-inline clearfix"]//div[@class = "field__items"]//text()').extract_first()
        date_of_birth = response.xpath('//span[@class="date-display-single"]//text()').extract_first()
        place_of_birth = response.xpath('//div[@class="field field--name-field-place-of-birth field--type-text field--label-inline clearfix"]//div[@class="field__item even"]//text()').extract_first()
        dobs = {'DOB': date_of_birth, 'POB': place_of_birth}
        dob_info = json.dumps({'info': [dobs]})
        remarks = response.xpath('//div[@class = "field field--name-field-remarks field--type-text-long field--label-above"]//div[@class = "field__item even"]//text()').extract()
        remarks = " ".join(remarks)
        atf_case_number = response.xpath('//div[@class = "field field--name-field-atf-case-number field--type-text field--label-inline clearfix"]//div[@class = "field__item even"]//text()').extract_first()
        fbi_number = response.xpath('//div[@class = "field field--name-field-fbi-number field--type-text field--label-inline clearfix"]//div[@class = "field__item even"]//text()').extract_first()
        nic_number = response.xpath('//div[@class = "field field--name-field-nic-number field--type-text field--label-inline clearfix"]//div[@class = "field__item even"]//text()').extract_first()
        federal_warant_number = response.xpath('//div[@class = "field field--name-field-federal-warrant-number field--type-text field--label-inline clearfix"]//div[@class = "field__item even"]//text()').extract_first()
        document_info = json.dumps({'document_info': None})
        document = []  
        document.append({'type': "NIC Number", 'id': nic_number})
        document.append({'type': "federal Warrant Number", 'id': federal_warant_number})
        document.append({'type': "atf case number", 'id': atf_case_number})
        document.append({'type': "FBI Number", 'id': fbi_number})
        document_info = json.dumps({'document_info': document}) 
        image = response.xpath('//div[@class="group-main-content field-group-div"]//img/@src').extract_first()
        yield Entity({
            "category": "Individual",
            "name":name,
            "date_of_birth":dob_info,
            "remarks":remarks,
            "image":image,
            "document":document_info,
            "aka":alias_info,
            "type": "SIP"
        })       