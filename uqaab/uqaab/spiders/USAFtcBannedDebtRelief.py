#Author : Sharjeel Ali Shaukat
import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class UsaFtcBannedDebtRelief(UqaabBaseSpider):
    name = "usa_ftc_banned_debt_relief"

    start_urls = [
        'https://www.ftc.gov/enforcement/cases-proceedings/banned-mortgage-relief-debt-relief-companies-people',
    ]

    company_words = ['llc', 'inc', 'group']
    aka_words = ['d/b/a', 'a/k/a']

    def structure_valid(self, response):
        data_cols = response.css('table tr')
        return len(data_cols) > 0

    def extact_data(self, response):
        data_rows = response.css('table tr')
        for row in data_rows[1:]:
            columns = row.css('p')
            links = row.css('td a::attr(href)').extract()
            name = columns.css('*::text').extract_first()
            if name is not None:
                if any(word in name.lower() for word in self.company_words):
                    category = 'Group'
                else:
                    category = 'Individual'

                alias = None

                if any(word in name.lower() for word in self.aka_words):
                    list = re.split(r'{}'.format('|'.join(self.aka_words)), name)
                    alias = list[-1]

                    no_commas = alias.count(',')
                    if ';' in alias:
                        aliases = alias.split(';')
                        alias = [{'aka': alias.replace('and', '')} for alias in aliases]
                    elif ',' in alias and no_commas > 1:
                        aliases = alias.split(',')
                        alias = [{'aka': alias.replace('and', '')} for alias in aliases]
                    elif 'and' in alias:
                        aliases = alias.split('and')
                        alias = [{'aka': alias} for alias in aliases]
                    else:
                        alias = [{'aka': alias}]

                if alias:
                    alias_info = json.dumps({'info': alias})
                else:
                    alias_info = json.dumps({'info': None})

                try:
                    view_case = "View Case: {}".format(links[0])
                    view_order = "View Order: {}".format(links[1])
                    remarks = '{} ; {}'.format(view_case,view_order)
                except IndexError:
                    remarks = None


                yield Entity({
                    "category": category,
                    "name": name,
                    "remarks": remarks,
                    "type": "SIP",
                    "aka": alias_info
                })