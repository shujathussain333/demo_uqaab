# -*- coding: utf-8 -*-
import scrapy
import tabula
import io
import json
import numpy as np
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class NAB_PROC_OFFENDERS(UqaabBaseSpider):
    name = 'nab_proc_off_spider'
    start_urls = ["http://www.nab.gov.pk/Downloads/PO.pdf",]

    def structure_valid(self, response):
        return True

    def extact_data(self, response):

        url = "http://www.nab.gov.pk/Downloads/PO.pdf"
        nab_off = tabula.read_pdf(url, encoding="latin1", lattice=True, multiple_tables=True,pages="all")
        cols = []

        num_cols = len(nab_off[0].columns)

        total_len = len(nab_off)

        for i in range(num_cols):
            cols.append(nab_off[0][i][0])

        df = pd.DataFrame()

        for page in range(1, total_len, 2):
            df = pd.concat([df, nab_off[page]])

        cols = [col.replace("\r", " ") for col in cols]

        df.columns = cols

        df = df.replace("\r", " ", regex=True)
        
        print("==========================")
        print(df.shape)
        
        print("==========================")
        print(df.columns)
        
        
        for index, row in df.iterrows():

            name = self.extract_name(row["Names of Accused"])
            remarks = self.extract_remarks(row["Ref: No / State v/s"])
            designation = self.extract_designation(row["Designation of accused"])
            address = self.extract_address(row["Address as per CNIC"])
            document = self.extract_cnic(row["CNIC No."])
            
            yield Entity({
                "name" : name,
                "remarks": remarks,
                "category": CATEGORY_OPTIONS[0],
                "designation": [designation],
                "address": address,
                "country": "PK",
                "document" : document,
                "type" : "San"
            })

    @staticmethod
    def extract_name(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

            
    @staticmethod
    def extract_remarks(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

    @staticmethod
    def extract_address(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

    @staticmethod
    def extract_designation(row):
        try:
            if row is np.nan:
                row = None
            return row

        except Exception:
            return None

    @staticmethod
    def extract_cnic(row):
        doc_list = []
        document_info = {}
        if row is np.nan or row == "-":
            doc_list = None
        else:        
            document_info["type"] = "CNIC"
            document_info["id"] = row
            doc_list.append(document_info)
        return json.dumps({'document_info': doc_list})