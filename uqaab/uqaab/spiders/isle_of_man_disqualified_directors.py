#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import re 

class IsleOfMandisqualifiedDirectors(UqaabBaseSpider):

    name = 'isle_of_man_disqualified_directors'
    start_urls = ['https://www.iomfsa.im/enforcement/disqualified-directors/',]

    def structure_valid(self, response):
        data_rows = response.xpath("//section[@class='accordion-item']")
        return len(data_rows) > 0

    def extact_data(self,response):
        dis_directors = response.xpath("//section[@class='accordion-item']//div[@class='rte']")
        for director in dis_directors:
            name = director.xpath("p[./strong='Name:' or ./strong = 'Name: ']/text()").extract_first()
            date_of_birth = director.xpath("p[./strong='Date of Birth: ']/text()").extract_first()
            if date_of_birth is None:
                date_of_birth = director.xpath("p[3]/text()").extract_first()
            address = director.xpath("p[./strong='Address (at date of disqualification): ']/text()").extract_first()
            period_of_disqualification = director.xpath("p[./strong='Period of Disqualification: ']/text()").extract_first()
            inclusion_date = director.xpath("p[./strong='Dates of Disqualification: ']/text()").extract_first()
            if inclusion_date is None:
                inclusion_date = director.xpath("p[./strong='Period of Disqualification: ']/following::p[1]/strong/text()").extract_first()     
            if inclusion_date:    
                if "From" in inclusion_date and "to" in inclusion_date:
                    seperable_date = re.search('From(.+?)to', inclusion_date)
                    inclusion_date = seperable_date.group(1)
                elif "From" in inclusion_date:
                    inclusion_date = inclusion_date.split("From")[1] 
                elif "to" in inclusion_date:
                    inclusion_date = inclusion_date.split("to")[0]    
                inclusion_date = inclusion_date.strip()
                inclusion_date = dateutil.parser.parse(inclusion_date)                                
            remark = director.xpath("p[./strong='Notes:']/text()").extract()
            remark = ' '.join(remark)
            yield Entity({
                "category": "Individual",
                "inclusion_date":inclusion_date,
                "address":address,
                "name":name,
        		"remarks":remark,	
        		"type": "SIP"
        	})