# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class UnSomaliaSpider(UqaabBaseSpider):
    name = 'UN-Somalia'
    allowed_domains = ['https://scsanctions.un.org/somalia/']
    start_urls = ['http://scsanctions.un.org/somalia/']

    def structure_valid(self, response):
        return 100    

    def extact_data(self, response):

        soup = BeautifulSoup(response.body, "lxml")
        individual = soup.findAll('individual')

        for i in range(0, len(individual)):
    
            category = 'Individual'
            first_name = ''
            second_name = ''
            third_name = ''
            fourth_name = ''
            

            if individual[i].find('first_name') is not None:
                first_name = individual[i].find('first_name').text 
               
            if individual[i].find('second_name') is not None:
                second_name = individual[i].find('second_name').text
                
            if individual[i].find('third_name') is not None:
                third_name = individual[i].find('third_name').text
                
            if individual[i].find('fourth_name') is not None:
                fourth_name = individual[i].find('fourth_name').text
                

            nationality = None
            remarks = None
            inclusion_date = None
            list_name = None
            sanction_type = None


            if individual[i].find('nationality') is not None: 
                nationality = individual[i].find('nationality').value.text
            
                
            if individual[i].find('comments1') is not None:
                remarks = individual[i].find('comments1').text
        
                
                
            if individual[i].find('listed_on') is not None:     
                inclusion_date = individual[i].find('listed_on').text
                
                
            if individual[i].find('un_list_type') is not None:
                list_name = 'UN ' + individual[i].find('un_list_type').text
                
                
                
            if individual[i].find('list_type') is not None:
                sanction_type = individual[i].find('list_type').value.text  + ' : ' + individual[i].find('un_list_type').text
            

            individual_address = individual[i].findAll('individual_address')
            
            for addr in range(0, len(individual_address)):
                
                address = []
                city = []
                country = []
            
                if individual_address[addr].find("street") is not None:
                    address.append(individual_address[addr].find("street").text)

                if individual_address[addr].find("city") is not None:
                    city.append(individual_address[addr].find("city").text)

                if individual_address[addr].find("country") is not None:
                    country.append(self.get_country_code(individual_address[addr].find("country").text))

            individual_dob = individual[i].findAll('INDIVIDUAL_DATE_OF_BIRTH'.lower())
            individual_pob = individual[i].findAll('INDIVIDUAL_PLACE_OF_BIRTH'.lower())

            dob_info = []
            pob_info = []

            for dob in range(0, len(individual_dob)):
                birth_year = individual_dob[dob].find("year")
                if birth_year is not None:
                    dob_info.append(birth_year.text)
                
                birth_date = individual_dob[dob].find("date")
                if birth_date is not None:
                    dob_info.append(birth_date.text)
            
            for pob in range(0, len(individual_pob)):                
                birth_city = individual_pob[pob].find("city")
                if birth_city is not None:
                    pob_info.append(birth_city.text)
                
                birth_country = individual_pob[pob].find("country")
                if birth_country is not None:
                    pob_info.append(birth_country.text)

            if len(dob_info) > len(pob_info):
                difference = len(dob_info) - len(pob_info)
                pob_info += [None] * difference
            elif len(dob_info) < len(pob_info):
                difference = len(pob_info) - len(dob_info)
                dob_info += [None] * difference

            dobs = [{'DOB': item[0], 'POB': item[1]} for item in zip(dob_info, pob_info) if item[0] or item[1]]
            
            document_info = []
            documents = individual[i].findAll("INDIVIDUAL_DOCUMENT".lower())
            
            for doc in range(0, len(documents)):
                
                doc_type = documents[doc].find("type_of_document")
                if doc_type is not None:
                    doc_type = doc_type.text
                
                doc_number = documents[doc].find("number")
                if doc_number is not None:
                    doc_number = doc_number.text
                
                doc_date_of_issue = documents[doc].find("DATE_OF_ISSUE".lower())
                if doc_date_of_issue is not None:
                    doc_date_of_issue = doc_date_of_issue.text
                    
                doc_note = documents[doc].find("note")
                if doc_note is not None:
                    doc_note = doc_note.text

                if doc_type is not None:
                    document_info.append({
                        "type" : doc_type,
                        "id": doc_number,
                        "date_of_issue": doc_date_of_issue,
                        "note" : doc_note
                    })
                
            alias = individual[i].findAll('individual_alias')
            aka = []
            for j in range(0, len(alias)):
                if alias[j].find('alias_name') is not None:
                    aka.append({
                        'aka': alias[j].find('alias_name').text,
                    })
        

            name = " ".join([first_name, second_name, third_name, fourth_name]).strip()
            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})

            if dobs:
                dob_info = json.dumps({'info': dobs})
            else:
                dob_info = json.dumps({'info': None})

            aka = json.dumps({'info': aka})

            yield Entity({
                'category': category,
                'name': name,
                'aka' : aka, 
                'nationality': nationality, 
                'inclusion_date': self.string_to_date(inclusion_date),
                'remarks': remarks,
                'type' : "San",
                'address': address or None,
                'city': city or None,
                'country': country or None,
                'date_of_birth' : dob_info,
                'document' : document_info
            })

     

        entity = soup.findAll('entity')

        for e in range(0, len(entity)):
    
            print("===========ENTITY " + str(e) + " =====================")
            
            ent_name = None
            if entity[e].find('first_name') is not None:
                ent_name = entity[e].find('first_name').text
    
                
            ent_inclusion_date = None
            if entity[e].find('listed_on') is not None:     
                ent_inclusion_date = entity[e].find('listed_on').text
        

            ent_remarks = None
            if entity[e].find('comments1') is not None:
                ent_remarks = entity[e].find('comments1').text


                
            ent_list_name = None
            if entity[e].find('un_list_type') is not None:
                ent_list_name = 'UN ' + entity[e].find('un_list_type').text

                
            ent_list_type = None
            if entity[e].find('list_type') is not None:
                ent_list_type = entity[e].find('list_type').value.text  + ' : ' + entity[e].find('un_list_type').text
        
                
            ent_addresses = entity[e].findAll("ENTITY_ADDRESS".lower())
            
            for ent_addr in range(0, len(ent_addresses)):
                
                ent_address = []
                ent_city = []
                ent_country = []
            
                if ent_addresses[ent_addr].find("street") is not None:
                    ent_address.append(ent_addresses[ent_addr].find("street").text)

                if ent_addresses[ent_addr].find("city") is not None:
                    ent_city.append(ent_addresses[ent_addr].find("city").text)

                if ent_addresses[ent_addr].find("country") is not None:
                    ent_country.append(self.get_country_code(ent_addresses[ent_addr].find("country").text))
                    
            all_entity_alias = entity[e].findAll('entity_alias')
            ent_alias = []
            for alias in range(0, len(all_entity_alias)):
                if all_entity_alias[alias].find('alias_name') is not None:
                    ent_alias.append({
                        'aka': all_entity_alias[alias].find('alias_name').text,
                    })
                    
            
            ent_alias = json.dumps({'info': ent_alias})

            yield Entity({
                'category': 'Group',
                'name': ent_name,
                'aka' : ent_alias,
                'inclusion_date': self.string_to_date(ent_inclusion_date),
                'remarks': ent_remarks,
                'address' : ent_address or None,
                'city': ent_city or None,
                'country': ent_country or None
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%Y-%m-%d')
        except (TypeError,ValueError):
            return None