# Author =  Barkat Khan
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 
from datetime import datetime

class SindhRevenueBoardBlacklisted(UqaabBaseSpider):

    name = 'sindh_revenue_board_blacklisted'
    start_urls = ['http://www.srb.gos.pk/blacklisted/blacklisted.jsp']
    baseurl = "http://www.srb.gos.pk"


    def structure_valid(self, response):
        self.data_rows = response.css("tr[class='active tbl_cnt']")    
        return len(self.data_rows) > 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

    def extact_data(self, response):
        data_rows =  self.data_rows

        for i in range(len(data_rows)):
            columns = data_rows[i].css('td')
            name = columns[1].css("::text").extract_first()
            sntn = columns[2].css("::text").extract_first()
            reason = columns[3].css("::text").extract_first()
            datee = columns[4].css("::text").extract_first().strip()
            href = "{0}{1}".format(self.baseurl,columns[5].css("a::attr(href)").extract_first().replace(".","",2))

            document_info = []

            if sntn:
                document_info.append({'type': "SNTN",'id': str(sntn)})
            
            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})
            
            if reason:
                remarksfield =  "Reason of Suspension: {0} , Notice for Suspension: {1}".format(reason,href)
            else:
                remarksfield = ""
            
            yield Entity({
                'remarks': remarksfield,
                'name': name,
                'document': document_info,
                'category': "Group", 
                'inclusion_date': self.string_to_date(datee),
                'type': "SIP"
            })
    
    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d-%m-%Y')
        except TypeError:
            return None