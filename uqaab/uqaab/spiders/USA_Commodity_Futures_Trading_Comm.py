#Author: Daniyal Faquih

import scrapy
import dateutil.parser
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class USA_Commodity_Futures_Trading_Comm(UqaabBaseSpider):
    name = "usa_commodity_futures_trading_comm"
    start_urls = [
    'https://www.cftc.gov/LawRegulation/Enforcement/EnforcementActions/index.htm?year=2019',
    'https://www.cftc.gov/LawRegulation/Enforcement/EnforcementActions/index.htm?year=2018'
    ]
    
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25
    }

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="table-responsive"]//tbody//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="table-responsive"]//tbody//tr')
        baseUrl = "https://www.cftc.gov"
        for data in data_rows:
            nextPageUrl = data.xpath('.//td[2]//a//@href').extract_first()
            if(nextPageUrl):
                nextPageUrl = baseUrl + nextPageUrl
                yield scrapy.Request(nextPageUrl, callback=self.extract_link)
        relativeBaseUrl = "https://www.cftc.gov/LawRegulation/Enforcement/EnforcementActions/index.htm"
        relativeNextPageUrl = response.xpath('//div[@class="views-element-container form-group"]//div[1]//nav[@class="pager-nav text-center"]//ul[@class="pagination js-pager__items"]//li[@class="pager__item pager__item--next"]//a//@href').extract_first()
        if(relativeNextPageUrl is not None):
            relativeNextPageUrl = relativeBaseUrl + relativeNextPageUrl
            yield scrapy.Request(relativeNextPageUrl, callback=self.extact_data)

    def extract_link(self, response):
        date = response.xpath('//div[@class="field field--name-body field--type-text-with-summary field--label-hidden field--item"]//p[1]//text()').extract_first()
        date= dateutil.parser.parse(date)
        names = response.xpath('//div[@class="view-content"]//div[@class="item-list"]//ul//li')
        summary = response.xpath('//div[@class="field field--name-body field--type-text-with-summary field--label-hidden field--item"]//p//text()').extract()
        summary = ''.join(summary[1:-3])		
        for nameData in names:
            name = nameData.xpath('.//div[@class="views-field views-field-field-related-links"]//div[@class="field-content"]//text()').extract_first()
            if(":" in name):
            	name = name.split(":")
            	name = name[1].strip()
            pdfLink = response.xpath('.//div[@class="views-field views-field-field-related-links"]//div[@class="field-content"]//a//@href').extract_first()
            remarks = "PDF Link: {} ,Summary: {}".format(pdfLink, summary)
            yield({
            	"name" : name,
            	"inclusion_date" : date,
            	"remarks" : remarks,
            	"category" : "Group",
            	"type" : "SIP"
            })