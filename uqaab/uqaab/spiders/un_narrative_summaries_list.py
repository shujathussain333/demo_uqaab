"""
Created by Muhammad Ilyas <muhammad.ilyas@lovefordata.com>
Edited by Sharjeel Ali Shaukat
"""
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser


class UnNarrativeList(UqaabBaseSpider):
    name = "un_narrative_summaries_list"
    start_urls = [
        'https://www.un.org/securitycouncil/sanctions/narrative-summaries?page=0'
    ]
    url = 'https://www.un.org'

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.27,
        "AUTOTHROTTLE_START_DELAY": 0.27,
        "AUTOTHROTTLE_MAX_DELAY": 0.30,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 800,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    company_words = ['general', 'company', 'exchange', 'enterprise']

    def structure_valid(self, response):
        data_rows = response.css('table.views-table tbody tr')
        return len(data_rows) > 0


                    
    def extact_data(self, response):
        data_rows = response.css('table.views-table tbody tr')
        next_page = response.css('li.next a::attr(href)').extract_first()

        for row in data_rows:
            columns = row.css('td')
            link = columns[1].css('a::attr(href)').extract_first()
            new_url = self.url + link
            yield scrapy.Request(url=new_url, callback=self.extract_individual)

        if next_page:
            new_page_url = self.url + next_page
            yield scrapy.Request(url=new_page_url, callback=self.extact_data)

    def extract_individual(self, response):
        name = response.css('h1::text').extract_first()
        inclusion_date = response.css('span.date-display-single::text').extract_first()
        reason = "Reason: {}".format(response.css('div.field-name-field-reason-for-listing p::text').extract_first())
        additional_info = "Additional Info: {}".format(response.css('div.field-name-field-additional-information p::text').extract_first())
        remarks = "{}, {}".format(reason, additional_info)
        inclusion_date = dateutil.parser.parse(inclusion_date)

        category = 'Individual'
        if any(word in name.lower() for word in self.company_words):
            category = 'Group'

        yield Entity({
            'name': name,
            'category': category,
            'inclusion_date': inclusion_date,
            'remarks': remarks,
            'type': 'SAN'
        })





