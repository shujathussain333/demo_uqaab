#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json
import dateutil.parser
class PakBureaueEmploymentSpider(UqaabBaseSpider):

    name = 'pakistan_bureau_overseas_employment_cancelled'
    start_urls = ['https://beoe.gov.pk/list-of-oeps?show=valid&page=1&sort=licence_valid.licence_num&order=asc']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="oep-filters"][1]//a[text()!="Valid Licences" and text()!="All Licences"]')
        return len(data_rows) > 0

    def extact_data(self,response):
        page_urls = response.xpath('//div[@class="oep-filters"][1]//a[text()!="Valid Licences" and text()!="All Licences"]/@href').extract()
        for page in page_urls:
            yield Request(page, callback=self.parse_page)       

    def parse_page(self,response):
        data_to_parse = response.xpath('//table[@class="table table-bordered table-striped"]//tr[td]')
        for row in data_to_parse:
            licence_no = row.xpath('td[1]/text()').extract_first()
            proprietor_name = row.xpath('td[2]/text()').extract_first()
            if proprietor_name:
                proprietor_name = proprietor_name.strip()
                if(proprietor_name == ""):
                    proprietor_name = "None"
            else:
                proprietor_name = "None"   
            licence_title = row.xpath('td[3]/text()').extract_first()
            status = row.xpath('td[4]/span/text()').extract_first()
            expiry_date = row.xpath('td[5]/text()').extract_first()
            if expiry_date:    
                expiry_date = dateutil.parser.parse(expiry_date)
            oep_head_office = row.xpath('td[6]/text()').extract_first()
            if oep_head_office:
                oep_head_office = oep_head_office.strip()
            branch_office = row.xpath('td[7]/text()').extract_first()
            if branch_office:
                branch_office = branch_office.strip()
            document_info = json.dumps({'document_info': None})
            document = []  
            document.append({'type': "Licence_NO", 'id': licence_no})
            if document:
                document_info = json.dumps({'document_info': document})  
            remarks = "OEP_head_Office: {}  Branch_office: {}".format(oep_head_office,branch_office) 
            yield Entity({
                "category": "group",
                "name":proprietor_name,
                "Status":status,
                "document":document_info,
                "exclusion_date":expiry_date,
                "remarks":remarks,
                "type": "SAN"
            })                      
        next_page_url = response.xpath('//ul[@class="pagination"][1]/li[last()]//a/@href').extract_first()
        if next_page_url:
            yield Request(next_page_url, callback=self.parse_page)         