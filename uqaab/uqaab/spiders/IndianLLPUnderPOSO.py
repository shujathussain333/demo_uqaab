# Author =  Barkat Khan

from uqaab.items import Entity
from uqaab.environment import config
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import json 
import os
import scrapy
import io 
import pandas as pd

class IndianLLPUnderPOSO(StaticDataSpider):

    name = 'IndianLLPUnderPOSO'

    def start_requests(self):
        file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'List_IndianLLPs_under_processOfStrikeOff_1.xls')
        url = "file://{path_to_file}".format(path_to_file=file_path)
        yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def structure_valid(self, response):
        with io.BytesIO(response.body) as resp_file:
            self.companiesnamedf = pd.io.excel.read_excel(resp_file, sheet_name='List of LLP Struck Off', skiprows=3)
            self.companiesdetailsdf = pd.io.excel.read_excel(resp_file, sheet_name='List of LLP UPSO', skiprows=3)
            self.tempdf = pd.io.excel.read_excel(resp_file, sheet_name='List of LLP UPSO')

        return len(self.companiesnamedf) > 0

    def extact_data(self, response):
        df = self.companiesnamedf
        for row, col in df.iterrows():
            document_info = []

            if str(df.loc[row, 'CIN']):
                document_info.append({'type': "CIN",'id': str(df.loc[row, 'CIN'])})
            
            if str(df.loc[row, 'SRN']):
                document_info.append({'type': "SRN",'id': str(df.loc[row, 'SRN'])})

            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})

            yield Entity({
                'category': 'Group',
                'type': 'SIP',
                'name': df.loc[row, 'COMPANY NAME'],
                'document': document_info
            })
        
        df = self.companiesdetailsdf
        
        for row, col in df.iterrows():

            document_info = []
            document_info.append({
                            "type": 'CIN',
                            "id": str(df.loc[row, 'CIN'])
                        })
            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})

            remarksfield =  "DATE_OF_REGISTRATION: {0} , LLP_STATUS: {1} , REGISTERED_STATE: {2}, ROC_CODE: {3}".format(
                df.loc[row, "DATE_OF_REGISTRATION"],
                df.loc[row, "LLP_STATUS"],
                df.loc[row, "REGISTERED_STATE"],
                df.loc[row, "ROC_CODE"])

            yield Entity({
                'category': 'Group',
                'type': 'SIP',
                'name': df.loc[row,'LLP_NAME'],
                'document': document_info,
                'remarks': remarksfield
            })
