from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
from tabula import read_pdf

class Autoriti_Monetari_Brunei_Darussalam(UqaabBaseSpider):
    name = 'amdb_alertlist'
    start_urls = ['https://www.ambd.gov.bn/SiteAssets/Pages/Financial-Consumer-Alert-and-Updates/ALERT%20LIST%20(asof28May2019)%20-%20English.pdf']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www.ambd.gov.bn/SiteAssets/Pages/Financial-Consumer-Alert-and-Updates/ALERT%20LIST%20(asof28May2019)%20-%20English.pdf'
        r = requests.get(url, allow_redirects=True)
        open('ambd.pdf', 'wb').write(r.content)

        df = read_pdf('ambd.pdf', pages='all')
        df = df[(df['No.'] != 'No.') & (df['Name of Unauthorised'] != "Entities/Website/Products")]
        index = list(df.index)
        for i, index_ in enumerate(index):
            if i != 0 and i != len(index):
                try:
                    if (~pd.isnull(df.loc[index_]['No.'])) & pd.isnull(df.loc[index[i - 1]]['No.']) & pd.isnull(
                            df.loc[index[i + 1]]['No.']):
                        for col in ['Name of Unauthorised', 'Website', 'Date Added to']:
                            df.loc[index_][col] = ' \n'.join(
                                j for j in list(df[col].loc[index[i - 1]:index[i + 1]]) if pd.isnull(j) == False)

                except Exception as e:
                    pass
        df = df[~df['No.'].isnull()]
        df = df[['Name of Unauthorised', 'Website', 'Date Added to']]
        df.columns = ['Company', 'Website', 'DOInclusion']
        df['Remarks'] = 'Website : ' + df['Website']
        df = df[['Company', 'Remarks', 'DOInclusion']]
        df.reset_index(inplace=True, drop=True)

