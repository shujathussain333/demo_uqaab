# -*- coding: utf-8 -*-
from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class UndpIneligbleListSpider(UqaabBaseSpider):
    name = 'UNDP-Ineligble-list'
    # allowed_domains = ['http://www.undp.org/content/undp/en/home/operations/procurement/business/protest-and-sanctions/ineligibility-list/']
    start_urls = ['http://www.undp.org/content/undp/en/home/operations/procurement/business/protest-and-sanctions/ineligibility-list//']

    def structure_valid(self, response):
        data_rows = response.css('table')[0].css('tr')[1:]
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table')[0].css('tr')[1:-1]
        for row in data_rows:
            columns = row.css('td::text')

            name = columns[0].extract().strip()

            if name is None or len(name) <= 0:
                name_column = row.css('td')
                name = name_column[0].css('p::text').extract_first()

            yield Entity({
                'name': name,
                # 'category' : self.individualOrGroup(columns[0].extract().strip()),
                'category' : 'Group',
                'country': self.get_country_code(columns[1].extract().strip()),
                'nationality': columns[1].extract().strip(),
                'remarks': columns[2].extract().strip(),
                'inclusion_date': self.string_to_date(columns[4].extract().strip()), 
                'exclusion_date': self.string_to_date(columns[5].extract().strip())
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d-%b-%y')
        except (TypeError,ValueError):
            return None

    @staticmethod
    def individualOrGroup(name):
        temp = name[0]+name[1]+name[2] # Index out of range
        if temp == 'Mr.':
            return 'Individual'
        else:
            return 'Group'
