from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import scrapy
import json


class Nacta(UqaabBaseSpider):
    name = "nacta"

    start_urls = [
        'https://nfs.punjab.gov.pk/?page=1',
    ]

    def structure_valid(self, response):
        data_rows = response.css('table tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        last_page = response.css('b::text').extract()
        last_page = int(last_page[-1]) + 1
        data_rows = response.css('table tr')
        for row in data_rows[1:]:
            columns = row.css('td')
            name = self.extract_name(columns[1].css('*::text').extract_first())
            aka = self.extract_aka(columns[1].css('*::text').extract_first())
            document_info = self.extract_cnic(columns[3].css('*::text').extract_first())
            city = '{} , {}'.format(columns[5].css('*::text').extract_first(), columns[4].css('*::text').extract_first())
            father_name = columns[2].css('*::text').extract_first()
            name = '{} {}'.format(name, father_name)
            remarks = 'Father Name: {}'.format(father_name)

            yield Entity({
                "category": "Individual",
                "name": name,
                "document": document_info,
                "remarks": remarks,
                "city": city,
                "type": "SIP",
                "aka": aka
            })

        for i in range(2, last_page):
            url = 'https://nfs.punjab.gov.pk/?page={}'.format(i)
            yield scrapy.Request(url, callback=self.extact_data)


    @staticmethod
    def extract_name(name):
        name = name.split('@')[0]
        return name

    @staticmethod
    def extract_aka(orig_name):
        aka = []
        try:
            names = orig_name.split('@')
            for name in names[1:]:
                aka.append({
                    'aka': name,
                })
            alias_info = json.dumps({'info': aka})
        except UnboundLocalError:
            alias_info = json.dumps({'info': None})
        return alias_info

    @staticmethod
    def extract_cnic(cnic):
        document = []
        try:
            document.append({'type': "National Identity",
                             'id': cnic})
            document_info = json.dumps({'document_info': document})
        except NoneType:
            document_info = json.dumps({'document_info': None})
        return document_info
