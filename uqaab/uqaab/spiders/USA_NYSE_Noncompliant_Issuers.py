#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class USA_NYSE_Noncompliant_Issuers(UqaabBaseSpider):
    name = 'usa_nyse_Noncompliant_Issuers'
    start_urls = ['https://www.nyse.com/api/regulatory/noncompliant-issuers']
    
    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        data_rows=Jresponse["results"]
        return len(data_rows) > 0

    @staticmethod
    def getJsonData(data):
        deficiency = ""
        name = data['issuerName']
        symbols = data['affectedSymbols']
        checkDeficiency = data['savedIndicators'][0]['deficiency']
        if(checkDeficiency):
            deficiency = data['savedIndicators'][0]['deficiency']['deficiencyText']
        market = data['market']
        return {"name":name,"symbols":symbols,"deficiency":deficiency,"market":market}

    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["results"]
        for data in actual_data:
            dataDict = self.getJsonData(data)
            remarks = "Symbols: {} ,Market: {} ,Deficiency: {}".format(dataDict["symbols"],dataDict["market"],dataDict["deficiency"])
            yield Entity({
            "name": dataDict["name"],
            "category": "Group",
            "type": "SAN",
            "remarks": remarks
            })