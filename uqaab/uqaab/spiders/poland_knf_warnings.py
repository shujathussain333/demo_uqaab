#Author: Sharjeel Ali Shaukat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class PolandKNFWarnings(UqaabBaseSpider):
    name = "poland_knf_warnings"

    start_urls = [
        'https://www.knf.gov.pl/en/CONSUMERS/Information_for_the_financial_market_consumers/Public_warnings',
    ]

    no_data_expressions = ['The entity is not registered in Poland.', 'not available', 'No data available']

    def structure_valid(self, response):
        data_cols = response.css('table tr')
        return len(data_cols) > 0

    def extact_data(self, response):
        data_rows = response.css('table tr')
        for row in data_rows:
            columns = row.css('td')
            if columns[0].css('*::text').extract_first() == 'No.':
                continue
            name = columns[1].css('*::text').extract_first()
            identification = columns[2].css('*::text').extract_first()
            prostecutar_office = "Prostecutar Office: {}".format(columns[3].css('*::text').extract_first())
            proceedings_result = "Proceedings Result: {}".format(columns[4].css('*::text').extract_first() if columns[4].css('*::text').extract_first() != None else '-')
            relevant_information = "Relevant Information: {}".format(columns[5].css('*::text').extract_first())
            
            if identification and identification not in self.no_data_expressions:
                document = [{"type": "Identification No", "id": identification}]
            else:
                document = None

            document_info = json.dumps({'document_info': document})
            remarks = "{} {} {}".format(prostecutar_office,proceedings_result,relevant_information)

            yield Entity({
                "category": "Group",
                "name": name,
                'document': document_info,
                "remarks": remarks,
                "type": "SIP",
            })
