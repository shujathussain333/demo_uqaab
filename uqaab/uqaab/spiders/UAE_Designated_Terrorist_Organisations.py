#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class UAE_Designated_Terrorist_Organisations(UqaabBaseSpider):
    name = 'uae_designated_terrorist_organisations'
    start_urls = ['https://www.thenational.ae/uae/government/list-of-groups-designated-terrorist-organisations-by-the-uae-1.270037']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="align-left"]//p')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="align-left"]//p')
        for data in data_rows[3:-2]:
        	name = data.xpath('.//text()').extract_first()
        	yield Entity({
            	"name": name,
            	"category": "Group",
            	"type": "SIP"
            })