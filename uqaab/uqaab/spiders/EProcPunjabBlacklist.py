# Author = Barkat Khan
import scrapy
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 

class EProcPunjabBlacklist(UqaabBaseSpider):

    name = 'eproc_punjab_blacklist'
    start_urls = ['https://eproc.punjab.gov.pk/blacklist.aspx']

    def structure_valid(self, response):
        
        data_rows = response.css("table[class='rgMasterTable'] tbody").css("tr")
        return len(data_rows) > 0
   

    def extact_dataitems(self, response):
        
        data_rows = response.css("table[class='rgMasterTable'] tbody").css("tr")
        for i in range(len(data_rows)):
            columns = data_rows[i].css('td')
            
            remarks_array = []
            remarks_array.append({
                'Reason': self.extract_field(columns[1]), 
                'Department': self.extract_field(columns[2]),
                'Blacklisting Type': self.extract_field(columns[3]),
                'Publish Date': self.extract_field(columns[4]),
                'Remarks': self.extract_field(columns[7]),
            })
            
            yield Entity({
                    'name': self.extract_field(columns[0]),
                    'inclusion_date': self.string_to_date(columns[5]),
                    'exclusion_date': self.string_to_date(columns[6]), 
                    'remarks':  json.dumps({'remarks': remarks_array}),
                    "category": "SIP",
                    "type": "Group"
                })

    def extact_data(self, response):

        E = Entity()
        E['category'] =  "Group"
        E['type'] = "SAN"

        linkdata_rows = response.css("table[class='rgMasterTable'] tfoot").css("div[class='rgWrap rgNumPart'] a::attr(href)").extract()
        linkdata_rows =[link.split("'")[-4] for link in linkdata_rows] 
        
        for link in linkdata_rows:
            view_state = response.xpath('//*[@id="__VIEWSTATE"]/@value').extract_first()
            formdata = {
                    # change pages here
                    "__EVENTTARGET": link,
                    "__EVENTARGUMENT":'',
                    "__VIEWSTATE": view_state,
                }

            url  = self.start_urls[0]
            yield scrapy.FormRequest(url=url, formdata=formdata, callback=self.extact_dataitems)#,
            
    @staticmethod
    def extract_field(response):
        try:
            value = response.css("::text").extract_first()
            if value is None:
                value = response.css("a::text").extract_first()
            return value
        except:
            return None
    
    def string_to_date(self,response):
        date_string =  self.extract_field(response)
        try:
            if date_string is not None:
                return datetime.strptime(date_string, '%d %b %Y')
        except:
            return None