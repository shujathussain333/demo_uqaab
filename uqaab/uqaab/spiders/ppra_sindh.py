import scrapy
##Author: Shahzaib Mumtaz
## Created on: 11-01-2019

from bs4 import BeautifulSoup
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class PPRASindhBlacklistScraper(UqaabBaseSpider):
    
    name = 'ppra_sindh_blacklist_scraper'
    start_urls = ['http://www.pprasindh.gov.pk/blacklistfirms.php']

    def structure_valid(self,response):
        
        soup = BeautifulSoup(response.body, 'lxml')
        table = soup.find('div',{'id':'rightPan'}).find('table')
        
        return len(table) > 0

    def extact_data(self,response):
        soup = BeautifulSoup(response.body, 'lxml')
        table = soup.find('div',{'id':'rightPan'}).find('table')
        rows = table.findAll('tr')[1:-2]
        for row in range(len(rows)):
            name_of_proc = rows[row].findAll('td')[1:-1][0].get_text().strip()
            name_entity = rows[row].findAll('td')[1:-1][1].get_text().strip()
            address = rows[row].findAll('td')[1:-1][2].get_text().strip() 
            address = address if len(address) > 0 else None

            yield Entity({
                'name': name_entity,
                'category': CATEGORY_OPTIONS[1],
                'address': address,
                'remarks': 'Procuring Agency: {0}'.format(name_of_proc)
            })