##Author: Shahzaib Mumtaz
## Created on: 11-01-2019

from bs4 import BeautifulSoup
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import json

class MalaysiaTop10WantedScraper(UqaabBaseSpider):
    name = 'malay_top10_wanted_scraper'
    start_urls = ['https://mahaguru58.blogspot.com/2010/12/top-10-malaysias-most-wanted-royal.html']


    def structure_valid(self,response):
        soup = BeautifulSoup(response.body, 'lxml')
        table = soup.find('div',{'class':'post-body entry-content'}).findAll('table',{'class':'div_content_2'},recursive=False)

        return len(table) > 0

    def extact_data(self,response):
        soup = BeautifulSoup(response.body, 'lxml')
        data_fields = ['Nama :','Nama Gelaran :','No. KP :','Bangsa :','Jantina :','Tarikh lahir :','Alamat :','Kesalahan :']
        table = soup.find('div',{'class':'post-body entry-content'}).findAll('table',{'class':'div_content_2'},recursive=False)

        for row in table:
            name = alias = malay_id = country = gender = address = crime = ''
            dob = json.dumps({'info': None})

            content = row.find('table',{'class':'div_content_2'}).findAll('tr')
            if len(content)>8:
                content = row.find('table',{'class':'div_content_2'}).findAll('tr')[1:]
            for i in range(0,len(content)):
                if i in [2,3]:
                    label = content[i].findAll('td')[0].get_text().strip()
                    if label in data_fields:
                        if label == data_fields[2]:
                            malay_id = content[i].findAll('td')[1].get_text().strip()
                        else:
                            country = content[i].findAll('td')[1].get_text().strip()                        
                    label2 = content[i].findAll('td')[2].get_text().strip()
                    if label2 in data_fields:
                        if label2 == data_fields[4]:
                            gender = content[i].findAll('td')[3].get_text().strip()
                            if gender.lower() == 'lelaki':
                                gender = 'Male'
                            else:
                                gender = 'Female'
                        else:
                            dob = content[i].findAll('td')[3].get_text().strip()
                            dob = json.dumps({'info': [{'DOB': dob, 'POB': None}]})
                else:
                    label = content[i].findAll('td')[0].get_text().strip()
                    if label in data_fields:
                        if label == data_fields[0]:
                            name = content[i].findAll('td')[1].get_text().strip()
                        if label == data_fields[1]:
                            alias = content[i].findAll('td')[1].get_text().strip()
                        if label == data_fields[6]:
                            address = content[i].findAll('td')[1].get_text().strip()
                        if label == data_fields[7]:
                            crime = content[i].findAll('td')[1].get_text().strip()
                            
            yield Entity({
                'name': name,
                'category': CATEGORY_OPTIONS[0],
                'aka': alias,
                'country': self.get_country_code(country),
                'gender': gender,
                'date_of_birth': dob,
                'address': address,
                'remarks': ' Malaysian ID.No: {0}, Crime: {1}'.format(malay_id, crime)
            })