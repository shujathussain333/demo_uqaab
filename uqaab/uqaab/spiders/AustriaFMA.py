##Author: Shahzaib Mumtaz
## Created at: 8- Jan - 2019

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class FMAAustriaScraper(UqaabBaseSpider):
    name = 'austria_fma_scraper'
    start_urls = ['https://www.fma.gv.at/en/category/news-en/?cat=81&filter-dropdown-year=&filter-dropdown-order=date_desc']
    links = []
    custom_settings = {
        "DOWNLOAD_DELAY": 0.3,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 90,
    }
    def structure_valid(self,response):
        page_links = response.css("div.morelink a::attr(href)").extract()
        return len(page_links) > 0

    def extact_data(self, response):
        page_links = response.css("div.morelink a::attr(href)").extract()
        last_page = response.css("ul.page-numbers a::text").extract()[2]
        self.links.extend(page_links)
        next_page = 2
        url = "https://www.fma.gv.at/en/category/news-en/page/{}/?cat=81&filter-dropdown-year&filter-dropdown-order=date_desc".format(next_page)
        yield scrapy.Request(url, callback=self.extract_links, errback=self.error_handler, meta={'next_page':next_page,'last_page':last_page})

    def extract_links(self,response):
        next_page = int(response.meta['next_page'])  + 1
        last_page = int(response.meta['last_page'])
        page_links = response.css("div.morelink a::attr(href)").extract()
        self.links.extend(page_links)
        if next_page <= last_page:
            url = "https://www.fma.gv.at/en/category/news-en/page/{}/?cat=81&filter-dropdown-year&filter-dropdown-order=date_desc".format(
                next_page)
            yield scrapy.Request(url, callback=self.extract_links, errback=self.error_handler,
                                 meta={'next_page': next_page, 'last_page': last_page})
        else:
            for link in self.links:
                yield scrapy.Request(link, callback=self.extract_details, errback=self.error_handler)

    def extract_details(self,response):
        name = response.xpath("//h1/text()").extract_first()
        inclusion_date = response.xpath("//time/@datetime").extract_first()
        remarks = response.xpath("//div[@class='content-wrap']//p/text()").extract()[0]


        yield Entity({
            'name': name,
            'category': "Groups",
            'remarks': remarks,
            'inclusion_date': inclusion_date
        })
