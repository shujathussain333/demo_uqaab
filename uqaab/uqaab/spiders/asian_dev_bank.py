from datetime import datetime
from urllib.parse import urlencode
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS


class AsianDevBankSpider(UqaabBaseSpider):
    name = "asian_dev_bank"
    start_idx = 1
    MAX_RECORDS_PER_PAGE = 999

    def start_requests(self):
        return [
            scrapy.Request(url=self.url_with_params(), callback=self.parse, errback=self.error_handler)
        ]

    def url_with_params(self):
        url = 'http://lnadbg4.adb.org/oga0009p.nsf/sancALL1P'
        params = {
            'OpenView': '',
            'Start': self.start_idx,
            'Count': self.MAX_RECORDS_PER_PAGE
        }
        full_url = '{0}?{1}'.format(url, urlencode(params))
        return full_url

    def structure_valid(self, response):
        data_rows = response.css('table')[1].css('tr')[1:]
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table')[1].css('tr')[1:]
        for row in data_rows:
            columns = row.css('td')[1:]
            name, alias = self.extract_name_and_alias(columns[0])
            address, country = self.extract_address_and_country(columns[1])
            yield Entity({
                'name': name,
                'category' : CATEGORY_OPTIONS[1],
                'aka': alias,
                'address': address,
                'country': country,
                'nationality': self.extract_and_clean(columns[3]),
                'inclusion_date': self.extract_effect_date(columns[4]),
                'exclusion_date': self.extract_lapse_date(columns[4]),
                'remarks': self.extract_and_clean(columns[5])
            })

        if len(data_rows) == self.MAX_RECORDS_PER_PAGE:
            self.start_idx += len(data_rows) - 1
            yield scrapy.Request(url=self.url_with_params(), callback=self.parse, errback=self.error_handler)

    def extract_name_and_alias(self, column):
        alias = []
        name = self.extract_and_clean(column)
        if '(a.k.a.' in name:
            alias_string = self.extract_alias(name)
            alias_string_idx = name.index(' (a.k.a.')
            name = name[0 : alias_string_idx]
            alias.append(alias_string)
        return name, alias

    @staticmethod
    def extract_alias(name):
        #TODO: Need better logic for extracting AKA
        alias_start_idx = name.index('(a.k.a. ') + len('(a.k.a. ')
        return name[alias_start_idx : -1]

    def extract_address_and_country(self, column):
        country = []
        address = []
        address_string = self.extract_and_clean(column)
        if address_string:
            country.append(self.get_country_code(address_string.split()[-1]))
            address.append(address_string)
        else:
            country.append(None)
            address.append(None)
        return address, country

    def extract_and_clean(self, column):
        try:
            text = column.css('font::text').extract()
            joined_text = ''.join(map(lambda x: x.replace('\n', ' '), text))
            return self.remove_extra_spaces(joined_text)
        except AttributeError:
            return None

    @staticmethod
    def remove_extra_spaces(text):
        return ' '.join(text.split())

    def extract_effect_date(self, column):
        try:
            return self.string_to_date(column.css('font::text').extract()[0].replace('|', '').strip())
        except IndexError:
            return None

    def extract_lapse_date(self, column):
        try:
            return self.string_to_date(column.css('font::text').extract()[1].strip())
        except IndexError:
            return None

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%b/%Y')
        except ValueError:
            return None