#Author: Daniyal Faquih
from uqaab.items import Entity
import dateutil.parser
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class USA_State_Sponsors_of_Terrorism(UqaabBaseSpider):
    name = 'usa_state_sponsors_of_terrorism'
    start_urls = ['https://www.state.gov/state-sponsors-of-terrorism/']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="table-responsive double-scroll"]//table//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="table-responsive double-scroll"]//table//tr')
        for data in data_rows[1:]:
        	name = data.xpath('.//td[1]//text()').extract_first()
        	date = data.xpath('.//td[2]//text()').extract_first()
        	date = dateutil.parser.parse(date)

        	yield Entity({
            	"name": name,
            	"category": "Group",
            	"type": "SIP",
                "inclusion_date": date
           	})