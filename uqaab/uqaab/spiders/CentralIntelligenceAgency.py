from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

base_url = "https://www.cia.gov"

class CentralIntelligenceAgencySpider(UqaabBaseSpider):
    name = "central_intelligence_agency"
    allowed_domains = ["https://www.cia.gov"]

    start_urls = [
        'https://www.cia.gov/library/publications/resources/world-leaders-1/index.html',
    ]
    list_path = '//*[@id="cosCountryList"]/*'

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.3,
        "AUTOTHROTTLE_START_DELAY": 0.3,
        "AUTOTHROTTLE_MAX_DELAY": 0.6,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 800,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    def structure_valid(self, response):
        return len(response.xpath(self.list_path).extract()) > 0

    def extact_data(self, response):
        country_base_url = base_url + '/library/publications/resources/world-leaders-1/'

        countries_list = response.xpath(self.list_path)
        for country in countries_list:
            country_url = country_base_url + country.xpath('a/@href').extract()[0].strip()
            country_name = country.xpath('a/text()').extract()[0].strip()

            yield Request(country_url,
                          callback=self.parse_country,
                          meta={'country': country_name},
                          dont_filter=True
                         )

    def parse_country(self, response):

        country_name = str(response.meta['country'])
        ul = response.xpath('//*[@id="countryOutput"]/div/div/ul/*')
        for li in ul:
            name = designation = ''
            lis = li.xpath('div/*').css('::text').extract()
            lis = [item for item in lis if item.strip() is not '']

            if len(lis) > 1:
                designation = lis[0]
                name = lis[1]

                yield Entity({
                    'category': 'Individual',
                    'name': name,
                    'designation': [designation],
                    'country': self.get_country_code(country_name)
                })
