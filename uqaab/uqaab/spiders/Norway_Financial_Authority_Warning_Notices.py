#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
import dateutil.parser
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class Norway_Financial_Authority_Warning_Notices(UqaabBaseSpider):
    name = 'norway_financial_authority_warning_notices'
    start_urls = ['https://www.finanstilsynet.no/api/SearchInvestorWarnings?l=en&p=']
    i = 1
    jsonPage = 1
    
    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["Hits"]
        return len(actual_data) > 0
   
    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        while (self.i <= self.jsonPage):
            yield scrapy.Request('https://www.finanstilsynet.no/api/SearchInvestorWarnings?l=en&p=%d&from=2019-01-01&to=2019-12-31' % self.i, callback = self.extract_link)
    
    def extract_link(self, response):
        self.i+=1
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["Hits"]
        self.jsonPage =  Jresponse["Page"] + 1
        if(len(actual_data) == 0):
            self.jsonPage = -1
        for data in actual_data:
            name=data['Name']
            date=dateutil.parser.parse(data['Published'])
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "inclusion_date": date
            })