##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class SouthAfricanPoliceServiceMostWanted(UqaabBaseSpider):
    name = 'south_african_police_service_most_wanted'
    start_urls = ['https://www.saps.gov.za/crimestop/wanted/list.php']
    url = 'https://www.saps.gov.za/crimestop/wanted/'

    def structure_valid(self, response):
        links = response.xpath("//div[@class='col-md-14 cust-td-border']//a/@href").extract()
        return len(links) > 0

    def extact_data(self, response):
        links = response.xpath("//div[@class='col-md-14 cust-td-border']//a/@href").extract()

        for link in links:
            new_url = self.url + link
            yield scrapy.Request(url=new_url, callback=self.extract_link)

    def extract_link(self, response):
        name = response.xpath("//h2/text()").extract_first()
        data_rows = response.css('tbody tr')

        for row in data_rows:
            columns = row.css('td')
            column = columns[0].css('*::text').extract_first()
            value = columns[1].css('*::text').extract_first()
            if "crime" in column.lower():
                crime = "Crime: {}".format(value)
            if "crime circumstances" in column.lower():
                crime_circumstances = "Crime: {}".format(value)
            if "crime date" in column.lower():
                crime_date = "Crime: {}".format(value)
            if "case number" in column.lower():
                case_number = "Crime: {}".format(value)
            if "station" in column.lower():
                station = "Station: {}".format(value)

        remarks = crime + ' ; ' + crime_circumstances + ' ; ' + crime_date  + ' ; ' + station
        document = []
        document.append({'type' : "Case Number", 'id' : case_number})
        document_info = json.dumps({'document_info': document})
        image = self.url + response.xpath("//td//img/@src").extract_first()

        yield Entity({
            "category": "Individual",
            "name": name,
            "remarks": remarks,
            "type": "SIP",
            "document": document_info,
            "image":image
        })


