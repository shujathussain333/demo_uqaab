# Author = Barkat Khan
import os
from datetime import datetime
import scrapy
from uqaab.environment import config
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import tabula
import json

class IndianChitFundCompanies(StaticDataSpider):

    name = 'indianchitfundcompanies'
    start_urls = ['http://www.mca.gov.in/MCA21/dca/RegulatoryRep/pdf/Chit_Fund_Companies.pdf']

    def structure_valid(self, response):
        self.file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'Chit_Fund_Companies.pdf')
        self.df = tabula.read_pdf(self.file_path, encoding="utf-8", lattice=True, guess=False, pages='all')        
        return len(self.df) > 0
    
    def extact_data(self, response):

        df= self.df
        datestring = self.string_to_date("31/12/2013")
        
        for index in range(len(df)):


            document_info = []

            if str(df.loc[index,'CIN']):
                document_info.append({'type': "CIN",'id': str(df.loc[index,'CIN'])})
            

            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})
            
            remarksfield =  "DATE OF INCORPORATION: {0} ".format(df.loc[index,'DATE OF\rINCORPORATION'])

            yield Entity({
                'category': 'Group',
                'type': 'SIP',
                'name': df.loc[index,'COMPANY NAME'],
                'address': df.loc[index,'REGISTERED OFFICE ADDRESS'],
                'document': document_info,
                'inclusion_date': datestring,
                'remarks': remarksfield
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%m/%Y')
        except TypeError:
            return None
