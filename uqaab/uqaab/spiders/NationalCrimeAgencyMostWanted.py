##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import scrapy
from scrapy import Request


class NationalCrimeAgencyMostWanted(UqaabBaseSpider):
    name = 'national_crime_agency_most_wanted'
    start_urls = [
        'https://www.nationalcrimeagency.gov.uk/most-wanted-search']

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='span9']//a")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.nationalcrimeagency.gov.uk"
        page_urls = response.xpath("//div[@class='span9']//a/@href").extract()
        for page in page_urls:
            absolute_url = base_url + page
            yield Request(absolute_url, callback=self.parse_page) 

    def parse_page(self, response):
        base_url = "https://www.nationalcrimeagency.gov.uk"
        name = response.xpath("//div[@class='item-page most-wanted-grid']//h2/text()").extract_first()
        image_url = response.xpath("//div[@class='item-page most-wanted-grid']//img/@src").extract_first()
        image = base_url + image_url
        summary = response.xpath("//div[@class='item-page most-wanted-grid']//div[@itemprop='articleBody']//text()").extract()
        summary = " ".join(summary)
        additional_information = response.xpath("//div[@class='item-page most-wanted-grid']//div[./span='Additional Information: ']/span[2]/text()").extract_first()
        location = response.xpath("//div[@class='span4 most-wanted-customfields most-wanted-basic']/span[text()='Location: ']/following::span[1]/text()").extract_first()
        crime = response.xpath("//div[@class='span4 most-wanted-customfields most-wanted-basic']/span[text()='Crime: ']/following::span[1]/text()").extract_first()
        remarks = "description: {} ,additional_information: {} , crime: {}".format(summary,additional_information,crime)  
        yield Entity({
            "category": "Individual",
            "name": name,
            "remarks": remarks,
            "address":location,
            "type": "SIP",
            "image":image
        })


