# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class EurpeonInvestmentBank(UqaabBaseSpider):
    
    name = 'eurpeon_investment_bank_excluded_entities'
    start_urls = ['https://www.eib.org/en/about/accountability/anti-fraud/exclusion/index.htm']

    def structure_valid(self, response):
        data_rows = response.xpath('//table/tbody/tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        details = response.xpath('//table/tbody/tr')
        for detail in details:
            name = detail.xpath('td[1]//strong/text()').extract_first()
            exclusion_date_trim = detail.xpath('td[2]/text()').extract_first()
            if "*" in exclusion_date_trim:
                exclusion_date = exclusion_date_trim.replace("*","")
            else:
                exclusion_date = exclusion_date_trim    
            excluded_untill = detail.xpath('td[3]/text()').extract_first()
            if exclusion_date:
                exclusion_date = dateutil.parser.parse(exclusion_date)
            type_of_exclusion = detail.xpath('td[4]/text()').extract_first()
            yield Entity({
                "category": "group",
                "name":name,
                "exclusion_date":exclusion_date,
                "remarks":type_of_exclusion,
                "type": "SAN"
            })

        
