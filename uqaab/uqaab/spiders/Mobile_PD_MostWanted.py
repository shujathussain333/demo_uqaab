#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request
import json

class MobilePDWantedSpider(UqaabBaseSpider):

    name = 'mobile_pd_most_wanted'
    start_urls = ['https://www.mobilepd.org/most-wanted/']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="col-xs-12 col-sm-8 col-md-9"]//div[@class="cols-xs-12 col-sm-3 col-md-4"]//a')
        return len(data_rows) > 0

    def extact_data(self,response):
        page_urls = response.xpath('//div[@class="col-xs-12 col-sm-8 col-md-9"]//div[@class="cols-xs-12 col-sm-3 col-md-4"]//a/@href').extract()
        for url in page_urls:
            yield Request(url, callback=self.parse_page)    	

    def parse_page(self,response):
        name = response.xpath('//div[@class="col-xs-12 col-sm-6 col-md-6 "]//div[sub/b="Name:"]/text()').extract_first()
        age = response.xpath('//div[@class="col-xs-12 col-sm-6 col-md-6 "]//div[sub/b="age:"]/text()').extract_first()
        date_of_b = response.xpath('//div[@class="col-xs-12 col-sm-6 col-md-6 "]//div[sub/b="dob:"]/text()').extract_first()
        dobs = {'DOB': date_of_b, 'POB': None}
        dob_info = json.dumps({'info': [dobs]})
        remarks = response.xpath('//div[@class="col-xs-12 col-sm-6 col-md-6 "]//div[sub/b="Crime: "]/text()').extract_first()
        image = response.xpath('//div[@class="col-xs-12 col-sm-12 col-md-12 wanted_wrap"]//img/@src').extract_first()
        yield Entity({
            "category": "Individual",
            "name":name,
            "date_of_birth":dob_info,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })        