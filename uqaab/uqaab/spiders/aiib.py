from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
import json

class AIIB_Scrapper(UqaabBaseSpider):
    name = 'aiib_debar'
    start_urls = ['https://www.aiib.org/en/about-aiib/who-we-are/debarment-list/.content/index/debarment-list.js']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        res = requests.get("https://www.aiib.org/en/about-aiib/who-we-are/debarment-list/.content/index/debarment-list.js")
        d = json.loads(res.text[9:-2])
        fp = open('aiib.json', 'w')
        json.dump(d, fp)
        fp.close()
        aiib_df = pd.read_json('aiib.json')
        aiib_df.columns = ['Address', 'Organization', 'DateAdded', 'Name', 'DOInclusion', 'Nationality', 'Category', 'DOExclusion']

        for index, row in aiib_df.iterrows():
            yield Entity ({
                'name': row.Name,
                'inclusion_date': row.DOInclusion,
                'nationality': row.Nationality,
                'exclusion_date': row.DOExclusion,
                'remarks': row.Category,
                'address': row.Address,
                'category': 'Group',
                'type': "SAN"
            })
