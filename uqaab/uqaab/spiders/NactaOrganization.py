# Author: Sharjeel Ali
#updated by : M.Shujat Hussain

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import pandas as pd
import numpy as np
import dateutil.parser
import dateutil
import tabula
import numpy as np
import dateutil.parser


class NactaOrganization(UqaabBaseSpider):
    name = 'nacta_organization'
    start_urls = ['https://nacta.gov.pk']

    def structure_valid(self, response):
        url = 'https://nacta.gov.pk/wp-content/uploads/2017/08/Proscribed-OrganizationsEng.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'https://nacta.gov.pk/wp-content/uploads/2017/08/Proscribed-OrganizationsEng.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)
        df = pd.DataFrame()
        for page in range(0, total_len):
            df = pd.concat([df, bin[page]])     
        df = df.iloc[1:-3]
        df = df.iloc[:, : 6]
        datas = []
        for index, row in df.iterrows():
            if row[1] is not np.nan and row[2] is not np.nan:
                datas.append({"name": row[1], "inclusion_data": dateutil.parser.parse(row[2])})
            if row[4] is not np.nan and row[5] is not np.nan:
                datas.append({"name": row[4], "inclusion_data": dateutil.parser.parse(row[5])})                                         
        for data in datas:
            yield Entity({
                'name': data["name"].strip(),
                'category': 'Group',
                'inclusion_date':data["inclusion_data"],
                "type": "SIP",
            })         
