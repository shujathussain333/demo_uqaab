from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
import time
from bs4 import BeautifulSoup

class SPVM_Scrapper(UqaabBaseSpider):
    name = 'spvm_scrapper'
    start_urls = ['https://spvm.qc.ca/en/MissingAndWantedPeople/Criminals']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        base_url = 'https://spvm.qc.ca'
        urls = []
        for i in range(1, 5):
            response = requests.get("https://spvm.qc.ca/en/MissingAndWantedPeople/Criminals/Page/" + str(i))
            soup = BeautifulSoup(response.text)
            urls.extend([base_url + i.a['href'] for i in soup.find_all('div', {'class': 'AvisDeRechercheVertical'})])
        alltables = []
        for url in urls:
            alltables.append(pd.read_html(requests.get(url).text)[0])
            time.sleep(2)
        for i in range(len(alltables)):
            alltables[i] = alltables[i].transpose()
            alltables[i].columns = alltables[i].loc[0]
            alltables[i].drop(0, inplace=True)
        spvm_df = pd.concat(alltables, sort=True)
        spvm_df.reset_index(drop=True, inplace=True)
        spvm_df['Name'] = spvm_df['First Name'] + spvm_df['Last Name']
        spvm_df.drop(['First Name', 'Last Name'], axis=1)
        spvm_df['Remarks'] = ''
        ex_columns = ['Eye color', 'Clothes', 'Case number', 'Case number', 'Height', 'Hair color', 'Investigator',
                      "Investigator's phone number", 'Weight']
        for col in ex_columns:
            spvm_df['Remarks'] = spvm_df['Remarks'] + col + '  :  ' + spvm_df[col].fillna('') + ','
        spvm_df.drop(ex_columns + ['First Name', 'Last Name'], axis=1, inplace=True)

        for index, row in spvm_df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'category': 'Group',
                'type': "SIP"
            })