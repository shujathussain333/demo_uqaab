import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class FirstcodeSpider(UqaabBaseSpider):
    name = 'icac_scrapper'
    #allowed_domains = ['https://www.icac.org.hk']
    start_urls = ['https://www.icac.org.hk/en/law/wanted/index.html']
    BASE_URL = 'https://www.icac.org.hk'

    def structure_valid(self, response):
        return 100

    def extact_data(self, response):
        
        mainList = response.xpath('//div[@id="mainWPList"]')
        indiv = mainList.xpath('./*[@class="wpItem"]')
        links = indiv.css(".wpItem a::attr(href)").extract()

        for url in links:
            absolute_url = self.BASE_URL + url
            yield scrapy.Request(absolute_url, callback=self.parse_attr, errback=self.error_handler)
        


    def parse_attr(self, response):
        table = response.xpath('//*[@class="wpInfos"]//tbody')
        rows = table.xpath('//tr')
        Name = rows[0].xpath('td//text()')[0].extract() #Name
        
        all_p = response.xpath('//*[@class="wpParticular"]//p')
        single_p = all_p[1]
        single_p = single_p.extract().replace('<p>', "")
        single_p = single_p.replace('</p>', "")
        breaks = single_p.split('<br>')
        
        for i in breaks:
            n = i.split(':')
            if "Date of Birth" in n[0]:
                DOB = n[1].replace('\t', '')
            elif "Nationality" in n[0]:
                Nationality = n[1]
                Nationality = n[1]
        Nationality = Nationality.strip('/')
        Nationality = Nationality.strip(" ")
        yield {
                'name': Name,
                'date_of_birth': DOB,
                'nationality': Nationality
                }
