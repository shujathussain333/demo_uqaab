# Author: M.Shujat Hussain
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
from PyPDF2 import PdfFileReader
from tabula import read_pdf
import io
import json
import pandas as pd
import numpy as np

class IndianDefaultorDirectorFirst(UqaabBaseSpider):
    
    name = 'indian_default_director_tenth'
    start_urls = ['http://www.mca.gov.in/MCA21/dca/EES_Companies_List/DIRLIST10_01900000_02050000.pdf']
    url = "http://www.mca.gov.in/MCA21/dca/EES_Companies_List/DIRLIST10_01900000_02050000.pdf"

    def structure_valid(self, response):
        df = read_pdf(self.url, encoding="utf-8",  pages= "all")
        return len(df) > 0
   
    def extact_data(self, response): 
        filedata = PdfFileReader(io.BytesIO(response.body))
        filecontent = filedata.getPage(0).extractText()
        datestring = self.string_to_date(filecontent.split(" ")[2])
        df = read_pdf(self.url, encoding="utf-8",  pages= "all", guess = False)
        for i in df.index:
            document_info = json.dumps({'document_info': None})
            document = []
            name = df['Name'][i]
            company_name = df['Company Name'][i]
            signatory_id = df['Signatory ID'][i]
            if signatory_id:
                document.append({'type': "Signatory ID",'id': str(signatory_id)})            
            cin = df['CIN'][i]
            if cin:
                document.append({'type': "CIN",'id': str(cin)})
            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document})                        
            defaulting_year = df['Defaulting Year'][i]
            remarks =  "Defaulting Year: {} ".format(defaulting_year)
            yield Entity({
                'category': 'Individual',
                'type': 'SIP',
                'name': name,
                'organization': company_name,
                'document': document_info,
                'inclusion_date': datestring,
                'remarks': remarks
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%m/%Y')
        except TypeError:
            return None