# -*- coding: utf-8 -*-
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class UkrspSpider(UqaabBaseSpider):
    name = 'ukrain_list_spider'
    start_urls = ['https://www.riskadvisory.com/sanctioned-individuals//']


    def structure_valid(self, response):
        data_rows = response.xpath('//table[@class="sanctions"]/tbody/tr')
        return len(data_rows) > 0

    def extact_data(self, response):

        for  entity in response.xpath('//table[@class="sanctions"]/tbody/tr'):

            name = self.extract_name(entity) 
            country = self.extract_country(entity)
            #role = self.extract_role(entity)
            remarks = self.extract_remarks(entity)
            category = self.extract_category(entity)
            designation = self.extract_designation(entity)


            yield Entity({
                "name" : name,
                "country": self.get_country_code(country),
                "category": category,
                "designation": designation,
                "remarks": remarks
            })

    @staticmethod
    def extract_name(entity):
        try:
            return entity.xpath('td[1]/text()').extract()[0].strip()
        except Exception:
            return None

    @staticmethod
    def extract_country(entity):
        try:
            return entity.xpath('td[2]/text()').extract()[0].strip()
        except Exception:
            return None

    @staticmethod
    def extract_role(entity):
        try:
            return entity.xpath('td[3]/text()').extract()[0].strip()
        except Exception:
            return None

    @staticmethod
    def extract_designation(entity):
        try:
            return [entity.xpath('td[3]/text()').extract()[0].strip()]
        except Exception:
            return []

    @staticmethod
    def extract_category(entity):
        try:
            category = entity.xpath('td[4]/text()').extract()[0].strip()

            if category in CATEGORY_OPTIONS:
                category = category
            else:
                category = CATEGORY_OPTIONS[-1]
            
            return category
        
        except Exception:
            return None

    @staticmethod
    def extract_remarks(entity):
        try:
            return entity.xpath('td[5]/text()').extract()[0].strip()
        except Exception:
            return None



