##Author: Shahzaib Mumtaz
## Created at: 8- Jan - 2019

import scrapy
from scrapy import http
import time
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class USStateAttorneyGeneralScraper(UqaabBaseSpider):
    name = 'us_attorney_scraper'
    start_urls = ['https://en.wikipedia.org/wiki/State_attorney_general']

    def structure_valid(self,response):
        soup = BeautifulSoup(response.body, 'html.parser')
        table = soup.find('table',{'class':'wikitable sortable'}).findAll('tr')[1:]
        return len(table) > 0

    def format_date(self, date):
        date = datetime.strptime(date,'%B %d, %Y')
        return date.date()

    def extact_data(self, response):
        thisdict = {}
        soup = BeautifulSoup(response.body, 'html.parser')
        table = soup.find('table',{'class':'wikitable sortable'}).findAll('tr')[1:]
        
        for i in table:
            name = i.findAll('td')[0].get_text().strip()
            thisdict['State'] = i.findAll('td')[1].get_text().strip()
            thisdict['Party'] = i.findAll('td')[2].get_text().strip()
            assum_off = i.findAll('td')[3].get_text().strip()
            assum_off = self.format_date(assum_off)
            thisdict['Assumed office'] = assum_off
            thisdict['Tenure'] = i.findAll('td')[4].get_text().strip()
            thisdict['Law School'] = i.findAll('td')[5].get_text().strip()
            thisdict['URL'] = response.url
            
            yield Entity({
                'name': name,
                'category':CATEGORY_OPTIONS[0],
                'nationality':'American',
                'country': 'US',
                'designation': 'State Attorney General',
                'remarks': 'State: {0}, Party: {1}, Assumed Office: {2}, Tenure: {3}, Law School: {4}'.format(
                    thisdict['State'], thisdict['Party'], thisdict['Assumed office'], thisdict['Tenure'], thisdict['Law School']
                )
            })