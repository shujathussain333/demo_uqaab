#Author  = Barkat Khan
#Edited by: Daniyal Faquih
#Correct the nationalities, previously on empty nationality entry previous row's nationality was being inserted 
#Corrected the inclusion date using dateutil as datetime was returning none

import io
import json
import re
from itertools import cycle
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class EU_Consolidated(UqaabBaseSpider):
    name = "eu_consolidated"
    start_urls = ["https://ofsistorage.blob.core.windows.net/publishlive/ConList.xls",]

    def structure_valid(self, response):
        
        with io.BytesIO(response.body) as resp_file:
            self.data = pd.io.excel.read_excel(resp_file, sheet_name=0)
        
        return len(self.data)>0

    def extact_data(self, response):
        
        df = self.data
        df.dropna(axis=1, how='all')
        df = df.fillna("")
        columns = df.columns

        if columns[0] =="Last Updated":
            inclusion_date = dateutil.parser.parse(columns[1])
            
        df.columns = df.iloc[0]
        df.reindex(df.index.drop(0))

        address_columns = [col for col in df.columns if "Address" in col]
        name_columns = [col for col in df.columns if "Name" in col]
        name_columns = sorted(name_columns, key=lambda x: x[5])
        address_columns = sorted(address_columns, key=lambda x: x[8])

        columns_to_drop = address_columns + name_columns
        
        df['NAME'] = df[name_columns].apply(lambda row: ' '.join(row.values.astype(str)), axis=1)
        df['ADDRESS'] = df[address_columns].apply(lambda row: row.values.astype(str), axis=1)

        df = df[df.columns.difference(columns_to_drop)]
        for index, row in df.iterrows():
            
            document_info = []
            if "Title" in row["Title"]:
                continue
            
            
            name = row["NAME"]
            name = re.sub('\s+',' ',name).strip()
            address =  list(filter(None, row["ADDRESS"]))
            pob = self.extract_pob(row["Town of Birth"], row["Country of Birth"])
            
            title = ""
            if row["Title"]:
                title =  row["Title"]
            
            nationality =""
            new_nationality = ""
            if row["Nationality"]:
                nationality = re.split(r"\d+\)|\[a-bA-B]+\)|\(\d+\)|\(\[a-bA-B]+\)",row["Nationality"])
                new_nationality_list = list(filter(None, nationality))
                new_nationality = ";".join(new_nationality_list)
               
            if row["NI Number"]:
                document_info.append({'type': "NI Number",'id': str(row["NI Number"])})
            
            if row["Group ID"]:
                document_info.append({'type': "Group ID",'id': str(row["Group ID"])})
            
            if row["Passport Details"]:
                passport_details = str(row["Passport Details"]).replace("Passport No:","")
                document_info.append({'type': "Passport Number",'id': str(passport_details)})

            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})
            
            dob_info = {'DOB': None, 'POB': None}
            
            if str(row["DOB"]):
                dob_info["DOB"] = str(row["DOB"])
            if row["Town of Birth"] or row["Country of Birth"]:
                dob_info["POB"] = self.extract_pob(row["Town of Birth"], row["Country of Birth"])
                    
            if dob_info == {'DOB': None, 'POB': None}:
                DOB_complete = json.dumps({'info': None})
            else:
                DOB_complete = json.dumps({'info': [dob_info]})
            
            
            remarksfield = ""
            for col  in ["Post/Zip Code", "Other Information", "Regime", "Listed On"]:
                remarksfield = "{0} {1}: {2}".format(remarksfield, col, row[col])

            designation = ""
            if row["Position"]:
                designation = row["Position"]
            
            country = ""
            if row["Country"]:
                country = row["Country"]
            
            category = ""
            if row["Group Type"]:
                category = row["Group Type"]
            else:
                category = "Individual"
            print(name)
            yield Entity({
                "name":name,  
                "nationality": new_nationality,
                "document": document_info, 
                "date_of_birth": DOB_complete, 
                "remarks": remarksfield,
                "designation": designation,
                "country": self.get_country_code(country), 
                "category": category,
                "type": "SAN",
                "inclusion_date": inclusion_date,
                "address": address
                }) 

    @staticmethod
    def extract_pob(tob, cob):
        tobs = re.split(r"\(\d+\)",tob)
        cobs = re.split(r"\(\d+\)",cob)
        new_tobs = list(filter(None, tobs))
        new_cobs = list(filter(None, cobs))
        
        poblist = []
        if len(new_tobs) == 0 and len(new_cobs) != 0:
            poblist = new_cobs
        elif len(new_tobs) != 0 and len(new_cobs) == 0:
            poblist = new_tobs

        elif len(new_tobs) >  len(new_cobs):
            maxi = new_tobs
            mini = new_cobs
            pob_tuple = list(zip(cycle(mini), maxi))
            for pob in pob_tuple:
                var_country, var_town = pob
                poblist.append("{0} {1}".format(var_town, var_country))
        
        elif len(new_tobs) <=  len(new_cobs):
            maxi = new_cobs
            mini = new_tobs
            pob_tuple = list(zip(cycle(mini), maxi))
            for pob in pob_tuple:
                var_town ,var_country= pob
                poblist.append("{0} {1}".format(var_town, var_country))
        
        pob = ";".join(poblist)
        return pob
