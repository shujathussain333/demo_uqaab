#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json

class ZimbabweWantedSpider(UqaabBaseSpider):

    name = 'zimbabwe_wanted'
    start_urls = ['http://www.zrp.gov.zw/index.php?option=com_content&view=article&id=132&Itemid=753']

    def structure_valid(self, response):
        data_rows =response.xpath('//div[@class="desc"]/a')
        return len(data_rows) > 0

    def extact_data(self,response):
        page_urls = response.xpath('//div[@class="desc"]/a/@href').extract()
        for page in page_urls:
            absolute_url = page
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        base_url = "http://www.zrp.gov.zw/"
        name = response.xpath('//div[@class="item-page"]//h2/text()').extract_first()
        name = name.split(":")
        name = name[1]
        offence = response.xpath('//div[@class="item-page"]//span[./strong="Offence: Robbery" or ./strong = "Offence"]//text()').extract()
        offence = " ".join(offence)
        national_registration_number = " "
        remarks = response.xpath('//div[@class="item-page"]//p//text()').extract()
        national_registration_number = response.xpath('//div[@class="item-page"]/p[3]//text()').extract()
        national_registration_number = "".join(national_registration_number)
        document_info = json.dumps({'document_info': None})
        document = []         
        if "National" in national_registration_number or "NR" in national_registration_number:
            document.append({'type': "National Reg_NO", 'id': national_registration_number})
            document_info = json.dumps({'document_info': document})                        
        address = None
        for j  in range(len(remarks)):
            if "Last known address" in remarks[j] or "address" in remarks[j]:
                if remarks[j]=="address: ":
                    address = remarks[j+1]
                else:                    
                    address = remarks[j]   
        remarks = " ".join(remarks)
        image_url = response.xpath('//div[@class="item-page"]//img/@src').extract_first()
        image = base_url+image_url
        if name:
            name = name.strip()
        yield Entity({
            "category": "Individual",
            "name":name,
            "address":address,
            "document":document_info,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })       