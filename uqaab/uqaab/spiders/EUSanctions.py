# Author: Sharjeel Ali
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import dateutil.parser


class EUSanctions(UqaabBaseSpider):
    name = 'EU_Sanctions'
    start_urls = [
        'https://webgate.ec.europa.eu/europeaid/fsd/fsf/public/files/xmlFullSanctionsList_1_1/content?token=n002hkee'
    ]

    def structure_valid(self, response):
        soup = BeautifulSoup(response.body, "xml")
        sdnEntry = soup.findAll('sanctionEntity')
        return len(sdnEntry) > 0

    def extact_data(self, response):

        soup = BeautifulSoup(response.body, "xml")
        sdnEntries = soup.findAll('sanctionEntity')

        for sdnEntry in sdnEntries:
            names = sdnEntry.findAll('nameAlias')
            subject_type = sdnEntry.find('subjectType')
            category = subject_type.get('code')
            regulation = sdnEntry.find('regulation')
            inclusion_date = regulation.get('publicationDate')
            identifications = sdnEntry.findAll('identification')
            name = names[0].get('wholeName')
            alias_info = json.dumps({'info': None})
            birthdates = sdnEntry.findAll('birthdate')
            if len(names) > 1:
                alias = [{'aka': alias.get('wholeName')} for alias in names[1:]]
                alias_info = json.dumps({'info': alias})
            document = [{'type': identification.get('identificationTypeCode'), 'id': identification.get('number')} for
                        identification in identifications]
            document_info = json.dumps({'document_info': document})
            dob = [{'DOB': birthday.get('birthdate') if birthday.get('birthdate') != None else birthday.get('year'),
                    'POB': None if birthday.get('place') == '' else birthday.get('place')} for birthday in birthdates]
            dob_info = json.dumps({'info': dob})
            addresses = sdnEntry.findAll('address')
            address = ["{} {} {} {} {}".format(address.get('zipCode'), address.get('street'),
                                               address.get('region'), address.get('city'),
                                               address.get('countryDescription')).strip() for address in
                       addresses]

            if len(address) == 0:
                address = None

            nationalities = sdnEntry.findAll('citizenship')
            nationality = '; '.join(["{}".format(nationality.get('countryDescription')) for nationality in nationalities])
            
            remarks = sdnEntry.find('remark')
            if remarks:
                remarks = remarks.text

            if category == 'person':
                category = 'Individual'
            else:
                category = 'Group'

            try:
                inclusion_date = dateutil.parser.parse(inclusion_date)
            except:
                inclusion_date = None

            yield Entity({
                "category": category,
                "name": name,
                'date_of_birth': dob_info,
                'nationality': nationality if nationality else None,
                "address": address,
                "document": document_info,
                "remarks": remarks,
                "type": "SAN",
                "aka": alias_info,
                "inclusion_date": inclusion_date
            })

