# Author =  Barkat Khan
import json
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class INDIANCOMPANIES_UNDER_POSO(UqaabBaseSpider):

    name = 'INDIANCOMPANIES_UNDER_POSO'
    url = 'https://connect2india.com/Under-Process-of-Striking-off-indian-companies'
    baseurl = "https://connect2india.com"

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.25,
        "DOWNLOAD_FAIL_ON_DATALOSS": True
    }

    def start_requests(self):
        return [
            scrapy.Request(
                url=self.url_with_params(),
                callback=self.parse,
                errback=self.error_handler)
        ]

    def url_with_params(self):
        return self.url

    def structure_valid(self, response):
        self.data_rows = response.css(
            "table[class='table table-striped table-bordered']")[0].css("tr")[1:]
        return len(self.data_rows) > 0

    @staticmethod
    def extractcompanyinfo(response):
        new_data_rows = response.css("div[class='col-md-6 details']")
        remarksfield = ""
        status = response.request.meta['status']
        url = response.url

        for row in new_data_rows:
            label = row.css('label::text').extract_first()
            data = row.css('p::text').extract_first()

            if data is None:
                continue

            data = data.strip()
            if data:
                if label == 'Registered Address':
                    companyaddress = data
                elif label == 'Company Name':
                    companyname = data
                elif label == 'Corporate Identification Number (CIN)':
                    companycin = data
                elif label == 'Registration Number':
                    companyregino = data
                elif label == "Primary Location":
                    companycity = data
                else:
                    if remarksfield:
                        remarksfield = "{0} , {1} : {2}".format(remarksfield, label, data)
                    else:
                        remarksfield = "{0} : {1}".format(label, data)

        remarksfield = "{0} , Status : {1} , Href : {2}".format(remarksfield, status, url)

        document_info = []

        if companycin:
            document_info.append({'type': "CIN", 'id': str(companycin)})
        if companyregino:
            document_info.append({'type': 'Registration Number', 'id': str(companyregino)})

        if document_info:
            document_info = json.dumps({'document_info': document_info})
        else:
            document_info = json.dumps({'document_info': None})

        yield Entity({
            'name': companyname,
            'city': companycity,
            'address': companyaddress,
            'remarks': remarksfield,
            'document': document_info,
            'category': 'Group'
        })

    def extact_data(self, response):
        data_rows = self.data_rows

        for row in data_rows:
            columns = row.css('td')
            companyurl = self.baseurl+columns[1].css('div a::attr(href)').extract_first()
            status = columns[6].css('div::text').extract_first()

            if companyurl:
                yield scrapy.Request(
                    url=companyurl,
                    meta={'status':status},
                    callback=self.extractcompanyinfo)

        nextpageurl = response.css("div[class='col-xs-12 col-sm-6 pagination right'] a::attr(href)"
                                  ).extract_first() 
        if nextpageurl:
            self.url = self.baseurl + nextpageurl
            yield scrapy.Request(
                url=self.url_with_params(),
                callback=self.parse,
                errback=self.error_handler)
