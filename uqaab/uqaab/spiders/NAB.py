import os
import io
import scrapy
import dateparser
from uqaab.items import Entity
from uqaab.environment import config
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd

class nab_static(StaticDataSpider):
    name = 'nab_static'

    def start_requests(self):
        file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'NAB.xlsx')
        url = "file://{path_to_file}".format(path_to_file=file_path)
        yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Sheet3')
        
        for index, entry in df.iterrows():
            inclusion_date = self.convert_to_date(entry['DO_Inclusion'])
            exclusion_date = self.convert_to_date(entry['DO_Exclusion'])

            yield Entity({
                'name': entry['Name'],
                'remarks': entry['Remarks'] if not pd.isnull(entry['Remarks']) else None,
                'inclusion_date': inclusion_date,
                'exclusion_date': exclusion_date,
                'country': 'PK',
                'category': 'Individual'
            })

    @staticmethod
    def convert_to_date(df_entry):
        if not pd.isnull(df_entry):
            return dateparser.parse(str(df_entry))
        return None

