#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import scrapy

class Denmark_Financial_Supervisory_Authority(UqaabBaseSpider):
	name = 'denmark_financial_supervisory_authority'
	start_urls = ['https://www.dfsa.dk/en/Supervision/Warnings-against-companies?searchString=&facets=(pagetype:)(period:)(subject:)(companyarea:)(letter:)(office:)(profession:)(regnskabsregel:)(regnskabsemne:)(temporaryfacetname1:)(temporaryfacetname2:)(temporaryfacetname3:)&options=(take:40)(skip:0)(sort:)']

	def structure_valid(self, response):
		dataRows = response.xpath('//div[@class="searchresult__wrapper"]//li[@class="searchresult__item"]')
		return len(dataRows) > 0

	def extact_data(self, response):
		dataRows = response.xpath('//div[@class="searchresult__wrapper"]//li[@class="searchresult__item"]')
		for data in dataRows:
			name = data.xpath('.//a[@class="searchresult__link"]//text()').extract_first()
			if("Warning against" in name):
				name = name.split("Warning against")
				name = name[1]
			name = name.replace("confusion/copying of website of","")
			name = name.replace("fraudulent use of the name and license of","")
			name = name.strip()
   			
			date = data.xpath('.//div[@class="col-xs-12 col-md-3"]//time[@class="searchresult__date"]//text()').extract_first()
			date = date.strip()
			date = dateutil.parser.parse(date)

			baseUrl = "https://www.dfsa.dk"
			nextPageUrl = baseUrl + data.xpath('.//div[@class="col-xs-12 col-md-9"]//div[@class="searchresult__content"]//a[@class="searchresult__link"]//@href').extract_first()

			if nextPageUrl is not None:
				yield scrapy.Request(nextPageUrl, callback=self.extract_link, meta={"Name":name,"date":date})

	def extract_link(self, response):
		name = response.meta.get('Name')
		date = response.meta.get('date')
		remarks = response.xpath('//div[@class="article__body glossary__article"]//p//text()').extract()
		remarks = ''.join(remarks[1:-3]).strip()
		remarks = "Summary: {}".format(remarks)
		yield Entity({
            "name": name,
            "category": "Group",
            "type": "SAN",
            "remarks": remarks,
            "inclusion_date": date
        })