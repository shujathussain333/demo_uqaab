from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class SecondcodeSpider(UqaabBaseSpider):
    name = 'sfc_scrapper'
    allowed_domains = ['www.sfc.hk']
    start_urls = ['https://www.sfc.hk/web/EN/regulatory-functions/enforcement/have-you-seen-these-people/people-subject-to-arrest-warrants/']
    url = 'https://www.sfc.hk/web/EN/'

    def structure_valid(self, response):
        links = response.css('div.haveyouseen-wrapper').css('a::attr(href)').extract()
        return len(links) > 0

    def extact_data(self, response):
        links = response.css('div.haveyouseen-wrapper').css('a::attr(href)').extract()

        for link in links:
            new_url = self.url + link
            yield Request(new_url, callback=self.extract_link)


    def extract_link(self, response):
        entities = response.css('div.personal_information').css('*::text').extract()
        image = response.xpath('//div[@class="content_wanted_person_wrapper"]//img/@src').extract_first()
        image = self.url + image
        search_by = ':'
        data = [s for s in entities if search_by.lower() in s.lower()]
        name = response.css('h1::text').extract_first()
        nationality = data[1].replace(':', '')
        gender = data[2].replace(':', '')
        dob = data[3].replace(':', '')
        pob = entities[-2].replace(':', '')
        dobs = {'DOB': dob, 'POB': pob}
        if dobs:
            dob_info = json.dumps({'info': [dobs]})
        else:
            dob_info = json.dumps({'info': None})
        remarks = response.css('div.content_wanted_person_wrapper').css('p::text').extract()
        remarks = ''.join(remarks).replace(':', '')
        yield Entity({
            'name': name,
            'category': "Individual",
            'remarks': remarks,
            'date_of_birth': dob_info,
            'nationality':nationality,
            'gender':gender,
            'type':'SIP',
            'image':image
        })