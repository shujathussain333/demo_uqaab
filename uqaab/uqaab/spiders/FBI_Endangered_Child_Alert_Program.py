#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class FBI_Endangered_Child_Alert_Program(UqaabBaseSpider):
    name = "fbi_endangered_child_alert_program"
    start_urls = ['https://www.fbi.gov/wanted/ecap']
 
    def structure_valid(self, response):
        data_divs = response.xpath('//div[@class="query-results pat-pager"]//ul//li')
        return len(data_divs) > 0

    def extact_data(self, response):
        data_divs = response.xpath('//div[@class="query-results pat-pager"]//ul//li')
        for data in data_divs:
            next_page = data.xpath('.//a//@href').extract_first()
            if next_page is not None:
                yield scrapy.Request(next_page, callback=self.extract_link)

        nextPageUrl = response.xpath('//p[@class="read-more text-center bottom-total visualClear"]/button/@href').extract_first()
        if nextPageUrl is not None:
            yield scrapy.Request(nextPageUrl, callback=self.extact_data)
    
    def extract_link(self, response):
        tableData = response.xpath('//div[@class="wanted-person-description"]//table/tbody//tr')
        name = response.xpath('//h1[@class="documentFirstHeading"]//text()').extract_first()
            
        imageUrl = response.xpath('//div[@class="col-md-4 wanted-person-mug"]/img/@src').extract_first()
            
        details = response.xpath('//div[@class="wanted-person-details"]//p//text()').extract()
        details = ' '.join(details)
        details = details.strip()
        
        remarksField = "Details: {}".format(details)

        yield Entity({
            "name": name,
            "category": "Individual",
            "type": "SIP",
            "remarks": remarksField,
            "image": imageUrl
        })