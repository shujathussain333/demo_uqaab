from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd

class Deliberate_Tax_Defaulter_Scrapper(UqaabBaseSpider):
    name = 'tax_defaulters'
    start_urls = ['https://www.gov.uk/government/publications/publishing-details-of-deliberate-tax-defaulters-pddd/current-list-of-deliberate-tax-defaulters']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        uk_gov_dfs = pd.read_html('https://www.gov.uk/government/publications/publishing-details-of-deliberate-tax-defaulters-pddd/current-list-of-deliberate-tax-defaulters')
        uk_gov_df = pd.concat(uk_gov_dfs)
        uk_gov_df.reset_index(inplace=True, drop=True)
        uk_gov_df.drop(['Unnamed: 7'], axis=1, inplace=True)
        uk_gov_df['Company'] = uk_gov_df['Business Trade or Occupation'].fillna('') + uk_gov_df['Business,\xa0trade\xa0oroccupation'].fillna('')
        uk_gov_df['DOInclusion'] = uk_gov_df['Period of Default'].fillna('') + uk_gov_df['Period\xa0of default'].fillna('')
        uk_gov_df.drop(['Period of Default', 'Period\xa0of default'], axis=1, inplace=True)
        uk_gov_df.drop(['Business Trade or Occupation', 'Business,\xa0trade\xa0oroccupation'], axis=1, inplace=True)
        uk_gov_df.columns = (['Address', 'Name', 'Remarks', 'Total amount of penalties charged','Total amount of tax/duty on which penalties are based', 'Company', 'DOInclusion'])

        for index, row in uk_gov_df.iterrows():
            yield Entity({
                'name': row.Name,
                'inclusion_date': row.DOInclusion,
                'organization': row.Company,
                'remarks': row.Remarks,
                'address': row.Address,
                'category': 'Group',
                'type': "SAN"
            })
