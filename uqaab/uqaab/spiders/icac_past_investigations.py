#Author: Sharjeel Ali Shaukat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request

class ICACPastInvestigations(UqaabBaseSpider):
    name = 'icac_past_investigations'
    start_urls = ['https://www.icac.nsw.gov.au/investigations/past-investigations']

    def structure_valid(self, response):
        names = response.xpath("//div[@class='article-widget']//a").extract()
        return len(names) > 0

    def extact_data(self, response):
        base_url = "https://www.icac.nsw.gov.au"
        cases_detail = response.xpath("//div[@class='article-widget']//a/@href").extract()
        for case_detail_link in cases_detail:
            yield Request(case_detail_link, callback=self.parse_case)
        next_page_link = response.xpath("//a[@id='NextLink']/@href").extract_first()
        if next_page_link:
            absolute_next_page_url = base_url + next_page_link
            yield Request(absolute_next_page_url, callback=self.extact_data)              

    def parse_case(self,response):
        base_url = "https://www.icac.nsw.gov.au"
        title = response.xpath("//article[@class='col-lg-9 col-md-12 col-sm-12 col-xs-12']/h1/text()").extract_first()
        remarks = response.xpath("//article[@class='col-lg-9 col-md-12 col-sm-12 col-xs-12']/p//text()").extract()
        remarks = " ".join(remarks)
        report_release_details = response.xpath("//article[@class='col-lg-9 col-md-12 col-sm-12 col-xs-12']//div[@class='bg grey pdd-40 mgn-top-40 report-links']//a/@href").extract()
        report_release_details = [base_url + each_detail if "https:" not in each_detail else each_detail for each_detail in report_release_details]
        report_release_details = ",".join(report_release_details)
        remarks = "case_detail: {} , further_detail_links: {}".format(remarks,report_release_details)
        yield Entity({
            "category": "unknown",
            "name": title,
            "remarks": remarks,
            "type": "SIP"
        })            


