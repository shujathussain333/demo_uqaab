# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import requests
from io import BytesIO
import pandas as pd
import numpy as np


class SECPDissolvedCompanies(UqaabBaseSpider):
    name = 'secp_dissolved_companies'
    start_urls = ['https://www.secp.gov.pk']

    def structure_valid(self, response):
        data = requests.post(
            "https://www.secp.gov.pk/document/list-of-dissolved-companies-as-on-11-dec-2018/?wpdmdl=33224")

        return data.status_code == 200

    def extact_data(self, response):
        data = requests.post(
            "https://www.secp.gov.pk/document/list-of-dissolved-companies-as-on-11-dec-2018/?wpdmdl=33224")
        with BytesIO(data.content) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0, header=None, index=None)
            df.drop(df.index[:0], inplace=True)
            # grab the first row for the header
            new_header = df.iloc[0]
            # take the data less the header row
            df = df[1:]
            # set the header row as the df header
            df.columns = new_header
            print (df.columns)
            
            for i in df.index:
                name = df['Company Name'][i]
                inclusion_date = df['Winding up Date'][i]
                if inclusion_date is np.nan:
                    inclusion_date = None
                city = df['CRO Name'][i]
                address = [df['Registered Address'][i]]
                status = "Status: {}".format(df['Status'][i])
                date_established = "Established Date: {}".format(df['Date of Incorporation'][i])
                cuin = "CUIN: {}".format(df['CUIN'][i])
                remarks = status + '; ' + date_established + '; ' + cuin

                yield Entity({
                    'category': 'Group',
                    'type': "SAN",
                    'name': name,
                    'city': city,
                    'inclusion_date': inclusion_date,
                    'address': address,
                    'remarks': remarks
                })
