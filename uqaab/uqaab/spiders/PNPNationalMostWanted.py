# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import io
import requests
from PyPDF2 import PdfFileReader

class PNPNationalMostWanted(UqaabBaseSpider):
    name = 'pnp_national_most_wanted'
    start_urls = ['http://pnp.gov.ph/']
    headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    def structure_valid(self, response):
        most_wanted = requests.post("http://pnp.gov.ph/images/most_wanted/MWPs-03022016-1.pdf", headers=self.headers)
        return most_wanted.status_code == 200

    def extact_data(self, response):
        most_wanted = requests.post("http://pnp.gov.ph/images/most_wanted/MWPs-03022016-1.pdf", headers=self.headers)
        with io.BytesIO( most_wanted.content) as resp_file:
            pdf_reader = PdfFileReader(resp_file)
            for page in pdf_reader.pages:
                person_details = page.extractText().split('\n')
                index_name = person_details.index("Name of Wanted Person")
                name = person_details[index_name + 1]

                index_inclusion_date = person_details.index("Date of MC")
                inclusion_date = person_details[index_inclusion_date + 1]
                index_offenses = person_details.index("Offense/s")
                offenses = "Offense: {} ".format(person_details[index_offenses + 1])
                index_criminal_case_nr = person_details.index("Criminal Case Nr")
                crimial_case = "Case No: {} ".format(person_details[index_criminal_case_nr + 1])
                index_issuing_court = person_details.index("Issuing Court")
                issuing_court = "Issuing Court: {} ".format(person_details[index_issuing_court + 1])
                index_reward = person_details.index("Reward")
                reward = "Reward: {} ".format(person_details[index_reward + 1])
                remarks = offenses + crimial_case + issuing_court + reward

                if len(name) > 0:
                    yield Entity({
                        'category': "Individual",
                        'name': name.strip(),
                        'remarks': remarks,
                        'inclusion_date': inclusion_date,
                        'type': 'SAN'
                    })








