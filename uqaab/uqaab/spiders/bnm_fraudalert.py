import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class FifthcodeSpider(UqaabBaseSpider):
    name = 'fraudalert'
    allowed_domains = ['www.bnm.gov.my']
    start_urls = ['http://www.bnm.gov.my/microsites/fraudalert/0302_seen.htm']

    def structure_valid(self, response):
        return 100

    def extact_data(self, response):
        Name_list = []
        Nationality_list = []
        Address_list = []
        content = response.css(".txt_stdContentFont")
        content_text = response.css(".txt_stdContentFont::text").extract()

        check = content.xpath(".//strong[.='Name:']/following-sibling::text()").extract()
        def chunks(l, n):
            for i in range(0, len(l), n):
                yield l[i:i+n]

        initialRows = list(chunks(check, 9))
        for initialRow in initialRows:
            Name = initialRow[0]
            Name = Name.replace('\xa0', "")
            Name = Name.replace('\r', "")
            Name = Name.replace('\n', "")
            Name = Name.strip(" ")

            Nationality = initialRow[6]
            Nationality = Nationality.replace('\xa0', "")
            Nationality = Nationality.replace('\r', "")
            Nationality = Nationality.replace('\n', "")
            Nationality = Nationality.strip(" ")

            Address = initialRow[8]
            Address = Address.replace('\xa0', "")
            Address = Address.replace('\r', "")
            Address = Address.replace('\n', "")

            yield {'name': Name,
                   'nationality': Nationality,
                   'address': Address
                    }
                 
        for j in content_text:
            if ("Name" in j) or ("Name:" in j):
                n = j.split(':')
                Name2 = n[1]

                Name_list.append(Name2)
            if ("Nationality" in j):
                n = j.split(':')
                Nationality2 = n[1]

                Nationality_list.append(Nationality2)

            if ("Last Address" in j) or ("Last Address :" in j):
                n = j.split(':')
                Address2 = n[1]
                Address_list.append(Address2)
                
        Nationality_list = Nationality_list[0:len(Name_list)]
        for i in range(0, len(Name_list)):
            yield {'name': Name_list[i],
                   'nationality': Nationality_list[i],
                   'address': Address_list[i]
                    }
            
        all_tables = response.xpath('//table//tbody')
        final_names = all_tables[0].xpath('./tr')[0].xpath('//p').css('.txtTitle04::text').extract()
        def hasNumbers(inputString):
            return any(char.isdigit() for char in inputString) 
        for eachName in final_names:
            if(hasNumbers(eachName)== False):
                eachName = eachName.replace('\r\n', " ")
                eachName = eachName.replace(' *', "")
                yield {'name': eachName,
                       'nationality': "",
                       'address': ""
                        }