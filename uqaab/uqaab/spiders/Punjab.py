import os
import io
import json
import scrapy
from datetime import datetime
from uqaab.items import Entity
from uqaab.environment import config
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd

class punjab_static(StaticDataSpider):
    name = 'punjab_static'

    def start_requests(self):
        file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'Punjab.xlsx')
        url = "file://{path_to_file}".format(path_to_file=file_path)
        yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Sheet2')

        for index, entry in df.iterrows():
            if entry['Documents'] and not pd.isnull(entry['Documents']):
                documents = json.dumps({'document_info': [{'type': 'CNIC', 'id': str(int(entry['Documents']))}]})
            else:
                documents = json.dumps({'document_info': None})

            dob = self.get_dob_string(entry['DOB'])

            yield Entity({
                'name': entry['Name'],
                'remarks': entry['Remarks'] if not pd.isnull(entry['Remarks']) else None,
                'address': entry['Address'] if not pd.isnull(entry['Address']) else None,
                'country': 'PK',
                'document': documents,
                'date_of_birth': dob,
                'category': 'Individual'
            })

    @staticmethod
    def get_dob_string(dob_string):
        if dob_string and not pd.isnull(dob_string):
            if isinstance(dob_string, datetime):
                return json.dumps({'info': [{'DOB': dob_string.strftime('%Y-%m-%d'), 'POB': None}]})
            else:
                return json.dumps({'info': [{'DOB': str(dob_string), 'POB': None}]})
        else:
            return json.dumps({'info': None})
