#Author: Daniyal Faquih
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.items import Entity
import pandas as pd
import dateutil.parser
import json

class GhostCompaniesAndNonExistent(UqaabBaseSpider):
    name = 'ghostcompanies'
    start_urls = ['http://www.sri.gob.ec/web/guest/empresas-inexistentes']

    def structure_valid(self, response):
        url = 'http://www.sri.gob.ec/BibliotecaPortlet/descargar/544e473b-eb65-47af-9135-e853c6a15558/Catastro%20Fantasma.xlsx'
        df = pd.read_excel(url)
        return len(df) > 0

    def extact_data(self, response):
        url = 'http://www.sri.gob.ec/BibliotecaPortlet/descargar/544e473b-eb65-47af-9135-e853c6a15558/Catastro%20Fantasma.xlsx'
        df = pd.read_excel(url)
        
        COL0 = "No"                     
        COL1 = "RUC_No"                 
        COL2 = "Business_Name"          
        COL3 = "Kind_of_taxpayer"       
        COL4 = "Zone"                   
        COL5 = "Province"               
        COL6 = "Five_Day_Notification"  
        COL7 = "Notification_Date"       
        COL8 = "Resolution_Number"      
        COL9 = "Date_Notification"      
        COL10 = "drop_Col_1"      
        COL11 = "drop_Col_2"      
        COL12 = "drop_Col_3"      
        COL13 = "drop_Col_4"       
        df = df.drop(df.index[[0,1]])
        cols = [COL0, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13]
        df.columns = cols
        for i in df.index[:-3]:
            name = df['Business_Name'][i]
            inclusion_date = df['Notification_Date'][i]
            inclusion_date = dateutil.parser.parse(str(inclusion_date))

            exclusion_date = df['Date_Notification'][i]
            exclusion_date = dateutil.parser.parse(str(exclusion_date))
            kindOfTP = df['Kind_of_taxpayer'][i]
            fiveDayNot = df['Five_Day_Notification'][i]
            remarks = "Kind of taxpayer: {} ,Five day notification: {}".format(kindOfTP, fiveDayNot)
            
            rucNo = df['RUC_No'][i]
            resoltionNo = df['Resolution_Number'][i]
            document = []
            document.append({'type': "Ruc_Number",'id': rucNo})
            document.append({'type': "Resolution_Number",'id': resoltionNo})

            if document:
                document_info = json.dumps({'document_info': document})
            
            zone = df['Zone'][i]
            province = df['Province'][i]
            address = "Zone: " + zone + " , " + "Province: " + province
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "document": document_info,
                "remarks": remarks,
                "inclusion_date": inclusion_date,
                "exclusion_date": exclusion_date,
                "address":address
            })