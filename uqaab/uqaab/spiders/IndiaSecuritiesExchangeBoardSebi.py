# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from io import BytesIO
from zipfile import ZipFile
import urllib
import pandas
import numpy as np


class IndiaSecuritiesExchangeBoardSebi(UqaabBaseSpider):
    name = 'india_securities_exchange_board_sebi'
    start_urls = ['https://www.bseindia.com/downloads/about/datal/file/SEBI%20DEBARRED%20entities%20on%2017012019.zip']

    def structure_valid(self, response):
        # data_rows = response.css('table.MsoNormalTable tr')
        return True

    def extact_data(self, response):
        url = urllib.request.urlopen(
            "https://www.bseindia.com/downloads/about/datal/file/SEBI%20DEBARRED%20entities%20on%2017012019.zip")
        zf = ZipFile(BytesIO(url.read()))
        match = [s for s in zf.namelist() if ".xls" in s][0]
        df = pandas.io.excel.read_excel(zf.open(match), sheet_name=0)
        df = df.replace(np.nan, '', regex=True)
        df = df[df['Name of the clients'] != '']

        for index, row in df.iterrows():
            organization = row["Scrip Name /Entity"]
            name = row["Name of the clients"]
            pan_no = "Pan: {}".format(row["PAN No."])
            date_of_order = "Order Date: {}".format(row["Date of Order "])
            order_period = "Order Details: {}".format(row["Order & Period of the Order"])
            notice = "Notice: {}".format(row["Notice "])
            remarks = pan_no + "; " + date_of_order + "; " + order_period + "; " + notice

            yield Entity({
                'category': "Group",
                'name': name.strip(),
                'remarks': remarks.strip(),
                'organization': organization.strip(),
                'type': 'SAN'
            })


