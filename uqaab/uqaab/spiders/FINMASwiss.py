##Author: Shahzaib Mumtaz
## Created on: 11-01-2019

import scrapy
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import requests
import dateutil.parser


class FINMASwissScraper(UqaabBaseSpider):
    name = 'finma_swiss_scraper'
    start_urls = [
        'https://www.finma.ch',
    ]

    custom_settings = {
        "DOWNLOAD_DELAY": 0.3,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 90,
        "HTTPCACHE_ENABLED": True,
    }

    def structure_valid(self, response):
        payload = {'ds': '77BC25B0-F2B7-4AA2-B0D8-5837913D9B93', 'Order': 1}
        resp = requests.post('https://www.finma.ch/en/api/search/getresult', data=payload)

        try:
            self.data = resp.json()['Items']
            return self.data is not None
        except:
            return False
    
    def extact_data(self, response):
        for item in self.data:
            name = item['Title']
            remarks = item['FacetColumn']
            url = self.start_urls[0] + item['Link']
            inclusion_date = dateutil.parser.parse(item['Date'])
            yield scrapy.Request(url, callback=self.extact_data2, errback=self.error_handler, meta={'name':name,'remarks':remarks,'inclusion_date':inclusion_date})
    
    def extact_data2(self, response):
        name = response.meta['name']
        remarks = response.meta['remarks']
        inclusion_date = response.meta['inclusion_date']

        soup = BeautifulSoup(response.body, 'html.parser')
        content = soup.find('table', {'class':'e-table vertical'}).findAll('tr')[1:]
        
        domicile = content[0].find('td').get_text().strip()
        address = content[1].find('td').get_text().strip()

        yield Entity({
            'name': name,
            'category': CATEGORY_OPTIONS[1],
            'remarks': 'Domicile: {0}, Commercial Register: {1}'.format(domicile, remarks),
            'inclusion_date': inclusion_date,
            'address': address
        })


