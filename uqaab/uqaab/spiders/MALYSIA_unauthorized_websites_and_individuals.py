# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class MalysiaUnauthorizesWebsites(UqaabBaseSpider):
    
    name = 'malysia_unauthorized_entities'
    start_urls = ['https://www.sc.com.my/regulation/enforcement/investor-alerts/sc-investor-alerts/list-of-unauthorised-websites-investment-products-companies-individuals']

    def structure_valid(self, response):
        data_rows = response.xpath('//ul[not(@class)]')
        return len(data_rows) > 0

    def extact_data(self, response):
        all_details = response.xpath('//ul[not(@class)]')
        for single_detail in all_details:
            further_detail = single_detail.xpath('li')
            for each_detail in further_detail:
                detail = each_detail.xpath('.//div[@class="a-inner-text"]//text()').extract()
                other_info = None
                if len(detail)>4:
                    other_info = detail[4] 
                if len(detail)<4:
                    detail.append(detail[2])    
                name_detail = detail[0]
                name_and_aliases = name_detail.split('\n')
                name = name_and_aliases[0]
                alias_info = json.dumps({'info': None})
                if len(name_and_aliases)>1:
                    aliases = name_and_aliases[1:]
                    alias = [{'aka': alias} for alias in aliases ] 
                    alias_info = json.dumps({'info': alias})                       
                location = detail[1]
                website = detail[2]
                year = detail[3]
                remarks = "website: {}  other_info: {}".format(website,other_info)   
                yield Entity({
                    "category": "Individual",
                    "name":name,
                    "remarks":remarks,
                    "aka":alias_info,
                    "address":location,
                    "type": "SAN"
                })       
