# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
class DevonAndCornwall(UqaabBaseSpider):

    name = 'devon_cornwal_wanted'
    start_urls = ['https://www.devon-cornwall.police.uk/news/court-and-convicted/',]

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="onlyOne group"]')
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.devon-cornwall.police.uk"
        details = response.xpath('//div[@class="onlyOne group"]')
        for persons in details:
            name = persons.xpath(".//figure/figcaption/text()").extract_first()
            remarks = persons.xpath(".//p/text()").extract_first()
            address_info = re.search('from(.+?)was', remarks)
            if address_info:
                address = address_info.group(1)
            image = persons.xpath(".//figure/a/@href").extract_first()
            image = base_url + image
            yield Entity({
                "category": "Individual",
                "address":address,
                "name":name,
        		"remarks":remarks,	
        		"type": "SIP",
        		"image":image
        	})
    



