# Author = Barkat Khan

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class BankingDirectory(UqaabBaseSpider):
    name = 'BankingDirectory'
    MAX_RECORDS_PER_PAGE = 20

    start_urls = [ 
        "http://www.bsp.gov.ph/banking/directory.asp?paging=prev&Start=0&Offset=20&BankName=&Address=&InstitutionTypeID=&submit=Find"
    ]

    def structure_valid(self, response):
        data_rows =response.css("table[class='tableborder']")[0].css("tr")[1:]
        return len(data_rows)>0

    def extact_data(self, response):

        data_rows =response.css("table[class='tableborder']")[0].css("tr")[1:-2]
        E = Entity()
        E['category'] = "Group"
        E['type'] = "SAN"

        for i in range(len(data_rows)):
            remarks = []
            columns = data_rows[i].css('td')
            
            E['name']=columns[1].css('p a::text').extract_first()
            typeoforganisation= columns[2].css('p::text').extract_first()
            contactperson = columns[3].css('p::text').extract_first()
            position = columns[3].css('em::text').extract_first()
            E['address'] = columns[4].css('p::text').extract_first()
            noofoffices= columns[4].css('em::text').extract_first()
            contactnumber = columns[5].css('p::text').extract_first()
            faxno= columns[5].css('em::text').extract_first()
            email = columns[6].css('p::text').extract_first()
            
            website = columns[6].css('a::attr(href)').extract_first()
            remarks.append('Type of Organisation: {0}'.format(typeoforganisation)) 
            remarks.append('Contact Person: {0}'.format(contactperson))
            remarks.append('Position: {0}'.format(position)) 
            remarks.append('No. of Offices: {0}'.format(noofoffices)) 
            remarks.append('Contact Number: {0}'.format(contactnumber))
            remarks.append('Fax No.: {0}'.format(faxno))
            remarks.append('Email: {0}'.format(email)),
            remarks.append('Website: {0}'.format(website))

            E['remarks'] =  ', '.join(remarks)

            yield E

        if len(data_rows) == self.MAX_RECORDS_PER_PAGE:
            data_rows = response.css("table[class='tableborder']")[0].css("tr")[-1]
            columns = data_rows.css('td')
            previous_next_links = columns[0].css('td a::attr(href)').extract()
            url = "http://www.bsp.gov.ph/banking/" + previous_next_links[-1]
            
            yield scrapy.Request(url=url, callback=self.parse, errback=self.error_handler)