import os
import io
import json
import scrapy
import dateparser
from uqaab.items import Entity
from uqaab.environment import config
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd

class sindh_static(StaticDataSpider):
    name = 'sindh_static'

    def start_requests(self):
        file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'Sindh.xlsx')
        url = "file://{path_to_file}".format(path_to_file=file_path)
        yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Sheet1')

        for index, entry in df.iterrows():
            name = self.get_name(entry['Name'])
            inclusion_date = self.convert_to_date(entry['DO_Inclusion'])
            address = self.get_address(entry['Address'])
            document = self.get_document(entry['Documents'])

            yield Entity({
                'name': name,
                'remarks': entry['Remarks'].strip() if not pd.isnull(entry['Remarks']) else None,
                'address': address,
                'country': 'PK',
                'document': document,
                'inclusion_date': inclusion_date,
                'category': 'Individual'
            })

    @staticmethod
    def get_name(name_str):
        if name_str and not pd.isnull(name_str):
            try:
                return name_str.strip()
            except AttributeError:
                return None
        return None

    @staticmethod
    def convert_to_date(df_entry):
        if not pd.isnull(df_entry):
            return dateparser.parse(str(df_entry))
        return None

    @staticmethod
    def get_address(address_str):
        if address_str and not pd.isnull(address_str):
            address = address_str.replace('r/o', '').replace('R/O', '')
            return address.strip()
        else:
            return None

    @staticmethod
    def get_document(doc_str):
        if doc_str and not pd.isnull(doc_str):
            try:
                documents = json.dumps({'document_info': [{'type': 'CNIC', 'id': str(int(doc_str))}]})
            except ValueError:
                documents = json.dumps({'document_info': [{'type': 'CNIC', 'id': doc_str}]})
        else:
            documents = json.dumps({'document_info': None})