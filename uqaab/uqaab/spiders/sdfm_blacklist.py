import re
import json
from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class SDFMBlacklist(UqaabBaseSpider):
    name = 'sdfm_blacklist'

    start_urls = [
        'http://www.sdfm.gov.ua/content/file/Site_docs/Black_list/zBlackListFull.xml',
    ]

    def structure_valid(self, response):
        return True    

    def extact_data(self, response):
        soup = BeautifulSoup(response.body, "lxml")
        accounts = soup.find_all('acount-list')
        for account in accounts:
            category = self.extract_category(account.select_one('type-entry'))
            name, aliases = self.extract_name_and_aliases(account.find_all('aka-list'))
            nationality = self.make_pipe_separated_string(account.find_all('nationality-list'))
            title = self.make_pipe_separated_string(account.find_all('title-list'))
            designation = self.make_pipe_separated_string(account.find_all('designation-list'))
            city, country, address = self.extract_addresses(account.find_all('address-list'))
            inclusion_date = self.string_to_date(account.select_one('date-entry').text)
            birth_info = self.extract_birth_info(account.find_all('date-of-birth-list'), account.find_all('place-of-birth-list'))
            try:
                comments = account.select_one('comments').text
            except AttributeError:
                comments = None

            yield Entity({
                'category': category,
                'name': name,
                'aka': aliases,
                'nationality': nationality, 
                'inclusion_date': inclusion_date,
                'remarks': comments,
                'city': city if city else None,
                'country': country if country else None,
                'address': address if address else None,
                'title': title,
                'designation': designation,
                'date_of_birth': json.dumps({'info': birth_info})
            })

    @staticmethod
    def extract_category(entry_type):
        TYPES_CATEGORY_MAP = {
            '1': 'Group',
            '2': 'Individual'
        }
        return TYPES_CATEGORY_MAP.get(entry_type.text, 'Unknown')

    @staticmethod
    def extract_name_and_aliases(aka_list):
        name = None
        akas = []
        for aka in aka_list:
            alias = ' '.join(a.text.strip() for a in aka.find_all(re.compile('^aka-name')))
            if aka.select_one('type-aka').text == 'N' and name is None:
                name = alias
            else:
                akas.append(alias)

        return name, akas

    @staticmethod
    def make_pipe_separated_string(selected_list):
        return '|'.join(item.text.strip() for item in selected_list) or None

    def extract_addresses(self, addresses):
        city_list = []
        country_list = []
        address_list = []
        for address in addresses:
            city = address.select_one('city')
            country = address.select_one('country')
            add_string = ' '.join(a.text.strip() for a in address.find_all(re.compile('^address')))
            city_list.append(city.text.strip() if city is not None else None)
            country_list.append(self.get_country_code(country.text.strip()) if country is not None else None)
            address_list.append(add_string if add_string else None)

        return city_list, country_list, address_list

    def extract_birth_info(self, dobs, pobs):
        date_strings = [dob.text for dob in dobs]
        dob_list = self.make_dob_list(date_strings)
        pob_list = [pob.text.strip() for pob in pobs]
        return self.combine_to_form_dob_info(dob_list, pob_list)

    def make_dob_list(self, dobs):
        date_list = []
        for dob in dobs:
            date_string_list = self.clean_and_split_date_string(dob)
            date_list = self.perform_date_conversion(date_string_list)

        return date_list

    def clean_and_split_date_string(self, dob_string):
        replaced_string = self.perform_text_replacements(dob_string)
        splitted_string = replaced_string.split(';')
        for string in list(splitted_string):
            if '-' in string:
                year_string = self.convert_range_to_comma_separated_string(string)
                if year_string:
                    splitted_string = map(lambda x:x if x != string else year_string, splitted_string)
        
        return splitted_string

    @staticmethod
    def perform_text_replacements(dob_string):
        replaced_string = dob_string.replace('Approximately', '')
        replaced_string = replaced_string.replace('Between', '')
        replaced_string = replaced_string.replace('Circa', '')
        replaced_string = replaced_string.replace('born in', '')
        replaced_string = replaced_string.replace(' and ', '-')
        replaced_string = replaced_string.replace('/', ';')
        replaced_string = replaced_string.replace(',', ';')
        return replaced_string
        
    @staticmethod
    def convert_range_to_comma_separated_string(range_string):
        try:
            start_end = list(map(int, range_string.split('-')))
            start_end.sort()
            return ';'.join(str(year) for year in range(start_end[0], start_end[1]+1))
        except ValueError:
            return None

    def perform_date_conversion(self, date_string_list):
        dates = []
        for date_string in date_string_list:
            date = self.try_date_conversion(date_string.strip())
            if date is not None:
                dates.append(date)
            elif ' in ' in date_string:
                date = self.dob_from_born_in_string(date_string)
                dates.append(date)
            else:
                pass

        return dates

    @staticmethod
    def try_date_conversion(date_string):
        formats_to_try = [
            '%Y',
            '%Y%m%d',
            '%b. %Y',
            '%b %Y',
            '%d %b. %Y',
            '%d %b %Y'
        ]
        for fmt in formats_to_try:
            try:
                return str(datetime.strptime(date_string, fmt))
            except ValueError:
                pass

    def dob_from_born_in_string(self, date_string):
        date_place = date_string.split(' in ')
        return self.try_date_conversion(date_place[0].strip())

    def combine_to_form_dob_info(self, dobs, pobs):
        info_list = [{'DOB': item[0], 'POB': item[1]} for item in zip(dobs, pobs)]
        if len(dobs) > len(pobs):
            info_list += [{'DOB': item, 'POB': None} for item in dobs[len(pobs) : ]]
        elif len(pobs) > len(dobs):
            info_list += [{'DOB': None, 'POB': item} for item in pobs[len(dobs) : ]]

        return info_list

    @staticmethod
    def string_to_date(date_string):
        return datetime.strptime(date_string, '%Y%m%d')