#Author: Daniyal Faquih

import numpy as np
import tabula
import json
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Bismark_Police_Most_Wanted(UqaabBaseSpider):
    name = 'bismark_police_most_wanted'
    start_urls = ['https://www.bismarcknd.gov/DocumentCenter/View/2906/Bismarck-PD-Active-Warrants?bidId=']
    
    def structure_valid(self, response):
        tables = tabula.read_pdf(self.start_urls[0], lattice= True, encoding="utf-8", pages= "all", multiple_tables=True)
        return len(tables) > 0

    def extact_data(self, response):
        tables = tabula.read_pdf(self.start_urls[0], lattice= True, encoding="utf-8", pages= "all", multiple_tables=True)
        bill = ""
        loopValue = 0
        for table in tables:
            table = table.dropna(how="all")
            colCount = len(table.columns)
            if(colCount == 5 or colCount == 4):
                name = []
                dob = []
                warrant = []
                col0 = "name"
                col1 = "dob"
                col2 = "warrant"
                col3 = "non"
                if(colCount == 5):
                    col4 = "none"
                    tempCols = [col0,col1,col2,col3,col4]
                    table.columns = tempCols
                    del table['non']
                    del table['none']
                else:
                    tempCols = [col0,col1,col2,col3]
                    table.columns = tempCols
                    del table['non']
                table['dob'] = np.where(table['dob'].isnull(), None, table['dob'])
                
                for i in table.index[1:]:
                    name.append(table['name'][i])
                    dob.append(table['dob'][i])
                    warrant.append(table['warrant'][i])
            elif(colCount == 3 or colCount == 2):
                col0 = "bill"
                col1 = "non"
                if(colCount == 3):
                    col2 = "none"
                    tempCols = [col0,col1,col2]
                    table.columns = tempCols
                    del table['non']
                    del table['none']
                else:
                	tempCols = [col0, col1]
                	table.columns = tempCols
                	del table['non']
                dataCounter = -1
                for i in table.index[1:]:
                    dataCounter+=1
                    bill = table['bill'][i]
                    dobs = {'DOB': dob[dataCounter], 'POB': ""}
                    if dobs:
                        dob_info = json.dumps({'info': [dobs]})
                    else:
                        dob_info = json.dumps({'info': None})
                    remarks = "Bill: {} ,Warrant: {}".format(bill,warrant[dataCounter])
                    print(name[dataCounter])
                    print(remarks)
                    print(dob_info)
                    print("__")
                    yield Entity({
                        "name": name[dataCounter],
                        "category": "Individual",
                        "type": "SIP",
                        "date_of_birth": dob_info,
                        "remarks": remarks
            	    })