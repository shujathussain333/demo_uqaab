# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import tabula
from uqaab.environment import config
import os


class SECPCeasedCompanies(StaticDataSpider):
    name = 'secp_ceased_companies'
    start_urls = ['https://www.secp.gov.pk']
    file_path = os.path.join (config ('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'List of ceased companies under Easy Exit from January 2013.pdf')

    def structure_valid(self, response):
        return True

    def extact_data(self, response):

        secp_defunct = tabula.read_pdf(self.file_path, encoding="latin1", lattice=True, pages='all')

        for index, row in secp_defunct.iterrows():
            name = row["Name of Company"]
            city = row['Company\rRegistration\rOffice']

            yield Entity({
                'category': 'Group',
                'type': "SAN",
                'name': name,
                'city': city,
            })
