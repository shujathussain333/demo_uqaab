from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class StateSponsorsTerrorismSpider(UqaabBaseSpider):
    name = "terrorist_exclusion_list"
    allowed_domains = ["https://www.state.gov"]

    start_urls = [
    'https://www.state.gov/j/ct/list/c14151.htm',
    ]
    def structure_valid(self, response):
        return 100

    def extact_data(self, response):
        table = response.css('table')[0]
        table_rows = table.css('tbody>tr')

        country_name = inclusion_date = ''
        temp = ''

        for row in table_rows[1:]:
            temp = row.xpath('td[1]/text()').extract()
            if len(temp) > 0:
                country_name = temp[0].strip()

            temp = row.xpath('td[2]/text()').extract()
            if len(temp) > 0:
                inclusion_date = temp[0].strip()
                inclusion_date = dateparser.parse(inclusion_date)

            yield Entity({
                'name': country_name,
                'type':'SAN',
                'category':'Country',
                'inclusion_date': inclusion_date,
            })