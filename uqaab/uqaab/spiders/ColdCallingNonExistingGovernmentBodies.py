#Author: Sharjeel Ali Shaukat

import io
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import numpy as np

class ColdCallingNonRegisteredNonExistingGovBodies(UqaabBaseSpider):
    name = 'cold_calling_non_existing_gov_bodies'
    start_urls = ['https://www.fsa.go.jp/en/refer/cold/201811-2.xlsx',]

    def structure_valid(self, response):
        df = self.create_dataframe(response)
        return len(df) > 0

    def extact_data(self, response):
        df = self.create_dataframe(response)
        df = df.replace(np.nan, '', regex=True)
        df.drop(df.index[:3], inplace=True)
        for index, row in df.iterrows():
            name = row[0]
            if len(name) > 1:
                address = [row[1].replace('\n', '').strip()]
                contact = "Contact: " + row[2].strip()
                website = "Website: " + row[3].strip()
                published_date = "Publicly Published: " + row[4].strip()
                remarks = published_date + ' ; ' + contact + '; ' + website + '; '

                yield Entity({
                    "category": "Group",
                    "name": name,
                    "address": address,
                    "remarks": remarks,
                    "type": "SIP",
                })

    @staticmethod
    def create_dataframe(response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0, header=None, index=None)

        return df
