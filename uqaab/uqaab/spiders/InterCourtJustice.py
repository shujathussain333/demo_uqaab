##Author: Shahzaib Mumtaz
## Created at: 8- Jan - 2019

import scrapy
from scrapy import http
import time
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class FMAAustriaScraper(UqaabBaseSpider):
    name = 'icj_scraper'
    start_urls = ['https://www.icj-cij.org/en/contentious-cases/culmination/desc']
    
    def structure_valid(self,response):
        soup = BeautifulSoup(response.body,'lxml')
        table = soup.find('table',{'class':'table table-condensed'}).findAll('tr',{'class':None})[1:]
        return len(table) > 0

    def extact_data(self, response):
        soup = BeautifulSoup(response.body,'lxml')
        table = soup.find('table',{'class':'table table-condensed'}).findAll('tr',{'class':None})[1:]
        for i in table:
            name = i.findAll('td')[0].get_text().strip()
            intro = i.findAll('td')[1].get_text().strip()
            culmi = i.findAll('td')[2].get_text().strip()

            yield Entity({ 
                'name':name,
                'category': CATEGORY_OPTIONS[4],
                'inclusion_date':  datetime.strptime(intro, '%Y'),
                'exclusion_date': datetime.strptime(culmi, '%Y'),
            })