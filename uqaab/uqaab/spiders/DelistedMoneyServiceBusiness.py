# Author= Barkat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 
import tabula 

class DelistedMoneyServiceBusiness(UqaabBaseSpider):
    name = 'delisted_msb'
    start_urls = [
        'http://www.bsp.gov.ph/banking/delistedMSBs.pdf'
    ]
    
    def structure_valid(self, response):
        url = response.request.url
        data_rows = tabula.read_pdf(url, encoding="utf-8", lattice=True)
        return len(data_rows) > 0
   
    def extact_data(self, response):
        url = response.request.url
        df = tabula.read_pdf(url, encoding="utf-8", lattice=True, pages= "all")
        df=df.replace("\r", " ", regex=True)
        
        for row, col in df.iterrows():
            
            if df.loc[row,"Institution Name"] == "Institution Name":
                continue
            
            name=self.extract_values(df.loc[row, "Institution Name"])
            address= self.extract_values(df.loc[row, "Address"])
            city = self.extract_values(df.loc[row, "Town"]) + " ," + self.extract_values(df.loc[row, "Province"]) + " ," + self.extract_values(df.loc[row, "Region"])
            
            document = json.dumps({'document_info': None})                
            document_field = self.extract_values(df.loc[row, 'BSP Registration No.'])
            if document_field is not None:
                document_array = [{
                    'type': 'BSP Registration No.',
                    'id': document_field
                }]
                document = json.dumps({'document_info': document_array}) 
            
            yield Entity({
                'name': name,
                'address': address,
                'city': city,
                'document': document,
                'remarks': 'Type of Office: {0}'.format(self.extract_values(df.loc[row, "Type of Office"])),
                'category': "Group",
                'type': "SAN"
            }) 
            
    @staticmethod
    def extract_values(value):
        try:
            if type(value) != str:
                return None
            else:
                return value
        except:
            return None