#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class CaliforniaFTBTaxDelequencies(UqaabBaseSpider):

    name = 'california_ftb_tax_delequencies'
    start_urls = ['https://www.ftb.ca.gov/about-ftb/newsroom/top-500-past-due-balances/delinquents-Corp.txt',]

    def structure_valid(self, response):
        data = str(response.body).split('\\t-\\t-\\t-\\t-\\t-\\t-\\r\\n\\t\\t')
        return len(data) > 0

    def extact_data(self,response):
        data = str(response.body).split('\\t-\\t-\\t-\\t-\\t-\\t-\\r\\n\\t\\t')  
        data_scrap = data[1].split('\\r\\n\\t\\t')
        for person in data_scrap:
            person_detail = person.split('\\t')
            name = person_detail[0].replace('"',"")
            address = person_detail[1].replace('"',"")
            total = person_detail[2].replace('"',"")
            line_filed = person_detail[4].replace('"',"")
            officers = person_detail[8].replace('"',"").strip("\\r\\n2")
            officers = officers.strip("\\r\\n3")
            officers = officers.strip("\\r\\n'")
            remarks = "Total: {} , Line_filed: {} , officers: {}".format(total,line_filed,officers)
            yield Entity({
                "category": "Individual",
                "name":name,
                "address":address,
                "remarks":remarks,  
                "type": "SIP"
            })             