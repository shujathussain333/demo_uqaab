#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request

class HumberPoliceWanted(UqaabBaseSpider):

    name = 'humber_police_wanted'
    start_urls = ['https://www.humberside.police.uk/wanted']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="teasers"]/div[@class="node node-wanted-person node-teaser clearfix" or "node node-wanted-person node-promoted node-teaser clearfix"]')
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.humberside.police.uk"
        page_urls = response.xpath('//div[@class="teasers"]/div[@class="node node-wanted-person node-teaser clearfix" or "node node-wanted-person node-promoted node-teaser clearfix"]')
        for page in page_urls:
            each_link = page.xpath('div[@class="content"]/a/@href').extract_first()
            absolute_url = base_url+each_link
            yield Request(absolute_url, callback=self.parse_page)  

        relative_next_url = response.xpath('//a[@title="Go to page 2"]/@href').extract_first()
        if relative_next_url:
            absolute_next_url = base_url + relative_next_url
            yield Request(absolute_next_url, callback=self.extact_data)

    def parse_page(self,response):
        base_url="https://www.gwent.police.uk"
        name = response.xpath('//div[@class="block block-system"]//h1/text()').extract_first()
        if name is None:
            name = response.xpath('//div[@class="page-title"]/h1/text()').extract_first()
        remarks = response.xpath('//div[@class="field field-name-body field-type-text-with-summary field-label-hidden"]//p[1]/text()').extract_first()
        if remarks is None:
            remarks=" "
        image = response.xpath('//div[@class="images"]//img/@src').extract_first()
        if image is None:
            image="None"
        if ":" in name:
            name = name.split(":")[1].strip()       
        yield Entity({
            "category": "Individual",
            "name":name,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })        
