# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
from time import strptime
from datetime import datetime
from scrapy import Request


class Bangladesh_deblist_Spider(UqaabBaseSpider):

    name = 'bangladesh_deblist_spider'

    total_pages = 20

    def start_requests(self):
        #' first start request
        # then calculate total pages
        # then run from page 1 to total_pages
        # https://doc.scrapy.org/en/latest/topics/spiders.html#scrapy.spider.Spider.start_requests

        for page in range(1, self.total_pages+1):

            payload =  "pageNo="+str(page)+"&size=10&searchFor=company&cmpName=&like=&firNameSearch=&lastNameSearch=&firName=&lastName=&dtFrom=&dtTo=&cntry=&state=&action=getDebarListCommon"
            url = "https://www.eprocure.gov.bd/InitDebarment"
            headers = {
            'content-type': "application/x-www-form-urlencoded"
            }
            yield Request(url, self.parse, method="POST", body=payload, headers=headers)

    def structure_valid(self, response):
        data_rows = response.xpath('//tr')
        return len(data_rows) > 0
        
    def extact_data(self, response):

        for entity in response.xpath('//tr'):
            name = self.extract_name(entity)
            address = self.extract_address(entity)
            country = self.extract_country(entity)
            inclusion_date = self.extract_date(entity,0, 11)
            exclusion_date = self.extract_date(entity, 15, 26)
            remarks = self.extract_remarks(entity)
            source = "https://www.eprocure.gov.bd/resources/common/DebarmentRpt.jsp"

            yield Entity({
                "category" : 'Group',
                "name" : name,
                "address" : [address],
                "country" : [self.get_country_code(country)],
                "inclusion_date" : inclusion_date,
                "exclusion_date" : exclusion_date,
                "category": CATEGORY_OPTIONS[1],
                "remarks" : remarks,
            })

    @staticmethod
    def extract_name(entity):
            return entity.xpath("td[2]/text()").extract_first()

    @staticmethod
    def extract_address(entity):
        return entity.xpath("td[4]/text()").extract_first()

    @staticmethod
    def extract_country(entity):
        return entity.xpath("td[3]/text()").extract_first()
    
    @staticmethod
    def extract_date(entity, start, end):

        format = '%m/%d/%Y'
        date = entity.xpath('td[6]/text()').extract_first()
        date = date[start:end]
        date = date.split('-')
        month = strptime(date[1], "%b").tm_mon
        date = str(month) + '/' + date[0] + '/' +  date[2]
        return datetime.strptime(date, format)

    @staticmethod
    def extract_remarks(entity):
        return entity.xpath("td[8]/span/text()").extract()[0]
    

