#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Canada_Caamf_Unauthorized_Websites(UqaabBaseSpider):
    name = "canada_caamf_unauthorized_websites"
    start_urls = ['https://www.amf-france.org/en_US/Resultat-de-recherche?isSearch=true&DOC_TYPE=&TEXT=unauthorized+websites&REFERENCE=&RG_NUM_ARTICLE=&RG_LIVRE=&DATE_PUBLICATION=&DATE_OBSOLESCENCE=&DATE_VIGUEUR_DEBUT=&DATE_VIGUEUR_FIN=&LANGUAGE=en&INCLUDE_OBSOLESCENT=false&ORDER_BY=OLD_TO_NOW&periode=Periode%3Bsourcedatetime1%3BDistribution_0020_date_0020_publication.5%3Bpub_180_day&typologie=Typologie%3Bsourcetree1%3B%2FMise_en_garde%2F%3BMise_en_garde']
    
    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="liste_item"]//ul//li[@class="resultat"]')
        return len(data_rows) > 0

    def extact_data(self, response):
        baseUrl = "https://www.amf-france.org"
        data_rows = response.xpath('//div[@class="liste_item"]//ul//li[@class="resultat"]')
        for data in data_rows:
            nextPageUrl = data.xpath('.//div[@class="bloc_unit content"]//a//@href').extract_first()
            if(nextPageUrl is not None):
                nextPageUrl = baseUrl +  nextPageUrl
                yield scrapy.Request(nextPageUrl, callback=self.extract_link)

    def extract_link(self, response):
        rawData = response.xpath('//div[@class="contenu_edito"]//div[@class="hc"]//p[2]//text()').extract()
        if("\xa0" in rawData[0]):
            rawData = response.xpath('//div[@class="contenu_edito"]//div[@class="hc"]//p[3]//text()').extract()
        for data in rawData:
            if("www." in data):
                name = data.strip().replace("- ","")
                yield Entity({
        	        "name": name,
        	        "category": "Group",
        	        "type": "SAN",
        	   })