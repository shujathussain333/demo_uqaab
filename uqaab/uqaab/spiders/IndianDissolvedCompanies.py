# Author = Barkat Khan

import json
from scrapy import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class IndianDissolvedCompanies(UqaabBaseSpider):
    name = 'Indian_Dissolved_Companies'
    url = 'https://www.registrationwala.com/incorporated-companies-list-in-india/dissolved-companies'
    baseurl = 'https://www.registrationwala.com/incorporated-companies-list-in-india/dissolved-companies'

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.3,
        "AUTOTHROTTLE_START_DELAY": 0.3,
        "AUTOTHROTTLE_MAX_DELAY": 0.6,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 800,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    def start_requests(self):
        return [
            Request(url=self.url_with_params(), callback=self.parse, errback=self.error_handler)
        ]

    def url_with_params(self):
        return self.url

    def structure_valid(self, response):
        self.data_rows = response.css("table[class='table table-bordered']").css("tbody").css("tr")
        return len(self.data_rows) > 0

    @staticmethod
    def extractdetails(response):
        company = dict()
        company['url'] = response.url.strip()

        keystats = response.css("#rw-home > div > div.col-md-5.col-12 > ul").css("li")[1:]

        for line in keystats:
            key = line.css("::text").extract_first().strip()
            value = line.css("strong::text").extract_first().strip()
            company[key] = value

        address_table = response.css(
            "#rw-address > div > div.col-sm-7.col-12 > div > table").css("tr")

        for line in address_table:
            columns = line.css("td")
            value = columns[0].css("::text").extract_first().strip()
            company['ADDRESS'] = value

        company_info_table = response.css(
            "#rw-home > div > div.col-md-7.col-12 > div:nth-child(1)").css("tr")

        for line in company_info_table:
            columns = line.css("td")
            key = columns[0].css("::text").extract_first().strip()
            value = columns[1].css("::text").extract_first().strip()
            company[key] = value

        number_of_directors = int(company['DIRECTORS'])
        if number_of_directors > 0:
            director_table = response.css(
                "#rw-home > div > div.col-md-7.col-12 > div.red-box.mt-50 > table").css("tr")

            directors_dict = dict()
            for director_info in director_table:
                line = director_table[director_info]
                columns = line.css("td")
                director_key = director_info + 1
                directors_dict[director_key] = dict()
                directors_dict[director_key]['Director Name'] = columns[0].css(
                    "a::text").extract_first().strip()
                directors_dict[director_key]['Director Profile'] = columns[0].css(
                    "a::attr(href)").extract_first().strip()
                directors_dict[director_key]['Date'] = columns[1].css(
                    "td::text").extract_first().strip()

            company['Directors Info'] = directors_dict

        status_table = response.css("#rw-trademark > div > div.col-md-5.col-12 > ul").css("li")[1:]
        for line in status_table:
            columns = line.css("li")
            key = line.css("::text").extract_first().strip()
            value = line.css("strong::text").extract_first().strip()
            company[key] = value

        trademark_table = response.css("#rw-trademark > div > div.col-md-7.col-12 > div > table"
                                      ).css("li")[1:]
        for line in trademark_table:
            columns = line.css("li")
            key = line.css("::text").extract_first().strip()
            value = line.css("strong::text").extract_first().strip()
            company[key] = value

        cin = company['CIN']
        registration_number = company['Registration Number']

        del company['CIN']
        del company['ADDRESS']
        del company['Registration Number']
        del company['Date of Incorporation']

        document_info = []

        if cin:
            document_info.append({'type': "CIN", 'id': str(cin)})
        if registration_number:
            document_info.append({'type': 'Registration Number', 'id': str(registration_number)})

        if document_info:
            document_info = json.dumps({'document_info': document_info})
        else:
            document_info = json.dumps({'document_info': None})

        remarksfield = json.dumps(company)
        name = response.request.meta['name']

        yield Entity({
            "category": "Group",
            "type": "SAN",
            "name": name,
            "document": document_info,
            "remarks": remarksfield
        })

    def extact_data(self, response):        
        for row in self.data_rows:
            columns = row.css('td')
            company_name = columns[1].css('::text').extract_first()
            href = columns[1].css('td a::attr(href)').extract_first()

            if href:
                yield Request(url=href, meta={"name":company_name}, callback=self.extractdetails)

        nextlink = response.css(
            "body > div:nth-child(6) > div > div.paging.text-right > ul > li:nth-child(14) > a::attr(href)"
            ).extract_first()

        if nextlink:
            self.url = "{0}{1}".format(self.baseurl, nextlink)
            yield Request(
                url=self.url_with_params(),
                callback=self.parse,
                errback=self.error_handler)
   