#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import json 

class IsleOfManProhibitedPersons(UqaabBaseSpider):

    name = 'isle_of_man_prohibited_persons'
    start_urls = ['https://www.iomfsa.im/enforcement/disciplinary-action/',]

    def structure_valid(self, response):
        data_rows = response.xpath("//section[@class='accordion-item']")
        return len(data_rows) > 0

    def extact_data(self,response):
        prohibited_persons = response.xpath("//section[@class='accordion-item']//div[@class='rte']")
        for person in prohibited_persons:
            name = person.xpath("p[./strong='Name:']/text()").extract_first()
            date_of_birth = person.xpath("p[./strong='Date of Birth: ']/text()").extract_first()
            address = person.xpath("p[./strong='Address (at date of the prohibition):']/text()").extract_first()
            remark = person.xpath("p[./strong='Particulars of prohibition: ']/text()").extract()
            remark = "".join(remark)
            aliases = person.xpath("p[./strong='Previous names:']/text()").extract()
            alias_info = json.dumps({'info': None})
            if aliases:
                alias = [{'aka': alias} for alias in aliases ]
                alias_info = json.dumps({'info': alias})
            inclusion_date = person.xpath("p[./strong/strong='prohibition']/text()").extract_first()
            if inclusion_date:
                inclusion_date = dateutil.parser.parse(inclusion_date)
            yield Entity({
                "category": "Individual",
                "inclusion_date":inclusion_date,
                "address":address,
                "aka":alias_info,
                "name":name,
        		"remarks":remark,	
        		"type": "SIP"
        	})