import dateutil
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import re
from scrapy import Request
import json


# This scrapper for debarred sanction list
class WorldBankDebarredList(UqaabBaseSpider):
    name = "world_bank_debarred_list"

    start_urls = [
        'http://www.worldbank.org/en/projects-operations/procurement/debarred-firms',
    ]

    custom_settings = {
        "DOWNLOAD_TIMEOUT": 600
    }

    def structure_valid(self, response):
       return True

    def extact_data(self, response):

        json_link = 'https://apigwext.worldbank.org/dvsvc/v1.0/json/APPLICATION/ADOBE_EXPRNCE_MGR/FIRM/SANCTIONED_FIRM'

        headers = {
            'apiKey': 'z9duUaFUiEUYSHs97CU38fcZO7ipOPvm',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        }

        yield Request(json_link, callback=self.extract_json, headers=headers, dont_filter=True)

    def extract_json(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        data_rows = jsonresponse['response']['ZPROCSUPP']
        for row in data_rows:
            name = row['SUPP_NAME']
            country = row['COUNTRY_NAME']
            inclusion_date = row['DEBAR_FROM_DATE']
            exclusion_date = row['DEBAR_TO_DATE']
            address = row['SUPP_ADDR']
            debar_reason = row['DEBAR_REASON']
            status = row['INELIGIBLY_STATUS']
            remarks = "Reason: {}, Status: {}".format(debar_reason, status)
            remarks = remarks.replace(' None', ' Not Available')
            if address is not None:
                address = [address]

            yield Entity({
                "name": name,
                "address": address,
                'country': country,
                "category": 'Individual',
                "type": "SAN",
                "exclusion_date": exclusion_date,
                "inclusion_date": inclusion_date,
                "remarks": remarks
            })
