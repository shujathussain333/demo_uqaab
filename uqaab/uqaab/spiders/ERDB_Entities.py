from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class ErdbEntitiesSpider(UqaabBaseSpider):
    name = 'ERDB-Entities'
    start_urls = ['https://www.ebrd.com/ineligible-entities.html']

    def structure_valid(self, response):
        data_rows = response.css('table')[0].css('tr')[2:]
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table')[0].css('tr')[2:]
        for row in data_rows:
            columns = row.css('td')
            name = self.extract_name(columns[0].css("*::text").extract())
            address = [self.extract_field(columns[1].css("*::text").extract())]
            nationality = self.extract_field(columns[2].css("*::text").extract())
            inclusion_date = self.extract_dates(columns[3].css("*::text").extract())
            exclusion_date = self.extract_dates(columns[4].css("*::text").extract())
            remarks = "Prohibited practice: {}".format(self.extract_field(columns[5].css("*::text").extract()))
            category = 'Group'
            if "mr" in name.lower():
                category ='Individual'

            yield Entity({
                'name': name,
                'category': category,
                'inclusion_date': inclusion_date,
                'exclusion_date': exclusion_date,
                'address': address,
                'nationality':nationality,
                'remarks':remarks
            })

    @staticmethod
    def extract_name(name):
        name = list(map(str.strip, name))
        name = [x for x in name if x]
        return name[0]

    @staticmethod
    def extract_field(data):
        data = ''.join(data).strip()
        return data

    @staticmethod
    def extract_dates(date):
        date = ''.join(date).strip()
        date = dateutil.parser.parse(date)
        return date