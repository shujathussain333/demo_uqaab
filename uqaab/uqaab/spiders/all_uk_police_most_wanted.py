# Author: Sharjeel Ali Shaukat

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class AllUKPoliceMostWanted(UqaabBaseSpider):
    name = 'all_uk_police_most_wanted'
    start_urls = ['https://crimestoppers-uk.org/give-information/most-wanted']
    payload = {"open_relay": "%EF%BF%BD%F2%BF%AA%BC%CC%B5%EF%BF%BD%EF%BF%BD%EF%BF%BD"}
    headers = {
        'origin': "https://crimestoppers-uk.org/",
        'upgrade-insecure-requests': "1",
        'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
        'content-type': "application/x-www-form-urlencoded",
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        'dnt': "1",
        'referer': "https://crimestoppers-uk.org/",
        'accept-encoding': "gzip, deflate",
        'accept-language': "en-US,en;q=0.8",
        'cache-control': "no-cache",
        'postman-token': "bece04e7-ee50-3764-ca50-e86d07ebc0f3"
    }
    list_of_people = []
    url = "https://crimestoppers-uk.org"

    custom_settings = {
        "DOWNLOAD_DELAY": 0.25
    }

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        yield scrapy.FormRequest(url=self.start_urls[0], formdata=self.payload, callback=self.extract_pages_data,
                                 headers=self.headers)

    def extract_pages_data(self, response, page=None, last_page=None):
        links = response.xpath("//div[@class='row wanted-gallery gallery-page']//a/@href").extract()
        self.list_of_people.extend(links)

        try:
            page = int(page) + 1
        except:
            page = 2
            last_page = response.xpath("//a[@class='page-link']//text()").extract()
            last_page = last_page[-1]

        if int(page) != int(last_page):
            new_url = "{}?page={}".format(self.start_urls[0], page)
            callback = lambda response: self.extract_pages_data(response, page, last_page)
            yield scrapy.FormRequest(url=new_url, formdata=self.payload, callback=callback,
                                     headers=self.headers)
        else:
            for link in self.list_of_people:
                people_url = self.url + link
                yield scrapy.FormRequest(url=people_url, formdata=self.payload, callback=self.extract_people_data,
                                         headers=self.headers)

    def extract_people_data(self, response):
        name = response.xpath("//*[contains(text(), 'Suspect name:')]/../text()").extract()[-1]
        police_force = "Police Force:{}".format(
            response.xpath("//*[contains(text(), 'Police force:')]/../text()").extract()[-1])
        crime_type = "Crime Type:{}".format(
            response.xpath("//*[contains(text(), 'Crime type:')]/../text()").extract()[-1])
        nick_name = response.xpath("//*[contains(text(), 'Nickname:')]/../text()").extract()[-1]
        nick_name = nick_name.strip()

        dob = response.xpath("//div[@class='col-md-11 col-lg-8']/text()").extract()
        dob = list(filter(lambda x: 'DOB' in x, dob))
        dob_info = json.dumps({'info': None})
        if len(dob) > 0:
            dob = dob[-1]
            dob = dob.strip().split(' ')[-1]
            dob = [{'DOB': dob, 'POB': None}]
            dob_info = json.dumps({'info': dob})

        alias_info = json.dumps({'info': None})
        if 'N/A' not in nick_name:
            nick_name = nick_name.split(',')
            alias = [{'aka': alias} for alias in nick_name]
            alias_info = json.dumps({'info': alias})

        cs_reference = response.xpath("//*[contains(text(), 'CS reference:')]/../text()").extract()[-1]
        document = [{'type': "CS Reference", 'id': cs_reference.strip()}]
        document_info = json.dumps({'document_info': document})

        full_details = response.xpath("//*[contains(text(), 'Full Details')]/../text()").extract()
        full_details = [item.strip() for item in full_details]
        full_details = [x for x in full_details if x]
        summary = "Summary:{}".format(full_details[0])
        remarks = police_force + ";" + crime_type + ";" + summary
        image = response.xpath("//div[@class='boxshadow']//img/@src").extract_first()
        yield Entity({
            "category": "Individual",
            "name": name,
            "document": document_info,
            "remarks": remarks,
            "type": "SIP",
            "aka": alias_info,
            "date_of_birth": dob_info,
            "image":image
        })
