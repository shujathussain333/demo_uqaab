from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
from bs4 import BeautifulSoup

class Department_of_Law_Public_Safety(UqaabBaseSpider):
    name = 'public_safety'
    start_urls = ['https://www.nj.gov/lps/ge/exclusion/exclusion_list/exclude_a.htm']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        ABC = 'abcdefghijklmnopqrstuvwxyz'
        dfs = []

        for alpha in ABC:
            try:
                print('Fetching Data...')
                soup = BeautifulSoup(
                    requests.get(f'https://www.nj.gov/lps/ge/exclusion/exclusion_list/exclude_{alpha}.htm').text)
                df = pd.read_html(str(soup.find('table', {'align': 'center', 'bordercolor': '#CCCCCC'})))[0]
                dfs.append(df)
            except Exception  as e:
                pass

            df = pd.concat(dfs)
            df['Name'] = df[1] + " " + df[0]
            df['DOB'] = df[2]
            df['DOInclusion'] = df[3]

            for index, row in df.iterrows():
                yield Entity({
                    'name': row.Name,
                    'date_of_birth': row.DOB,
                    'inclusion_date': row.DOInclusion,
                    'category': 'Individual',
                    'type': "SIP"
                })