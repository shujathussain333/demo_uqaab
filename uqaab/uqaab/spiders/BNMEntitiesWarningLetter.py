# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import pandas as pd
import numpy as np
import dateutil.parser
import camelot
import dateutil

class BNMEntitiesWarningLetter(UqaabBaseSpider):
    name = 'bnm_entities_warning_letter'
    start_urls = ['https://www.bnm.gov.my']

    def structure_valid(self, response):
        url = 'http://www.bnm.gov.my/documents/2018/List-of-Entities-Issued-Warning-Letter-by-BNM.pdf'

        bin = camelot.read_pdf(url)
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'http://www.bnm.gov.my/documents/2018/List-of-Entities-Issued-Warning-Letter-by-BNM.pdf'

        bin = camelot.read_pdf(url,pages='1-end')

        for i in range(0, len(bin)):
            df = bin[i].df
            df = df.iloc[1:]
            for index, row in df.iterrows():
                if len(row[1]) > 1:
                    try:
                        inclusion_date = dateutil.parser.parse(row[3])
                    except:
                        inclusion_date = None
                    yield Entity({
                        'category': "Group",
                        'name': row[1],
                        'type': 'SAN',
                        'address': [row[2]],
                        'inclusion_date': inclusion_date
                    })



