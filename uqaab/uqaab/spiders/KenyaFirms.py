# -*- coding: utf-8 -*-
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class KenyaFirms(UqaabBaseSpider):
    name = 'kenya_firms_spider'
    start_urls = ['http://www.ppoa.go.ke/2015-08-24-14-46-48/debarred-firms']

    def structure_valid(self, response):
        data = response.css('table tr')[2:]
        return len(data) > 0

    def extact_data(self, response):
        data_rows = response.css('table tr')[2:]
        for row in data_rows:
            columns = row.css('td')
            name = columns[1].css("*::text").extract_first().strip()
            address = []
            address.append(columns[2].css("*::text").extract_first().strip())
            address.append(columns[3].css("*::text").extract_first().strip())
            inclusion_date = self.extract_dates(columns[4].css("*::text").extract())
            exclusion_date = self.extract_dates(columns[5].css("*::text").extract())
            ground='GROUND FOR DEBARMENT: {}'.format(columns[6].css("*::text").extract())
            particulars='PARTICULARS OF REGISTERED PROPRIETOR/S: {}'.format(columns[7].css("*::text").extract())
            persons='PERSONS THAT SIGNED TENDER DOCUMENTS: {}'.format(columns[8].css("*::text").extract())
            remarks = '{} , {} , {}'.format(ground, particulars, persons)

            yield Entity({
                'name': name,
                'category': 'Group',
                'inclusion_date': inclusion_date,
                'exclusion_date': exclusion_date,
                'address': address,
                'remarks': remarks.strip(),
                'type':'SAN'
            })

    @staticmethod
    def extract_dates(date):
        date = ''.join(date).strip()
        date = dateutil.parser.parse(date)
        return date