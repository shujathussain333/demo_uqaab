##Author: Shahzaib Mumtaz
## Created at: 8- Jan - 2019

import scrapy
from scrapy import http
import time
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class USAInsuranceDirectoryScraper(UqaabBaseSpider):
    name = 'usa_insurance_directory_scraper'
    start_urls = ['https://www.iii.org/services/directory/company-categories/state-insurance-departments']

    def structure_valid(self,response):
        soup = BeautifulSoup(response.body,'html.parser')
        insurance_content = soup.find('div',{'class':'view-content'}).findAll('div',recursive=False)
        company_content = soup.find('div',{'class':'grid-962 seven-column-962'}).findAll('dl')
        return len(insurance_content) > 0 and len(company_content) > 0

    def extact_data(self, response):
        thisdict = {}
        soup = BeautifulSoup(response.body,'html.parser')
        insurance_content = soup.find('div',{'class':'view-content'}).findAll('div',recursive=False)
        for item in insurance_content:
            field = item.findAll('div',{'class':None},recursive=False)
            name = field[0].get_text().strip()
            address = item.find('div',{'class':'views-field views-field-field-company-address'}, recursive=False)
            thisdict['Street'] = address.find('div',{'class':'street-block'}).find('div',{'class':'thoroughfare'}).get_text().strip()
            thisdict['Locality'] = address.find('div',{'class':'addressfield-container-inline locality-block country-US'}).find('span',{'class':'locality'}).get_text().strip()
            thisdict['State'] = address.find('div',{'class':'addressfield-container-inline locality-block country-US'}).find('span',{'class':'state'}).get_text().strip()
            thisdict['Postal Code'] = address.find('div',{'class':'addressfield-container-inline locality-block country-US'}).find('span',{'class':'postal-code'}).get_text().strip()
            thisdict['Country'] = address.find('span',{'class':'country'}).get_text().strip()
            thisdict['Phone'] = item.find('div',{'class':'views-field views-field-field-phone'}).get_text().strip()
            thisdict['Address'] = thisdict['Street'] + ' ' + thisdict['Locality'] + ' ' + thisdict['State'] +' '+ thisdict['Postal Code']
            url = item.find('div',{'class':'views-field views-field-field-website-url'}).get_text().strip()
            thisdict['URL'] = url
            thisdict['Description'] = field[1].get_text().strip()
            
            yield Entity({
                'name':name,
                'category':CATEGORY_OPTIONS[1],
                'address':thisdict['Address'],
                'country': self.get_country_code(thisdict['Country']),
                'remarks': 'Phone: {0}, Description: {1}'.format(thisdict['Phone'], thisdict['Description'])
            })

        company_content = soup.find('div',{'class':'grid-962 seven-column-962'}).findAll('dl')
        thisdict = {}
        for item in company_content:
            phone = 'NA'
            url= 'NA'
            name = item.find('dt').get_text().strip()
            address = item.find('dd').find('p').get_text().strip()
            thisdict['Street'] = address.splitlines()[0].strip()
            thisdict['Locality'] = address.splitlines()[1].strip()
            thisdict['Country'] = address.splitlines()[2].strip()
            phone = address.splitlines()[3].strip().split('Phone: ')[-1]
            thisdict['Phone'] = phone
            url = address.splitlines()[4].strip().split('Web: ')[-1]
            thisdict['Address'] = thisdict['Street'] + ' ' + thisdict['Locality']
            
            yield Entity({
                'name':name,
                'category':CATEGORY_OPTIONS[1],
                'address':thisdict['Address'],
                'country': self.get_country_code(thisdict['Country']),
                'remarks':'Phone: {0}'.format(thisdict['Phone'])
            })
        