from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd

class Hong_Kong_Enforcement(UqaabBaseSpider):
    name = 'hkex'
    start_urls = ['https://www.hkex.com.hk/Listing/Rules-and-Guidance/Disciplinary-and-Enforcement/Enforcement-Notices-and-Announcements/Listing-Enforcement-Notices-Announcements?sc_lang=en']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www.hkex.com.hk/Listing/Rules-and-Guidance/Disciplinary-and-Enforcement/Enforcement-Notices-and-Announcements/Listing-Enforcement-Notices-Announcements?sc_lang=en'
        dfs = pd.read_html(url)
        dfs[1].drop('LE Notice', axis=1, inplace=True)
        dfs[1].columns = ['DOInclusion', 'Name', 'Remarks']
        df = dfs[1]

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'inclusion_date':row.DOInclusion,
                'category': 'Group',
                'type': "SIP"
            })