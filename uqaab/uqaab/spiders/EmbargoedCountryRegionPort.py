# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd


class EmbargoedCountryRegionPort(UqaabBaseSpider):
    name = 'embargoed_country_region_port'
    start_urls = ['https://www.export.pitt.edu/embargoed-and-sanctioned-countries']

    def structure_valid(self, response):
        data_rows = response.css('table tr')
        return len(data_rows) > 0

    def extact_data(self, response):

        data_rows = response.css('table tr')
        data_rows = data_rows[2:-1]
        dictionary = {}

        for row in data_rows:
            columns = row.css('td')
            targetted_ear = columns[0].css('div').css('*::text').extract()
            self.extract_ear(targetted_ear, dictionary)
            targetted_itar = columns[1].css('div').css('*::text').extract()
            self.extract_itar(targetted_itar, dictionary)
            targetted_ofac = columns[2].css('div').css('*::text').extract()
            self.extract_ofac(targetted_ofac, dictionary)

        for key, value in dictionary.items():
            k = key.replace(b'*', b'').decode('utf-8')
            yield Entity({
                "name": k,
                "remarks": value,
                "category": 'Country',
                "type": "SIP"
            })

    def extract_ear(self, targetted_ear, dictionary):
        text = ''.join(map(str, targetted_ear))
        if len(text) > 1:
            cleancountrylist = pd.Series(text.strip().split(',')).drop_duplicates().tolist()
            for country in cleancountrylist:
                if len(country.split(":")) > 1:
                    value_type = country.split(":")[0]
                country = country.split(":")[-1]
                try:
                    dictionary[country.encode('ascii', 'ignore').strip()] = ' ' + dictionary[
                        country.encode('ascii', 'ignore').strip()] + ',' + value_type
                except KeyError:
                    dictionary.update({country.encode('ascii', 'ignore').strip(): value_type + ',' + 'EAR'})

    def extract_itar(self, targetted_itar, dictionary):
        text = ''.join(map(str, targetted_itar))
        if len(text) > 1:
            cleancountrylist = pd.Series(text.strip().split(',')).drop_duplicates().tolist()

            for country in cleancountrylist:
                if len(country.split(":")) > 1:
                    value_type = country.split(":")[0]
                country = country.split(":")[-1]
                try:
                    dictionary[country.encode('ascii', 'ignore').strip().strip()] = ',' + dictionary[
                        country.encode('ascii', 'ignore').strip().strip()] + ',' + value_type + ',' + 'ITAR'
                except KeyError:
                    dictionary.update({country.encode('ascii', 'ignore').strip(): value_type + ',' + 'ITAR'})

    def extract_ofac(self, targetted_ofac, dictionary):
        text = ''.join(map(str, targetted_ofac))
        if len(text) > 1:
            cleancountrylist = pd.Series(text.strip().split(',')).drop_duplicates().tolist()
            for country in cleancountrylist:
                if len(country.split(":")) > 1:
                    value_type = country.split(":")[0]
                country = country.split(":")[-1]
                try:
                    dictionary[country.encode('ascii', 'ignore').strip()] = ' ' + dictionary[
                        country.encode('ascii', 'ignore').strip()] + ',' + value_type + ',' + 'OFAC'
                except KeyError:
                    dictionary.update({country.encode('ascii', 'ignore').strip(): value_type + ',' + 'OFAC'})
