# Author: Barkat Khan

from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class CRA(UqaabBaseSpider):
    name = 'credit_rating_agencies'
    start_urls = [
        'https://www.esma.europa.eu/supervision/credit-rating-agencies/risk'
    ]

    def structure_valid(self, response):
        data_rows = response.css('tbody')
        return len(data_rows) > 0
   
    def extact_data(self, response):
        for tablecount in range(2):
            data_rows = response.css('tbody')[tablecount].css('tr')

            for i in range(len(data_rows)):
                columns = data_rows[i].css('td')

                if tablecount == 0:
                    datekey = 'inclusion_date'
                    datefield = self.extract_field(columns[3])
                    
                elif tablecount == 1:
                    datekey = 'exclusion_date'
                    datefield = self.extract_datefield(columns[3])
                    
                datevalue = self.string_to_date(datefield)
                document = json.dumps({'document_info': None})
                
                document_field = self.extract_field(columns[4])
                if document_field is not None:
                    document_array = [{
                        'type': 'Legal entity identifier',
                        'id': document_field
                    }]
                    document = json.dumps({'document_info': document_array}) 

                yield Entity({
                    'name': self.extract_field(columns[0]),
                    'country': self.get_country_code(self.extract_field(columns[1])),
                    'remarks': 'Status: {0}'.format(self.extract_field(columns[2])),
                    'document': document,
                    'category': 'Group', 
                    datekey : datevalue
                })
                
    @staticmethod
    def extract_datefield(col):
        val = col.css('td p a::text').extract_first()
        if val is None:
            val = col.css('td a::text').extract_first()
        
        return val

    @staticmethod
    def extract_field(col):
        val = col.css('td p::text').extract_first()
        if val is None:
            val = col.css('td::text').extract_first()
        
        return val

    def string_to_date(self, date_string):
        try:
            return datetime.strptime(date_string, '%d %B %Y')
        except TypeError:
            return None
