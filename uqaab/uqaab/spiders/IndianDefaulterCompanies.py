# Author = Barkat Khan
import os
from datetime import datetime
import scrapy
from uqaab.environment import config
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd
import tabula
import json
from PyPDF2 import PdfFileReader

class IndianDefaulterCompanies(StaticDataSpider):

    name = 'indiandefaultercompanies'
    start_urls = ['http://www.mca.gov.in/MinistryV2/defaultercompanieslist.html']
    baseurl = "http://www.mca.gov.in"

    def structure_valid(self, response):
        pdffilelinks = response.css("ul[class='resposive_tablet3'] a::attr(href)").extract()
        pdffilelinkslist = list(map(lambda href:self.baseurl+href, pdffilelinks))
        self.pdffilenames = list(map(lambda filenames:filenames.split("/")[-1] , pdffilelinks))
        return len(pdffilelinkslist) > 0
    
    def extact_data(self, response):

        for files in self.pdffilenames :
            file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', files)
            df = tabula.read_pdf(file_path, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True)        
            
            filedata = PdfFileReader(file_path, "rb")
            filecontent = filedata.getPage(0).extractText()
            datestring = self.string_to_date(filecontent.split(" ")[2])

            for index in range(len(df)):
                document_info = []

                if str(df.loc[index,'CIN']):
                    document_info.append({'type': "CIN",'id': str(df.loc[index,'CIN'])})
                

                if len(document_info) > 0:
                    document_info = json.dumps({'document_info': document_info})
                else:
                    document_info = json.dumps({'document_info': None})
                
                yield Entity({
                    'category': 'Group',
                    'type': 'SIP',
                    'document' : document_info,
                    'inclusion_date' : datestring,
                    'name': df.loc[index,'Company Name']})

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%m/%Y')
        except TypeError:
            return None