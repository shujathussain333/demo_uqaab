# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import pandas as pd
import numpy as np
import dateutil.parser
import camelot
import dateutil

class CIMANotRegistered(UqaabBaseSpider):
    name = 'cima_not_registered'
    start_urls = ['https://www.cima.ky']

    def structure_valid(self, response):
        url = 'https://www.cima.ky/upimages/noticedoc/WebsiteswithCaymanAddressesNotRegisteredorLicensed_1544544156.pdf'

        bin = camelot.read_pdf(url)
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'https://www.cima.ky/upimages/noticedoc/WebsiteswithCaymanAddressesNotRegisteredorLicensed_1544544156.pdf'

        bin = camelot.read_pdf(url,pages='1-end')

        for i in range(0, len(bin)):
            df = bin[i].df
            df = df.iloc[1:]

            for index, row in df.iterrows():
                if "Please note that this entity is not the same entity" not in row[0]:
                    name = row[0]
                    website = "Website: {}".format(row[2])
                    address = row[1]
                    yield Entity({
                        'category': "Group",
                        'name': name,
                        'type': 'SAN',
                        'address': [address],
                        'remarks':website,
                    })



