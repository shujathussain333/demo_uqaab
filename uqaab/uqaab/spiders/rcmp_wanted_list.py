# Author: Ilyas Yousuf

import bs4 as bs
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import scrapy

class RCMPWantedList(UqaabBaseSpider):
    name = "rcmp_wanted_list"
    start_urls = ['http://www.rcmp-grc.gc.ca/en/wanted']
    
    def structure_valid(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        return len(soup.find_all('a')) > 0

    def isIndividual(self, name, ultag, image):
        category = "Individual"
        aliases = []
        remarks = ''
        dob_info = None
        nationality = None
        for li in filter(None, ultag.find_all('li')):
            if li.find("strong") is not None:
                data = li.find("strong").text.replace(" ", '').replace(":", "")
                if data == 'Aliases':
                    aliases.append(li.text.split(": ")[1])
                if data =='Born':
                    dob_info = li.text.split(": ")[1]
                if data =='Place of Birth':
                    nationality = li.text.split(": ")[1]
                if dob_info or nationality:
                    dob = [{'DOB': dob_info, 'POB': nationality}]
                else:
                    dob = None
                if data == 'Sex' or data == 'EyeColour' or data == 'Weight' or data == 'HairColour' or data == 'Height':
                    remarks = remarks + li.text.split(": ")[0] + ":" + li.text.split(": ")[1] + ","    
        return ({
            'category' : category,
            'name' : name,
            'aka' : aliases, 
            'remarks' : remarks,
            'type' : "SIP",
            'date_of_birth' : json.dumps({'info': dob}),
            'image':image
        })

    def extact_data(self, response):
        url = "http://www.rcmp-grc.gc.ca/en/"
        soup = bs.BeautifulSoup(response.body, "lxml")
        for i in soup.find_all('a'):
            if i.get('href').find("wanted") !=-1:
                data_link = url+i.get("href")
                yield scrapy.Request(data_link, callback=self.extract_individual, errback=self.error_handler)
                
    def extract_individual(self, response):
        soup = bs.BeautifulSoup(response.body,'lxml')
        name = soup.find('h1',{"class":"page-header mrgn-tp-md"}).text.lstrip()
        ultag = soup.find('ul', {'class': 'colcount-md-2 mrgn-tp-md list-unstyled'})
        image = soup.find("img")
        image = "http://www.rcmp-grc.gc.ca" + image["src"]
        yield Entity(self.isIndividual(name, ultag, image))