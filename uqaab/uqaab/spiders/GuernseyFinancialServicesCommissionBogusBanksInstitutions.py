# -*- coding: utf-8 -*-
#Author: Sharjeel ali Shaukat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class GuernseyFinancialServicesCommissionBogusBankInstitutions(UqaabBaseSpider):
    name = 'guernsey_fin_services_comm_bogus_bank_inst'
    start_urls = [
        'https://www.gfsc.gg/consumers/scams-and-bogus-financial-institutions/bogus-banks-and-other-financial-institutions']

    def structure_valid(self, response):
        data_cols = response.css('tbody tr')
        return len(data_cols) > 0

    def extact_data(self, response):
        data_rows = response.css('tbody tr')
        data_rows = data_rows[1:]
        for row in data_rows:
            columns = row.css('td')

            name = columns[0].css('*::text').extract_first()
            remarks = columns[1].css('*::text').extract_first()

            yield Entity({
                'name': name,
                'category': 'Group',
                'type': 'SIP',
                'remarks': remarks
            })
