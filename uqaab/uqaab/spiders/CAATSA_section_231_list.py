#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import scrapy

class CaatsaSectionListSpider(UqaabBaseSpider):
    name = "caatsa_section_231_list"
    start_urls = ['https://www.state.gov/caatsa-section-231d-defense-and-intelligence-sectors-of-the-government-of-the-russian-federation/']

    def structure_valid(self, response):
        tableRows = response.xpath('//div[@class="table-responsive double-scroll"]//table//tr')
        return len(tableRows) > 0

    def extact_data(self, response):
        rows = response.xpath('//div[@class="table-responsive double-scroll"]//table//tr')
        for row in rows:
            rawData = row.xpath('.//td//text()').extract()
            name = rawData[0]
            dateOfInclusion = dateutil.parser.parse(rawData[1])
            yield Entity({
                'name': name,
                'inclusion_date': dateOfInclusion,
                'type': 'SIP',
                'category': 'Group'
            })