# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
from scrapy import Request
import json
class USAstategamingExclusion(UqaabBaseSpider):
    
    name = 'usa_state_gaming_exclusion_list'
    start_urls = ['http://www.njdge.org/exclusion/exclusion_list/exclude_a.htm']

    def structure_valid(self, response):
        data_rows = response.xpath('(//div[@align="left" and not(@class)])[5]/table//tr[position()>1]')
        return len(data_rows) > 0

    def extact_data(self, response):
        base_url = "http://www.njdge.org/exclusion"
        details = response.xpath('(//div[@align="left" and not(@class)])[5]/table//tr[position()>1]')
        for detail in details:
            last_name = detail.xpath('td[1]/a/text()').extract_first()
            first_name = detail.xpath('td[2]/a/text()').extract_first()
            name = first_name + " " + last_name
            if "\n" in name:
                name = str.join("", name.splitlines())
                name = " ".join(name.split())
            date_of_birth = detail.xpath('td[3]//text()').extract_first()
            dobs = {'DOB': date_of_birth, 'POB': None}
            dob_info = json.dumps({'info': [dobs]})            
            order_date = detail.xpath('td[4]//text()').extract_first()
            if order_date:    
                order_date = dateutil.parser.parse(order_date)            
            remarks_pdf = detail.xpath('td[1]/a/@href').extract_first()
            trim_string = remarks_pdf.replace('..','')
            remarks = base_url+trim_string
            if ".htm"  in remarks:
                yield Request(remarks, callback=self.parse_page, meta={'Name': name, 'date_of_birth': dob_info, 'order_date':order_date,'remarks':remarks})
            else:        
                remarks = "further_PDF_details: {}".format(remarks)
                yield Entity({
                    "category": "individual",
                    "name":name,
                    "exclusion_date":order_date,
                    "date_of_birth":dob_info,
                    "remarks":remarks,
                    "type": "SIP"
                })
                
    def parse_page(self,response):
        base_url = "http://www.njdge.org"
        name = response.meta.get('Name')
        date_of_birth = response.meta.get('date_of_birth')
        order_date = response.meta.get('order_date')
        remarks = response.meta.get('remarks')
        img_extract = response.xpath('//div[@align="center"][img]/img/@src').extract_first()
        image_url = img_extract.replace('..','')
        image = base_url + image_url
        remarks = "further_details: {}".format(remarks)
        yield Entity({
            "category": "individual",
            "name":name,
            "exclusion_date":order_date,
            "date_of_birth":date_of_birth,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })      


