#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Japan_National_Police_Agency_Designated_Boryokudan(UqaabBaseSpider):
    name = 'japan_national_police_agency_designated_boryokudan'
    start_urls = ['http://www.japanzine.jp/article/jz/2361/japans-most-wanted']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="entry post clearfix"]//p')
        return len(data_rows) > 0
    
    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="entry post clearfix"]//p')
        for data in data_rows[2:-2]:
            rawData = data.xpath('.//text()').extract()
            rawData = [x.replace('\n', '') for x in rawData]
            for rawChar in rawData:
                if rawChar == '':
                    rawData.remove(rawChar)
            name = rawData[0]
            remarks = rawData[1]
            yield Entity({
                "name": name,
                "category": "Individual",
                "type": "SIP",
                "remarks": remarks
            })