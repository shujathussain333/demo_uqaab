#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import json

class Singapore_Monetary_Authority_Enforcement(UqaabBaseSpider):
    name = 'singapore_monetary_authority_enforcement'
    start_urls = ['https://www.mas.gov.sg/regulation/enforcement/enforcement-actions?page=1&q=&sort=&rows=All#MasXbeEnforcementActionKeyword']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="mas-table mas-table--full-width-tablet mas-table--shadow-primary-3"]/table/tbody//tr')
        return len(data_rows) > 0
    
    @staticmethod
    def cleanData(str):
        try:
            return str.strip()
        except:
            return str
    
    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="mas-table mas-table--full-width-tablet mas-table--shadow-primary-3"]/table/tbody//tr')
        for data in data_rows:
            issueDate = data.xpath('.//td[1]//text()').extract_first()
            issueDate = dateutil.parser.parse(issueDate)
            companyName = data.xpath('.//td[2]//text()').extract_first()
            companyName = self.cleanData(companyName)
            if(companyName is None):
            	companyName = "Anonymous"

            actiontype = data.xpath('.//td[3]//text()').extract_first()
            actiontype = self.cleanData(actiontype)
            title = data.xpath('.//td[4]//a//text()').extract_first()
            title = self.cleanData(title)
            if(title is None):
            	title = ""    
            
            title = [{'title': title}]
            titleInfo = json.dumps({'info': title})
            remarks = "Action Type: {}".format(actiontype)
            yield Entity({
                "name": companyName,
                "title": titleInfo,
                "inclusion_date": issueDate,
                "category": "Group",
                "type": "SAN",
                "remarks": remarks
            })