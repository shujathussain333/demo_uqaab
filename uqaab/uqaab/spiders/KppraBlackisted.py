# Author =  Barkat Khan
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from datetime import datetime

class KppraBlackisted(UqaabBaseSpider):

    name = 'kppra_blackisted'
    start_urls = ['https://www.kppra.gov.pk/blacklistfirms/']

    def structure_valid(self, response):
        self.data_rows = response.css("table[class='table-bordered table-striped']").css("tr")[1:]  
        return len(self.data_rows) > 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

    def extact_data(self, response):
        data_rows =  self.data_rows
      
        for i in range(len(data_rows)):

            columns = data_rows[i].css('td')
            name_of_procurement_entity = columns[1].css("::text").extract_first().strip()
            companyname = columns[2].css("td::text").extract_first().strip()
            reason = self.extract_reason(columns[3])
            href = columns[7].css("a::attr(href)").extract_first()
            inclusiondate = self.string_to_date(columns[4].css("td").css("::text").extract_first())
            blacklisttype = columns[5].css("td").css("::text").extract_first()
            exclusion = columns[6].css("td").css("::text").extract_first()
            
            if blacklisttype == "Permanent":
                exclusiondate = None
            else:
                exclusiondate = self.string_to_date(exclusion)

            remarksfield =  "Blacklisting Type: {0} , Reason: {1} , Name of procurement entity: {2}".format(blacklisttype, reason, name_of_procurement_entity)

            yield Entity({
                    'inclusion_date': inclusiondate,
                    'exclusion_date': exclusiondate ,
                    'name': companyname,
                    'remarks': remarksfield,
                    'category':  "Group", 
                    'type': "SIP"
            })

    @staticmethod
    def extract_reason(response):
        fieldvalue = response.css("font span::text").extract_first()
        
        if not fieldvalue:
            fieldvalue = response.css("::text").extract_first()
        return fieldvalue
    
    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d-%b-%Y')
        except TypeError:
            return None

