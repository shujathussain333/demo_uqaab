# Author: Huzaifa Qamer
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import io
import pandas as pd
import dateutil.parser
import numpy as np


class CsaSpider(UqaabBaseSpider):
    name = 'canadian_securities_administrators'
    start_urls = [
        'https://cto-iov.csa-acvm.ca/SearchArticlesExcel.asp?Instance=101&Attr7=1&Attr7=2&Attr3=1&Attr2=1&Attr2=2&Attr2=3&Attr2=4&Attr2=13&Attr2=5&Attr2=6&Attr2=8&Attr2=10&Attr2=14&Attr2=15&Attr2=16&Attr2=22&Attr2=20&Attr2=18&Attr2=17&Attr2=19&Attr2=21&RegDateMin=1971%2f05%2f06&RegDateMax=2019%2f02%2f26']

    def structure_valid(self, response):
        format = ['Issued Date', 'Company Name', 'Jurisdiction', 'CTO Type', 'Status',
                   'Exception', 'Expiry Date', 'FFCTO', 'Persons']
        with io.BytesIO(response.body) as resp_file:
            df = pd.read_table(resp_file, encoding='cp1251', error_bad_lines=False)

        columns = df.columns
        return set(format) == set(columns)

    def extact_data(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.read_table(resp_file, encoding='cp1251', error_bad_lines=False)
        for index, row in df.iterrows():
            name = row['Company Name']
            if name is not np.nan:
                try:
                    inclusion_date = dateutil.parser.parse(row['Issued Date'])
                except TypeError:
                    inclusion_date = None
                try:
                    exclusion_date = dateutil.parser.parse(row['Expiry Date'])
                except TypeError:
                    exclusion_date = None
                yield Entity({
                    "category": "Group",
                    "name": name,
                    "type": "SIP",
                    "inclusion_date": inclusion_date,
                    "exclusion_date": exclusion_date
                })
