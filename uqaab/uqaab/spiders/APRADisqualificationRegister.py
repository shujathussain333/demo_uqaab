##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser



class APRADisqualificationRegister(UqaabBaseSpider):
    
    name = 'apra_disqualification_register'
    start_urls = ['https://www.apra.gov.au/disqualification-register']

    def structure_valid(self, response):
        data_rows = response.css('tbody tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('tbody tr')
        data_rows = data_rows[3:]
        for row in data_rows:
            columns = row.css('td')
            last_name = columns[0].css('*::text').extract_first()
            first_name = columns[1].css('*::text').extract_first()
            name = first_name + last_name

            if len(name) > 1:
                date_inclusion = columns[2].css('*::text').extract_first()
                remarks = "Section:{}".format(columns[3].css('*::text').extract_first())
                organization = columns[4].css('*::text').extract_first().strip()

                try:
                    inclusion_date = dateutil.parser.parse(date_inclusion)
                except:
                    inclusion_date = None

                yield Entity({
                    "category": "Individual",
                    "name": name,
                    "organization": organization if organization else None,
                    "remarks": remarks,
                    "inclusion_date": inclusion_date
                })





