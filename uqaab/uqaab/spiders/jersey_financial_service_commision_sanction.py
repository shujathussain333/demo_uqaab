#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class JerseyFSCSanctions(UqaabBaseSpider):

    name = 'jersey_fsc_sanctions'
    start_urls = ['https://www.jerseyfsc.org/the-commission/sanctions/terrorist-sanctions/',]

    def structure_valid(self, response):
        data_rows = response.xpath("//h2[text()='TERRORIST SANCTIONS']/following::ul[1]")
        return len(data_rows) > 0

    def extact_data(self,response):
        sanctioned_group = response.xpath("//h2[text()='TERRORIST SANCTIONS']/following::ul[1]/li[position()>1 and position()<last()]")
        for group in sanctioned_group:
            name = group.xpath(".//a/text()").extract_first()
            if "–" in name:
                name = name[name.find("–")+1:name.find(")")]
            else:
                name = name[0:name.find("(")]      
            yield Entity({
                "category": "unknown",
                "name":name,
        		"type": "SAN"
        	})