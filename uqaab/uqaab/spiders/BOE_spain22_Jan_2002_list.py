import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class BoeSpainListSpider(UqaabBaseSpider):
    name = "boe_spain22_jan_2002_list"
    allowed_domains = ["http://www.boe.es"]

    start_urls = [
    'http://www.boe.es/diario_boe/xml.php?id=BOE-A-2002-1217',
    ]

    content_path = '//documento/texto/p/text()'

    def structure_valid(self, response):
        return len(response.xpath(self.content_path).extract()) > 0

    def extact_data(self, response):
        print("                 *******Extracting MSB Registrant Search*******")

        content_list = response.xpath(self.content_path).extract()
        skip = True
        list_2_flag = flag = False

        count = row_to_be_skipped = 0
        preserve_row = ''

        for row in content_list[:-1]:
            
            count += 1
            if row == 'Personas físicas:':
                skip = False
                continue

            if skip:
                continue
            elif row == 'Personas jurídicas:':
                print("Start working for list 2")
                flag = True
                continue
            elif not list_2_flag:
                # Check if next line is part of current line
                # print(row)
                if content_list[count][:1].isdigit():
                    if 'Personas jurídicas:' not in row:
                        preserve_row = ' '.join([preserve_row, row]).strip()
                    yield self.parse_individual(preserve_row)
                    preserve_row = ''
                else:
                    preserve_row = preserve_row + row
                

                list_2_flag = flag
            else:
                if content_list[count][:1].isdigit():
                    preserve_row = ' '.join([preserve_row, row]).strip()
                    yield self.parse_group(preserve_row)
                    preserve_row = ''
                else:
                    preserve_row = preserve_row + row
                    continue

        # Handle last record separately

        print("                 *******Finished Extracting MSB Registrant Search*******")

    def parse_individual(self, row):
        row_temp = row

        name = address = designation = ''
        aliases = []
        designations = []

        aka = 'A.K.A.'
        fka = 'F.K.A.'
        
        if aka in row:
            row_temp = row.split(aka)
        elif fka in row:
            row_temp = row.split(fka)
        elif aka not in row and fka not in row:
            if ';' in row_temp:
                semicolon_separated = row_temp.split(';')
                # Case 2, separated by ';' => name + designation(multiple check) + Address
                if len(semicolon_separated) > 2:
                    name = semicolon_separated[0].strip()
                    name = self.remove_index(name)
                    address = semicolon_separated[-1].strip()
                    if address == '':
                        address = None
                    designation = semicolon_separated[1: -1]
                    for item in designation:
                        if item is not '':
                            designations.append(item)

                # Case 3, separated by ';' => name + Address
                if len(semicolon_separated) == 2:
                    name = semicolon_separated[0].strip()
                    name = self.remove_index(name)
                    address = semicolon_separated[-1].strip()
                    if address == '':
                        address = None

                # Case 4, separated by ';' => name
                if len(semicolon_separated) == 1:
                    name = semicolon_separated[0].strip()
                    name = self.remove_index(name)

                row_temp = '' # To stop next execution

            # Case 1, separated by ',' => name + designation(if contains ';', fetch address)
            if ',' in row_temp:
                comma_separated = row_temp.split(',')
                if len(comma_separated) > 1:
                    name = comma_separated[0].strip()
                    name = self.remove_index(name)
                    designation = ','.join(comma_separated[1:]).strip()
                    if ';' in designation:
                        semicolon_separated = designation.split(';')
                        if len(semicolon_separated) > 1:
                            designation = semicolon_separated[:-1]
                            address = semicolon_separated[-1].strip()
                            if address == '':
                                address = None
                            for item in designation:
                                if item is not '':
                                    designations.append(item.strip())
                    else:
                        comma_separated = designation.split(',')
                        for item in comma_separated:
                            if item is not '':
                                designations.append(item.strip())
            
            return Entity({
            
                'name': name,
                'aka': aliases,
                'designation': designations,
                'address': address,
                'category': 'Individual'
            })
            
        
        # Fetch Name
        if name is '':
            name = row_temp[:1][0]
            name = self.trim_last_char(name)
            name = self.remove_index(name)

        # Fetch Aliases
        aliases_temp = row_temp[1:]
        for alias in aliases_temp:
            if alias is not '':
                alias = self.trim_last_char(alias)
                aliases.append(alias)

        return Entity({
            'name': name,
            'aka': aliases,
            'designation': designations,
            'address': address,
            'category': 'Individual'
        })

    def parse_group(self, row):
        row_temp = row

        name = address = ''
        aliases = []
        addresses = []

        aka = 'A.K.A.'
        fka = 'F.K.A.'
        
        if aka in row:
            row_temp = row.split(aka)
        elif fka in row:
            row_temp = row.split(fka)
        elif aka not in row and fka not in row:
            if ',' in row_temp:
                comma_separated = row_temp.split(',')

                if len(comma_separated) > 1:
                    name = comma_separated[0].strip()
                    name = self.remove_index(name)
                    address = ','.join(comma_separated[1:]).strip()
                    if ';' in address:
                        addresses_temp = address.split(';')
                        for item in addresses_temp:
                            if item is not '':
                                addresses.append(item)
                    else:
                        addresses.append(address.strip())
                else:
                    name = comma_separated[0].strip()
            else:
                name = self.remove_index(row)
            
            return Entity({
                'name': name,
                'aka': aliases,
                'address': addresses,
                'category': 'Group'
            })
            
            return
        # Fetch Name
        if name is '':
            name = row_temp[:1][0]
            name = self.trim_last_char(name)
            name = self.remove_index(name)

        # Fetch Aliases and address
        aliases_temp = ''.join(row_temp[1:])
        if ')' in aliases_temp:
            alias_address = aliases_temp.split(')')
            address = alias_address[-1]
            aliases_temp = ''.join(alias_address[:-1]).strip()
            if ';' in aliases_temp:
                aliases_temp = aliases_temp.split(';')
                for item in aliases_temp:
                    if item is not '':
                        aliases.append(item)
            else:
                aliases.append(aliases_temp)

        return Entity({
            'name': name,
            'aka': aliases,
            'address': addresses,
            'category': 'Group'
        })
        
    def trim_last_char(self, data):
        if len(data) > 0:
            data = data.strip()
            temp = ''.join(data[-1:])
            if temp is ';' or temp is '(' or temp is '.' or temp is ')':
                data = data[:-1].strip()
            temp = ''.join(data[-1:])
            if temp is ';' or temp is '(' or temp is '.' or temp is ')':
                data = data[:-1].strip()
        return data
    
    def remove_index(self, data):
        if len(data) > 0:
            data = data.strip()
            data = ''.join(data.split('.')[1:]).strip()
        return data