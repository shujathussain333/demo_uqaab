# Author: Barkat Khan

from datetime import datetime
import calendar
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import re


class NationalSecurityGovAU(UqaabBaseSpider):

    name = 'nationalsecurity_gov_au'
    start_urls = [
        'https://www.nationalsecurity.gov.au/listedterroristorganisations/pages/default.aspx'
    ]

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='item link-item']")
        return len(data_rows) > 0

    def extact_data(self, response):
        itemlinks = response.xpath("//div[@class='item link-item']/a/@href").extract()
        itemnames = response.xpath("//div[@class='item link-item']/a/text()").extract()
        itemdescription = response.xpath("//div[@class='description']/text()").extract()
        
        for index in range(len(itemlinks)):
            E = Entity()
            E["name"] = itemnames[index]
            E['inclusion_date'] = self.string_to_date(itemdescription[index].split(",")[0])
            E['category'] = 'Group'
            E['type']="SAN"
            link = itemlinks[index]
            
            yield Request(url=link, meta={'entity':E}, callback=self.extract)
    
    def extract(self, response):
        E = response.request.meta['entity']
        othernames = response.xpath("//div[@class='ms-rtestate-field']/p/*/text()").extract()
        othernames = othernames[0].replace("Also known as: ", ""). replace("(", "" ). replace(")", "" )
        othernames = re.split('; |,',othernames)
        E["aka"] = [aka.strip() for aka in othernames]
        return E
    
    @staticmethod
    def string_to_date(date_string):
        try:
            pattern = "|".join(calendar.month_name[1:])
            monthname = re.search(pattern, date_string, re.IGNORECASE).group(0)
            day_year = re.findall(r'\d+',date_string)
            datestring_extracted = day_year[0] + " " +  monthname + " " +  day_year[1] 
            return datetime.strptime(datestring_extracted, '%d %B %Y')
        except TypeError:
            return None