##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser


class BelgiumFSMAUnlawfullCompanies(UqaabBaseSpider):
    name = 'belgium_fsma_unlawfull_companies'
    start_urls = ['https://www.fsma.be/en/warnings/companies-operating-unlawfully-in-belgium']
    rows = []

    def structure_valid(self, response):
        data_rows = response.css('tbody tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        url = 'https://www.fsma.be'
        links = response.xpath("//li[@class='pager-item']//a/@href").extract()
        data_rows = response.css('tbody tr')
        self.rows.extend(data_rows[1:])

        for link in links:
            new_url = url + link
            page = 1
            callback = lambda response: self.extract_link(response, page)
            yield scrapy.Request(url=new_url, callback=callback)

    def extract_link(self, response, page):
        data_rows = response.css('tbody tr')
        data_rows = data_rows[1:]
        data_rows.extend(self.rows)
        self.rows = []
        for row in data_rows:
            columns = row.css('td')
            name = columns[0].css('a::text').extract_first()
            website = columns[1].css('li::text').extract()
            fraud = columns[2].css('li::text').extract()
            date_inclusion = columns[3].css('span::text').extract_first()
            website = "Website: {}".format(' '.join(website))
            fraud = "Type of fraud: {}".format(' '.join(fraud))
            remarks = "{}; {}".format(website, fraud)
            try:
                inclusion_date = dateutil.parser.parse(date_inclusion)
            except:
                inclusion_date = None

            yield Entity({
                "category": "Group",
                "name": name,
                "remarks": remarks,
                "inclusion_date": inclusion_date
            })
