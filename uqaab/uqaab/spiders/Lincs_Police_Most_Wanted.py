#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Lincs_Police_Most_Wanted(UqaabBaseSpider):
    name = "lincs_police_most_wanted"
    start_urls = ['https://www.lincs.police.uk/news-campaigns/news/?filters=1177']
        
    def structure_valid(self, response):
        data_divs = response.xpath('//div[@class="listing quarters clearfix"]//div[@class="listing-item mobile-stack"]')
        return len(data_divs) > 0

    def extact_data(self, response):
        baseUrl = "https://www.lincs.police.uk"
        pages = response.xpath('//div[@class="listing quarters clearfix"]//div[@class="listing-item mobile-stack"]')
        for page in pages:
            next_page = page.xpath('.//a//@href').extract_first()
            next_page = baseUrl + next_page
            if next_page is not None:
                yield scrapy.Request(next_page, callback=self.extract_link)
            
    def extract_link(self, response):
        nameRawData = response.xpath('//div[@class="constraint clearfix mobile-padding"]/h1/text()').extract_first()
        if("Wanted" in nameRawData):
            nameRawData = nameRawData.split("Wanted")
        elif("wanted" in nameRawData):
            nameRawData = nameRawData.split("wanted")

        nameCleanData = ""
        for name in nameRawData:
            if(name != ""):
                nameCleanData = name
                break

        nameCleanData = nameCleanData.replace(":","")
        nameCleanData = nameCleanData.replace("-","")
        nameCleanData = nameCleanData.lstrip()
        
        remarks = response.xpath('//article[@class="content has-sidebar indent-text clearfix mobile-stack"]//p//text()').extract()
        remarks = ' '.join(remarks)

        baseUrl = "https://www.lincs.police.uk"
        imageSource = response.xpath('//aside[@class="sidebar mobile-stack"]/figure/a/@href').extract_first()
        imageSource = baseUrl + imageSource
        
        yield Entity({
            "name": nameCleanData,
            "category": "Individual",
            "type": "SAN",
            "image": imageSource,
            "remarks": remarks
        })