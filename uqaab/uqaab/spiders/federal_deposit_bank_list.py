
"""
Created by: Muhammad Ilyas <muhammad.ilyas@lovefordata.com>
"""

import pandas as pd
from uqaab.items import Entity
import io
from datetime import datetime
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class FederalDepositBankList(UqaabBaseSpider):
    name = "federal_deposit_bank_list"
    allowed_domains = ['www.fdic.gov']
    start_urls = [
        'https://www.fdic.gov/bank/individual/failed/banklist.csv'
    ]
    
    def structure_valid(self, response):
        df = pd.read_csv(io.BytesIO(response.body), encoding='ISO-8859–1')
        return len(df) > 0

    def extact_data(self, response):
        df = pd.read_csv(io.BytesIO(response.body), encoding='ISO-8859–1')
        category = "Group"
        for i in df.index:
            name = df['Bank Name'][i]
            city = '{0} {1}'.format(df['City'][i], df['ST'][i])
            remarks = "CERT :" + str(df['CERT'][i]) + ", Acquiring Institution: " + df['Acquiring Institution'][i]
            inclusion_date = self.string_to_date(df['Closing Date'][i])

            yield Entity({
                'category': category,
                'name': name,
                'remarks': remarks,
                'type' : "San",
                'city': city,
                'country': 'US',
                'inclusion_date': inclusion_date
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d-%b-%y')
        except TypeError:
            return None