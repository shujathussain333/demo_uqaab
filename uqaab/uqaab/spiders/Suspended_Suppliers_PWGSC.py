# Author = Barkat Khan

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
from scrapy import Request
from datetime import datetime

class Suspended_Suppliers_PWGSC(UqaabBaseSpider):
    name = 'suspended_suppliers_pwgsc'
    start_urls =  ['https://www.tpsgc-pwgsc.gc.ca/ci-if/four-inel-eng.html']

    def structure_valid(self, response):
        self.data_rows = response.css("body > main > table > tbody").css("tr")
        return len(self.data_rows) > 0
    
    def extact_data(self, response):
        data_rows = self.data_rows

        for i in range(len(data_rows)):
            columns = data_rows[i].css("td")
            name = "".join(columns[0].css("::text").extract()).replace(u'\xa0', u' ')
            address = "".join(columns[1].css("::text").extract()).replace(u'\xa0', u' ')
            status = columns[2].css("::text").extract_first()
            inclusiondate = self.string_to_date(columns[3].css("::text").extract_first())
            exclusiondate = self.string_to_date(columns[4].css("::text").extract_first())
            
            replacefrom = columns[1].css("abbr::text").extract()
            replacewith = columns[1].css("::attr(title)").extract()
            for from_, with_ in zip(replacefrom, replacewith):
                address = address.replace(from_, with_)
            
            yield Entity({
                "name": name, 
                "address": address, 
                "Status": status,
                "inclusion_date": inclusiondate, 
                "exclusion_date": exclusiondate,
                "category": "Group",
                "type": "SAN"
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%B %d, %Y')
        except:
            return None



    