# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class PhilpinesCeasedCompanies(UqaabBaseSpider):
    
    name = 'philpines_ceased_desisted_companies'
    start_urls = ['http://www.sec.gov.ph/public-information-2/sec-issuances/cease-and-desist-order/']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@id="accordion-2-c1"]/table//a')
        return len(data_rows) > 0

    def extact_data(self, response):
        details = response.xpath('//div[@id="accordion-2-c1"]/table//a')
        for detail in details:
            name = detail.xpath('.//text()').extract_first()
            remarks = detail.xpath('.//@href').extract_first()
            remarks = "further_details: {}".format(remarks)
            yield Entity({
                "category": "group",
                "name":name,
                "remarks":remarks,
                "type": "SAN"
            })

        
