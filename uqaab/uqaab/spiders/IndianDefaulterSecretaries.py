# Author = Barkat Khan
import os
from datetime import datetime
import scrapy
from uqaab.environment import config
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd
import tabula
import json
from PyPDF2 import PdfFileReader

class IndianDefaulterSecretaries(StaticDataSpider):

    name = 'indiandefaultersecretaries'
    start_urls = ['http://www.mca.gov.in/MinistryV2/defaultersecretarieslist.html']
    baseurl = "http://www.mca.gov.in"
    
    def structure_valid(self, response):
        pdffilelinks = response.css("div[dir='ltr']")[2].css("ul li a::attr(href)").extract()
        self.pdffilelinkslist = list(map(lambda href:self.baseurl+href, pdffilelinks))
        self.pdffilenames = list(map(lambda filenames:filenames.split("/")[-1], pdffilelinks))
        return len(self.pdffilelinkslist) > 0
    
    def extact_data(self, response):

        for files in self.pdffilenames:
            file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', files)
            df = tabula.read_pdf(file_path, encoding="utf-8", lattice=True, guess=False, pages='all')        
            
            filedata = PdfFileReader(file_path, "rb")
            filecontent = filedata.getPage(0).extractText()
            datestring = self.string_to_date(filecontent.split(" ")[2])
           
            for index in range(len(df)):
                document_info = []

                if str(df.loc[index,'CIN']):
                    document_info.append({'type': "CIN",'id': str(df.loc[index, 'CIN'])})
                if str(df.loc[index,'Signatory ID']):
                    document_info.append({'type': "Signatory ID",'id': str(df.loc[index, 'Signatory ID'])})
                
                if len(document_info) > 0:
                    document_info = json.dumps({'document_info': document_info})
                else:
                    document_info = json.dumps({'document_info': None})
                
                remarksfield =  "Defaulting Year: {0} ".format(df.loc[index, 'Defaulting Year'])

                yield Entity({
                    'category': 'Individual',
                    'type': 'SIP',
                    'name': df.loc[index,'Name'],
                    'organization': df.loc[index,'Company Name'],
                    'document': document_info,
                    'inclusion_date': datestring,
                    'remarks': remarksfield
                })





















 
   

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%m/%Y')
        except TypeError:
            return None
