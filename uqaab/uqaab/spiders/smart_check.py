from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from bs4 import BeautifulSoup
import requests

class SmartCheckScrapper(UqaabBaseSpider):
    name = 'smart_check'
    start_urls = ['https://www.smartcheck.gov/redlist']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        r = requests.get("https://www.smartcheck.gov/redlist")
        soup = BeautifulSoup(r.content, "lxml")
        table = soup.select_one("tbody")

        rows = table.find_all('tr')

        for tr in rows:
            td = tr.find_all('td')
            row = [i.text for i in td]

            yield Entity({
                'name': row[0],
                'inclusion_date': row[1],
                'category': 'Group',
                'type': "SAN"
            })
#Update
