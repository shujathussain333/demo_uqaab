#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
class IsleOfManSanctions(UqaabBaseSpider):

    name = 'isle_of_man_sanctions'
    start_urls = ['https://www.gov.im/categories/tax-vat-and-your-money/customs-and-excise/sanctions-and-export-control#accordion',]

    def structure_valid(self, response):
        data_rows = response.xpath("//h2[text()='Current Sanctions Regimes']/following::ul[1]")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.gov.im"
        sanc_list = response.xpath("//h2[text()='Current Sanctions Regimes']/following::ul[1]/li")
        for country in sanc_list:
            aliases = "None"
            name = country.xpath(".//a/text()").extract_first()
            if "/" in name:
                name_alias = name.split("/")
                name = name_alias[0]
                aliases = name_alias[1]
            alias = [{'aka': aliases}]
            alias_info = json.dumps({'info': alias})                 
            further_detail = country.xpath(".//a/@href").extract_first()
            if further_detail:
                further_detail = base_url + further_detail
            sanc_notice = country.xpath(".//text()").extract_first()
            remarks = "Sanc_notive: {} , further_details: {}".format(sanc_notice,further_detail)
            yield Entity({
                "category": "Individual",
                "aka":alias_info,
                "name":name,
        		"remarks":remarks,	
        		"type": "SIP"
        	})