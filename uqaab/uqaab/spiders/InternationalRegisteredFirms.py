# Author =  Barkat Khan

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class InternationalRegisteredFirms(UqaabBaseSpider):
    name = 'internationalregisteredfirms'
    start_urls = ['https://pcaobus.org/International/Registration/Pages/InternationalRegisteredFirms.aspx']
    max_records = 100

    def structure_valid(self, response):
        data_rows = response.css("table")[4].css("tr")[1:]
        return len(data_rows) > 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

    def extact_data(self, response):
        data_rows =response.css("table")[4].css("tr")[1:]

        for i in range(len(data_rows)):
            remarks= []
            columns = data_rows[i].css('tr')

            remarks.append('Link: {0}'.format(self.extract_href(columns[0])))
            remarks.append('Category: {0}'.format(self.extract_category(columns[0])))
            remarks.append('Category Description: {0}'.format(self.extract_categorydesc(columns[0])))

            city_state_country = self.extract_citystatecountry(columns[0])
            
            if len(city_state_country) == 2:
                city = city_state_country[0]
                country = city_state_country[1]
            elif len(city_state_country) == 3:
                city = city_state_country[0] +" "+ city_state_country[1]
                country = city_state_country[2]
            else:
                country = ""
                city = ""
            
            yield Entity({
                    'city': city,
                    'country' : self.get_country_code(country), 
                    'remarks' : ', '.join(remarks),
                    'name' : self.extract_name(columns[0]),
                    'category' :  "Group", 
                    'type' : "SAN"
            })
        
        if len(data_rows) >= self.max_records:
            view_state = response.xpath('//*[@id="__VIEWSTATE"]/@value').extract_first()

            pages= response.css("td[class = 'ms-paging']")[-1].css("*::text").extract()[-2]
            nextrecords =int(pages.split("-")[-1]) +1
            event_argument = "dvt_firstrow={"+str(nextrecords)+"};dvt_startposition={}"
            formdata = {
                    # change pages here
                    "__EVENTTARGET": "ctl00$SPWebPartManager1$g_b1a8baec_b652_4598_b67c_0c545c9ceffe",
                    "__EVENTARGUMENT":event_argument,
                    "__VIEWSTATE": view_state,
                }

            url  = self.start_urls[0]
            yield scrapy.FormRequest(url=url, formdata=formdata, callback=self.extact_data)


    @staticmethod
    def extract_name(col):
        try:
            name = col.css('a::text').extract_first()
            return name
        except Exception as err:
            return None
    
    @staticmethod
    def extract_href(col):
        try:
            href = col.css('a::attr(href)').extract_first()
            return href
        except Exception as err:
            return None    
    
    @staticmethod
    def extract_category(col):
        try:
            category = col.css('abbr::text').extract_first()
            return category
        except Exception as err:
            return None    
    
    @staticmethod
    def extract_citystatecountry(col):
        try:
            c_s_c = col.css('td::text').extract()
            return c_s_c
        except Exception as err:
            return None

    @staticmethod
    def extract_categorydesc(col):
        try:
            cdesc = col.css('abbr::attr(title)').extract_first()
            return cdesc
        except Exception as err:
            return None
    


