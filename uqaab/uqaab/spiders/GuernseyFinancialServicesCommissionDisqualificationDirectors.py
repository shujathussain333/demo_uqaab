##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import re


class GuernseyFinancialServicesCommissionDisqualifiedDirectors(UqaabBaseSpider):
    name = 'guernsey_fin_services_comm_disqualified_dir'
    start_urls = ['https://www.gfsc.gg/commission/enforcement/disqualified-directors']

    def structure_valid(self, response):
        items = response.css("div.accordion-item")
        return len(items) > 0

    def extact_data(self, response):
        items = response.css("div.accordion-item")
        for item in items:
            name = item.css("span.text::text").extract_first()
            date_reg_exp2 = re.compile(
                r'([0-3]?\d)\s*(Jan(?:uary)?(?:aury)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|June?|July?|Aug('
                '?:ust)?|Sept?(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?(?:emeber)?).?,?\s([1-2]\d{3})')
            dates = [x.group() for x in
                     date_reg_exp2.finditer(name)]
            address = [name.split('of')[-1].strip()]
            dob = [{'DOB': birthday,
                    'POB': None} for birthday in dates]
            dob_info = json.dumps({'info': dob})
            summary = item.css("p::text").extract()
            remarks = ' '.join(summary)
            name = name.split(' ')
            name = "{} {} {} {}".format(name[0],name[1],name[2],name[3])
            yield Entity({
                "category": "Individual",
                "name": name,
                'date_of_birth': dob_info,
                'address': address,
                "remarks": remarks,
                "type": "SIP",
            })
