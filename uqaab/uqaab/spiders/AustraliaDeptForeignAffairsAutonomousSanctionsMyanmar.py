# Author: Sharjeel Ali
# Warning: List has 5 records but scrapping 4 due to problem in the html
import json
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class DeptForeignAffairsAutonomousSanctionsMyanmar(UqaabBaseSpider):
    name = 'dept_foreign_affairs_autonomous_sanctions_myanmar'
    start_urls = ['https://www.legislation.gov.au/Details/F2018L01409/Html/Text#_Toc523482436']

    def structure_valid(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        data_rows = data_rows[1:]
        datas = []
        for row in data_rows:
            try:
                columns = row.css('td')
                column_name = columns[1].css('p').css('*::text').extract_first()
                if "Name" in column_name:
                    dictionary = {}
                    name = columns[2].css('p').css('*::text').extract_first()
                    # few columns not available for some records
                    dictionary.update({"name": name, "dob": None, "aka":None, "designation": None, "document": None})
                    #This is done due to problem in html on Additional Info
                    datas.append(dictionary)

                elif "aka" in column_name:
                    aka = columns[2].css('p').css('*::text').extract_first()
                    dictionary["aka"] = aka


                elif "Birth" in column_name:
                    dob = columns[2].css('p').css('*::text').extract_first()
                    dobs = {'DOB': dob, 'POB': None}
                    dob_info = json.dumps({'info': [dobs]})
                    dictionary.update({"dob": dob_info})


                elif "Additional" in column_name:
                    del datas[-1]
                    additional_info = columns[2].css('p').css('*::text').extract_first()
                    passport_index, designation_index = map(
                        additional_info.lower().find,
                        ['passport', 'military'])
                    extra_values = [("passport", passport_index), ("designation", designation_index)]
                    designation = self.extract_designation(additional_info, extra_values)
                    document = self.extract_passport(additional_info, extra_values)
                    dictionary.update({"designation": designation, "document": document})

                    if dictionary["aka"]:
                        alias_info = json.dumps({'info': [{'aka': aka}]})
                    else:
                        alias_info = json.dumps({'info': None})

                    dictionary.update({"aka": alias_info})

                    datas.append(dictionary)

            except (TypeError, AttributeError):
                continue

        for data in datas:
            yield Entity({
                "name": data['name'],
                "aka": data["aka"],
                "category": "individual",
                "type": "SAN",
                "date_of_birth": data["dob"],
                "designation": data["designation"],
                "country": "Myanmar",
                "document": data["document"],

            })

    def extract_designation(self, information, extra_values):
        designation_info = json.dumps({'info': None})
        for index, item in enumerate(extra_values):
            if "designation" in item[0] and item[1] > -1:
                try:
                    designations = information[item[1]:extra_values[index + 1][1]]
                except:
                    designations = information[item[1]:]
                designation = {"designation": designations.strip()}
        if designation:
            designation_info = json.dumps({'info': [designation]})
        return designation_info

    def extract_passport(self, information, extra_values):
        document = []
        for index, item in enumerate(extra_values):
            if "passport" in item[0] and item[1] > -1:
                try:
                    passport = information[item[1]:extra_values[index + 1][1]]
                except:
                    passport = information[item[1]:]

                document.append({'type': "passport",
                                 'id': passport})

        return document
