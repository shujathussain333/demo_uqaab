from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests

class Department_of_State_Pennsylvania(UqaabBaseSpider):
    name = 'dos'
    start_urls = ['https://www.dos.pa.gov/BusinessCharities/Charities/Resources/Pages/Enforcement-and-Disciplinary-Actions.aspx']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        dfs = pd.read_html(requests.get('https://www.dos.pa.gov/BusinessCharities/Charities/Resources/Pages/Enforcement-and-Disciplinary-Actions.aspx').text)
        dfs[0].drop(0, axis=0, inplace=True)
        dfs[0].columns = ['Name', 'DOInclusion', 'Remarks']
        df = dfs[0]

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'inclusion_date':row.DOInclusion,
                'category': 'Group',
                'type': "SIP"
            })