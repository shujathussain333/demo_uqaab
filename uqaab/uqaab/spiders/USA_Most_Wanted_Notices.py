#Author: Daniyal Faquih

import scrapy
import json
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class USA_Most_Wanted_Notices(UqaabBaseSpider):
    name = 'usa_most_wanted_notices'
    start_urls = ['https://www.state.gov/dss-most-wanted/']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="entry-content"]/p/a')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="entry-content"]/p/a')
        for data in data_rows[1:]:
            nextPageUrl = data.xpath('./@href').extract_first()
            if nextPageUrl is not None:
                yield scrapy.Request(nextPageUrl, callback=self.extract_link)

    def extract_link(self, response):
	    data_rows = response.xpath('//div[@class="entry-content"]//p/text()').extract()
	    name = ""
	    alias = ""
	    sex = ""
	    dob = ""
	    pob = ""
	    height = ""
	    weight = ""
	    eyes = ""
	    hair = ""
	    occupation = ""
	    nationality = ""
	    summary = "" 

	    imageSource = response.xpath('//div[@class="entry-content"]//figure[@class="inline-image inline-image--pull pull-left"]/img/@src').extract_first()
	    if(len(data_rows) <= 12):   	
	        name = data_rows[0].strip()
	        sex = data_rows[1].strip()
	        dob = data_rows[2].strip()
	        pob = data_rows[3].strip()
	        height = data_rows[4].strip()
	        weight = data_rows[5].strip()
	        eyes = data_rows[6].strip()
	        hair = data_rows[7].strip()
	        occupation = data_rows[8].strip()
	        nationality = data_rows[9].strip()
	        summary = data_rows[10].strip()
	       	
	    elif(len(data_rows) == 13):
	        name = data_rows[0].strip()
	        alias = data_rows[1].strip()
	        check = False
	        if(";" in alias):
	        	alias = alias.split(";")
	        elif("," in alias):
	        	alias = alias.split(",")
	        else:
	        	alias = [alias]
	        sex = data_rows[2].strip()
	        dob = data_rows[3].strip()
	        pob = data_rows[4].strip()
	        height = data_rows[5].strip()
	        weight = data_rows[6].strip()
	        eyes = data_rows[7].strip()
	        hair = data_rows[8].strip()
	        occupation = data_rows[9].strip()
	        nationality = data_rows[10].strip()
	        summary = data_rows[11].strip()
	    alias_info = json.dumps({'info': None})
	    alias = [{'aka': al} for al in alias]
	    if alias:
	        alias_info = json.dumps({'info': alias})

	    desig_info = json.dumps({'info': None})
	    if occupation:
	        designation = [{'designation': occupation}]
	        desig_info = json.dumps({'info': designation})

	    dobs = {'DOB': dob, 'POB': pob}
	    if dobs:
	        dob_info = json.dumps({'info': [dobs]})
	    else:
	        dob_info = json.dumps({'info': None})

	    remarks = "Height: {} ,Weight: {} ,Eyes: {},Hair: {},Summary: {}".format(height, weight, eyes, hair, summary)
	    yield Entity({
	        "name": name,
	        "aka": alias_info,
	        "category": "Individual",
	        "type": "SIP",
	        "date_of_birth": dob_info,
	        "gender": sex,
	        "remarks": remarks,
	        "nationality": nationality,
	        "designation": desig_info,
	        "image": imageSource
	    })