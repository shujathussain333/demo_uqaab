#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class CaliforniaFTBTaxDelequenciesPersonal(UqaabBaseSpider):

    name = 'california_ftb_tax_delequencies_personal'
    start_urls = ['https://www.ftb.ca.gov/about-ftb/newsroom/top-500-past-due-balances/delinquents-PIT.txt',]

    def structure_valid(self, response):
        data = str(response.body).split('\\t-\\t-\\t-\\t-\\t-\\r\\n2\\t"4,8"\\t')
        return len(data) > 0

    def extact_data(self,response):
        data = str(response.body).split('\\t-\\t-\\t-\\t-\\t-\\r\\n2\\t"4,8"\\t')
        data_scrap = data[1].split('\\r\\n\\t\\t')
        for person in data_scrap:
            person_detail = person.split('\\t')
            name = person_detail[0].replace('"',"")
            address = person_detail[1].replace('"',"")
            sub_total = person_detail[2].replace('"',"")
            license = person_detail[-3].replace('"',"")
            status = person_detail[-2].replace('"',"")
            number = person_detail[-1].replace('"',"")
            document_info = json.dumps({'document_info': None})
            if number:
                document = []  
                document.append({'type': "Number", 'id': number})   
                document_info = json.dumps({'document_info': document})                         
            remarks = "Total: {} , License: {}".format(sub_total,license)
            yield Entity({
                "category": "Individual",
                "name":name,
                "address":address,
                "document":document_info,
                "remarks":remarks,  
                "type": "SIP"
            })             