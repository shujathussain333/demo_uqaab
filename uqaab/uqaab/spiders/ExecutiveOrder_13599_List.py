import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class ExecutiveOrder13599ListSpider(UqaabBaseSpider):
    name = "executive_order_13599_list"
    allowed_domains = ["https://www.treasury.gov"]

    start_urls = [
    'https://www.treasury.gov/ofac/downloads/13599/13599list.txt',
    ]

    def structure_valid(self, response):
        data = str(response.body).split('_________________________________')
        data_list = data[1].split('\\n\\n')
        data_list = data_list[1:]
        return len(data_list) > 0

    def extact_data(self, response):
        print("                 *******Extracting Blocked Solely Pursuant to Executive Order 13599*******")
        data = str(response.body).split('_________________________________')
        data_list = data[1].split('\\n\\n')
        data_list = data_list[1:]
        info_separator = 'For more information, please see: '
        remarks_separator = ' Additional Sanctions Information - '
        for item in data_list:
            name = city = country = address = remarks = extra_info = additional_info = ''
            temp = name_alias = ''
            aliases = []
            aliases_temp = []

            temp_list = ''
            temp = item.replace('\\n',' ')

            if info_separator in temp:
                temp_list = temp.split(info_separator)
                
                extra_info = temp_list[1].strip()
                temp_list = temp_list[0].strip()
                
                temp = temp_list

            if remarks_separator in temp:
                temp_list = temp.split(remarks_separator)

                info = temp_list[0].strip()
                remarks = temp_list[1].strip()

                temp = temp_list
            else:
                info = temp

            if ';' in info[:-1]:
                info_arr = info.split(';')[:-1]
            else:
                name_address = info.split(',')
                info_arr = name_address[:1]
                address = ', '.join(name_address[1:])
            
            aka = '(a.k.a.'
            fka = '(f.k.a.'
            name_temp = ''.join(info_arr[:1])
            
            alias = ''

            if aka in name_temp:
                name_alias = name_temp.split(aka)
            elif fka in name_temp:
                name_alias = name_temp.split(fka)
            elif ', ' in name_temp:
                name_alias = name_temp.split(', ')
                address = name_alias[-1]
            
            if name_alias is not '':
                name = ''.join(name_alias[:1]).strip()
            else:
                name = name_temp

            if address is '':
                alias = ''.join(name_alias[1:]).strip()
                if alias is not '':
                    aliases_temp.append(alias)
            
            aka = ' a.k.a. '
            fka = ' f.k.a. '

            if len(info_arr) > 2:
                alias = ''.join(info_arr[1: -1])

                if aka in alias:
                    alias_temp = alias.split(aka)
                elif fka in alias:
                    alias_temp = alias.split(fka)
                
                for item in alias_temp:
                    if (item is not '') and (')' not in item):
                        aliases_temp.append(item)
                    elif item is not '':
                        item = item.split(')')
                        aliases_temp.append(''.join(item[:1]))
                        additional_info = ')'.join(item[1:])
            
                
            if address is '':
                address = ''.join(info_arr[-1:])

            if '), ' in address:
                splited_line = address.split('), ')
                address = splited_line[-1]

                if aka in splited_line[0]:
                    alias_temp = splited_line[0].split(aka)
                elif fka in splited_line[0]:
                    alias_temp = splited_line[0].split(fka)
                
                for item in alias_temp:
                    if item is not '':
                        aliases_temp.append(item)


            for a in aliases_temp:
                aliases.append(a)
            
            if ',' in address:
                address = address.split(',')
                if len(address) == 1:
                    country = address
                    city = None
                    address = None
                elif len(address) == 2:
                    country = address[-1].strip()
                    city = ','.join(address[:-2])
                    address = None
                else:
                    country = address[-1].strip()
                    city = address[-2].strip()
                    address = ','.join(address[:-2])
            
            yield Entity({
                'category': 'Group',
                'name': name,
                'aka': aliases,
                'address': address,
                'city': city if city else None,
                'country': self.get_country_code(country),
                'remarks': remarks,
            })

        print("                 *******Finished Extracting Blocked Solely Pursuant to Executive Order 13599*******")