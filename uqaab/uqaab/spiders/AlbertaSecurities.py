# Author: Barkat Khan

from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
from tabula import read_pdf
import numpy as np

class AlbertaSecurities(UqaabBaseSpider):
    
    name = 'albertasecurities'
    start_urls = ['https://www.albertasecurities.com/issuer-regulation/reporting-issuer-list#sort=%40z95xcreateddate%20descending']
    url = "https://www.albertasecurities.com/-/media/ASC-Documents-part-1/Reporting-Issuers/2019/07/Reporting-issuers-list-as-of-July-4-2019.ashx"
    
    def structure_valid(self, response):
        df = read_pdf(self.url, encoding="utf-8",  pages= "all")
        return len(df) > 0
   
    def extact_data(self, response):
        df = read_pdf(self.url, encoding="utf-8",  pages= "all")
        df['Name'] = np.where(df['Name'].isnull(), None, df['Name'])
        df['Cease Traded'] = np.where(df['Cease Traded'].isnull(), None, df['Cease Traded'])
        df['Nature of Default'] = np.where(df['Nature of Default'].isnull(), None, df['Nature of Default'])
        for i in df.index:
            name = df['Name'][i]
            cease_traded = df['Cease Traded'][i]
            nature_of_default = df['Nature of Default'][i]
            remarks = "Nature_of_Default: {}".format(nature_of_default)
            yield Entity({
                "category": "group",
                "name": name,
                "remarks": remarks,
                "type": "SAN"
            })        


        
        