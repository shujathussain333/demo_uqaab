#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
from googletrans import Translator
import json
import dateutil.parser

class HungarianDecisions(UqaabBaseSpider):


    name = 'hungarian_decisions_2019'
    start_urls = ['http://www.gvh.hu/dontesek/versenyhivatali_dontesek/dontesek_2019?pagenum=1',]

    def structure_valid(self, response):
        data_rows = response.xpath("//table[@class='gvhlist']//tbody//tr")
        return len(data_rows) > 0

    def extact_data(self,response):
        translator = Translator()
        decisions = response.xpath("//table[@class='gvhlist']//tbody//tr")
        for decision in decisions:
            disclosure_date = decision.xpath("td[1]/text()").extract_first().strip() 
            if disclosure_date:
                disclosure_date = translator.translate(disclosure_date).text
                disclosure_date = dateutil.parser.parse(disclosure_date)                       
            decision_number = decision.xpath("td[a]/a/text()").extract_first()            
            name_and_aliases = decision.xpath("td[3]/text()").extract()
            if len(name_and_aliases)>1:
                aliases = [translator.translate(x.strip()).text for x in name_and_aliases[1:]]
            name = name_and_aliases[0].strip()  
            name = name.replace(",","") 
            if name:
                name = translator.translate(name).text                      
            document_info = json.dumps({'document_info': None})
            alias_info = json.dumps({'info': None})
            if decision_number:
                decision_number = translator.translate(decision_number).text
                document = []  
                document.append({'type': "Decision Number", 'id': decision_number})
            if len(aliases)>0:
                alias = [{'aka': alias} for alias in aliases ]
                alias_info = json.dumps({'info': alias})
            type_of_case = decision.xpath("td[4]/text()").extract_first()
            if type_of_case:
                type_of_case = translator.translate(type_of_case.strip()).text 
            remarks = "type_of_case:{}".format(type_of_case)     
            yield Entity({
                "category": "Group",
                "name":name,
                "inclusion_date":disclosure_date,
                "aka":alias_info,
                "document":document_info,
                "remarks":remarks,
                "type": "SAN"
            })                                                                     
        relative_next_url = response.xpath("//a[text()='Következő oldal >>']/@href").extract_first()
        if relative_next_url:
            absolute_next_url = relative_next_url
            yield Request(absolute_next_url, callback=self.extact_data)