#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json

class NewJerseyWantedSpider(UqaabBaseSpider):

    name = 'new_jersey_wanted'
    start_urls = ['https://www.njsp.org/wanted/index.shtml#top']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class= "wantedRowContent"]//a')
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.njsp.org/wanted/"
        page_urls = response.xpath('//div[@class= "wantedRowContent"]//a/@href').extract()
        for each_link in page_urls:
            absolute_url = base_url + each_link
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        base_url = "https://www.njsp.org/wanted/"
        name = response.xpath('//div[@class= "col-sm-9 col-sm-push-3 col-md-9 col-md-push-3"]//h1[@class="wantedH1"]/text()').extract_first()
        date_of_birth = response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="DOB "]/td[2]/text()').extract_first()
        place_of_birth = response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="POB"]/td[2]/text()').extract_first()
        dobs = {'DOB': date_of_birth, 'POB': place_of_birth}
        dob_info = json.dumps({'info': [dobs]}) 
        nic_no = response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="NIC No"]/td[2]/text()').extract_first()
        sbi_no = response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="SBI No"]/td[2]/text()').extract_first()
        fbi_no = response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="FBI No"]/td[2]/text()').extract_first()
        fpc =  response.xpath('//div[@class= "col-xs-12 col-sm-5 col-md-5"]//table//tr[td[1]/b="FPC"]/td[2]/text()').extract_first() 
        fujitive_unit_file_number = response.xpath('//div[@class= "col-xs-12 col-sm-7 col-md-7"]//p[./b="Fugitive Unit File Number:"]/text()').extract_first()         
        document_info = json.dumps({'document_info': None})
        document = []  
        document.append({'type': "Nic Number", 'id': nic_no})
        document.append({'type': "SBI Number", 'id': sbi_no})
        document.append({'type': "FBI Number", 'id': fbi_no})
        document.append({'type': "FPC Number", 'id': fpc})
        document.append({'type': "fugitive unit file number", 'id': fujitive_unit_file_number})
        document_info = json.dumps({'document_info': document})        
        aka = response.xpath('//div[@class= "col-xs-12 col-sm-7 col-md-7"]//p[./b="AKA:"]/text()').extract()
        alias = [{'aka': alias} for alias in aka ]
        alias_info = json.dumps({'info': alias})        
        remarks = response.xpath('//div[@class= "col-xs-12 col-sm-7 col-md-7"]//p[./b="Remarks: "]/text()').extract_first()
        image_url = response.xpath('//div[@class= "col-xs-12 col-sm-7 col-md-7"]/img/@src').extract_first()
        image = base_url+ image_url
        yield Entity({
            "category": "Individual",
            "name":name,
            "aka":alias_info,
            "date_of_birth":dob_info,
            "document":document_info,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })    