
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request

class FinancialMarketAuthority(UqaabBaseSpider):

    name = 'fma_warnings_alerts'
    start_urls = ['https://www.fma.govt.nz/news-and-resources/warnings-and-alerts/',]

    def structure_valid(self, response):
        data_rows = response.xpath("//ol[@id='results']")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.fma.govt.nz/"
        results = response.xpath("//ol[@id='results']/li")
        for result in results:
            scam_date = result.xpath(".//p[@class='result_date']/text()").extract_first()
            company_name = result.xpath(".//h4/a/text()").extract_first()
            company_website_details = result.xpath(".//p[@class='result_path']/a/@href").extract_first()
            remarks = result.xpath(".//div[@class='result-description']/text()").extract_first()
            remarks = "Company_further_details: {} , remarks: {}".format(company_website_details,remarks)
            yield Entity({
                "category": "Group",
                "inclusion_date":scam_date,
                "name":company_name,
        		"remarks":remarks,	
        		"type": "SIP"
        	})    
        next_page_link = response.xpath("//li[@class='next']/a/@href").extract_first()
        if next_page_link:
            absolute_next_page_url = base_url + next_page_link
            yield Request(absolute_next_page_url, callback=self.extact_data)            




