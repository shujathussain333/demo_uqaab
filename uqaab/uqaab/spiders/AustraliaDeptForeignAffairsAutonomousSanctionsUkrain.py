# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class DeptForeignAffairsAutonomousSanctionsUkrain(UqaabBaseSpider):
    name = 'dept_foreign_affairs_autonomous_sanctions_ukrain'
    start_urls = ['https://www.legislation.gov.au/Details/F2017C00919']

    def structure_valid(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        data_rows = data_rows[1:]
        datas = []
        for row in data_rows:
            columns = row.css('td')

            if len(columns) >= 5:
                column_name = columns[2].css('p span').css('*::text').extract_first()
                value = columns[3].css('p span').css('*::text').extract_first()
            elif len(columns) >= 3:
                column_name = columns[1].css('p span').css('*::text').extract_first()
                value = columns[2].css('p span').css('*::text').extract_first()
            try:
                if "Individual" in column_name:
                    dictionary = {}
                    name = value
                    category = "Individual"
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name, "category": category, "address": address, 'dob': None,'aka':None})

                elif "Entity" in column_name:
                    dictionary = {}
                    name = value
                    category = "Group"
                    address = []
                    # Added address, designation and dob default as report contains both company and individual data
                    dictionary.update(
                        {"name": name, "category": category, "address": address, 'dob': None,'aka':None})

                elif 'aka' in column_name:
                    alias_info = json.dumps({'info': [{'aka': value}]})
                    dictionary.update({"aka": alias_info})

                elif "Address" in column_name:
                    address = [value]
                    dictionary.update({"address": address})

                elif "Birth" in column_name:
                    dob = value
                    dobs = {'DOB': dob, 'POB': None}
                    dob_info = json.dumps({'info': [dobs]})
                    dictionary.update({"dob": dob_info})

                elif "Information" in column_name:
                    remarks = value
                    dictionary.update({"remarks": remarks})
                    #This is done because of duplicates records in html
                    if dictionary not in datas:
                        datas.append(dictionary)

            except (TypeError, AttributeError):
                continue


        for data in datas:
            yield Entity({
                "name": data['name'],
                "address": data["address"],
                "category": data["category"],
                "type": "SAN",
                "date_of_birth": data["dob"],
                "aka":data['aka']
            })
