# -*- coding: utf-8 -*-
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
from datetime import datetime
import re
import requests
from scrapy import Request
from scrapy.http import HtmlResponse 
from scrapy.http import TextResponse


class AfghanNPASpider(UqaabBaseSpider):

    name = 'afghan_npa_spider'
    total_pages = 8

    def structure_valid(self, response):
        return True
        # data_rows = response.xpath('//div')
        # return len(data_rows) > 0

    def start_requests(self):

        for page in range(1, self.total_pages+1):

            url = "http://www.npa.gov.af/Operation.aspx/Txt"

            payload = "{PassPanel:'CMSsDebarredCompanies_Search',Pid:'',Pid2:'0-" + str(page) + "-10-undefined-undefined-%',Lan:'E'}"

            headers = {'Content-Type': "application/json"}
            
            yield scrapy.Request(url, callback=self.parse, method="POST", body=payload, headers=headers, errback=self.error_handler)
        
    def extact_data(self, response):
        ## Data is extracted in the parse method above
        date_format = '%m/%d/%Y'

        response = TextResponse(response.url, body=response.text, encoding="utf-8")

        cresponse = response.text.encode().decode('unicode-escape').replace('\r','').replace('\n','').replace("\xa0Â", '')

        response = HtmlResponse(url="afgh_npa", body=cresponse, encoding="utf-8")

        records = len(response.xpath("//div/text()").extract())

        index = 0

        while index < records:
            name = response.xpath('//div/text()').extract()[index]
            remarks =  response.xpath('//div/text()').extract()[index+1]
            inclusion_date =  datetime.strptime(response.xpath('//div/text()').extract()[index+2], date_format)
            exclusion_date = datetime.strptime(response.xpath('//div/text()').extract()[index+3], date_format)
            index = index+4

            yield Entity({
                "name" : name,
                "inclusion_date": inclusion_date,
                "exclusion_date": exclusion_date,
                "remarks": remarks,
                "category": CATEGORY_OPTIONS[1]
            })
            