#Author: Daniyal Faquih
import scrapy
from scrapy.selector import Selector
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import re

class INTDL_India_Tax_Defaulters_List(UqaabBaseSpider):
    name = "intdl_india_tax_defaulters_list"
    start_urls = ['https://incometaxindia.gov.in/Pages/tax-defaulters.aspx']
            
    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="faq-sub-content exempted-result "]//h1[@class="faqsno-heading taxdefaulter"]//ul[@class="headerMneu"]')
        return len(data_rows) > 0

    def extact_data(self, response):
        for entity in self.extract_items(response):
            yield entity

    def extract_form_data(self, response, page, last_page):
        for entity in self.extract_items(response, page, last_page):
            yield entity

    def extract_items(self, response, page=None, last_page=None):
        products = response.xpath('//div[@class="faq-sub-content exempted-result "]//h1[@class="faqsno-heading taxdefaulter"]//ul[@class="headerMneu"]')
        view_state = response.xpath('//*[@id="__VIEWSTATE"]/@value').extract_first()
        event_validation = response.xpath('//*[@id="__EVENTVALIDATION"]/@value').extract_first()
        try:
            page = page + 1
        except TypeError:
            page = 2
            last_page = response.xpath('//div[@id="ctl00_SPWebPartManager1_g_6ca195fe_86cc_48f0_992b_768127f01eb2_ctl00_totalRecordsDiv"]//span//text()').extract()
            last_page = last_page[-1]
            last_page = re.findall(r'\d+', last_page)
            last_page = int(last_page[0])
        for product in products:
            name = product.xpath('./li//span[@class="Nm_field"][1]//text()').extract_first()
            pan = product.xpath('./li//span[@class="Nm_field"][2]//text()').extract_first()
            panPattern = re.findall("N\. A\.|NO PAN|-|NOT AVAILABLE|NA|N\.A\.",pan)
            if(len(panPattern) > 0):
                pan = None
            partnersList = product.xpath('./li//span[@class="Nm_field"][3]//text()').extract()
            partners = ''.join(partnersList)
            partners = partners.replace("\n","").strip()
            partnersPattern = re.findall("N\. A\.|NOT AVAILABLE|N\.A\.",partners)
            if(len(partnersPattern) > 0):
                partners = None
            
            arrear = product.xpath('./li//span[@class="Nm_field arrearamount fquph"]//text()').extract_first()
            document = []
            document.append({'type': "PAN",'id': pan})
            remarks = "Partners: {} ,Arrear: {}".format(partners,arrear)   
            yield Entity({
                "name" : name,
                "category": "Group",
                "type": "SAN",
                "remarks" : remarks,
                "document"  : document
            })
        if page <= last_page:
            formdata = {"ctl00$SPWebPartManager1$g_6ca195fe_86cc_48f0_992b_768127f01eb2$ctl00$txtPageNumber":'{}'.format(page),
                "__EVENTTARGET": "ctl00$SPWebPartManager1$g_6ca195fe_86cc_48f0_992b_768127f01eb2$ctl00$txtPageNumber",
                "__EVENTARGUMENT": "",
                "__VIEWSTATE": view_state,
                "__EVENTVALIDATION": event_validation,
            }
            callback = lambda response: self.extract_form_data(response, page, last_page)
            yield scrapy.FormRequest(url=self.start_urls[0], formdata=formdata, callback=callback)