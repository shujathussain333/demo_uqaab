from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class EUDevBankIDBSpider(UqaabBaseSpider):
    name = "eu_dev_bank_idb"
    
    start_urls = [
        'https://idblegacy.iadb.org/en/topics/transparency/integrity-at-the-idb-group/sanctioned-firms-and-individuals,1293.html',
        ]

    def structure_valid(self, response):
        data_rows = response.css('tbody')[0].css('tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('tbody')[0].css('tr')
        for row in data_rows:
            columns = row.css('span')

            category = 'Individual' if columns[1].css('::text').extract_first().split('/')[0] == 'Individual' else 'Group'
            yield Entity({
                'name': columns[0].css('::text').extract_first(),
                'nationality': columns[1].css('::text').extract_first().split('/')[1],
                'category': category,
                'title': [columns[2].css('::text').extract_first()],
                'country': [self.get_country_code(columns[3].css('::text').extract_first())],
                'inclusion_date': self.string_to_date(columns[4].css('::text').extract_first()),
                'exclusion_date': self.string_to_date(columns[5].css('::text').extract_first()),
                'remarks': columns[6].css('::text').extract_first()
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%b/%d/%Y')
        except ValueError:
            return None