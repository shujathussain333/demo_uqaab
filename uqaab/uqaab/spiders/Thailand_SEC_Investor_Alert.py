#Author: Daniyal Faquih
import scrapy
import dateutil.parser
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Thailand_SEC_Investor_Alert(UqaabBaseSpider):
    name = "thailand_sec_investor_alert"
    start_urls = ['https://market.sec.or.th/public/idisc/en/Viewmore/invalert-head?PublicFlag=Y']
    
    def structure_valid(self, response):
        data_rows = response.xpath('//table[@class="table table-striped table-hover"]//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//table[@class="table table-striped table-hover"]//tr')
        for data in data_rows[1:]:
            rawData = data.xpath('.//td[@class="RgCol_Left"]//text()').extract()
            name = rawData[0]
            remarks = rawData[1]
            remarks = "Type of action: {}".format(remarks)
            date = dateutil.parser.parse(data.xpath('.//td[@class="RgCol_Center"]//text()').extract_first())
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "inclusion_date": date,
                "remarks": remarks
            })