# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
from scrapy import Request
import json
from googletrans import Translator

class HungarianCourtdecions(UqaabBaseSpider):
    
    name = 'hungarian_court_decisions'
    start_urls = ['http://www.gvh.hu/dontesek/birosagi_dontesek_vj_szam_alapjan']

    def structure_valid(self, response):
        data_rows = response.xpath("//table[@class='gvhlist']//tbody//tr")
        return len(data_rows) > 0

    def extact_data(self, response):
        base_url = "http://www.njdge.org/exclusion"
        cases_detail = response.xpath("//table[@class='gvhlist']//tbody//tr")
        for case in cases_detail:
            aliases = []
            disclosure_date = case.xpath("td[1]/text()").extract_first().strip()
            name_and_aliases = case.xpath("td[3]/text()").extract()
            if len(name_and_aliases)>1:
                aliases = [x.strip() for x in name_and_aliases[1:]]
            name = name_and_aliases[0].strip()
            court = case.xpath("td[4]/text()").extract_first().strip()
            vj_detail = case.xpath("td[a]/a/@href").extract_first()
            yield Request(vj_detail, callback=self.parse_page, meta={'Name': name, 'disclosure_date':disclosure_date , 'court':court,'aliases':aliases})                             
    

    def parse_page(self,response):
        translator = Translator()
        name = response.meta.get('Name')
        name = translator.translate(name).text
        disclosure_date = response.meta.get('disclosure_date')
        if disclosure_date:
            disclosure_date = translator.translate(disclosure_date).text
            disclosure_date = dateutil.parser.parse(disclosure_date)
        court = response.meta.get('court')
        if court:
            court = translator.translate(court).text
        aliases = response.meta.get('aliases')
        case_number = response.xpath("//div[@class='Text']/p[3]/b[text()='Az ügy száma:']/following::text()[1]").extract_first()
        document_info = json.dumps({'document_info': None})
        alias_info = json.dumps({'info': None})
        if case_number:
            case_number = translator.translate(case_number).text
            document = []  
            document.append({'type': "Case_Number", 'id': case_number}) 
        if len(aliases)>0:
            aliases = [translator.translate(aka).text for aka in aliases]
            alias = [{'aka': alias} for alias in aliases ]
            alias_info = json.dumps({'info': alias})
        remarks = "Court:{}".format(court)                 
        yield Entity({
            "category": "Group",
            "name":name,
            "inclusion_date":disclosure_date,
            "aka":alias_info,
            "document":document_info,
            "remarks":remarks,
            "type": "SAN"
        })          