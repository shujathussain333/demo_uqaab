# Author =  Barkat Khan

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import re
from datetime import datetime


class CabinetAndMinistryOfJapan(UqaabBaseSpider):

    name = 'Japan_Ministry'
    url = 'http://japan.kantei.go.jp/96_abe/meibo/daijin/aso_e.html'
    baseurl = "http://japan.kantei.go.jp" 


    def start_requests(self):
        return [
            scrapy.Request(url=self.url_with_params(), callback=self.parse, errback=self.error_handler)
        ]

    def url_with_params(self):
        return self.url

    def structure_valid(self, response):
        self.data_rows = response.css("ul[class='list-link'] li a::attr(href)").extract()
        return len(self.data_rows) > 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

    def extractpersoninfo(self, response):
        left_panel = response.css("div[class='left']")
        imagesrc = left_panel.css("p img::attr(src)").extract_first().strip()
        imagesrc = re.sub('\s+',' ',imagesrc).strip()
        imagesrc = "{0}{1}".format(self.baseurl,imagesrc  )
        newremarksfield = self.extract_remarks(left_panel)
        E = response.request.meta['entity']
        profilelink = response.url

        if newremarksfield:
            E['remarks'] = "Image: {0} ; {1} ; {2} ; Profile: {3}".format(imagesrc,newremarksfield, E['remarks'], profilelink )
        else:
            E['remarks'] = "image: {0} ; {1} ; Profile: {2}".format(imagesrc, E['remarks'], profilelink )

        yield E

    def extractdatafromlinks(self, response):
        
        new_data_rows = response.css("table[class='cabinetListDl']").css("tr")
        url = response.url

        for i in range(len(new_data_rows)):
            personlink = new_data_rows[i].css('th a::attr(href)').extract_first()
            designation = ";".join(new_data_rows[i].css('td::text').extract())
            personname = self.extract_personaname(new_data_rows[i]).strip()
            inclusion_date = response.css('p[class="date"]::text').extract_first()
            inclusion_date = inclusion_date.split(",",1)[-1]

            remarksfield = ""
            if "(R)" in personname:
                remarksfield = "(R):{0}".format("Member of the House of Representatives")
            if "(C)" in personname:
                remarksfield = "(C):{0}".format("Member of the House of Counsellor")
            
            E = Entity()
            E['category'] = "Group"
            E['type'] = "SAN"
            E['inclusion_date']  = self.string_to_date(inclusion_date)
            E['name'] = re.sub('\s+', ' ', personname).strip() 
            E['designation'] =  re.sub('\s+', ' ', designation).strip() 
            E['remarks'] = remarksfield

            if personlink:
                personlink = "{0}{1}".format(self.baseurl,personlink)
                yield Request(url=personlink,meta={"entity":E}, callback=self.extractpersoninfo)
            
            else:
                yield E
                
    def extact_data(self, response):
        data_rows = self.data_rows
        for link in data_rows:
            
            listurl = "{0}{1}".format(self.baseurl,link)
            if listurl: 
                yield Request(url=listurl, callback=self.extractdatafromlinks)
    
    @staticmethod
    def extract_personaname(response):
        name = response.css('th a::text').extract_first()
        if not name:
            name = response.css('th::text').extract_first()
        return name.strip()

    @staticmethod
    def extract_remarks(response):
        table1 = response.css("table").css("tr")
        value = ""
        if table1:
            for i in range(len(table1)):
                columns = table1[i].css("td::text").extract()
                value = "{0} ; {1}".format(value,"".join(columns))
                
        else:
            body1 = list(map(str.strip, response.css("div[class='body']::text").extract()))
            body2 = list(map(str.strip, response.css("div[class='body'] p::text").extract()))
            remarks_list1 = list(filter(None, body1)) 
            remarks_list2 = list(filter(None, body2)) 
            if len(remarks_list1)>0:
                value = " ; ".join(remarks_list1)
            elif len(remarks_list2)>0:
                value = " ; ".join(remarks_list2)
        
        return value

    @staticmethod
    def string_to_date(date_string):
        if date_string:
            return datetime.strptime(date_string, ' %B %d, %Y')
        else:
            return None