from datetime import datetime
from bs4 import BeautifulSoup
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
from uqaab.database.databaseFactory import get_enabled_database

class OfacSDNSpider(UqaabBaseSpider):
    name = 'OFAC_SDN'
    start_urls = ['https://www.treasury.gov/ofac/downloads/sdn.xml']
    available_programs = []
    new_programs = []

    def structure_valid(self, response):
        return 100

    def extact_data(self, response):
        db = get_enabled_database()
        watchlist_program = db.get_program(self.list_id).split('/')
        base_name = watchlist_program[0]
        program_name = watchlist_program[1].lower()
        self.available_programs = db.get_sub_programs_list(self.list_id)

        soup = BeautifulSoup(response.body, "xml")
        sdnEntry = soup.findAll('sdnEntry')

        for i in range(0, len(sdnEntry)):
            progs = []
            for prog in sdnEntry[i].findAll('program'):
                progs.append(prog.text.lower())
                if prog.text not in self.available_programs and prog.text not in self.new_programs:
                    self.new_programs.append('{0}/{1}'.format(base_name, prog.text))
            if program_name in progs:
                name = ''
                remarks = None
                addresses = []
                city = []
                country = []
                category = None
                aka = []
                date_of_birth = None
                document = []
                nationality = None
                title = []
                gender = None

                if sdnEntry[i].find('firstName') is not None:
                    name = sdnEntry[i].find('firstName').text

                if sdnEntry[i].find('lastName') is not None:
                    name += ' ' + sdnEntry[i].find('lastName').text

                if sdnEntry[i].find('title') is not None:
                    title.append(sdnEntry[i].find('title').text)

                for address in sdnEntry[i].findAll('address'): 
                    if address.find('address1') is not None:
                        addresses.append(address.find('address1').text)

                    if address.find('address2') is not None:
                        addresses.append(address.find('address2').text)

                    if address.find('address3') is not None:
                        addresses.append(address.find('address3').text)

                    if address.find('city') is not None:
                        city.append(address.find('city').text)

                    if address.find('country') is not None:
                        country_string = address.find('country').text
                        country.append(self.get_country_code(country_string))

                if len(city) == 0:
                    city = None
                if len(country) == 0:
                    country = None

                date_of_birth = [date.text for date in sdnEntry[i].findAll('dateOfBirth')]
                place_of_birth = [place.text for place in sdnEntry[i].findAll('placeOfBirth')]

                if len(date_of_birth) > len(place_of_birth):
                    difference = len(date_of_birth) - len(place_of_birth)
                    place_of_birth += [None] * difference
                elif len(date_of_birth) < len(place_of_birth):
                    difference = len(place_of_birth) - len(date_of_birth)
                    date_of_birth += [None] * difference

                dobs = [{'DOB': item[0], 'POB': item[1]} for item in zip(date_of_birth, place_of_birth) if item[0] or item[1]]
                if dobs:
                    dob_info = json.dumps({'info': dobs})
                else:
                    dob_info = json.dumps({'info': None})

                if sdnEntry[i].find('remarks') is not None:
                    remarks = sdnEntry[i].find('remarks').text

                if sdnEntry[i].find('sdnType') is not None:
                    category = sdnEntry[i].find('sdnType').text

                for alias in sdnEntry[i].findAll('aka'):
                    aka_name = ''
                    if alias.find('firstName') is not None:
                        aka_name = alias.find('firstName').text
                    if alias.find('lastName') is not None:
                        aka_name += ' ' + alias.find('lastName').text

                    aka.append(aka_name.strip())

                for doc in sdnEntry[i].findAll('id'):
                    if doc.find('idType').text == 'Nationality of Registration':
                        nationality = doc.find('idNumber').text
                    elif doc.find('idType').text == 'Gender':
                        gender = doc.find('idNumber').text
                    else:
                        document.append({'type': doc.find('idType').text,
                                          'id': doc.find('idNumber').text})

                if document:
                    document_info = json.dumps({'document_info': document})
                else:
                    document_info = json.dumps({'document_info': None})

                yield Entity({
                    'category': category,
                    'name': name.strip(),
                    'remarks': remarks,
                    'address': addresses,
                    'city': city,
                    'country': country,
                    'date_of_birth': dob_info,
                    'aka': aka,
                    'document': document_info,
                    'nationality': nationality,
                    'title': title,
                    'gender': gender
                })