# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import scrapy

class SFC_Enforcement(UqaabBaseSpider):

    name = 'sfc_enforcement'
    start_urls = ['https://www.sfc.hk/edistributionWeb/gateway/EN/news-and-announcements/news/enforcement-news/']
    baseurl = 'https://www.sfc.hk/edistributionWeb/gateway/EN/news-and-announcements/news/enforcement-news/'    

    def structure_valid(self, response):
        self.links = response.css("#yearMenu option::attr(value)").extract()[0:2]
        return len(self.links) > 0
    
    def extractdetails(self, response):
        data_rows = response.css("table tr")[1:]
        
        for i in range(len(data_rows)):
            columns = data_rows[i].css("td")
            date = columns[0].css("::text").extract_first().strip()
            date = self.string_to_date(date)
            action = columns[2].css("a::text").extract_first().strip()
            action_reference = columns[2].css("a::attr(href)").extract_first()

            remarksfield = ""
            remarksfield = "Action: {0}, Action Reference: {1}{2}".format(action, self.baseurl, action_reference)
            newcolumns = columns[1].css("a")
            
            if len(newcolumns) == 0:
                newname = columns[1].css("::text").extract_first().strip()
                yield Entity({
                    "name": newname,
                    "remarks": remarksfield, 
                    "inclusion_date": date,
                    "category": "Group",
                    "type": "SIP"
                })
            else:
                names_href_dict = {}
                for line in newcolumns:
                    nameslist = line.css("::text").extract()
                    for name in nameslist:
                        newname = name.strip()
                        if newname:
                            break
                    
                    remarksfield = "{0}".format(remarksfield)
                    yield Entity({
                        "name": newname,
                        "remarks": remarksfield, 
                        "inclusion_date": date,
                        "category": "Group",
                        "type": "SIP"
                    })
        
    def extact_data(self, response):
        years = self.links
        for year in years:
            self.url = "{0}{1}{2}".format(self.baseurl, "?year=",year)
            yield scrapy.Request(url=self.url, callback=self.extractdetails)

    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%d %b %Y')
        except:
            return None