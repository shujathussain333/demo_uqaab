
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request

class IFSCommisionBezile(UqaabBaseSpider):

    custom_settings = {
        'DOWNLOAD_DELAY' : 0.25
    }        

    name = 'ifs_commision_bezile_warnings'
    start_urls = ['https://www.ifsc.gov.bz/?cat=4',]
    i = 0
    max_id = 25

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@id='content-part']/div")
        return len(data_rows) > 0

    def extact_data(self, response):
        while self.i <= self.max_id:
            self.i+=1
            yield Request('https://www.ifsc.gov.bz/?cat=4&paged=%d' % self.i, callback = self.extract_link)    

    def extract_link(self,response):
        warnings = response.xpath("//div[@id='content-part']/div")
        for warning in warnings:
            warning_detail = warning.xpath(".//h2/a/@href").extract_first()
            name = warning.xpath(".//h2/a/text()").extract_first()
            if warning_detail:
                yield Request(warning_detail, callback=self.parse_page,meta={"name":name})   

    def parse_page(self,response):
        further_details = response.xpath("//div[@class='entry']/p/a/@href").extract_first()
        name = response.meta.get('name')
        if "–" in name:
            name = name.split("–")[1]
        remarks = "further_details: {}".format(further_details)
        yield Entity({
            "category": "Group",
            "name":name,
            "remarks":remarks,
            "type":"SIP"
        })                
