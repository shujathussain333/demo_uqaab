##Author: Shahzaib Mumtaz
## Created on: 11-01-2019

import scrapy
from bs4 import BeautifulSoup
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class PPRABlacklistScraper(UqaabBaseSpider):
    
    name = 'ppra_blacklist_scraper'
    start_urls = ['http://www.ppra.org.pk/blacklist.asp']

    def structure_valid(self,response):
        soup = BeautifulSoup(response.body,'lxml')
        table = soup.find('table',{'border':'1'})
        return len(table) > 0

    def extact_data(self,response):
        soup = BeautifulSoup(response.body,'lxml')
        table = soup.find('table',{'border':'1'})
        rows = table.findAll('tr')[1:]
        
        for i in rows:
            name_of_proc = i.findAll('td')[1].get_text().strip()
            name_entity = i.findAll('td')[2].get_text().strip()
            addresses = i.findAll('td')[3].get_text().strip().split('\n\r\n')
            inclusion = i.findAll('td')[4].get_text().strip()
            
            try:
                inclusion = datetime.strptime(inclusion,'%d-%m-%Y').date()
            except:
                inclusion = None
            
            exclusion = i.findAll('td')[5].get_text().strip()
            
            try:
                exclusion = datetime.strptime(exclusion,'%d-%m-%Y').date()
            except:
                exclusion = None
            
            reason = i.findAll('td')[6].get_text().strip()
            type_of_ban = i.findAll('td')[7].get_text().strip()

            yield Entity({
                'name': name_entity,
                'category': CATEGORY_OPTIONS[1],
                'address': [address.replace('\r\n', ' ') for address in addresses],
                'inclusion_date': inclusion,
                'exclusion_date': exclusion,
                'remarks': 'Name of Procurer: {0}, Reason: {1}, Type of Blacklisting: {2}'.format(name_of_proc, reason, type_of_ban)
            })