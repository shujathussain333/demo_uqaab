#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request

class NigeriaIcpsWantedSpider(UqaabBaseSpider):

    name = 'nigeria_ngicps_wanted'
    start_urls = ['https://icpc.gov.ng/wanted-persons/']

    def structure_valid(self, response):
        data_rows =response.xpath('//div[@class="infinite-content-area infinite-item-pdlr infinite-sidebar-style-none clearfix"]//p[img]')
        return len(data_rows)>0

    def extact_data(self,response):
        details = response.xpath('//div[@class="infinite-content-area infinite-item-pdlr infinite-sidebar-style-none clearfix"]//p[img]')
        for detail in details:
            name = detail.xpath('following-sibling::p[1]/text()').extract_first()
            if name is None:
                name = detail.xpath('following-sibling::h3/text()').extract_first()
            elif "persons" in name:
                name = detail.xpath('following-sibling::h3/text()').extract_first()
            elif "person" in name:
                partial_name =  detail.xpath('following-sibling::p[1]/strong[1]/text()').extract_first()
                if len(partial_name)<4:
                    partial_name2 =  detail.xpath('following-sibling::p[1]/strong[2]/text()').extract_first()
                    name = partial_name + " " + partial_name2
                else:
                    name = partial_name        
            image = detail.xpath('img/@src').extract_first()
            remarks1 =   detail.xpath('following-sibling::p[1]/text()').extract()
            remarks1 = "".join(remarks1)
            remarks2 = detail.xpath('following-sibling::p[2]/text()').extract()
            remarks2 = "".join(remarks2)
            remarks3 = detail.xpath('following-sibling::p[3]/text()').extract()
            remarks3 = "".join(remarks3)
            remarks4 = detail.xpath('following-sibling::p[4]/text()').extract()
            remarks4 = "".join(remarks4)
            remarks5 = detail.xpath('following-sibling::p[5]/text()').extract()
            remarks5 = "".join(remarks5)
            remarks = remarks1 + " " + remarks2 + " " + remarks3 + " " + remarks4 + " " + remarks5       
            yield Entity({
                "category": "Individual",
                "name":name,
                "remarks":remarks,
                "image":image,
                "type": "SIP"
            })        