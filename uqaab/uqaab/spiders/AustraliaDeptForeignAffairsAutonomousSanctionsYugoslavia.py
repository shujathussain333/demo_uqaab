# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json


class DeptForeignAffairsAutonomousSanctionsYugoslavia(UqaabBaseSpider):
    name = 'dept_foreign_affairs_autonomous_sanctions_YU'
    start_urls = ['https://www.legislation.gov.au/Details/F2018L00099']

    def structure_valid(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('table.MsoNormalTable tr')
        data_rows = data_rows[1:]
        datas = []
        for row in data_rows:
            try:
                columns = row.css('td')
                # extracted column name in array because Name of Individual as two text in row ('x','Name of individual')
                column_name = columns[1].css('p span').css('*::text').extract()[-1]

                if 'Name' in column_name:
                    dictionary = {}
                    name = columns[2].css('p').css('*::text').extract_first()
                    category = "Individual"
                    # Few columns are not available in some thats why those columns have been set to None
                    # For example aka and citizenship
                    dictionary.update(
                        {"name": name, "category": category, 'aka': None, 'nationality': None, 'dob': None})

                elif 'aka' in column_name:
                    value = columns[2].css('p').css('*::text').extract_first()
                    akas = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                                value) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

                    aka = [{'aka': aka} for aka in akas]
                    alias_info = json.dumps({'info': aka})
                    dictionary.update({"aka": alias_info})

                elif "Date" in column_name:
                    dob = columns[2].css ('p').css ('*::text').extract_first ( )
                    dobs = {'DOB': str (dob), 'POB': None}
                    dictionary.update ({"dob": dobs})

                elif "Place" in column_name:
                    pob = columns[2].css('p').css('*::text').extract_first()
                    dictionary['dob']['POB'] = pob
                    dictionary.update({"dob": dobs})



                elif "Citizenship" in column_name:
                    nationality = columns[2].css('p').css('*::text').extract_first()
                    dictionary.update({"nationality": nationality})


                elif "Additional" in column_name:
                    remarks_additional = columns[2].css('p').css('*::text').extract_first()
                    dictionary.update({"remarks_additional": remarks_additional})

                elif "Listing" in column_name:
                    remarks_listing = columns[2].css('p').css('*::text').extract_first()
                    dictionary.update({"remarks_listing": remarks_listing})
                    datas.append(dictionary)

            except (TypeError, AttributeError, IndexError):
                continue

        for data in datas:
            remarks = data['remarks_additional'] + '.' + data['remarks_listing']
            if data['dob']:
                dob_info = json.dumps({'info': [data['dob']]})
            else:
                dob_info = json.dumps({'info': None})
            yield Entity({
                "name": data['name'],
                "category": data["category"],
                "type": "SAN",
                "date_of_birth": dob_info,
                "nationality": data["nationality"],
                "remarks": remarks
            })
