# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser


class PnpSecuritiesandExchangeCommissionUnregistredEntities(UqaabBaseSpider):
    name = 'pnp_securities_and_exchange'
    start_urls = ['http://www.sec.gov.ph/warning-on-online-investments-unregistered-entities/']

    def structure_valid(self, response):
        data_rows = response.css('li').css('strong').css('*::text').extract()
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.css('li').css('strong').css('*::text').extract()
        remarks = response.css('ol').css('*::text').extract()
        remark = ', '.join([r for r in remarks if r != '\n'])

        date_inclusion = response.css('p.post-date').css('*::text').extract_first()
        date_inclusion = dateutil.parser.parse(date_inclusion)
        for company_name in data_rows[:-2]:
            name = company_name
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "inclusion_date": date_inclusion,
                "remarks": remark.strip()
            })
