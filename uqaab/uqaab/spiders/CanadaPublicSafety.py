# -*- coding: utf-8 -*-
from datetime import datetime
import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS

class CandaPubicSafetyList(UqaabBaseSpider):
    name = 'canada_public_safety_spider'
    start_urls = ["https://www.publicsafety.gc.ca/cnt/ntnl-scrt/cntr-trrrsm/lstd-ntts/crrnt-lstd-ntts-en.aspx"]


    def structure_valid(self, response):
        data_rows = response.xpath('//h2')
        return len(data_rows) > 0

    def extact_data(self, response):

        detail_length = len(response.xpath('//p/strong/following-sibling::text()'))

        name_index = 5

        detail_index = 0

        while detail_index < detail_length-1:

            name = response.xpath('//h2/text()').extract()[name_index]
            name_index = name_index+1
            aka = response.xpath('//p/strong/following-sibling::text()').extract()[detail_index]
            remarks = response.xpath('//p/strong/following-sibling::text()').extract()[detail_index+1]
            inclusion_date = response.xpath('//p/strong/following-sibling::text()').extract()[detail_index+2]
            review_date = response.xpath('//p/strong/following-sibling::text()').extract()[detail_index+3]
            match_date = re.search(r'\d+-\d+-\d+', review_date)

            if match_date is None:
                    detail_index = detail_index + 3
            else:
                detail_index = detail_index + 4
            
            yield Entity({
                "name" : name,
                "aka": aka.split(','),
                "inclusion_date": inclusion_date,
                "remarks": remarks,
                "category": CATEGORY_OPTIONS[1]
            })





