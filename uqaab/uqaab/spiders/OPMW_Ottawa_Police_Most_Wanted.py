#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Ottawa_Police_Most_Wanted(UqaabBaseSpider):
    name = "ottawa_police_most_wanted"
    start_urls = ['https://www.ottawapolice.ca/en/crime/Most-Wanted.aspx']
    
    def structure_valid(self, response):
        data_rows = response.xpath('//div[@id="printAreaContent"]/table//tr//td')
        return len(data_rows) > 0
                    
    def extact_data(self, response):
        tableData = response.xpath('//div[@id="printAreaContent"]/table//tr//td')
        iterator = 0
        imageSource = ""
        for data in tableData[:4]:
            if(iterator%2 == 0):
                baseUrl = "www.ottawapolice.ca"
                imageSource = data.xpath("./img/@src").extract_first()
                imageSource = baseUrl + imageSource
            else:
                bioData = data.xpath(".//p//text()").extract()
                name = bioData[1]
                dob = bioData[2]
                dob = dob.replace("Born","")
                dob = dob.lstrip()
                yield Entity({
                    "name": name,
                    "category": "Group",
                    "type": "SAN",
                    "date_of_birth": dob,
                    "image": imageSource
                })
            iterator+=1
            
