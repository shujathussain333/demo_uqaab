# -*- coding: utf-8 -*-
import datetime
import re
import scrapy
import requests
import numpy as np
import io
import json
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import dateutil.parser


class NZPoliceResInd_Spider(UqaabBaseSpider):
    name = 'nzpolice_res_individuals_spider'
    start_urls = [
        "http://www.police.govt.nz/advice/personal-community/counterterrorism/designated-entities/lists-associated-with-resolutions-1267-1989-2253-1988"]

    def structure_valid(self, response):
        link = response.xpath("//ul[@class='doclist']//li//a/@href").extract()
        return len(link) > 0

    def extact_data(self, response):

        link = response.xpath("//ul[@class='doclist']//li//a/@href").extract()
        response = requests.get("https://www.police.govt.nz{}".format(link[-1]))

        df = self.create_dataframe(response)

        name_cols = [col for col in df if "Name" in col]

        nat_cols = [col for col in df if "Nationality" in col]

        addr_cols = [col for col in df if "Addres" in col]

        pass_cols = [col for col in df if "Passport" in col and len(col) == 10]

        dob_cols = [col for col in df if "DOB" in col]

        pob_cols = [col for col in df if "POB" in col]

        global DOB_info

        for index, row in df.iterrows():

            # to check ids does not contains any floating point
            if float(row["ID"]).is_integer():
                id = int(row["ID"])
                next_id = id + 1

                df2 = df[(df['ID'] > id) & (df['ID'] < next_id)]
                df2 = df2[['Name_1', 'Name_2', 'Name_3', 'Name_4', 'LastName']]
                df2 = df2.replace(np.nan, '', regex=True)
                df2 = df2.add([' '] * (df2.columns.size - 1) + ['']).sum(axis=1)

                alias_info = json.dumps({'info': None})
                aliases = df2.tolist()
                alias = [{'aka': re.sub('\s+', ' ', alias)} for alias in aliases]

                if alias:
                    alias_info = json.dumps({'info': alias})

                ############ Extract entity info ##############

                name = self.extract_name(row[name_cols])

                nationality = self.extract_nationality(row[nat_cols])

                organization = self.extract_organization(row["Associated with"])

                passport = self.extract_passport(row[pass_cols])

                document = json.dumps([passport])

                addresses = self.extract_addresses(row[addr_cols])

                title = self.extract_title(row["Title"])

                remarks = self.extract_remarks(row["Other"])

                inclusion_date = self.extract_incl_date(row["NZ_designation_date"])

                ############ Extract date ##############

                # print("========== DOB POB DATA =================")
                # print(row[dob_cols])
                # print(row[pob_cols])

                DOB_info = []

                if row["DOB_1"] is not np.nan and row["POB_1"] is not np.nan:
                    self.extract_date(row["DOB_1"], row["POB_1"], row["POB_1_Country"])

                if row["DOB_1"] is np.nan and row["POB_1"] is not np.nan:
                    dob_pob = {}
                    dob_pob["DOB"] = np.nan
                    if row["POB_1_Country"] is not np.nan:
                        dob_pob["POB"] = row["POB_1"] + " " + row["POB_1_Country"]
                    else:
                        dob_pob["POB"] = row["POB_2"]
                    DOB_info.append(dob_pob)

                if row["DOB_1"] is not np.nan and row["POB_1"] is np.nan:
                    self.extract_date(row["DOB_1"], np.nan, np.nan)

                ## DOB_2
                if row["DOB_2"] is not np.nan and row["POB_2"] is not np.nan:
                    self.extract_date(row["DOB_2"], row["POB_2"], row["POB_2_Country"])

                if row["DOB_2"] is np.nan and row["POB_2"] is not np.nan:
                    dob_pob = {}
                    dob_pob["DOB"] = np.nan
                    if row["POB_2_Country"] is not np.nan:
                        dob_pob["POB"] = row["POB_2"] + " " + row["POB_2_Country"]
                    else:
                        dob_pob["POB"] = row["POB_2"]
                    DOB_info.append(dob_pob)

                if row["DOB_2"] is not np.nan and row["POB_2"] is np.nan:
                    dob_pob = {}
                    self.extract_date(row["DOB_2"], np.nan, np.nan)

                for col in dob_cols[2:]:
                    if row[col] is not np.nan:
                        dob_pob = {}
                        self.extract_date(row[col], np.nan, np.nan)

                DOB_complete = json.dumps(DOB_info)

                yield Entity({
                    "name": name,
                    "remarks": remarks,
                    "date_of_birth": DOB_complete,
                    "inclusion_date": inclusion_date,
                    "category": CATEGORY_OPTIONS[0],
                    "document": document,
                    "address": addresses,
                    "nationality": nationality,
                    "organization": organization,
                    "title": title,
                    "aka": alias_info
                })

    @staticmethod
    def extract_name(data):

        n = list(data)
        names = []
        for name in n:
            if name is not np.nan:
                names.append(name)
        res = ",".join(names)
        return res

    @staticmethod
    def extract_organization(row):
        if row == 1.0:
            return "Taliban"
        elif row == 2.0:
            return "Al-Qaida"
        else:
            return "None"

    @staticmethod
    def extract_passport(row):
        n = list(row)
        document_info = {}
        for p in n:
            if p is not np.nan:
                document_info["type"] = "Passport"
                document_info["id"] = str(p)
        return document_info

    @staticmethod
    def extract_addresses(row):
        n = list(row)
        addresses = []
        for add in n:
            add = str(add)
            if "nan" not in add:
                addresses.append(add)
        return addresses

    @staticmethod
    def extract_title(row):
        T = []
        if "nan" not in str(row):
            title = row.split(",")
            for t in title:
                if "nan" not in str(t):
                    T.append(t)
        return T

    @staticmethod
    def extract_remarks(row):
        if row is np.nan:
            row = None
        return row

    @staticmethod
    def extract_incl_date(date):

        res = None

        if date is not None and "SCA" in str(date):
            date_reg_exp2 = re.compile(
                r'(\d{1,2}(/|-|\.)\w{3}(/|-|\.)\d{1,4})|([a-zA-Z]{3}\s\d{1,2}(,|-|\.|,)?\s\d{4})|(\d{1,2}(/|-|\.)\d{1,2}(/|-|\.)\d+)')
            dates = [x.group() for x in
                     date_reg_exp2.finditer(date)]
            date = dates[0]

        try:
            date = str(date).replace(".", "").replace("\n", "")

            if isinstance(date, datetime.datetime) is False:
                check = date.split()[-1]
                if "/" in check:
                    sdate = check
                    sdate = datetime.datetime.strptime(sdate, "%d/%m/%Y")
                    res = sdate

                else:
                    adate = date.split()[-3:]
                    adate = " ".join(adate)
                    adate = datetime.datetime.strptime(adate, "%d %B %Y")
                    res = adate
            elif date.isspace():
                res = date
            else:
                res = date
        except Exception:
            res = date

        return res

    @staticmethod
    def extract_nationality(row):
        data = list(row)
        nats = []
        for nat in data:
            if "nan" not in str(nat):
                nats.append(nat)
        return "|".join(nats)

    @staticmethod
    def extract_date(date, pob, pob_country):
        if isinstance(date, datetime.datetime) is False:

            date = str(date)
            date = date.replace('and', '-')
            date = date.replace('.', '')
            date = date.replace('or', '|')
            date = date.replace(" ", '')
            date = date.replace('(', '').replace(')', '')
            date = re.sub(r'[a-zA-Z]', '', date).strip()

            if "-" in date:

                if date[0] == "-":
                    date = date.split("-")[1] + date
                    date = date.strip()

                D = date.split("-")

                date_range = range(int(D[0]), int(D[1]) + 1)

                for dob in date_range:
                    dob_pob = {}
                    dob = str(dob) + ' ' + '01' + ' ' + '01'
                    dob = datetime.datetime.strptime(dob, "%Y %m %d")
                    dob_pob["DOB"] = str(dob)

                    if pob is not np.nan:
                        dob_pob["POB"] = pob
                    if pob_country is not np.nan:
                        dob_pob["POB"] = pob + " " + pob_country
                    if pob is np.nan:
                        dob_pob["POB"] = None

                    DOB_info.append(dob_pob)

            elif '/' in date:

                if len(date) == 7:

                    dob_pob = {}
                    dob = date.split('/')[0]
                    dob = str(dob) + ' ' + '01' + ' ' + '01'
                    dob = datetime.datetime.strptime(dob, "%Y %m %d")
                    dob_pob["DOB"] = str(dob)

                    if pob is not np.nan:
                        dob_pob["POB"] = pob
                    if pob_country is not np.nan:
                        dob_pob["POB"] = pob + " " + pob_country
                    if pob is np.nan:
                        dob_pob["POB"] = None

                    DOB_info.append(dob_pob)

                else:

                    dob_pob = {}
                    dob = datetime.datetime.strptime(date, "%d/%m/%Y")
                    dob_pob["DOB"] = str(dob)

                    if pob is not np.nan:
                        dob_pob["POB"] = pob
                    if pob_country is not np.nan:
                        dob_pob["POB"] = pob + " " + pob_country
                    if pob is np.nan:
                        dob_pob["POB"] = None

                    DOB_info.append(dob_pob)

            elif '|' in date:

                dob_pob = {}

                dates = date.split("|")

                for d in dates:

                    dob = str(d) + ' ' + '01' + ' ' + '01'
                    dob = datetime.datetime.strptime(dob, "%Y %m %d")
                    dob_pob["DOB"] = str(dob)

                    if pob is not np.nan:
                        dob_pob["POB"] = pob
                    if pob_country is not np.nan:
                        dob_pob["POB"] = pob + " " + pob_country
                    if pob is np.nan:
                        dob_pob["POB"] = None

                    DOB_info.append(dob_pob)


            else:

                dob_pob = {}
                dob = date.split()[0]
                dob = dob + ' ' + '01' + ' ' + '01'
                dob = datetime.datetime.strptime(dob, "%Y %m %d")
                dob_pob["DOB"] = str(dob)

                if pob is not np.nan:
                    dob_pob["POB"] = pob
                if pob_country is not np.nan:
                    dob_pob["POB"] = pob + " " + pob_country
                if pob is np.nan:
                    dob_pob["POB"] = None

                DOB_info.append(dob_pob)
        else:

            dob_pob = {}
            dob_pob["DOB"] = str(date)

            if pob is not np.nan:
                dob_pob["POB"] = pob
            if pob_country is not np.nan:
                dob_pob["POB"] = pob + " " + pob_country
            if pob is np.nan:
                dob_pob["POB"] = None
            if len(dob_pob) is not 0:
                DOB_info.append(dob_pob)
            # else:
            #    DOB_info.append(str(date))

    def strip_spaces(self, a_str_with_spaces):
        return float(str(a_str_with_spaces).replace(' ', ''))

    def create_dataframe(self, response):
        with io.BytesIO(response.content) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Individuals', converters={'ID': self.strip_spaces})
        return df
