#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy.http import Request
import pandas as pd
import numpy as np
import json
import re
import dateutil.parser

class Belgium_Federal_Public_Service_Finance (UqaabBaseSpider):
    name = "belgium_federal_public_service_finance"
    start_urls = ["https://finance.belgium.be/en/treasury/financial-sanctions"]

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        href = response.xpath ('.//span[@class="file-with-file-info"]//a/@href').extract_first ( )
        baseUrl = "https://finance.belgium.be/"
        href = baseUrl + href
        df = pd.read_excel(href)
        df['Gender'] = np.where(df['Gender'].isnull(), None, df['Gender'])
        df['Birth place'] = np.where(df['Birth place'].isnull(), None, df['Birth place'])
        df['Function'] = np.where(df['Function'].isnull(), None, df['Function'])
        df['Number'] = np.where(df['Number'].isnull(), None, df['Number'])
        df['Remark'] = np.where(df['Remark'].isnull(), None, df['Remark'])
        df['Regulation'] = np.where(df['Regulation'].isnull(), None, df['Regulation'])
        for i in df.index:
            wholeName = df['Wholename'][i]
            wholeNameList = wholeName.split("\n")
            wholeNameList = list(filter(None, wholeNameList))
            
            name = wholeNameList[0]
            
            alias_info = json.dumps({'info': None})
            aliases = wholeNameList[1:]
            alias = [{'aka': alias} for alias in aliases]

            if alias:
                alias_info = json.dumps({'info': alias})

            gender = df['Gender'][i]
            if gender is not None:
                gender = gender.replace("\n",'')
                gender = "Male" if df['Gender'][i] == "M" else "Female"
            
            birthDate = str(df['Birth date'][i])
            birthDate = birthDate.split("\n")
            birthDate = list(filter(None, birthDate))
            birthDate = ' ,'.join(birthDate)
                
            birthPlace = df['Birth place'][i]
            if(birthPlace is not None):
                birthPlace = birthPlace.split("\n")
                birthPlace = list(filter(None, birthPlace))
                birthPlace = ' ,'.join(birthPlace)
              
            dobs = {'DOB': birthDate, 'POB': birthPlace}
            if dobs:
                dob_info = json.dumps({'info': [dobs]})
            else:
                dob_info = json.dumps({'info': None})

            country = df['Embargos'][i]
            country = country.split("\n")
            country = list(filter(None, country))
            country = ' ,'.join(country)
            
            inclusion_date = str(df['Publication date'][i])
            inclusion_date = inclusion_date.split("\n")
            inclusion_date = list(filter(None, inclusion_date))
            inclusion_date = inclusion_date[0]
            
            inclusion_date = dateutil.parser.parse(inclusion_date)

            category = "Group" if df['type'][i] == "E" else "Individual"

            designations = df['Function'][i]
            if(designations is not None):
                designations = designations.split("\n")
                designations = list(filter(None, designations))
                designation = [{'designation': designation} for designation in designations]
                designation_info = json.dumps({'info': designation})
            else:
                designation_info = json.dumps({'designation': designations})
            
            sheetremark = df['Remark'][i]
            if(sheetremark is not None):
                sheetremark = sheetremark.split("\n")
                sheetremark = list(filter(None, sheetremark))
                sheetremark = ' , '.join(sheetremark)
                sheetremark = sheetremark.replace("\\n","")          

            regulations = df['Regulation'][i]

            if(regulations is not None):
                regulations = regulations.split("\n")
                regulations = list(filter(None, regulations))
                regulations = ' , '.join(regulations)
            
            links = df['Links'][i]
            links = links.split("\n")
            links = ' , '.join(links)

            if((regulations is not None) and (sheetremark is not None)):    
                remarks = "Links: {} ,Regulations: {} ,Remarks: {}".format(links,regulations,sheetremark)
            else:
                remarks = "Links: {} ,Regulations: {} ,Remarks: {}".format(links,"","")
            passportData = ""
            nrnData = ""
            nationalIdentityData = ""

            document = []
            number = df['Number'][i]
            if(number is not None):
                number = number.replace("\\n","")
                number = number.split("\n")
                number = list(filter(None, number))
                passportCheck = False
                nrnCheck = False
                nidCheck = False
                for num in number:
                    if("passport" in num and passportCheck == False):
                        passportData = num
                        passportCheck = True
                    elif("NRN" in num and nrnCheck == False):
                        #nrnData.append(num)
                        nrnData = num
                        nrnData = nrnData.replace("NRN ","")
                        nrnCheck = True
                    elif("identification" in num and nidCheck == False):
                        #nationalIdentityData.append(num)
                        nationalIdentityData = num
                        nidCheck = True
                re_passport = re.compile(r'\d+')
                re_nid = re.compile(r'\w*\d+[/]*[-]*\d*')
                
                passportData = re.findall(re_passport,passportData)
                passportData = ' , '.join(passportData)

                nationalIdentityData = re.findall(re_nid,nationalIdentityData)
                nationalIdentityData = ' , '.join(nationalIdentityData)
                
            document.append({'type': "passport",'id': passportData})
            document.append({'type': "nrn",'id': nrnData})
            document.append({'type': "national identification",'id': nationalIdentityData})

            if document:
                document_info = json.dumps({'document_info': document})
            
            yield Entity({
                "name": name,
                "aka": alias_info,
                "category": category,
                "type": "SAN",
                'date_of_birth': dob_info,
                "document": document_info,
                "gender": gender,
                "remarks": remarks,
                "country": country,
                "designation": designation_info,
                "inclusion_date": inclusion_date
            })