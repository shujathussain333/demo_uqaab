#Author: Sharjeel Ali Shaukat
#Edited By: Daniyal Faquih
#Reason: Column name was changed in excel file

import numpy as np
import io
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class NBFCSCancelledFirms(UqaabBaseSpider):
    name = 'nbfcs_cancelled_firms'
    start_urls = ['https://rbidocs.rbi.org.in/rdocs/content/docs/841246844A.xlsx', ]

    def structure_valid(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0, header=None, index=None)
        return len(df) > 0

    def extact_data(self, response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0, header=None, index=None)

        df = df.drop(df.columns[0], axis=1)
        df = df.iloc[2:]
        # grab the first row for the header
        new_header = df.iloc[0]
        # take the data less the header row
        df = df[1:]
        # set the header row as the df header
        df.columns = new_header

        for index, row in df.iterrows():

            if type(row["Regional Office"]) is str:
                city=row["Regional Office"]

            if row["Name of the company"] is not np.nan:
                yield Entity({
                    "category": "Group",
                    "name": row["Name of the company"].strip(),
                    "type": "SAN",
                    "address": [str(row["Address 1"]) + ' ' + str(row["Address 2"]) + '' + str(row["Address 3"])],
                    "city":city
                })