"""
Created by Muhammad Ilyas <muhammad.ilyas@lovefordata.com>
"""

from datetime import datetime
import bs4 as bs
from uqaab.items import Entity
import json
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class CanadianSacList(UqaabBaseSpider):
    name = "canadian_sanctions_list"
    allowed_domains = ['www.international.gc.ca']
    start_urls = ['https://www.international.gc.ca/world-monde/assets/office_docs/international_relations-relations_internationales/sanctions/sema-lmes.xml']
    
    def structure_valid(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        return len(soup.find_all('record')) > 0
          
    def isEntity(self, i):
        name = ""
        category = "Group"
        country = ""
        aliases = ""
        country = None
        if i.find('entity') is not None:
            name = i.find('entity').text
        if i.find('country') is not None:
            country = str(i.find('country').text).split(" / ")[0]
        if i.find('aliases') is not None:
            aliases = i.find('aliases').text
        
        return ({
            'category' : category,
            'name': name,
            'aka' : aliases, 
            'type' : "San",
            'country' : self.get_country_code(country),
        })

    def isIndividual(self, i):
        name = ""
        category = "Individual"
        country = ""
        aliases = []
        dob = None
        country = None
        if i.find('givenname') is not None:
            name = i.find('givenname').text
        if  i.find('lastname') is not None:
            name = '{0} {1}'.format(name, i.find('lastname').text)
        if i.find('country') is not None:
            country = i.find('country').text.split(" / ")[0]
        if i.find('aliases') is not None:
            aliases.append(i.find('aliases').text)
        if i.find('dateofbirth') is not None:
            dob_info = i.find('dateofbirth').text
            dob = [{'DOB': dob_info, 'POB': None}]

        return ({
            'category': category,
            'name': name,
            'aka' : aliases,
            'type' : "San",
            'country' : self.get_country_code(country),
            'date_of_birth' : json.dumps({'info': dob})
        })

    def extact_data(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        for i in soup.find_all('record'):
            if i.find('entity') is not None:
                yield Entity(self.isEntity(i))
            else:
                yield Entity(self.isIndividual(i))
