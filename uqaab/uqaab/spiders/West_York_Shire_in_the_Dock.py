#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class West_York_Shire_in_the_Dock(UqaabBaseSpider):
    name = "west_york_shire_in_the_dock"
    start_urls = ['https://www.westyorkshire.police.uk/in-the-dock']
        
    def structure_valid(self, response):
        data_divs = response.xpath('//div[@class="views-row"]')
        return len(data_divs) > 0
    
    def extact_data(self, response):
        baseUrl = "https://www.westyorkshire.police.uk"
        pages = response.xpath('//div[@class="views-row"]')

        for page in pages:
            next_page = page.xpath('.//a//@href').extract_first()
            next_page = baseUrl + next_page
            if next_page is not None:
                yield scrapy.Request(next_page, callback=self.extract_link)

        relativeNextPageUrl = response.xpath('//a[@class="button"]/@href').extract_first()
        if(relativeNextPageUrl):
            relativeNextPageUrl = relativeNextPageUrl.replace("/in-the-dock","")
            relativeNextPageUrl = self.start_urls[0] + relativeNextPageUrl
            yield scrapy.Request(relativeNextPageUrl, callback=self.extact_data)

    def extract_link(self, response):
        name = response.xpath('//div[@class="field field--name-field-dock-person-name field--type-string field--label-hidden field--item"]/text()').extract_first()
        city = response.xpath('//div[@class="field field--name-field-dock-person-area field--type-string field--label-hidden field--item"]/text()').extract_first()
        remarks = response.xpath('//div[@class="field field--name-body field--type-text-with-summary field--label-above"]/div[@class="field--item"]//p//text()').extract()
        remarks = ''.join(remarks)
        baseUrl = "https://www.westyorkshire.police.uk"
        imageSource = response.xpath('//div[@class="field field--name-field-dock-person-image field--type-image field--label-hidden field--item"]//a//@href').extract_first()
        yield Entity({
            "name": name,
            "category": "Individual",
            "type": "SAN",
            "city": city,
            "remarks": remarks,
            "image":imageSource
        })