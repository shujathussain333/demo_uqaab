# Author = Barkat Khan

from uqaab.environment import config
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import json


class AlertList(UqaabBaseSpider):
    name = 'alertlist'
    url = 'https://www.sfc.hk/web/EN/alert-list/a.html'
    visitedlinks = []

    def start_requests(self):
        return [
            scrapy.Request(url=self.url_with_params(), callback=self.parse, errback=self.error_handler)
        ]

    def url_with_params(self):
        self.visitedlinks.append(self.url)
        return self.url

    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 800,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    def structure_valid(self, response):
        data_rows = response.css("table[class='alert__list']")[0].css("tr")
        return len(data_rows) > 0

    def extact_data(self, response):

        data_rows = response.css("table[class='alert__list']")[0].css("tr")[1:]

        for row in data_rows:
            columns = row.css('td')
            remarks = None

            if columns[0].css('a::text').extract_first() is None:
                data_rows = response.css("ul[class='glossary_nav alertlist_nav noprint']"
                                         )[0].css("li")

                for count in range(len(data_rows)):
                    nextlink = data_rows[count].css('a::attr(href)').extract_first()
                    next_url = "https://www.sfc.hk/web/EN/" + nextlink
                    yield scrapy.Request(
                        url=next_url,
                        callback=self.parse,
                        errback=self.error_handler
                    )

            else:
                try:
                    remarks.append(columns[1].css('::text').extract_first().replace("\n", "").replace("\t", ""))
                except:
                    remarks = None
                yield Entity({
                    'category': 'Group',
                    'type': "SAN",
                    'name': columns[0].css('a::text').extract_first(),
                    'remarks': remarks
                })
