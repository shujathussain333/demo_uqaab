#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class CMMSE_Center_of_Medicare_and_Medicare_Services_Enforcement(UqaabBaseSpider):
    name = 'cmmse_center_of_medicare_and_medicare_services'
    start_urls = ['https://www.cms.gov/Medicare/Compliance-and-Audits/Part-C-and-Part-D-Compliance-and-Audits/PartCandPartDEnforcementActions-.html']

    def structure_valid(self, response):
        data_rows = response.xpath('//*[@id="table-container"]/table//tr')
        return len(data_rows) > 0

    def extact_data(self, response):
        products = response.xpath('//*[@id="table-container"]/table//tr')
        for product in products[1:]:
            name = product.xpath('td[2]//text()').extract_first()
            name = name.replace("\n","")
            name = ' '.join(name.split())

            inclusion_date = product.xpath('td[1]//text()').extract_first()
            exclusion_date = product.xpath('td[5]//text()').extract_first()

            inclusion_date = dateutil.parser.parse(inclusion_date)
            exclusion_date = dateutil.parser.parse(exclusion_date)

            actionTaken = product.xpath('td[3]//text()').extract_first()
            basisForAction = product.xpath('td[4]//text()').extract_first()
            remarksfield = "Action taken: {} ,Basis for action: {}".format(actionTaken,basisForAction)
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "inclusion_date": inclusion_date,
                "exclusion_date": exclusion_date,
                "remarks": remarksfield
            })
