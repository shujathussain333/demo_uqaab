# Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import urllib.request

class Interpol_Wanted_New(UqaabBaseSpider):
    name = 'interpol_wanted_new'
    start_urls = ['https://ws-public.interpol.int/notices/v1/red']
    
    custom_settings = {
        "DOWNLOAD_DELAY": 0.10,
    }
    def structure_valid(self, response):
        jsonresponse = json.loads(response.body)
        total_records = jsonresponse['total']
        return total_records > 0

    def extact_data(self, response):
        countryUrls = ['https://ws-public.interpol.int/notices/v1/red?&nationality=AF',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BB',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BJ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BF',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=BI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CV',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CF',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=HR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DJ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=EC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=EG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SV',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GQ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ER',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=EE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ET',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=FJ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=FI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=FR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=DE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=HT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=HN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=HU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=914',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ID',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IQ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=IT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=JM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=JP',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=JO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KP',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LV',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LB',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MV',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ML',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MX',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=FM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ME',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NP',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=MK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=NO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=OM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=PT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=QA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=RO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=RU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=RW',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=KN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=VC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=WS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ST',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=RS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SI',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SB',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ZA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SS',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ES',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=LK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=916',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SD',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=CH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=SY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TJ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TH',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TL',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TO',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TT',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TR',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TC',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=TV',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=UG',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=UA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=922',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=UNK',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=AE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=GB',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=US',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=UY',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=UZ',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=VU',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=VA',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=VE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=VN',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=YE',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ZM',
                    'https://ws-public.interpol.int/notices/v1/red?&nationality=ZW']

        for url in countryUrls:
            try:
                 url_2 = urllib.request.urlopen(url)
                 jsonresponse = json.loads(url_2.read().decode())
                 total_records = jsonresponse['total']
                 if(total_records > 0):
                     yield scrapy.Request(url + '&resultPerPage=160&page=1', callback = self.extract_link)
            except:
               print("+_____________________Except__________________________")
         
    def extract_link(self, response):
        jsonresponse = json.loads(response.body)
        links = jsonresponse['_embedded']['notices']
        
        for link in links:
            new_url = link['_links']['self']['href']
            yield scrapy.Request(url=new_url, callback=self.extract_details)
    
    def extract_details(self, response):
        data = json.loads(response.body)
        forename = data['forename']
        if(forename is None):
            forename = ""
        name = data['name']
        if(name is None):
            name = ""
        name = forename + " " + name
        name = name.strip()
        if(name == ""):
            name = "Anonymous"
        
        date_of_birth = data['date_of_birth']
        place_of_birth = data['place_of_birth']
        dobs = [{'DOB' : date_of_birth, 'POB' : place_of_birth}]
        
        if dobs:
            dob_info = json.dumps({'info': dobs})
        else:
            dob_info = json.dumps({'info': None})

        entity_id = data['entity_id']
            
        document = []
        document.append({'type': "Entity ID",'id': entity_id})
        
        if document:
            document_info = json.dumps({'document_info' : document})

        arrest_warrents = data['arrest_warrants']
        arrest_warrents = json.dumps(arrest_warrents).strip()
        
        remarks = "Arrest Warrents Details: {}".format(arrest_warrents.strip("[{}]"))
        remarks = remarks.replace('null', '""')
        remarks = remarks.replace('"', '')
           
        nationalities = data['nationalities']
        try:
            nationality = ' , '.join(nationalities)
        except TypeError:
            nationality = nationalities

        imageUrl = ""   
        
        try:
            imageUrl = data['_links']['thumbnail']['href']
        except:
            imageUrl = ""
        
        gender = data['sex_id']
        yield Entity({
                "category": "Individual",
                "name": name,
                'date_of_birth': dob_info,
                'nationality': nationality,
                "remarks": remarks,
                "type": "SIP",
                "image":imageUrl,
                "gender":gender,
                "document":document_info
        })