# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import tabula
import pandas as pd
import numpy as np
import dateutil.parser


class BankNegaraMalaysiaCompanyAlerts(UqaabBaseSpider):
    name = 'bank_negara_malaysia_company_alerts'
    start_urls = ['http://www.bnm.gov.my']

    def structure_valid(self, response):
        url = 'http://www.bnm.gov.my/documents/2019/FCA_20190114_EN.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)

        return total_len > 0

    def extact_data(self, response):
        url = 'http://www.bnm.gov.my/documents/2019/FCA_20190114_EN.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)
        cols = ["No", "Name", "Website", "DateAdded"]

        df = pd.DataFrame()
        for page in range(0, total_len):
            df = pd.concat([df, bin[page]])

        cols = [col.replace("\r", " ") for col in cols]

        df.columns = cols
        df = df[df['No'] != 'No']
        df = df.replace(np.nan, '', regex=True)

        for index, row in df.iterrows():

            name = row["Name"]
            date_inclusion = self.extract_date(row["DateAdded"])
            remarks = row["Website"]

            if len(name) > 0:
                yield Entity({
                    'category': "Group",
                    'name': name.strip(),
                    'remarks': 'Website: {0}'.format(remarks),
                    'inclusion_date': date_inclusion,
                    'type': 'SAN'
                })

    def extract_date(self, date_exclusion):
        try:
            date_reg_exp2 = re.compile(
                r'(\d{1,2}(/|-|\.)\w{3}(/|-|\.)\d{1,4})|([a-zA-Z]{3}\s\d{1,2}(,|-|\.|,)?\s\d{4})|(\d{1,2}(/|-|\.)\d{1,2}(/|-|\.)\d+)')
            dates = [x.group() for x in
                     date_reg_exp2.finditer(date_exclusion)]
            date_inclusion = dates[0]
            date_inclusion = dateutil.parser.parse(date_inclusion)
        except:
            date_inclusion = None
        return date_inclusion


