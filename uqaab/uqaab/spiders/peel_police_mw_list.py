# Author: Ilyas Yousuf

import bs4 as bs
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import scrapy

class PPMWList(UqaabBaseSpider):
    name = "peel_police_mw_list"
    start_urls = ['https://www.peelpolice.ca/Modules/News/Search.aspx?feedId=5dd101f7-e47a-4cdb-98c1-2e2cd6b2d1f9&page=1']
    page_num = 1

    def structure_valid(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        data = soup.find_all("div", {'class' : 'newsItem'})
        return len(data) > 0

    def isIndividual(self, content, image):
        category = "Individual"
        remarks = ""
        name = ""
        for i in content:
            if i.find("Name:") != -1 :
                name = i.split("Name: ")[1]
            elif i.find("NAME:") != -1 :
                name = i.split("NAME:")[1]
            if i.find("Description:") != -1 or i.find("DESCRIPTION:") != -1 :
                remarks = remarks + i + ","
            if i.find("Charges:") != -1 or i.find("CHARGES:") != -1 :
                remarks = remarks + i + ","
            if i.find("AGE:") != -1:
                remarks = remarks + i + ","

        return ({
            'category': category,
            'name': name,
            'remarks': remarks,
            'type' : "SIP",
            'image':image
        })

    def extact_data(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        data = soup.find_all("div", {'class' : 'newsItem'})

        if len(data) > 0:
            for i in range(len(data)):

                content = str(data[i].find("span", {'id':'cphContent_rpNews_lblDescription_'+str(i)})).split("<br/>")
                image = data[i].find("img")
                yield Entity(self.isIndividual(content, image['src']))
        
            self.page_num += 1
            url = 'https://www.peelpolice.ca/Modules/News/Search.aspx?feedId=5dd101f7-e47a-4cdb-98c1-2e2cd6b2d1f9&page={}'.format(self.page_num)
            yield scrapy.Request(url, callback=self.extact_data, errback=self.error_handler)