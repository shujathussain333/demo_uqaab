#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request
import json

class SanPabloWanted(UqaabBaseSpider):

    name = 'san_pablo_wanted'
    start_urls = ['https://www.sanpabloca.gov/1078/Most-Wanted']

    def structure_valid(self, response):
        data_rows = response.xpath('//table//tr//a/@href')
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.sanpabloca.gov"
        page_urls = response.xpath('//table//tr//a/@href').extract()
        for page in page_urls:
            each_link = page
            absolute_url = base_url + each_link
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        base_url="https://www.sanpabloca.gov"
        name = response.xpath('//table[@class="telerik-reTable-2"]//tr[1]/td[2]/text()').extract_first()
        date_of_birth = response.xpath('//table[@class="telerik-reTable-2"]//tr[2]/td[2]/text()').extract_first()
        dobs = {'DOB': date_of_birth, 'POB': None}
        dob_info = json.dumps({'info': [dobs]})
        description = response.xpath('//table[@class="telerik-reTable-2"]//tr[3]/td[2]/text()').extract_first()
        associations = response.xpath('//table[@class="telerik-reTable-2"]//tr[./td[1]="Associations"]/td[2]/text()').extract_first()
        if associations is None:
            associations = " "
        sppd_case = response.xpath('//table[@class="telerik-reTable-2"]//tr[./td[1]="SPPD Case #"]/td[2]/text()').extract_first()
        document_info = json.dumps({'document_info': None})
        document = []  
        document.append({'type': "SPPD Case #", 'id': sppd_case})
        if document:
            document_info = json.dumps({'document_info': document})      
        remarks = response.xpath('//table[@class="telerik-reTable-2"]//tr[./td[1]="Miscellaneous"]/td[2]/text()').extract_first()
        remarks = "Miscellaneous: {} description: {}  associations: {}".format(remarks, description,associations)
        image_url = response.xpath('//table[@class="telerik-reTable-2"]//tr[1]//img/@src').extract_first()
        image = base_url + image_url
        if name:
            name = name.strip()
        yield Entity({
            "category": "Individual",
            "name":name,
            "date_of_birth":dob_info,
            "remarks":remarks,
            "image":image,
            "document":document_info,
            "type": "SIP"
        })       