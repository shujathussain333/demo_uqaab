import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class FacfoaRegulationsSpider(UqaabBaseSpider):
    name = "freezing_assets_of_corrupt_foreign_officials"
    allowed_domains = ["http://laws-lois.justice.gc.ca"]


    content_path = '//Regulation/Schedule[1]/List[1]/*/Text/text()'

    def start_requests(self):
        start_url = 'http://laws-lois.justice.gc.ca/eng/XML/SOR-2011-78.xml'

        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        }

        yield Request(start_url, callback=self.parse, headers=headers, dont_filter=True)

    def structure_valid(self, response):
        test_path = response.xpath(self.content_path).extract()
        return len(test_path) > 0    

    def extact_data(self, response):

        aka_separator = 'also known among other names as'
        dob_separator = 'born on'

        print("                 *******Extracting Special Economic Measures (Burma) Regulations Entities List*******")
        
        content_list = response.xpath(self.content_path).extract()

        for row in content_list:
            aliases = []
            name = family_info = date_of_birth = aka = ''
            temp_row = row
            if aka_separator in row:

                splited_row = row.split(aka_separator)

                name = ''.join(splited_row[:1])[:-2].strip()
                aka = splited_row[-1].split(')')[:1][0].strip()

                temp = ''.join(splited_row)
                temp_row = temp
            if dob_separator in temp_row:

                splited_row = row.split(dob_separator)

                date_of_birth = ''.join(splited_row[-1].split(',')[:2])
                date_of_birth = dateparser.parse(date_of_birth).strftime('%Y-%m-%d')
                
                family_info = ', '.join(splited_row[-1].split(',')[2:]).strip()
            else:
                row_temp = row.split(',')
                name = row_temp[0].strip()
                family_info = row_temp[1].strip()
            
            aliases.append(aka)

            yield Entity({
                'name': name,
                'aka': aliases,
                'date_of_birth': date_of_birth,
                'category': 'Individual'
            })
            
        print("                 *******Finished Extracting Special Economic Measures (Burma) Regulations Entities List*******")