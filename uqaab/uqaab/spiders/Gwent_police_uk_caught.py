#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import re
import scrapy
from scrapy import Request

class GwentPoliceSpider(UqaabBaseSpider):

    name = 'gwent_police_uk_caught'
    start_urls = ['https://www.gwent.police.uk/index.php?id=151']

    def structure_valid(self, response):
        data_rows = response.xpath("//a[text()='\nFind out more\n']/@href")
        return len(data_rows) > 0

    def extact_data(self,response):
        base_url = "https://www.gwent.police.uk"
        page_urls = response.xpath("//a[text()='\nFind out more\n']/@href").extract()
        for page in page_urls:
            absolute_url = base_url + page
            yield Request(absolute_url, callback=self.parse_page)    	

    def parse_page(self,response):
        base_url="https://www.gwent.police.uk"
        name = response.xpath("//div[@role='main']//div[@class='col-md-8']/h1/text()").extract_first()
        age = response.xpath("//div[@role='main']//div[@class='col-md-8']/p[1]/text()").extract_first()
        address = response.xpath("//div[@role='main']//div[@class='col-md-8']/p[2]/text()").extract_first()
        offences = response.xpath("//div[@role='main']//div[@class='col-md-8']/ul//text()").extract()
        offences = "".join(offences)
        if "\n" in offences:
            offences = offences.replace("\n",",")
        sentence = response.xpath("//div[@role='main']//div[@class='col-md-8']/p[3]/text()").extract_first()
        if "\n" in sentence:
            sentence = sentence.replace("\n","")
        remarks = "offences: {} Sentence: {}  Age: {}".format(offences, sentence, age)
        image_url = response.xpath("//div[@role='main']//div[@class='col-md-4']//figure/img/@src").extract_first()
        image = base_url + image_url
        yield Entity({
            "category": "Individual",
            "name":name,
            "address":address,
            "remarks":remarks,
            "image":image,
            "type": "SIP"
        })     