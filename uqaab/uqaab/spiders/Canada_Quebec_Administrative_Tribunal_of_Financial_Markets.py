#Author: Daniyal Faquih
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class Canada_Quebec_Administrative_Tribunal(UqaabBaseSpider):
    name = 'canada_quebec_administrative_tribunal'
    start_urls = ['https://www.tmf.gouv.qc.ca/en/decisions/notice-of-decisions/']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="contenu"]//div[@class="csc-default"]')
        return len(data_rows) > 0

    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="contenu"]//div[@class="csc-default"]')
        for data in data_rows:
            rawData = data.xpath('.//h4[1]//text()').extract()
            rawData = ''.join(rawData)
            if("(files" in rawData):
                rawData = rawData.split("(files")
            elif("(file" in rawData):
                rawData = rawData.split("(file")
            else:
                rawData = rawData.split("(dossier")
            name = rawData[0].replace("Notice for","").strip()
            
            remarks = data.xpath('.//div[@class="indent"]//p[1]//text()').extract_first()
            remarks = "Summary: {}".format(remarks)
            yield Entity({
                "name": name,
                "category": "Group",
                "type": "SAN",
                "remarks": remarks
            })