import io
from datetime import datetime

import pandas as pd

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class USDeniedPersonsSpider(UqaabBaseSpider):
    name = "us_denied_persons"
    
    start_urls = [
        'https://www.bis.doc.gov/dpl/dpl.txt',
    ]

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        df = pd.read_table(io.BytesIO(response.body))

        for row in df.itertuples():
            yield Entity({
                'name': row.Name.strip(),
                'address': row.Street_Address.strip() if pd.notna(row.Street_Address) else None,
                'city': row.City.strip() if pd.notna(row.City) else None,
                'country': self.get_country_code(row.Country.strip()) if pd.notna(row.Country) else None,
                'inclusion_date': self.string_to_date(row.Effective_Date),
                'exclusion_date': self.string_to_date(row.Expiration_Date),
                'category': 'Individual'
            })
            
    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%m/%d/%Y')
        except TypeError:
            return None
