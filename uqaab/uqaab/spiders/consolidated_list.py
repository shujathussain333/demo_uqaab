import re
import io
import json
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from operator import itemgetter


class cobsolidated(UqaabBaseSpider):
    name = 'consolidated_xls'
    start_urls = [
        "https://dfat.gov.au/international-relations/security/sanctions/Documents/regulation8_consolidated.xls", ]

    def structure_valid(self, response):
        df = self.create_dataframe(response)
        format = ['Reference', 'Name of Individual or Entity', 'Type', 'Name Type',
                  'Date of Birth', 'Place of Birth', 'Citizenship', 'Address',
                  'Additional Information', 'Listing Information', 'Committees',
                  'Control Date']
        columns = df.columns
        return set(format) == set(columns)

    def extact_data(self, response):
        df = self.create_dataframe(response)
        df = df[df['Name Type'] != 'Original Script']
        df[['Address', 'Additional Information', 'Date of Birth', 'Place of Birth', 'Citizenship']] = df[
            ['Address', 'Additional Information', 'Date of Birth', 'Place of Birth', 'Citizenship']].fillna(value='')
        for index, row in df.iterrows():
            if isinstance(row["Reference"], int):

                id = row["Reference"]
                df2 = df.loc[df.Reference.str.contains('^' + str(id) + "[a-z]+", na=False)]

                alias_info = json.dumps({'info': None})
                alias = None
                if not df2.empty:
                    df2 = df2['Name of Individual or Entity']
                    aliases = df2.tolist()
                    alias = [{'aka': re.sub('\s+', ' ', alias)} for alias in aliases]

                if alias:
                    alias_info = json.dumps({'info': alias})

                addresses = self.extract_addresses(row["Address"])
                name = self.extract_name(row["Name of Individual or Entity"])
                additional_information = row["Additional Information"]
                extracted_information_rows = self.create_list_additional_information(additional_information)
                designation_info = json.dumps({'info': None})
                title_info = json.dumps({'info': None})
                document_info = json.dumps({'document_info': None})
                document = []
                remarks = None
                for index, item in enumerate(extracted_information_rows):

                    if "title" in item[0]:
                        title_info = self.extract_title(item, index, additional_information, extracted_information_rows)

                    elif "designation" in item[0]:
                        designation_info = self.extract_designation(item, index, additional_information,
                                                                    extracted_information_rows)

                    elif "passport" in item[0]:
                        document = self.extract_passport(item, index, additional_information,
                                                         extracted_information_rows, document)

                    elif "national identification" in item[0]:
                        document = self.extract_identification(item, index, additional_information,
                                                               extracted_information_rows, document)

                    elif "other info" in item[0]:
                        remarks = self.extract_remarks(item, index, additional_information, extracted_information_rows)

                if len(document) > 0:
                    document_info = json.dumps({'document_info': document})

                category = row["Type"]
                dobs = self.extract_dob(row["Date of Birth"], row["Place of Birth"])

                if len(dobs) > 0:
                    dob_info = json.dumps({'info': None})
                else:
                    dob_info = json.dumps({'info': dobs})

                nationality = row["Citizenship"]

                yield Entity({
                    "category": category,
                    "nationality": nationality,
                    "name": name,
                    'date_of_birth': dob_info,
                    "address": addresses,
                    "document": document_info,
                    "title": title_info,
                    "remarks": remarks,
                    "designation": designation_info,
                    "type": "SAN",
                    "aka": alias_info
                })

    @staticmethod
    def extract_name(data):
        return data

    @staticmethod
    def extract_organization(row):
        return 0

    @staticmethod
    def extract_addresses(row):
        addresses = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                         row) if x and not re.match(r"\s*[a-zA-Z]\)", x)]
        return addresses

    @staticmethod
    def create_list_additional_information(row):
        title_index, designation_index, passport_index, national_identity_index, address_index, other_info_index, physical_description_index = map(
            row.lower().find,
            ['title', 'designation', 'passport n', 'national identification', 'address', 'other info',
             'physical description'])
        extra_values = [("title", title_index), ("designation", designation_index),
                        ("passport", passport_index),
                        ("national identification", national_identity_index), ("address", address_index),
                        ("other information", other_info_index),
                        ("physical description", physical_description_index)]
        extra_values = [x for x in sorted(extra_values, key=itemgetter(1))]
        extra_values = [i for i in extra_values if i[1] > -1]

        return extra_values

    @staticmethod
    def extract_title(item, index, information, rows):
        try:
            titles = information[item[1]:rows[index + 1][1]].split(":")[1:]
        except IndexError:
            titles = information[item[1]:].split(":")[1:]

        # list is converted to string because it is spliting after :
        # string contains multiple :
        titles = " ".join(str(x) for x in titles)

        titles = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                      titles.strip()) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

        title = [{'title': title.split('.')[0]} for title in titles]
        title_info = json.dumps({'info': title})

        return title_info

    @staticmethod
    def extract_designation(item, index, information, rows):
        try:
            designations = information[item[1]:rows[index + 1][1]].split(":")[-1]
        except IndexError:
            designations = information[item[1]:].split(":")[-1]

        designations = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                            designations) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

        designation = [{'designation': designation} for designation in designations]

        designation_info = json.dumps({'info': designation})
        return designation_info

    @staticmethod
    def extract_passport(item, index, information, rows, document):
        re_passport1 = r'([A-Z]+[\d@]+[\w@]*|[\d@]+[A-Z]+[\w@]*)'
        re_passport2 = r'[0-9]{5,22}'
        try:
            passport_details = information[item[1]:rows[index + 1][1]]
        except IndexError:
            passport_details = information[item[1]:]

        passport_details = re.compile("(%s|%s)" % (re_passport1, re_passport2)).findall(
            passport_details)
        for number in passport_details:
            document.append({'type': "passport",
                             'id': number})
        return document

    @staticmethod
    def extract_identification(item, index, information, rows, document):
        re_identification = r'\d+(?:-\d+)+'
        re_identification1 = r'([A-Z]+[\d@]+[\w@]*|[\d@]+[A-Z]+[\w@]*)'
        re_identification2 = r'[0-9]{5,22}'
        try:
            id_details = information[item[1]:rows[index + 1][1]]
        except IndexError:
            id_details = information[item[1]:]

        id_details = re.compile(
            "(%s|%s|%s)" % (re_identification, re_identification1, re_identification2)).findall(
            id_details)
        for number in id_details:
            document.append({'type': "national identification no",
                             'id': number[0]})
        return document

    @staticmethod
    def extract_remarks(item, index, information, rows):
        try:
            remarks = information[item[1]:rows[index + 1][1]].split(":")[-1]
        except IndexError:
            remarks = information[item[1]:].split(":")[-1]
        return remarks

    @staticmethod
    def extract_dob(dob, pob):
        dob = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                   str(dob)) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

        pob = [x for x in re.split(r'((?:^\s*[a-zA-Z0-9]\))|(?:\s+[a-zA-Z0-9]\)))\s*',
                                   pob) if x and not re.match(r"\s*[a-zA-Z]\)", x)]

        dob_list = []

        if len(dob) > 0:
            for index, item in enumerate(dob):
                try:
                    dob_list.append({"dob": item, "pob": pob[index]})
                except IndexError:
                    dob_list.append({"dob": item, "pob": None})
        return dob

    @staticmethod
    def create_dataframe(response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name='Sheet1')

        return df
