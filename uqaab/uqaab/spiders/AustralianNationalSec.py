# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
from scrapy import Request
from scrapy.http import HtmlResponse
import re


class AustralianNatSecSpider(UqaabBaseSpider):

    name = 'australia_nat_sec_spider'
    start_urls =  ["https://www.nationalsecurity.gov.au/Listedterroristorganisations/Pages/default.aspx"]


    def structure_valid(self, response):
        data_rows = response.xpath('//*[@id="cbqwpctl00_m_g_d7dfc6dd_7fc1_4efe_8aa8_9418cde17530"]/ul/li')
        return len(data_rows) > 0

    def extact_data(self, response):


        for entity in response.xpath('//*[@id="cbqwpctl00_m_g_d7dfc6dd_7fc1_4efe_8aa8_9418cde17530"]/ul/li'):
            E = Entity()

            E["name"] = self.extract_name(entity)
            E["inclusion_date"] = self.extract_date(entity)
            E["category"] = 'Group'
            link = self.extract_link(entity)
            request =  Request(url=link, callback=self.extact)
            request.meta['E'] = E 
            yield request
        
    def extact(self, response):

        E = response.meta['E']
        aka = response.xpath('//*[@id="ctl00_PlaceHolderMain_ctl06_ctl01__ControlWrapper_RichHtmlField"]/p[contains(., "AKA: ")]')\
        .extract_first()
        also_known = response.xpath('//*[@id="ctl00_PlaceHolderMain_ctl06_ctl01__ControlWrapper_RichHtmlField"]/p[contains(., "Also known as: ")]')\
        .extract_first()                            

        try:
            if aka is None and also_known is None:
                E["aka"] = []

            elif aka is None:

                also_known = also_known.encode().decode("unicode-escape")
                also_resp = HtmlResponse(url="also", body=also_known, encoding="utf-8")
                also_known = also_resp.xpath('//strong//text()').extract_first()
                
                if also_known is None:
                    also_known = also_resp.xpath('//p//text()').extract_first()
                
                also_known = also_known.replace("Also known as: ", "")
                also_known = also_known.replace("(","")
                also_known = also_known.replace(")","")
            
                if bool(re.search(';', also_known)) is True:
                    also_known = also_known.split(";")
                else:
                    also_known = also_known.split(",")
                E["aka"] = also_known

            else:
                  
                aka = aka.encode().decode("unicode-escape")       
                also_resp = HtmlResponse(url="also", body=aka, encoding="utf-8")
                aka = also_resp.xpath('//strong//text()').extract_first()

                if aka is None:
                    aka = also_resp.xpath('//p//text()').extract_first()

                aka = aka.replace("AKA: ", "")
                aka = aka.replace("(","")
                aka = aka.replace(")","")
                
                if bool(re.search(';', aka)) is True:
                    aka = aka.split(";")
                else:
                    aka = aka.split(",")
                E["aka"] = aka

        except ValueError:
            return None


        remarks = response.xpath('//*[@id="ctl00_PlaceHolderMain_ctl06_ctl01__ControlWrapper_RichHtmlField"]/p[13]/text()')\
                    .extract_first()
        
        E["remarks"] = remarks

        return E
        

    @staticmethod
    def extract_name(entity):
            return entity.xpath('div/a/text()').extract_first().encode().decode("unicode-escape")
    
    @staticmethod
    def extract_date(entity):

        try:
            line = entity.xpath('div/div/text()').extract_first()
            line = line.replace("\xa0", " ")
            line = line.replace(".","")
            line = line.replace("-","")
            date = line.split()[-3:]
            check = bool(re.match(r'[a-zA-Z]+', ' '.join(date)))
            
            if check is True:
                date = line[58:74]
                date = date.split()
                date = date[0] + ' ' + date[1] + ' ' + date[2]
                date = datetime.strptime(date, "%d %B %Y")

            else: 
                date = date[0] + ' ' + date[1] + ' ' + date[2]
                date = datetime.strptime(date, "%d %B %Y")
            return date
        except ValueError:
            return None    

    @staticmethod
    def extract_link(entity):
        return entity.xpath('div/a/@href').extract_first()