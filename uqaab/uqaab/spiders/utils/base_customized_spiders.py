import os
import io
import traceback
import logging
import logging.handlers
import scrapy
from .emails import send_error_mail, send_warning_mail
import requests
import pandas as pd
from dateutil import parser
from bs4 import BeautifulSoup
from uqaab.database.databaseFactory import get_enabled_database
from uqaab.environment import config
from uqaab.scrapper_test import ScrapperTest
import sys
cdpath=os.path.join(config('PROJECT_PATH'), 'scheduling')
sys.path.append(cdpath)
from change_detection import get_sublist_ids_of_changed_lists

CATEGORY_OPTIONS = ['Individual', 'Group', 'Vessel', 'Country', 'Unknown']

class DataSourceModifiedException(Exception):
    pass

class UqaabBaseSpider(scrapy.Spider):
    countries_xlsx_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'country.xlsx')

    def __init__(self, list_id=None, name=None, **kwargs):
        if list_id is None:
            raise TypeError('List ID is not provided. Use command "scrapy crawl <spider-name> -a list_id=<list-id>" ')

        self.list_id = list_id
        self.sublist_id = None
        self.start_time = None
        self.setup_logging()
        self.df = pd.read_excel(self.countries_xlsx_path)
        super().__init__(name, **kwargs)

    def setup_logging(self):
        self.setup_root_logger()
        self.setup_core_logger()

    def setup_root_logger(self):
        self._create_folder_if_not_exists()
        filename = 'run.log'
        log_filename = os.path.join('logs', self.name, filename)
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        handler = logging.handlers.TimedRotatingFileHandler(log_filename, when='D', interval=1, backupCount=7)
        formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s: %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    def setup_core_logger(self):
        logger = logging.getLogger('scrapy.core.scraper')
        logger.setLevel(logging.INFO)

    def _create_folder_if_not_exists(self):
        folder_name = os.path.join('logs', self.name)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

    def _create_traceback_message(self):
        fp = io.StringIO()
        traceback.print_exc(file=fp)
        return fp.getvalue()

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def parse(self, response):
        try:
            if self.sublist_id is None:
                self.sublist_id = self._insert_new_sublist_version()
            if not self.structure_valid(response):
                raise DataSourceModifiedException('Could not scrape data using spider named `%s`' % self.name)
            for entity in self.extact_data(response):
                yield entity
        except:
            traceback = self._create_traceback_message()
            self.logger.error(traceback)
            if self.settings.getbool('MAIL_SEND_FAILURE_EMAIL'):
                self.error_email_wrapper(traceback)

    def get_country_code(self, country_string):
        if country_string is None or country_string.strip() == '':
            return None

        country_string = country_string.strip()
        country_rows = self.df.loc[self.df['countryName'] == country_string.title()]
        if not country_rows.empty:
            return country_rows.iloc[0]['countryCode']

        country_rows = self.df.loc[self.df['countryCode'] == country_string.upper()]
        if not country_rows.empty:
            return country_rows.iloc[0]['countryCode']

        country_rows = self.df.loc[self.df['isoAlpha3'] == country_string.upper()]
        if not country_rows.empty:
            return country_rows.iloc[0]['countryCode']

        return country_string

    def structure_valid(self, response):
        """
        Perform DOM validation and return True if DOM can be scraped else return False
        """
        raise NotImplementedError('{}.structure_valid not defined'.format(self.__class__.__name__))

    def extact_data(self, response):
        """
        Perform actual scraping here and yield entities
        """
        raise NotImplementedError('{}.extact_data not defined'.format(self.__class__.__name__))

    def error_handler(self, failure):
        self.error_email_wrapper(failure)

    def _get_program_name(self):
        db = get_enabled_database()
        program_name = db.get_program(self.list_id)
        return program_name

    def error_email_wrapper(self, error_message):
        program_name = self._get_program_name()
        send_error_mail(self, program_name, error_message)

    def warning_email_wrapper(self, subject, message):
        program_name = self._get_program_name()
        send_warning_mail(self, program_name, subject, message)

    def _insert_new_sublist_version(self):
        db = get_enabled_database()
        new_sublist = db.insert_new_sublist_record(self.list_id)
        db.close_connection()
        return new_sublist

    def run_tests(self):
        scrapy_test = ScrapperTest()
        scrapy_test.check_entities_validity(sublistid=self.sublist_id, program=self.name, started_on=self.start_time, list_id=self.list_id)

    def populate_name_and_initials(self):
        logging.info("Running populate name and db initials")
        db = get_enabled_database()
        db.populate_name_and_initials()


    def change_detection(self):
        get_sublist_ids_of_changed_lists(self.list_id)

class StaticDataSpider(UqaabBaseSpider):

    def parse(self, response):
        if self.static_data_already_exists():
            return self._copy_data()
        else:
            return super().parse(response)

    def static_data_already_exists(self):
        db = get_enabled_database()
        data_count = db.get_static_data_count(self.list_id)
        return data_count > 0

    def _copy_data(self):
        db = get_enabled_database()
        db.copy_data(self.list_id, 'tbl_staticData', 'tbl_entities')
