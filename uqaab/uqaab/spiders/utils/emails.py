from datetime import timezone
from scrapy.mail import MailSender
from scrapy.utils.project import get_project_settings


settings = get_project_settings()


def _send_email(subject, message):
    mailer = MailSender.from_settings(settings)
    mailer.send(to=settings.getlist('MAIL_TO'), subject=subject, body=message)


def _spider_start_time_with_tz(spider):
    spider_start_time = spider.crawler.stats.get_value('start_time')
    current_time = spider_start_time.replace(tzinfo=timezone.utc).astimezone(tz=None)
    return current_time

def send_list_error_mail(email_subject, base_message, errors):
    errors_message = ""
    if len(errors) > 0:
        errors_message = """Following errors were found regarding this list
        {errors}""".format(errors='\n\n'.join(errors))
    email_body = """
    {base_message}
    {error_details}
    """.format(base_message=base_message, error_details=errors_message)
    _send_email(email_subject, email_body)

def send_error_mail(spider, program_name, traceback):
    current_time = _spider_start_time_with_tz(spider)
    email_subject = "Error occurred while running a scraper."
    email_body = """
    Spider Failure Notification:

    File Name: {name}
    Program Name: {program_name}
    Start Time: {start_time}
    Error Info:
    -------------------------------------
    {traceback}

    """.format(name=spider.name,
               program_name=program_name,
               start_time=current_time.strftime('%d-%m-%Y %H:%M'),
               traceback=traceback)
    _send_email(email_subject, email_body)


def send_warning_mail(spider, program_name, subject, message):
    current_time = _spider_start_time_with_tz(spider)
    email_subject = "Warning: {}".format(subject)
    email_body = """
    File Name: {name}
    Program Name: {program_name}
    Start Time: {start_time}
    -------------------------------------
    {message}

    """.format(name=spider.name,
               program_name=program_name,
               start_time=current_time.strftime('%d-%m-%Y %H:%M'),
               message=message)
    _send_email(email_subject, email_body)