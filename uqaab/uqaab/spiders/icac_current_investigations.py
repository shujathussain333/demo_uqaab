#Author: Sharjeel Ali Shaukat

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import scrapy


class ICACCurrentInvestigations(UqaabBaseSpider):
    name = 'icac_current_investigations'
    start_urls = ['https://www.icac.nsw.gov.au/investigations/current-investigations']

    def structure_valid(self, response):
        names = response.xpath("//div[@class='article-widget']")
        return len(names) > 0

    def extact_data(self, response):
        data = response.css("div.article-widget a::attr(href)").extract()

        for row in data:
            yield scrapy.Request(url=row, callback=self.extract_link)


    def extract_link(self, response):
        name = response.xpath("//h1/text()").extract_first()
        remarks = response.xpath("//article//p/text()").extract()
        remarks = ''.join(remarks[1:])

        yield Entity({
            "category": "Unknown",
            "name": name,
            "remarks": remarks,
            "type": "SIP",
        })
