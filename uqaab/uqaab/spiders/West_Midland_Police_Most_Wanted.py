#Author: Daniyal Faquih
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class West_Midland_Police_Most_Wanted(UqaabBaseSpider):
    name = 'west_midland_police_most_wanted'
    start_urls = ['https://api.flickr.com/services/rest?per_page=1000&page=1&extras=can_addmeta%2Ccan_comment%2Ccan_download%2Ccan_share%2Ccontact%2Ccount_comments%2Ccount_faves%2Ccount_views%2Cdate_taken%2Cdate_upload%2Cdescription%2Cicon_urls_deep%2Cisfavorite%2Cispro%2Clicense%2Cmedia%2Cneeds_interstitial%2Cowner_name%2Cowner_datecreate%2Cpath_alias%2Crealname%2Crotation%2Csafety_level%2Csecret_k%2Csecret_h%2Curl_c%2Curl_f%2Curl_h%2Curl_k%2Curl_l%2Curl_m%2Curl_n%2Curl_o%2Curl_q%2Curl_s%2Curl_sq%2Curl_t%2Curl_z%2Cvisibility%2Cvisibility_source%2Co_dims%2Cpubliceditability&get_user_info=1&jump_to=&user_id=61718807%40N07&viewerNSID=&method=flickr.people.getPhotos&csrf=&api_key=96e1dccab4e47a7b19eb253db3a4f630&format=json&hermes=1&hermesClient=1&reqId=7d7f0cdb&nojsoncallback=1']

    def structure_valid(self, response):
        stringResponse = response.body
        stringResponse = stringResponse.decode("utf-8")
        jsonResponse=json.loads(stringResponse)
        actualData=jsonResponse["photos"]
        dataToScrap=actualData['photo']
        return len(dataToScrap) > 0

    def extact_data(self, response):
        stringResponse = response.body
        stringResponse = stringResponse.decode("utf-8")
        jsonResponse=json.loads(stringResponse)
        actualData=jsonResponse["photos"]
        dataToScrap=actualData['photo']
        name = ""
        remarks = ""
        imageSource = ""
        for data in dataToScrap:
            title = data['title']
            if("WANTED" in title):
                title = title.split("WANTED")
                name = title[-1]
                charToRemove = ":|?-"
                for char in charToRemove:
                    if(char in name):
                        name = name.replace(char,"")
                name = name.strip()
                description = data['description']['_content']
                description = description.replace("\n","")
                remarks = "Description: {}".format(description)
                imageSource = data['url_o']
                yield Entity({
                    "name": name,
                    "category": "Individual",
                    "type": "SAN",
                    "remarks": remarks,
                    "image":imageSource
                })