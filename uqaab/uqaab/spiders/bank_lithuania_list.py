# Author: Ilyas Yousuf

import bs4 as bs
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class BankLithuaniaWarningList(UqaabBaseSpider):
    name = "bank_lithuania_warning_list"
    allowed_domains = ['www.lb.lt']
    start_urls = [
        'https://www.lb.lt/en/news/bank-of-lithuania-warning-beware-risky-investments'
    ]
    
    def structure_valid(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        return len(soup.find('table').find_all("td")) > 0

    def extact_data(self, response):
        soup = bs.BeautifulSoup(response.body, "lxml")
        data = soup.find('table').find_all("td")
        for i in range(2, len(data), 2):
            yield Entity({
                'remarks' : 'Website: {0}'.format(data[i].find("p").text),
                'name' : data[i+1].find("p").text
            })