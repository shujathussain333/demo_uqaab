#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class Bangladesh_Securities_Exchange_Commission(UqaabBaseSpider):
    name = 'bangladesh_securities_exchange_commission'
    start_urls = ['http://www.sec.gov.bd/home/enforcementreport/2019']

    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="switchcontent col-md-12"]//table//tr[td]')
        return len(data_rows) > 0
    
    def extact_data(self, response):
        data_rows = response.xpath('//div[@class="switchcontent col-md-12"]//table//tr[td]')
        for data in data_rows:
            date = data.xpath('.//td[2]//text()').extract_first()
            date = dateutil.parser.parse(date)
            name = data.xpath('.//td[3]//a//p//text()').extract_first()
            pdfLink = data.xpath('.//td[4]/a/@href').extract_first()
            remarks = "PDF File Link: {}".format(pdfLink)
            if("Non-compliance securities law against " in name):
            	name = name.replace("Non-compliance securities law against ","")
            if("Order against " in name):
            	name = name.replace("Order against ","")
            yield Entity({
                "name": name,
                "inclusion_date": date,
                "category": "Group",
                "type": "SAN",
                "remarks": remarks
            })