##Author: Sharjeel Ali Shaukat
## Created on: 11-01-2019

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class HongKongICACMostWanted(UqaabBaseSpider):
    name = 'hong_kong_icac_most_wanted'
    start_urls = ['https://www.icac.org.hk/en/law/wanted/index.html']
    url = 'https://www.icac.org.hk'

    def structure_valid(self, response):
        links = response.xpath("//div[@id='mainWPList']//a/@href").extract()
        return len(links) > 0

    def extact_data(self, response):
        links = response.xpath("//div[@id='mainWPList']//a/@href").extract()

        for link in links:
            new_url = self.url + link
            callback = lambda response: self.extract_link(response)
            yield scrapy.Request(url=new_url, callback=callback)

    def extract_link(self, response):
        personal_details = response.css('tbody tr td').css('*::text').extract()
        name = personal_details[0]
        alias = personal_details[1]
        charge = "Charge: {}".format(personal_details[2])
        case_brief = "Case Brief: {}".format(response.xpath("//div[@class='caseBrief details']/text()").extract_first())
        other_data = response.xpath("//div[@class='wpParticular']//p/text()").extract()
        dobs = {'DOB': None, 'POB': None}
        document = []
        remarks = charge + ';' + case_brief
        image =self.url + response.xpath("//span[@class='imgwrap']//img/@src").extract_first()

        for data in other_data:
            if 'hkic' in data.lower():
                document.append({'type': "national identity no",'id': data.split(':')[-1].strip()})
            if 'passport' in data.lower():
                document.append({'type': "passport", 'id': data.split(':')[-1].strip()})
            if 'id' in data.lower():
                document.append({'type': "national identity no", 'id': data.split(':')[-1].strip()})
            if 'date of birth' in data.lower():
                dobs['DOB'] = data.split(':')[-1].strip()
            if 'place of birth' in data.lower():
                dobs['POB'] = data.split(':')[-1].strip()
            if 'occupation' in data.lower():
                designation = [{'designation':  data.split(':')[-1].strip()}]
                designation_info = json.dumps({'info': designation})
            if 'nationality' in data.lower():
                nationality = data.split(':')[-1].replace('/', '').strip()

        document_info = json.dumps({'document_info': document})
        dob_info = json.dumps({'info': [dobs]})
        alias_info = json.dumps({'info': None})

        if len(alias) > 1:
            aliases = alias.strip().split(";")
            alias = [{'aka': aka.strip()} for aka in aliases]
            alias_info = json.dumps({'info': alias})

        yield Entity({
            "category": "Individual",
            "name": name,
            'date_of_birth': dob_info,
            'nationality': nationality,
            "document": document_info,
            "remarks": remarks,
            "designation": designation_info,
            "type": "SIP",
            "aka": alias_info,
            "image":image
        })


