#Edited by: Daniyal Faquih

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS
import camelot
import json


class FIA_HTraffickers(UqaabBaseSpider):
    name = 'fia_htraffickers_spider'
    start_urls = ["http://www.fia.gov.pk/en/redbooktriff.pdf", ]
    split_words = ['S/O', 'D/O', 'W/O']

    def structure_valid(self, response):
        url = "http://www.fia.gov.pk/en/redbooktriff.pdf"
        bin = camelot.read_pdf(url, pages='5-17')
        return len(bin) > 0

    def extact_data(self, response):
        url = "http://www.fia.gov.pk/en/redbooktriff.pdf"
        bin = camelot.read_pdf(url, pages='5-17')
        for i in range(0, len(bin)):
            df = bin[i].df
            for row in df.itertuples():
                if row[2] != 'NAME, PARANTAGE AND ADDRESS':
                    if len(row[2]) > 0:
                        document = []
                        details = re.split(r'{}'.format('|'.join(self.split_words)), row[2])
                        name = details[0]
                        name = " ".join(re.findall("[a-zA-Z]+", name))
                        
                        if("NAME PARANTAGE AND ADDRESS" not in name):
                            alias_info = json.dumps({'info': None})
                            if '@' in name:
                                aka = name.split('@')[-1]
                                name = name.split('@')[0]
                                alias = [{'aka': aka}]
                                alias_info = json.dumps({'info': alias})
                            address = [details[-1].split('R/O')[-1].strip().replace('\n', '')]
                            cnic = row[3]
                            cnic = {'type': 'national identity', 'id': cnic}
                            passport = row[5]
                            passport = {'type': 'passport', 'id': passport}
                            remarks = "FATHER/HUSBAND: {}, FIR: {}".format(details[-1].split('R/O')[0], row[4])
                            document.append(cnic)
                            document.append(passport)
                            document_info = json.dumps({'document_info': document})
                            if(" s o" in name):
                                name = name.split(" s o")
                                name = name[0]
                            elif(" d o" in name):
                                name = name.split(" d o")
                                name = name[0]
                            elif(" r o" in name):
                                name = name.split(" r o")
                                name = name[0]
                            elif(" w o" in name):
                                name = name.split(" w o")
                                name = name[0]
                            
                            yield Entity({
                                'category': 'Individual',
                                'name': name.strip(),
                                'remarks': remarks,
                                'address': address,
                                'aka': alias_info,
                                'document': document_info,
                                'type': 'SIP',
                                'image': '/media/images/FIAMostWantedHTraffickers/name.png'
                            })