from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests
from bs4 import BeautifulSoup

class Norfolk_Scrapper(UqaabBaseSpider):
    name = 'norfolk_scrapper'
    start_urls = ['https://www.norfolk.police.uk/wanted-persons']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        url = 'https://www.norfolk.police.uk/wanted-persons'
        base_url = 'https://www.norfolk.police.uk'
        soup = BeautifulSoup(requests.get(url).text)
        list_ = soup.find('ul', {'class': 'listing listing-gallery'})
        urls = [item.find('a')['href'] for item in list_.find_all('li')]
        Name = [item.find('h3').text for item in list_.find_all('li')]
        DOInclusion = [item.find('time').text for item in list_.find_all('li')]
        remarks = []

        for url_ in urls:
            try:
                remarks.append(' '.join([p_.text for p_ in BeautifulSoup(requests.get(base_url + url_).text).find('article', {
                                             'class': 'article'}).find_all('p')]))
            except:
                remarks.append('')

        df = pd.DataFrame({'Name': Name, 'DOInclusion': DOInclusion, 'Remarks': remarks})

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'inclusion_date': row.DOInclusion,
                'category': 'Group',
                'type': "SIP"
            })