#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class GovCanadaVenezuleaSanctions(UqaabBaseSpider):

    name = 'canada_venezulea_sanctions'
    start_urls = ['https://www.canada.ca/en/global-affairs/news/2018/05/venezuela-sanctions.html',]

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='col-md-auto']/ul/li")
        return len(data_rows) > 0

    def extact_data(self,response):
        sanctioned_persons = response.xpath("//div[@class='col-md-auto']/ul/li")
        for person in sanctioned_persons:
            name = person.xpath("text()").extract_first()
            yield Entity({
                "category": "Individual",
                "name":name,
        		"type": "SAN"
        	})