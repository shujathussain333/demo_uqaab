#Author: Daniyal Fauqih
import scrapy
import io
import requests
import pandas as pd
import json
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import re
import numpy as np

class BISConsolidatedList(UqaabBaseSpider):

    name = "bislist_spider"
    start_urls = ['https://api.trade.gov',]

    @staticmethod
    def func(row):
        if(row.name == 'name'):
            row = row.where((pd.notnull(row)), None)
        if(row.name == 'start_date'):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "end_date"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "title"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "addresses"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "nationalities"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "places_of_birth"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "dates_of_birth"):
            row = row.where((pd.notnull(row)), None)
        if(row.name == "remarks"):
            row = row.where((pd.notnull(row)), None)           
        return row

    def structure_valid(self, response):
        return True

    @staticmethod
    def extract_dates(date, places):
        DatesDict = {}
        year_pattern = re.compile(r"\d{4}")
        date_matched = re.findall(year_pattern, date)
        
        if len(date_matched) > 0:
            date = date_matched[0]
        if places is np.nan or places == "nan":
            places = None
        if date is np.nan or date == "nan":
            date = None 
        if date or places:
            DatesDict["DOB"] =  date
            DatesDict["POB"] = places
            DOB_info.append(DatesDict)

    def extact_data(self, response):
        url = "https://api.trade.gov/consolidated_screening_list/search.csv?api_key=OHZYuksFHSFao8jDXTkfiypO"
        r = requests.get(url)
        df = pd.read_csv(io.BytesIO(r.content))
        dfObj = df.apply(self.func)
        global DOB_info

        for i in dfObj.index:
            name = dfObj['name'][i]            
            title_info = json.dumps({'info': None})
            title = {'title': dfObj['title'][i]}
            if title:
                title_info = json.dumps({'info': [title]})
            
            address = dfObj['addresses'][i]

            try:
                inclusion_date = dateutil.parser.parse(dfObj['start_date'][i])
            except (ValueError, TypeError):
                inclusion_date = None
            try:
                exclusion_date = dateutil.parser.parse(dfObj['end_date'][i])
            except (ValueError, TypeError):
                exclusion_date= None

            nationality = dfObj['nationalities'][i]

            remarks = dfObj['remarks'][i]
            remarks = "Remarks: {}".format(remarks)
            
            DOB_info = []
            places = dfObj['places_of_birth'][i]
            dates = dfObj['dates_of_birth'][i]
            if(dates is not None):
                dates = str(dates)
                dates = dates.replace('to', '-')
                dates = dates.replace('.', '')
                dates = dates.replace('or', '|')
                dates = dates.replace(" ", '')
                dates = dates.strip()
                dates = dates.split(";")
                for date in dates:
                    self.extract_dates(date, places)
            if len(DOB_info) > 0:
                DOB_complete = json.dumps({'info': DOB_info})
            else:
                DOB_complete = json.dumps({'info': None})
            yield Entity({
                "name": name,
                "remarks": remarks,
                "date_of_birth" : DOB_complete,
                "category": "Individual",
                "type": "SAN",
                "nationality":nationality,
                "inclusion_date" : inclusion_date,
                "exclusion_date" : exclusion_date,
                "address" : address,
                "title" : title_info
            })