from scrapy import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import dateutil.parser


base_url = "http://gaming.nv.gov"

class NevadaSpider(UqaabBaseSpider):
    name = "nevada_gaming_control_board"
    url = ["http://gaming.nv.gov"]

    start_urls = [
    'http://gaming.nv.gov/index.aspx?page=72',
    ]
    def structure_valid(self, response):
        return 100    

    def extact_data(self, response):

       persons = response.css('table td a::attr(href)').extract()
       for person in persons:
           link = person
           newurl = link
           if 'gaming' not in link:
               newurl = self.url[0] + '/' + link
           yield Request(url=newurl, callback=self.extract_person)

    def extract_person(self, response):

        name = response.xpath('//h1/text()').extract_first()
        aliases = response.xpath(
            "//*[contains(text(), 'Aliase(s)')]/following::td[1]/text()").extract()
        aliases = list(map(str.strip, aliases))
        aliases = [x for x in aliases if x]
        alias_info = json.dumps({'info': None})
        if len(aliases) > 0:
            aka = [{'aka': aka} for aka in aliases]
            alias_info = json.dumps({'info': aka})

        dob = response.xpath(
            "//*[contains(text(), 'Birth')]/following::td[1]/text()").extract_first()
        dobs = {'DOB': dob, 'POB': None}
        dob_info = json.dumps({'info': [dobs]})
        address = response.xpath(
            "//*[contains(text(), 'Address')]/following::td[1]/text()").extract()
        address = list(map(str.strip, address))
        address = [x for x in address if x]
        if len(address) == 0:
            address = None
        country = response.xpath(
            "//*[contains(text(), 'Country')]/following::td[1]/text()").extract_first()
        exclusion = response.xpath(
            "//*[contains(text(), 'Exclusion')]/following::td[1]/text()").extract_first()
        image = response.css("table img::attr(src)").extract_first()
        image = self.url[0] + '/' + image

        yield Entity({
            "name": name,
            "address": address,
            "category": 'Individual',
            "type": "SIP",
            "date_of_birth": dob_info,
            "exclusion_date": dateutil.parser.parse(exclusion),
            "country": country,
            "aka": alias_info,
            "image": image
        })


