# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request

class OCCAlerts(UqaabBaseSpider):
    name = 'occ_gov_alerts'
    start_urls =  ['https://www.occ.gov/news-issuances/alerts/2019/index-2019-alerts.html']
    baseurl = "https://www.occ.gov"
    
    def structure_valid(self, response):
        
        self.data_links = response.css("ul[class='navSubSubmenu'] > li > a::attr(href)").extract()[0:2]
        return len(self.data_links) > 0

    def extractfields(self, response):
        data_rows = response.css("table[class='autoList'] tr")[1:]
        
        if len(data_rows)>0:
            for row in data_rows:
                
                inclusiondate = row.css("td[class='tdate']::text").extract_first()
                name = row.css("td[class='tcontent'] a::text").extract_first()
                href= row.css("td[class='tcontent'] a::attr(href)").extract_first()

                remarksfield = ""
                if href:
                    remarksfield = "Profile: {0}{1}".format(self.baseurl, href)            
                
                yield Entity({
                    "name": name, 
                    "remarks": remarksfield,
                    "inclusion_date": self.string_to_date(inclusiondate),
                    "category": "Group",
                    "type": "SIP"
                })

    def extact_data(self, response):
        data_links = self.data_links
        
        for link in data_links:
            if link:
                self.url = "{0}{1}".format(self.baseurl, link)
                yield Request(url=self.url, callback=self.extractfields)
        
    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string.strip(), '%m/%d/%Y')
        except:
            return None