# Author: Amir Saleem
# Updated: Sharjeel Ali Shaukat

import json
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class USMarshalScraper (UqaabBaseSpider):
    name = 'us_marshals_spider'
    start_urls = ['https://www.usmarshals.gov/history/directors.htm']

    def structure_valid(self, response):
        data = response.xpath('//table[1]//tr')
        return len(data) > 0

    def extact_data(self, response):
        rows = response.css('table tr')
        for row in rows[4:]:
            columns = row.css('td')
            name = columns[0].css('*::text').extract_first().strip()
            title = columns[1].css('*::text').extract_first().strip().replace('\t', '').replace('\n', '')
            remarks = "Appointment Date: {}".format(columns[2].css('*::text').extract_first().strip())
            title = [{'title': title}]
            title_info = json.dumps({'info': title})

            if len(name) > 0:
                yield Entity({
                    'name': name,
                    'title': title_info,
                    'nationality': 'American',
                    'country': 'US',
                    'remarks': remarks,
                    'category': 'Individual',
                    'type': 'PEP'})
