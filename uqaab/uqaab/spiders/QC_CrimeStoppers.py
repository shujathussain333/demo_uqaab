#Author: Daniyal Faquih
import scrapy
from scrapy.selector import Selector
import json
from datetime import datetime, timedelta
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser

class QC_CrimeStoppers(UqaabBaseSpider):
    name = "qc_crime_stoppers"
    start_urls = ['http://www.qccrimestoppers.com/sitemenu.aspx?P=wanteds&ID=102']
    
    def structure_valid(self, response):
        data_divs = response.xpath('//div[@class="col-sm-7"]')
        return len(data_divs) > 0

    def extact_data(self, response):
        item = {}
        picSrcArr = []
        picSrcCounter = 0
        dataDiv = response.xpath('//div[@class="col-sm-7"]')
        pictureDiv = response.xpath('//div[@class="col-sm-5"]')

        for picture in pictureDiv:
            src = picture.select('.//div//img/@src').extract()
            picSrcArr.insert(picSrcCounter, src)
            picSrcCounter+=1
        
        picSrcCounter = -1        
        
        for data in dataDiv:
            picSrcCounter+=1
            warrant = data.xpath('.//table//tbody[1]//tr[8]/td/text()').extract_first()
            warrant_2 = data.xpath('.//table//tbody[1]//tr[9]/td/text()').extract_first() 
            if((warrant is not None) and (warrant_2 is not None)):
                remarks = "Age: {} ,Race: {} ,Violation: {} ,Warrant: {}, WantedBy: {}".format(data.xpath('.//table//tbody[1]//tr[4]//td[5]//text()').extract_first(),data.xpath('.//table//tbody[1]//tr[3]//td[5]//text()').extract_first(),data.xpath('.//table//tbody[1]//tr[1]//td[1]//div[1]//text()').extract_first(),data.xpath('.//table//tbody[1]//tr[8]/td/text()').extract_first(),data.xpath('.//table//tbody[1]//tr[9]/td/text()').extract_first())
            else:
                remarks = "Age: {} ,Race: {} ,Violation: {} ,Warrant: {}, WantedBy: {}".format(data.xpath('.//table//tbody[1]//tr[4]//td[5]//text()').extract_first(),data.xpath('.//table//tbody[1]//tr[3]//td[5]//text()').extract_first(),data.xpath('.//table//tbody[1]//tr[1]//td[1]//div[1]//text()').extract_first(),"None",data.xpath('.//table//tbody[1]//tr[8]/td/text()').extract_first())     
            dob = data.xpath('.//table//tbody[1]//tr[4]//td[2]//text()').extract_first()
            dob = list(filter(lambda x: 'DOB' in x, dob))
            dob_info = json.dumps({'info': None})
            if len(dob) > 0:
                dob = dob[-1]
                dob = dob.strip().split(' ')[-1]
                dob = [{'DOB': dob, 'POB': None}]
                dob_info = json.dumps({'info': dob})
            doi = data.xpath('.//table//tbody[1]//tr[1]//td[1]//div[2]//text()').extract_first()
            try:
                inclusion_date = dateutil.parser.parse(doi)
            except:
                inclusion_date = None
            yield Entity({
                "name": data.xpath('.//table//tbody[1]//tr[2]//td[2]//text()').extract_first(),
                "category": "Individual",
                "type": "SAN",
                "date_of_birth": dob_info,
                "gender": data.xpath('.//table//tbody[1]//tr[3]//td[2]//text()').extract_first(),
                "image": picSrcArr[picSrcCounter],
                "inclusion_date" : inclusion_date,
                "remarks": remarks
            })