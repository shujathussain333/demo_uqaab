#Author: Daniyal Faquih

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateutil.parser
import json
import scrapy

class USA_NYSE_Disciplinary_Actions(UqaabBaseSpider):
    name = 'usa_nyse_disciplinary_actions'
    start_urls = ['https://www.nyse.com/api/regulatory/disciplinary-actions/filter?market=&year=&sortType=&sortOrder=up&filterToken=&max=10&offset=']
    i = 0
    
    custom_settings = {
        'DOWNLOAD_DELAY' : 0.25
    }

    def structure_valid(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        data_rows=Jresponse["results"]
        return len(data_rows) > 0

    def extract_link(self, response):
        self.i+=10
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        Jresponse=json.loads(jsonresponse)
        actual_data=Jresponse["results"]
        for data in actual_data:
            date = data['decisionAcceptanceDate']
            date = dateutil.parser.parse(date)
            actionType = data['actionType']
            market = data['market']
            number = data['number']
            document = []
            document.append({'type': "number",'id': number})
            if document:
                document_info = json.dumps({'document_info': document})
            name = data['name']
            pdfFileName = "https://www.nyse.com" + data['fileName']
            remarks = "Market: {} ,Action Type: {} ,Report url: {}".format(market,actionType,pdfFileName)
            yield Entity({
            "name": name,
            "inclusion_date": date,
            "category": "Group",
            "type": "SAN",
            "remarks": remarks,
            "document": document_info
            })

    def extact_data(self, response):
        jsonresponse=response.body
        jsonresponse=jsonresponse.decode("utf-8")
        jsonresponse=json.loads(jsonresponse)
        max_id = jsonresponse["totalCount"]
        while self.i <= max_id:
            yield scrapy.Request('https://www.nyse.com/api/regulatory/disciplinary-actions/filter?market=&year=&sortType=&sortOrder=up&filterToken=&max=10&offset=%d' % self.i, callback = self.extract_link)