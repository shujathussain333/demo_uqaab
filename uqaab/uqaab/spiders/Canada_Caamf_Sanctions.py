#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from googletrans import Translator

class Canada_Caamf_Sanctions(UqaabBaseSpider):
    name = "canada_caamf_sanctions"
    start_urls = ['https://www.amf-france.org/Sanctions-et-transactions/Decisions-de-la-commission/Chronologique/Liste-Chronologique?year=2019&docType=sanction']
    
    def structure_valid(self, response):
        data_rows = response.xpath('//div[@class="liste_item"]//div[@class="bloc_unit"]')
        return len(data_rows) > 0

    def extact_data(self, response):
        translator = Translator()
        data_rows = response.xpath('//div[@class="liste_item"]//div[@class="bloc_unit"]')
        for data in data_rows:
            rawData = data.xpath('.//h3[@id="titre_contenu_list"]//span[@class="liste"]//text()').extract_first()
            rawData = translator.translate(rawData)
            rawData = rawData.text
            staticText_1 = ""
            staticText_2 = "respect to"
            staticText_3 = "respect of"
            name = ""
            if("regard to" in rawData):
                name = rawData.split("regard to")
            elif("respect to" in rawData):
                name = rawData.split("respect to")
            elif("respect of" in rawData):
                name = rawData.split("respect of")
            name = name[1].strip()
            yield Entity({
                "category": "Group",
                "name": name,
                "type": "SAN",
            })

        nextPageUrl = response.xpath('//div[@class="ctn_navarticle"]//span[@class="suivant"]/a/@href').extract_first()
        if(nextPageUrl is not None):
            yield scrapy.Request(nextPageUrl, callback=self.extact_data)