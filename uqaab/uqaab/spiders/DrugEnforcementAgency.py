#Author: Daniyal Faquih

import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
import json
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider

class DrugEnforcementAgencySpider(UqaabBaseSpider):
    name = "drug_enforcement_agency_list"
    start_urls = ['https://www.dea.gov/fugitives/all']
    i = 0
    
    def structure_valid(self, response):
        dataDivs = response.xpath("//div[@class='teaser teaser--fugitive']")
        return len(dataDivs) > 0

    def extact_data(self, response):
        #print("____________ : "+str(self.i)+" : __________\n")
        self.i+=1
        baseUrl = "https://www.dea.gov/"
        dataDivs = response.xpath("//div[@class='teaser teaser--fugitive']")
        for data in dataDivs:
            nextPageUrl = data.xpath('.//a//@href').extract_first()
            nextPageUrl = baseUrl+nextPageUrl
            if nextPageUrl is not None:
                yield scrapy.Request(nextPageUrl, callback=self.extract_link)
        
        baseUrlForNextPage = "https://www.dea.gov/fugitives/all"
        relativeNextPageUrl = response.xpath('//li[@class="pager__item pager__item--next"]/a/@href').extract_first()
        if(relativeNextPageUrl):
            relativeNextPageUrl =  baseUrlForNextPage + relativeNextPageUrl
            yield scrapy.Request(relativeNextPageUrl, callback=self.extact_data)
        
    @staticmethod
    def checkNone(str):
        if(str):
            return str.strip()
        return str

    def extract_link(self, response):
        name = response.xpath('//div[@class="content--two-column--content"]//h1/text()').extract_first()
        aka = response.xpath('//div[@class="content--two-column--content"]//div[@class="field field--aka"]//text()').extract()
        if(aka):
            aka = aka[-1].strip()
        
        akaArr = {'aka': aka}
       

        alias_info = json.dumps({'info': None})
        if aka:
            alias_info = json.dumps({'info': [akaArr]})    

        wantedFor = response.xpath('//div[@class="content--two-column--content"]//div[@class="field field--violations"]/div//text()').extract_first()
        wantedFor = self.checkNone(wantedFor)

        tableData = response.xpath('//div[@class="content--two-column--content"]//tbody')
        
        race = tableData.xpath('//tr[1]/td[2]/text()').extract_first()
        race = self.checkNone(race)
        
        sex = tableData.xpath('//tr[2]/td[2]/text()').extract_first()
        sex = self.checkNone(sex)

        height = tableData.xpath('//tr[3]/td[2]/text()').extract_first()
        height = self.checkNone(height)        

        weight = tableData.xpath('//tr[4]/td[2]/text()').extract_first()
        weight = self.checkNone(weight)

        hairColor = tableData.xpath('//tr[5]/td[2]/text()').extract_first()
        hairColor = self.checkNone(hairColor)

        eyeColor = tableData.xpath('//tr[6]/td[2]/text()').extract_first()
        eyeColor = self.checkNone(eyeColor)

        yearOfBirth = tableData.xpath('//tr[7]/td[2]/text()').extract_first()
        yearOfBirth = self.checkNone(yearOfBirth)
    
        dobs = {'DOB':yearOfBirth, 'POB':None}
        if dobs:
            dob_info = json.dumps({'info': [dobs]})
        else:
            dob_info = json.dumps({'info': None})
        
        addressArr = []   
        lastKnownAddress = tableData.xpath('//tr[8]/td[2]/text()').extract_first()
        lastKnownAddress = self.checkNone(lastKnownAddress)
        if lastKnownAddress:
            addressArr.append(lastKnownAddress)         

        ncic = tableData.xpath('//tr[9]/td[2]/text()').extract_first()
        ncic = self.checkNone(ncic)
        
        document = []
        document_info = json.dumps({'document_info': None})
        document.append({'type': "Ncic Number", 'id': ncic})
        document_info = json.dumps({'document_info': document})

        jurisdiction = tableData.xpath('//tr[10]/td[2]/text()').extract_first()
        jurisdiction = self.checkNone(jurisdiction)

        remarks = "Wanted for: {} ,Race: {} ,Height: {} ,Weight: {} ,Hair Color: {} ,Eye Color: {}".format(wantedFor, race, height, weight, hairColor, eyeColor)
       
        notes = tableData.xpath('//tr[11]/td[2]/text()').extract_first()
        notes = self.checkNone(notes)
        imageSource = response.xpath('//div[@class="container"]//div[@class="proportions"]//img/@src').extract_first()
        imageSource = "https://www.dea.gov" + imageSource
        
        city = response.xpath('//div[@class="field field_division"]//text()').extract_first()
        city = self.checkNone(city)
        #print("___________________________________")
        #print(name)
        #print(city)

        yield Entity({
            "name": name,
            "aka": alias_info,
            "category": "Individual",
            "type": "SAN",
            "date_of_birth": dob_info,
            "gender": sex,
            "city": city,
            "remarks": remarks,
            "address": addressArr,
            "country": jurisdiction,
            "image": imageSource,
            "document": document_info
        })