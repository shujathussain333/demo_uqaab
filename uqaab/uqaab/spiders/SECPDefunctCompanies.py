# Author: Sharjeel Ali

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import requests
from io import BytesIO
import pandas as pd
import numpy as np
import tabula

class SECPDefunctCompanies(UqaabBaseSpider):
    name = 'secp_defunct_companies'
    start_urls = ['https://www.secp.gov.pk']

    def structure_valid(self, response):
        data = requests.post(
            "https://www.secp.gov.pk/document/list-of-defunct-companies-struck-off-by-the-registrar-from-january-2013/?wpdmdl=30378")

        return data.status_code == 200

    def extact_data(self, response):
        data = requests.post(
            "https://www.secp.gov.pk/document/list-of-defunct-companies-struck-off-by-the-registrar-from-january-2013/?wpdmdl=30378")
        with BytesIO(data.content) as resp_file:
            secp_defunct = tabula.read_pdf(resp_file, encoding="latin1", lattice=True, pages='all')

            for index, row in secp_defunct.iterrows():
                name = row["Name of Company"]
                city = row['Company\rRegistration\rOffice']

                yield Entity({
                    'category': 'Group',
                    'type': "SAN",
                    'name': name,
                    'city': city,
                })
