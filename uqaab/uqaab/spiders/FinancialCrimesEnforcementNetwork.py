import re
import json
import dateutil.parser
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider


class FinCENSpider(UqaabBaseSpider):
    name = "financial_crimes_enforcement_network"
    allowed_domains = ["fincen.gov"]
    start_urls = [
        'https://www.fincen.gov/enforcement-actions-failure-register-money-services-business'
    ]
    aka_words = ['d/b/a', 'a/k/a', 'dba']

    def structure_valid(self, response):
        return response.body

    def extact_data(self, response):
        rows = response.css('table tr')
        data_rows = rows[1:]
        for row in data_rows:
            columns = row.css('td')
            name = columns[0].css('a::text').extract_first()
            if 'and' in name:
                name = name.split(' and ')
                name = name[0]
            alias = None
            if any(word in name.lower() for word in self.aka_words):
                _list = re.split(r'{}'.format('|'.join(self.aka_words)), name)
                alias = _list[-1]
                name = _list[0]
                alias = [{'aka': alias}]

            if alias:
                alias_info = json.dumps({'info': alias})
            else:
                alias_info = json.dumps({'info': None})

            inclusion_date = columns[1].css('*::text').extract_first()
            inclusion_date = dateutil.parser.parse(inclusion_date)
            matter_no = columns[2].css('*::text').extract_first()
            document = []
            document.append({'type': "matter no",
                             'id': matter_no})
            if document:
                document_info = json.dumps({'document_info': document})

            industry_type = "Industry Type: {}".format(columns[3].css('*::text').extract_first())
            detail_url = "Detail Document URI: {}".format(columns[0].css(
                'a::attr(href)').extract_first()
                                                         )
            remarks = '{} ; {}'.format(industry_type, detail_url)

            yield Entity({
                "category": 'Group',
                "name": name,
                "remarks": remarks,
                "type": "SIP",
                "aka": alias_info,
                "document": document_info,
                "inclusion_date": inclusion_date
            })
