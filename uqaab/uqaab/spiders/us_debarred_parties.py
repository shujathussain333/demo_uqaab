import io
import json
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import numpy as np

class USDebarredParties(UqaabBaseSpider):
    name = "us_debarred_parties"

    start_urls = [
        'https://www.pmddtc.state.gov/sys_attachment.do?sys_id=83f75ea0db843b805c3070808c9619ef',
    ]

    def structure_valid(self, response):
        format = ['Party Name', 'Date Of Birth', 'Federal Register Notice', 'Notice Date',
                  'Corrected Notice', 'Corrected Notice Date']

        df = self.create_dataframe(response)
        columns = df.columns
        return set(format) == set(columns)

    def extact_data(self, response):

        df = self.create_dataframe(response)
        for index, row in df.iterrows():
            name, aliases = self.extract_name_and_aliases(row['Party Name'])

            yield Entity({
                'name': name,
                'aka': aliases,
                'date_of_birth': self.extract_and_jsonify_dob(row['Date Of Birth']),
                'inclusion_date': row['Notice Date'],
                'type':'SIP',
                'category':'Group'
            })

    def extract_name_and_aliases(self, name_column):
        name = name_column
        aliases = []
        if '(a.k.a.' in name:
            name, aliases = self.separate_name_and_alias(name)
            aliases = aliases.split(';')

        return name, aliases

    @staticmethod
    def separate_name_and_alias(name):
        aka = name[name.index('(a.k.a. '):]
        name = name.replace(aka, '').strip()
        aka = aka.replace('(a.k.a. ', '').replace(')', '').strip()
        return name, aka

    def extract_and_jsonify_dob(self, dob_column):
        date_string = dob_column
        if dob_column is np.nan:
            dobs = json.dumps({'info': None})
        else:
            dobs = json.dumps({'info': [{'DOB': date_string, 'POB': None}]})
        return dobs

    @staticmethod
    def create_dataframe(response):
        with io.BytesIO(response.body) as resp_file:
            df = pd.io.excel.read_excel(resp_file, sheet_name=0)
        return df
