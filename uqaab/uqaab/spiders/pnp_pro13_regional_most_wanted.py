# -*- coding: utf-8 -*-
import scrapy
import tabula
import io
import json
import numpy as np
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from uqaab.spiders.utils.base_customized_spiders import CATEGORY_OPTIONS


class PNPPro13RegionalMW(UqaabBaseSpider):
    name = 'pnp_pro_13_regional_mw'
    start_urls = ["http://pro13.pnp.gov.ph/wp-content/uploads/2018/05/MOST-WANTED-PERSONS-REGIONAL-LEVEL.pdf", ]

    def structure_valid(self, response):
        return True

    def extact_data(self, response):

        url = "http://pro13.pnp.gov.ph/wp-content/uploads/2018/05/MOST-WANTED-PERSONS-REGIONAL-LEVEL.pdf"
        kenya = tabula.read_pdf(url, encoding="latin1", lattice=True, multiple_tables=True, pages="all",
                                spreadsheet=True, )

        num_cols = len(kenya[0].columns)

        total_len = len(kenya)
        cols = []


        for i in range(num_cols):
            cols.append(kenya[0][i][0])

        df = pd.DataFrame()

        for page in range(0, total_len):
            df = pd.concat([df, kenya[page]])

        cols = [col.replace("\r", " ") for col in cols]

        df.columns = cols

        df = df.replace("\r", " ", regex=True)

        df = df.iloc[1:]

        for index, row in df.iterrows():
            name,alias_info = self.extract_name_aliases(row["Name/Alia s"].encode('ascii', 'ignore'))
            addresses =  self.extract_addresses(row["Last known address"])
            designation =  self.extract_designation(row["Position"])
            offense = row["Nature of offense"]
            price = row["Head Price/ Reward"]
            case_number = row["Crimin al Case Numbe r"]
            remarks=self.extract_remarks(offense,price,case_number)
            yield Entity({
                'name': name,
                'address': addresses,
                'designation': designation,
                'country': "Philippines",
                'remarks': remarks,
                'category': 'Individual',
                "type": "SAN",
            })

    @staticmethod
    def extract_name_aliases(name):
        name = name.decode("utf-8")
        alias_info = json.dumps({'info': None})
        aliases = []
        aka = None
        if '@' in name:
            name_aliases = name.split('@')
            aka = name_aliases[1]
            name = name_aliases[0]

        elif 'AKA' in name:
            name_aliases = name.split('AKA')
            aka = name_aliases[1]
            name = name_aliases[0]

        if aka:
            aliases = [{'aka': aka}]

        if aliases:
            alias_info = json.dumps({'info': aliases})

        return name, alias_info

    @staticmethod
    def extract_addresses(addresses):
        if addresses is np.nan:
            addresses = []
        else:
            addresses = [addresses]
        return addresses

    @staticmethod
    def extract_designation(designation):
        if designation is np.nan:
            designation_info = json.dumps({'info': None})
        else:
            designation = [{'designation': designation}]
            designation_info = json.dumps({'info': designation})
        return designation_info

    @staticmethod
    def extract_remarks(offense,price,case_number):
        offense_string = ''
        price_string = ''
        case_number_string = ''

        if offense is not np.nan:
            offense_string = "Offense: {}".format(offense)
        if price is not np.nan:
            price_string = "Price: {}".format(price)
        if case_number is not np.nan:
            case_number_string = "Case number: {}".format(case_number)

        remarks = case_number_string + ' ' + offense_string + ' ' + price_string

        return remarks.split()