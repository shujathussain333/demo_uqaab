import os
import io
import scrapy
from uqaab.environment import config
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import pandas as pd
from uqaab.database.databaseFactory import get_enabled_database

class BahamasLeaks(StaticDataSpider):
    name = 'bahamas_leaks'

    def start_requests(self):
        file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'Bahamas.csv')
        url = "file://{path_to_file}".format(path_to_file=file_path)
        yield scrapy.Request(url, callback=self.parse, errback=self.error_handler)

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        db = get_enabled_database()
        program_name = db.get_program(self.list_id).split('/')[1].strip()

        with io.BytesIO(response.body) as resp_file:
            df = pd.read_csv(resp_file)

        df.drop_duplicates(subset=['Name'], inplace=True)
        df = df[df['Nationality'] == program_name]

        for index, entry in df.iterrows():
            yield Entity({
                'name': self.clean_string(entry['Name']),
                'category': entry['Category'],
                'address': self.clean_string(entry['Address']),
                'nationality': self.clean_string(entry['Nationality']),
                'organization': self.clean_string(entry['Organization']),
            })

    @staticmethod
    def clean_string(name_str):
        if name_str and not pd.isnull(name_str):
            try:
                return name_str.strip()
            except AttributeError:
                return None
        return None