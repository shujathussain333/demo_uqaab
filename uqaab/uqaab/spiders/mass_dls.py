from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import pandas as pd
import requests

class MASS_DLS_Enforcement_Actions(UqaabBaseSpider):
    name = 'dls_enforcement_actions'
    start_urls = ['https://www.mass.gov/service-details/dls-enforcement-actions']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        df = pd.read_html(requests.get('https://www.mass.gov/service-details/dls-enforcement-actions').text)[0]
        df.dropna(axis='columns')
        df['Name'] = df[4]
        df['DOInclusion'] = df[2]
        df['Category'] = df[0]
        df['Nationality'] = df[5]
        df['Remarks'] = 'Licence Type : ' + df[1] + ', Docket Number : ' + df[3]

        for index, row in df.iterrows():
            yield Entity({
                'name': row.Name,
                'remarks': row.Remarks,
                'nationality': row.Nationality,
                'inclusion_date':row.DOInclusion,
                'category': 'Individual',
                'type': "SIP"
            })

