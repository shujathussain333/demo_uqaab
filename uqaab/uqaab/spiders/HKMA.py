# Author = Barkat Khan

from datetime import datetime
import io
import pandas as pd
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider



class HKMA(UqaabBaseSpider):
    
    name = 'hkma_restrictedlicensedbank'
    start_urls = ["https://www.hkma.gov.hk/media/eng/doc/key-functions/banking-stability/banking-policy-and-supervision/list_of_rlb.xls", ]

    baseurl = "https://www.hkma.gov.hk"

    def structure_valid(self, response):

        with io.BytesIO(response.body) as resp_file:
            self.df = pd.io.excel.read_excel(resp_file, sheet_name='RLBs')
        
        return len(self.df) > 0 
        
    
    def extact_data(self, response):

        df = self.df
        
        last_updated = df.loc[0, 'Restricted Licence Banks'].replace("At ","")
        inclusion_date = self.string_to_date(last_updated)
        
        catch_params = ["Incorporated in Hong Kong", "Incorporated outside Hong Kong"]
        part1_start = df.index[df['Restricted Licence Banks'].str.strip() == catch_params[0]][0]
        part2_start = df.index[df['Restricted Licence Banks'].str.strip() == catch_params[1]][0]
        
        ending_indexes = df.index[df['Restricted Licence Banks'].str.startswith("Change")]
        part1_end = ending_indexes[0]
        part2_end = ending_indexes[1]

        in_hk_list = list(df.loc[part1_start:part1_end, 'Restricted Licence Banks'])[1:-1]
        out_hk_list = list(df.loc[part2_start:part2_end, 'Restricted Licence Banks'])[1:-1]
        
        hk_dict = dict()      

        for name in in_hk_list:
            hk_dict[name] = catch_params[0]

        for name in out_hk_list:
            hk_dict[name] = catch_params[1]

        for key,val in hk_dict.items():
            yield Entity({
                "name": key, 
                "remarks": "location: {0}".format(val),
                "inclusion_date": inclusion_date, 
                "category": "Group",
                "type": "SIP"
            })

    @staticmethod
    def string_to_date(date_string):
        try:
            if date_string:
                return datetime.strptime(date_string, '%d %B %Y')
        except:
            return None