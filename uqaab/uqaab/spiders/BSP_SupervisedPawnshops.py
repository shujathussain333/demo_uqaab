#Author = Barkat Khan

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json 
import tabula


class  BSP_SupervisedPawnshops(UqaabBaseSpider):

    name = 'bsp'

    start_urls = [ 
        "http://www.bsp.gov.ph/banking/pawndir.pdf"
    ]
    
    def structure_valid(self, response):
        url = response.request.url
        data_rows = tabula.read_pdf(url)
        return len(data_rows) > 0
   
    def extact_data(self, response):
        url = response.request.url
        df = tabula.read_pdf(url, encoding="utf-8", lattice=True, pages= "all", pandas_options={"skiprows":1})
        
        df.loc[-1] = df.columns
        df=df.replace("\r", " ", regex=True)
        cols = ["INSTITUTION_NAME", "BRANCH_NAME", "REGISTRATION_NO" ,"OFFICE_TYPE","COROLLARY_BUSINESS", "ADDRESS", "TOWN", "PROVINCE", "REGION", "CONTACT_PERSON","CONTACT_NUMBER"]
        df.columns = cols
        
        E = Entity()
        E['category'] = "Group"
        E['type'] = "SAN"
        
        for row, col in df.iterrows():
            remarks = []
            
            E['name']=self.extract_values(df.loc[row, "INSTITUTION_NAME"])
            E['address'] = self.extract_values(df.loc[row, "ADDRESS"])
            E['city'] = self.extract_values(df.loc[row, "TOWN"]) + " ," + self.extract_values(df.loc[row,"PROVINCE"]) + " ," + self.extract_values(df.loc[row,"REGION"])

            remarks.append('BRANCH NAME: {0}'.format(self.extract_values(df.loc[row, "BRANCH_NAME"])))
            remarks.append('REGISTRATION NO: {0}'.format(self.extract_values(df.loc[row, "REGISTRATION_NO"])))
            remarks.append('OFFICE TYPE: {0}'.format(self.extract_values(df.loc[row, "OFFICE_TYPE"])))
            remarks.append('COROLLARY BUSINESS: {0}'.format(self.extract_values(df.loc[row, "COROLLARY_BUSINESS"])))
            remarks.append("CONTACT PERSON: {0}".format(self.extract_values(df.loc[row, "CONTACT_PERSON"])))
            remarks.append("CONTACT NUMBER: {0}".format(self.extract_values(df.loc[row, "CONTACT_NUMBER"])))
            
            E['remarks'] =  ', '.join(remarks)
            yield E 


    @staticmethod
    def extract_values(value):
        try:
            if type(value) != str or value.startswith("Unnamed"):
                return 'N/A'
            else:
                return value
        except Exception as err:
            return 'N/A'
