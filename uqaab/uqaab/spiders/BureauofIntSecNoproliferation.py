# Author: Sharjeel Ali

import re
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json
import tabula
import pandas as pd
import numpy as np
from uqaab.database.databaseFactory import get_enabled_database
import dateutil.parser


class BureauofIntSecNonproliferation(UqaabBaseSpider):
    name = 'bureau_of_int_sec_non_proliferation'
    start_urls = ['https://www.state.gov/documents/organization/284359.pdf']

    def structure_valid(self, response):
        url = 'https://www.state.gov/documents/organization/284359.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)
        return total_len > 0

    def extact_data(self, response):
        db = get_enabled_database()
        watchlist_program = db.get_program(self.list_id).split('/')
        program_name = watchlist_program[1].lower()

        url = 'https://www.state.gov/documents/organization/284359.pdf'

        bin = tabula.read_pdf(url, encoding="utf-8", lattice=True, guess=False, pages='all', multiple_tables=True,
                              pandas_option={"headers": None})
        total_len = len(bin)
        cols = ["Type", "Entity", "Locationofentity", "DateImposed", "Dateofexpiration", "FederalRegisternotice", "7"]

        df = pd.DataFrame()
        for page in range(0, total_len):
            df = pd.concat([df, bin[page]])

        cols = [col.replace("\r", " ") for col in cols]

        df.columns = cols
        df = df[df['Entity'] != 'Entity']
        df = df.replace(np.nan, '', regex=True)
        df = df.replace("\r", ' ', regex=True)
        df = df[df['Entity'] != '']
        df = df[df['Type'] == program_name]

        for index, row in df.iterrows():
            name, aliases = self.extract_name_and_alises(row["Entity"])
            country = row["Locationofentity"]
            date_inclusion = self.extract_date(row["DateImposed"])
            date_exclusion = self.extract_date(row["Dateofexpiration"])
            remarks = row["FederalRegisternotice"]

            yield Entity({
                'category': "Group",
                'name': name.strip(),
                'remarks': remarks,
                'inclusion_date': date_inclusion,
                'exclusion_date': date_exclusion,
                'country': country,
                'aka': aliases,
                'type': 'SAN'
            })

    def extract_date(self, date):
        try:
            date_reg_exp2 = re.compile(
                r'(\d{1,2}(/|-|\.)\w{3}(/|-|\.)\d{1,4})|([a-zA-Z]{3}\s\d{1,2}(,|-|\.|,)?\s\d{4})|(\d{1,2}(/|-|\.)\d{1,2}(/|-|\.)\d+)')
            dates = [x.group() for x in
                     date_reg_exp2.finditer(date)]
            date = dateutil.parser.parse(dates[0])
        except:
            date = None
        return date

    def extract_name_and_alises(self, name):
        details = name.split('[')
        name = details[0]
        try:
            aka = details[1].split(':')[-1]
            aliases = aka.split(';')
            alias = [{'aka': re.sub('\s+', ' ', alias.replace('aka', ''))} for alias in aliases]
            alias_info = json.dumps({'info': alias})
        except (IndexError, TypeError):
            alias_info = json.dumps({'info': None})
        return name, alias_info
