import scrapy
import traceback
from scrapy.http import Request
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import dateparser

class FourthcodeSpider(UqaabBaseSpider):
    name = 'unauthorized_websites'
    allowed_domains = ['www.sc.com.my']
    start_urls = ['https://www.sc.com.my/list-of-unauthorised-websites-investment-products-companies-individuals/']

    def structure_valid(self, response):
        return 100

    def extact_data(self, response):
        table = response.xpath('//*[@class="tab_format"]//tbody')
        rows = table.xpath('//tr')

        for i in range(0, len(rows)):
          check = rows[i].xpath("@class").extract()
          check2 = rows[i].xpath('td//text()').extract()
          row = rows[i].xpath('td//text()').extract()

          if (len(row)>0):
              check3 = rows[i].xpath('td//text()')[0].extract()[0].isdigit()

          else:
              check3 = False

          if check3:
              Name = rows[i].xpath('td//text()')[1].extract()
              Address = rows[i].xpath('td//text()')[2].extract()
              DO_Inclusion = rows[i].xpath('td//text()')[4].extract()

              yield {
                  "name" : Name,
                  "address" : Address,
                  "inclusion_date" : DO_Inclusion
                     }
