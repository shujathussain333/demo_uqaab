#Author: Daniyal Faquih

import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import json

class FBI_Violent_Criminal_Apprehension_Program(UqaabBaseSpider):
    name = "fbi_violent_criminal_apprehension_program"
    start_urls = ['https://www.fbi.gov/wanted/vicap']
    
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
    }
    
    def structure_valid(self, response):
        data_divs = response.xpath('//div[@class="query-results pat-pager"]//ul//li')
        return len(data_divs) > 0

    def extact_data(self, response):
        data_divs = response.xpath('//div[@class="query-results pat-pager"]//ul//li')
        for data in data_divs:
            next_page = data.xpath('.//a//@href').extract_first()
            if next_page is not None:
                yield scrapy.Request(next_page, callback=self.extract_link)

        nextPageUrl = response.xpath('//p[@class="read-more text-center bottom-total visualClear"]/button/@href').extract_first()
        if nextPageUrl is not None:
            yield scrapy.Request(nextPageUrl, callback=self.extact_data)
    
    def extract_link(self, response):
        name = response.xpath('//h1[@class="documentFirstHeading"]//text()').extract_first()
        imageUrl = response.xpath('//div[@class="col-md-4 wanted-person-mug"]/img/@src').extract_first()
        
        alias = response.xpath('//div[@class="wanted-person-aliases"]//p//text()').extract_first()
        alias_info = json.dumps({'info': None})
        if(alias != None):
            if len(alias) > 0:
                alias = alias.split(',')
                alias = [{'aka': alias} for alias in alias]
                alias_info = json.dumps({'info': alias})
        
        tableData = response.xpath('//div[@class="wanted-person-description"]//table/tbody//tr')
        dataDict =  {}
        for data in tableData:
            tmpData = data.xpath('.//td//text()').extract()
            dataDict[tmpData[0]] = tmpData[1]
        
        age = dataDict.get('Age')
        age = "" if age is None else age        

        sex = dataDict.get('Sex')
        sex = "" if sex is None else sex
        
        occupation = dataDict.get('Occupation')
        occupation = "" if occupation is None else occupation
        
        race = dataDict.get('Race')
        race = "" if race is None else race       

        scars = dataDict.get('Scars and Marks')
        scars = "" if scars is None else scars

        hair = dataDict.get('Hair')
        hair = "" if hair is None else hair

        eyes = dataDict.get('Eyes')
        eyes = "" if eyes is None else eyes

        height = dataDict.get('Height')
        height = "" if height is None else height

        weight = dataDict.get('Weight')
        weight = "" if weight is None else weight


        reward = response.xpath('//div[@class="wanted-person-reward"]//p//text()').extract()
        remarks = response.xpath('//div[@class="wanted-person-remarks"]//p//text()').extract()
        details = response.xpath('//div[@class="wanted-person-details"]//p//text()').extract()
        reward = ' '.join(reward)
        remarks = ' '.join(remarks)
        details = ' '.join(details)
        reward = reward.strip()
        remarks = remarks.strip()
        details = details.strip()
            
        remarksField = "Eyes: {} ,Height: {} ,Weight: {} ,Hair: {} ,Age: {} ,Race: {} ,Reward: {} ,Remarks: {} ,Details: {}, ,Scars and marks: {}".format(eyes, height, weight, hair, age, race, reward, remarks, details, scars)

        yield Entity({
            "name": name,
            "gender": sex,
            "category": "Individual",
            "type": "SIP",
            "remarks": remarksField,
            "image": imageUrl,
            "designation": occupation,
            "aka": alias_info
        })