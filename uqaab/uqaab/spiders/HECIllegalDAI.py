# Author = Barkat Khan
import scrapy
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import re


class HECIllegalDAI(UqaabBaseSpider):
    name = 'hec_illegal_dai'
    start_urls = ['http://www.hec.gov.pk/english/universities/Pages/Illegal-DAIs.aspx']

    def structure_valid(self, response):
        self.data_rows = response.css("table[class='ms-rteTable-1']").css("tr")
        return len(self.data_rows) > 0
   
    def extact_data(self, response):
        data_rows = self.data_rows 

        for i in range(len(data_rows)):
            columns = data_rows[i].css('td[class="ms-rteTableOddCol-1"]')

            if len(columns.extract()) == 1:
                value = "".join(columns[0].css("::text").extract()).strip()
                value = re.sub('\s+', ' ', value).strip().replace("\u200b","")
                
                if value  == "Name of Institution" or len(value)==0 or "Ilegal/Fake" in value:
                    continue

                yield Entity({
                    'name': value, 
                    'category': "Group",
                    'type': "SIP"
                })
                