# -*- coding: utf-8 -*-
#///////////////////Muhammad Shujat Hussain\\\\\\\\\\\\\\\

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
import scrapy
import json
import re
import dateutil.parser

class ThilandSanctionedEntities(UqaabBaseSpider):
    
    name = 'thiland_sanctions_persons'
    scraping_url = 'https://market.sec.or.th/public/idisc/api/Enforce/GetEnforces'
    start_urls = ['https://www.sec.or.th/Pages/LongLiveTheKing10TH.html']

    def structure_valid(self, response):
        return True

    def extact_data(self, response):
        headers = {'Content-type': 'application/json'}
        frmdata = {"rtk":"ADD37DF8521B28578DD3E662DFFA5C42936416844EC23CFBFA125BDE9C6DEB","Lang":"en","QueryType":"RECENT","OffenderFlag":"","OffenderTxt":"","VioTypeTxt":"ALL","DateFlag":"","StartDateTxt":"","EndDateTxt":"","FreeSearchFlag":"","FreeSearchTxt":""}
        yield scrapy.Request(url=self.scraping_url, method='POST', headers=headers, body=json.dumps(frmdata),callback=self.parse_page)

    def parse_page(self,response):
        details = response.xpath('//table/tr[td]')
        for detail in details:
            order_date = detail.xpath('td[2]/text()').extract_first()
            if order_date:    
                order_date = dateutil.parser.parse(order_date)            
            name = detail.xpath('td[3]/text()').extract_first()
            summarized_facts =  detail.xpath('td[5]/text()').extract_first()
            enforcement_type = detail.xpath('td[7]/text()').extract_first()
            further_detail = detail.xpath('td[8]/text()').extract_first()
            remark = detail.xpath('td[9]/text()').extract_first()
            relevant_section_law = detail.xpath('td[3]/text()').extract_first()
            sec_news = detail.xpath('td[6]/a/@href').extract_first()
            remarks = "remark: {} , summarized_facts: {} , details: {} , Santion_law: {} ,Sec_News: {},Enforcement_type: {}".format(remark , summarized_facts ,further_detail , relevant_section_law ,sec_news , enforcement_type)
            alias_info = json.dumps({'info': None})
            alias = None
            if "formerly" in name:
                name_aliases = name.split("formerly",1)
                name = name_aliases[0]
                aliases = name_aliases[1]
                result = re.findall(r'\(([^]]*)\)', aliases)
                aliases = result[0] 
                alias = [{'aka': aliases}]
                alias_info = json.dumps({'info': alias})
            category = "individual"
            if "Limited" in name or "Ltd." in name:
                category = "group"
            yield Entity({
                "category": category,
                "name":name,
                "aka":alias_info,  
                "exclusion_date":order_date,    
                "remarks":remarks,
                "type": "SAN"
            })                  