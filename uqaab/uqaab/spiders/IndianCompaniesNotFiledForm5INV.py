# Author = Barkat Khan
import os
from datetime import datetime
import scrapy
from uqaab.environment import config
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import StaticDataSpider
import tabula
import json
from PyPDF2 import PdfFileReader
import pandas as pd

class IndianCompaniesNotFiledForm5INV(StaticDataSpider):

    name = 'IndianCompanies5INV'
    start_urls = ['http://www.mca.gov.in/Ministry/pdf/consolidatedlist_02122014.pdf']

    def structure_valid(self, response):
        self.file_path = os.path.join(config('PROJECT_PATH'), 'uqaab', 'uqaab', 'spiders', 'static_data', 'consolidatedlist_02122014.pdf')
        self.df = tabula.read_pdf(self.file_path, encoding="utf-8", lattice=True, guess=False, pages='all')        
        
        return len(self.df) > 0
    
    def extact_data(self, response):

        df = self.df
        filedata = PdfFileReader(self.file_path, "rb")
        filecontent = filedata.getPage(0).extractText()
        datestring = self.string_to_date("01/02/2015")
       
        for index in range(len(df)):

            if df.loc[index, 'Name of Company']!= "" and pd.isnull(df.loc[index, 'CIN']):
                roc = df.loc[index, 'Name of Company']
                continue

            document_info = []

            if str(df.loc[index,'CIN']):
                document_info.append({'type': "CIN",'id': str(df.loc[index,'CIN'])})
            

            if len(document_info) > 0:
                document_info = json.dumps({'document_info': document_info})
            else:
                document_info = json.dumps({'document_info': None})
            
            remarksfield =  "ROC: {0} ".format(roc)

            yield Entity({
                'category': 'Group',
                'type': 'SIP',
                'name': df.loc[index,'Name of Company'],
                'document': document_info,
                'inclusion_date': datestring,
                'remarks': remarksfield
                })

    @staticmethod
    def string_to_date(date_string):
        try:
            return datetime.strptime(date_string, '%d/%m/%Y')
        except TypeError:
            return None
