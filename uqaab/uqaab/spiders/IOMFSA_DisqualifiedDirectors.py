# Author = Barkat Khan
from datetime import datetime
from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from re import findall
from json import dumps

class IOMFSA_DisqualifiedDirectors(UqaabBaseSpider):
    name = 'iomfsa_disqualified_directors'
    start_urls =  ['https://www.iomfsa.im/enforcement/disqualified-directors/']
    
    def structure_valid(self, response):
        self.data_rows = response.css("div[class='rte']")
        return len(self.data_rows) > 0

    def extact_data(self, response):
        data_rows = self.data_rows[1:]

        for row in data_rows:
            remarksfield = ""
            E  = Entity()
            E['category'] = "Group"
            E['type'] = "SIP"
            
            for columns in row.css("p"):
                key = columns.css("strong::text").extract_first()
                fields = columns.css("::text").extract() 

                if len(fields) == 1:
                    key = fields[0]
                    
                    if "Name" in key:
                        E['name'] = fields[0].strip()

                if len(fields) >= 2:
                    key = fields[0]
                    
                    if "Name" in key:
                        E['name'] = fields[1].strip()
                    
                    elif "Address" in key:
                        E['address'] = " ".join(fields[1:])
                    
                    elif "Dates of Disqualification" in key:
                        
                        inclusion_exclusion = findall("\d+\w+\s+\w+\s\d+ | \d+\s+\w+\s\d+", " ".join(fields))
                        if len(inclusion_exclusion) ==2:
                            E['inclusion_date'] = self.string_to_date(inclusion_exclusion[0])
                            E['exclusion_date'] = self.string_to_date(inclusion_exclusion[1])
                    
                    elif "Date of Birth" in key:
                        dob_info = {}
                        dob = fields[1].strip()
            
                        if dob:
                            dob_info["DOB"] = str(dob)
                            dob_info["POB"] = None
                        
                        if len(dob_info)>0:
                            DOB_complete = dumps({'info': [dob_info]})
                        else:
                            DOB_complete = dumps({'info': None})
                        
                        E["date_of_birth"] = DOB_complete
                        
                    else:
                        value = " ".join(fields[1:])
                        if key and value:
                            if remarksfield:
                                remarksfield = "{0} ;{1}:{2} ".format(remarksfield, key, value) 
                            else:
                                remarksfield = "{0}:{1} ".format(key, value)
                
                        E['remarks'] = remarksfield
            yield E
            
    @staticmethod
    def string_to_date(date_string):
        date_string = date_string.strip()
        try:
            if "th" in date_string:
                return datetime.strptime(date_string, '%dth %B %Y')
            else:      
                return datetime.strptime(date_string, '%d %b %Y')
        except:
            return None