# Author: Barkat Khan

from uqaab.items import Entity
from uqaab.spiders.utils.base_customized_spiders import UqaabBaseSpider
from scrapy import Request
import json


class EUMostWanted(UqaabBaseSpider):

    name = 'eumostwanted'
    start_urls = [
        'https://eumostwanted.eu/']

    custom_settings = {
        "AUTOTHROTTLE_ENABLED": True,
        "DOWNLOAD_DELAY": 0.25,
        "AUTOTHROTTLE_START_DELAY": 0.25,
        "AUTOTHROTTLE_MAX_DELAY": 0.40,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 10000,
        "DOWNLOAD_FAIL_ON_DATALOSS": False
    }

    def structure_valid(self, response):
        data_rows = response.xpath("//div[@class='view-content']/div/.//a/@href")
        return len(data_rows) > 0

    def extact_data(self, response):
        itemlinks=response.xpath("//div[@class='view-content']/div/.//a/@href").extract()

        for index in range(len(itemlinks)):
            E = Entity()
            E['category'] = 'Individual'
            E['type'] = 'SIP'
            link = itemlinks[index]
            
            yield Request(url=link, meta={'entity':E}, callback=self.extract)   
    
    def extract(self, response):
        E = response.request.meta['entity']
    
        headers  = response.xpath("//div[@class='wanted_top_right']/div/@class").extract()
        remarks = []
        image = response.xpath("//img[contains(@src,'http')]/@src").extract()
        image = image[-1]


        if len(headers) > 0:
            for classname in headers:
                if 'state-of-case' in classname:
                    value = self.extract_stateofcase(response, classname)[0]
                    remarks.append('State of case: {0}.'.format(value))

                elif 'date-of-birth' in classname:
                    E['date_of_birth'] = self.extract_dateofbirth(response, classname)[0]

                elif 'alias' in classname:
                    E['aka'] = self.extract_alias(response, classname)[0]

                elif 'crime' in classname:
                    value = self.extract_crime(response, classname)[0]
                    remarks.append('Crime: {0}.'.format(value))

                elif 'nationality' in classname:
                    nationalities = self.extract_nationality(response, classname)
                    E['nationality'] = ', '.join(nationalities)

                elif 'languages' in classname:
                    value = self.extract_languages(response, classname)[0]
                    remarks.append('Languages: {0}.'.format(value))

                elif 'title' in classname:
                    E['name'] = self.extract_title(response, classname)[0]

                elif 'ethnic' in classname:
                    value = self.extract_ethnic(response, classname)[0]
                    remarks.append('Ethnic Origin: {0}.'.format(value))

                elif 'characteristics' in classname:
                    value = self.extract_identifiers(response, classname)[0]
                    remarks.append('Characteristics: {0}.'.format(value))

                elif 'gender' in classname:
                    E['gender'] = self.extract_gender(response, classname)[0]

                elif 'approximate-height' in classname:
                    value = self.extract_height(response, classname)[0]
                    remarks.append('Approximate Height: {0} cm.'.format(value))

        E['remarks'] = ' '.join(remarks)
        E['image'] = image
        yield E

    def extract_alias(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_title(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/h2/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_gender(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_crime(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_identifiers(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_ethnic(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None

    def extract_nationality(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None


    def extract_languages(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/ul[@class='links inline']/li/text()").extract()
            return value
        except Exception as err:
            return None


    def extract_stateofcase(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/div[@class='field-items']/div/text()").extract()
            return value
        except Exception as err:
            return None


    def extract_dateofbirth(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/span/text()").extract()
            return value
        except Exception as err:
            return None


    def extract_height(self, response, classname):
        try:
            value = response.xpath("//div[@class='"+classname+"']/span/text()").extract()
            return value
        except Exception as err:
            return None
